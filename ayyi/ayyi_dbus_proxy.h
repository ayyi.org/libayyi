/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <dbus/dbus-glib-bindings.h>
#include <ayyi/ayyi_dbus.h>

GQuark          ayyi_msg_error_quark ();
#define AYYI_MSG_ERROR ayyi_msg_error_quark()

void            ayyi_dbus_object_new (AyyiAction*, const char* name, gboolean from_source, AyyiIdx src_idx, guint32 parent_idx, AyyiSongPos* start, guint64 len, guint32 inset);
void            ayyi_dbus_part_new   (AyyiAction*, const char* name, uint64_t id, gboolean from_source, AyyiIdx src_idx, guint32 parent_idx, AyyiSongPos* start, guint64 len, guint32 inset);
void            ayyi_dbus_object_del (AyyiAction*, int object_type, uint32_t object_idx);

DBusGProxyCall* dbus_get_prop_string (AyyiAction*, int object_type, int property_type);
DBusGProxyCall* dbus_set_prop_int    (AyyiAction*, int object_type, int property_type, AyyiIdx, int val);
DBusGProxyCall* dbus_set_prop_int64  (AyyiAction*, int object_type, int property_type, AyyiIdx, int64_t val);
DBusGProxyCall* dbus_set_prop_float  (AyyiAction*, int object_type, int property_type, AyyiIdx, double val);
DBusGProxyCall* dbus_set_prop_bool   (AyyiAction*, int object_type, int property_type, AyyiIdx, gboolean val);
DBusGProxyCall* dbus_set_prop_string (AyyiAction*, int object_type, int property_type, AyyiIdx, const char* val);
DBusGProxyCall* dbus_set_prop_f_pair (AyyiAction*, int object_type, int property_type, GArray* object_idx, double val1, double val2);

void            dbus_undo            (AyyiService*, int n);

GArray*         ayyi_index_array_new (guint, guint, guint);
