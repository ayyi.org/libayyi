/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __ayyi_utils_c__
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <glib.h>

#include "ayyi_typedefs.h"
#include "ayyi_types.h"
#include "ayyi_shm.h"
#include "ayyi_client.h"
#include "ayyi_utils.h"


/*
 *  Print and free the GError
 */
void
warn_gerror (const char* msg, GError** error)
{
	if(*error){
		printf("%s %s: %s\n", ayyi_warn, msg, (*error)->message);
		g_error_free(*error);
		*error = NULL;
	}
}


/*
 *  Print and free the GError
 */
void
info_gerror (const char* msg, GError** error)
{
	if(*error){
		printf("%s: %s\n", msg, (*error)->message);
		g_error_free(*error);
		*error = NULL;
	}
}


gchar*
path_from_utf8 (const gchar* utf8)
{
	GError* error = NULL;

	if (!utf8) return NULL;

	gchar* path = g_locale_from_utf8(utf8, -1, NULL, NULL, &error);
	if (error) {
		printf("Unable to convert to locale from UTF-8:\n%s\n%s\n", utf8, error->message);
		g_error_free(error);
	}
	if (!path) {
		// if invalid UTF-8, text probaby still in original form, so just copy it
		path = g_strdup(utf8);
	}

	return path;
}


/*
 *  Scan a directory and return a list of any subdirectoies. Not recursive.
 *  -the list, and each entry in it, must be freed.
 */
GList*
get_dirlist (const char* path)
{
	GList* dir_list = NULL;
	char filepath[256] = {0,};
	const gchar* file;
	GError* error = NULL;
	GDir* dir;

	if((dir = g_dir_open(path, 0, &error))){
		while((file = g_dir_read_name(dir))){
			if(file[0] == '.') continue;
			snprintf(filepath, 255, "%s/%s", path, file);

			if(g_file_test(filepath, G_FILE_TEST_IS_DIR)){
				dbg (2, "found dir: %s", filepath);
				dir_list = g_list_append(dir_list, g_strdup(filepath));
			}
		}
		g_dir_close(dir);
	}else{
		if(_debug_ > 1) pwarn ("cannot open directory. %s", error->message);
		g_error_free(error);
		error = NULL;
	}

	return dir_list;
}


/*
 *  Change, eg, "name-2" to "name-3"
 *	Increases the length of a string of unknown length so use with care.
 */
void
string_increment_suffix (char* new, const char* orig, int new_max)
{
	// TODO length of "new" musn't exceed new_max.

	char delimiter[] = "-";

	//split the string by delimiter
	char* suffix = g_strrstr(orig, delimiter);
	if(!suffix){ dbg (2, "delimiter not found"); sprintf(new, "%s%s1", orig, delimiter); return; } //delimiter not found.
	if(suffix == orig + strlen(orig)){ dbg (0, "delimiter at end: %s", orig); sprintf(new, "%s1", orig); return; } //delimiter is at the end.
	suffix++;
	unsigned pos = suffix - orig;
	dbg (2, "pos=%i suffix=%s", pos, suffix);

	int number = atoi(suffix);

	char stem[256];
	strncpy(stem, orig, pos-1);
	stem[pos-1] = 0; //strncpy doesnt terminate the string
	dbg (2, "stem=%s num=%i->%i", stem, number, number+1);
	sprintf(new, "%s%s%i", stem, delimiter, number+1);
}


int
get_terminal_width ()
{
	struct winsize ws;

	if (ioctl(1, TIOCGWINSZ, (void *)&ws) != 0) printf("ioctl failed\n");

	return ws.ws_col;
}


bool
audio_path_get_leaf (const char* path, char* leaf)
{
	//gives the leafname contained in the given path.
	//-if the path is a directory, leaf should be empty, but without checking we have no way of knowing...

	//TODO just use g_path_get_basename() instead.

	g_return_val_if_fail(strlen(path), FALSE);

	//look for slashes and chop off anything before:
	char* pos;
	if((pos = g_strrstr(path, "/"))){
		pos += 1; //move to the rhs of the slash.
	}else pos = (char*)path;

	//make leaf contain the last segment:
	g_strlcpy(leaf, pos, 128);

	return true;
}


/*
 *  Strip off directory information, file extension, L/R flags, trailing underscores.
 *
 *  Returned value must be freed.
 */
gchar*
audio_path_get_base (const char* path)
{
	gchar* basename = g_path_get_basename(path);

	char* pos;
	if((pos = g_strrstr(basename, "."))){
		*pos = '\0';
	}

	if((pos = g_strrstr(basename, "-L")) == basename + strlen(basename) - 2){
		*pos = '\0';
	}
	if((pos = g_strrstr(basename, "-R")) == basename + strlen(basename) - 2){
		*pos = '\0';
	}

	void remove_trailing(char* s)
	{
		while(s[strlen(s) -1] == '_') s[strlen(s) -1] = '\0';
	}

	remove_trailing(basename);

	return basename;
}


bool
audio_path_get_wav_leaf (char* leaf, const char* path, int len)
{
	//gives the leafname contained in the given path but substitutes a '.wav' extension.
	//-if the path is a directory, leaf should be empty, but without checking we have no way of knowing...

	g_return_val_if_fail(strlen(path), FALSE);

	//look for slashes and chop off anything before:
	char* pos;
	if((pos = g_strrstr(path, "/"))){
		pos += 1; //move to the rhs of the slash.
	}else pos = (char*)path;

	//make leaf contain the last segment:
	g_strlcpy(leaf, pos, len);

	dbg (2, "path=%s --> %s", path, leaf);

	if((pos = g_strrstr(leaf, "."))){
		strcpy(pos + 1, "wav");
	}

	return true;
}


char*
audio_path_truncate (char* path, char chr)
{
	//remove any instances of @chr from the end of the path string.

	while(path[strlen(path) - 1] == chr){
		path[strlen(path) - 1] = '\0';
	}
	return path;
}

