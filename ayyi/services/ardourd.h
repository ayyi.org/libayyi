#ifndef __services_ardourd_h__
#define __services_ardourd_h__

#include "ayyi/ayyi_client.h"

#define ARDOURD_DBUS_SERVICE   "org.ayyi.ardourd.ApplicationService"
#define ARDOURD_DBUS_PATH      "/org/ayyi/ardourd/Ardourd"      //the "node" tag in the xml file, and string in the dbus_g_connection_register_g_object() call.
#define ARDOURD_DBUS_INTERFACE "org.ayyi.ardourd.Application"   //the "interface" name given in the xml file

struct _ardourd_service
{
	AyyiService             ayyi_service;

	struct _shm_song*       song;
	struct _shm_seg_mixer*  amixer; //FIXME

	int                     segments[1];

	AyyiServicePrivate*     priv;
};

struct _ArdourdMixerService
{
	AyyiService             ayyi_service;

	struct _shm_song*       song;
	struct _shm_seg_mixer*  amixer;

	int                     segments[1];

	AyyiServicePrivate*     priv;
};

#ifdef __ayyi_client_c__
AyyiSongService ardourd_song   = {{ARDOURD_DBUS_SERVICE, ARDOURD_DBUS_PATH, ARDOURD_DBUS_INTERFACE, NULL, NULL, NULL}, NULL, NULL, {SEG_TYPE_SONG}, NULL};
AyyiMixerService ardourd_mixer = {{ARDOURD_DBUS_SERVICE, ARDOURD_DBUS_PATH, ARDOURD_DBUS_INTERFACE, NULL, NULL, NULL}, NULL, NULL, {SEG_TYPE_MIXER}, NULL};
#else
extern AyyiSongService ardourd_song;
extern AyyiMixerService ardourd_mixer;
#endif

#endif //__services_ardourd_h__
