/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __ayyi_client_c__
#define __ayyi_private__
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <glib.h>
#include <gmodule.h>

#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_dbus.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include <ayyi/ayyi_dbus.h>
#include "ayyi/ayyi_client.h"

AyyiService* known_services[2];

#include "services/ardourd.h"
#define plugin_path "/usr/lib/ayyi/plugins:/usr/local/lib/ayyi/plugins"

typedef	AyyiPluginPtr (*infoFunc)();
typedef void          (*AyyiPluginCallback)            (AyyiPluginPtr, const GError*, gpointer);

static void           ayyi_client_ping                 (void (*pong)(const char* s));
static void           ayyi_plugin_load                 (const gchar* filename, AyyiPluginCallback, gpointer);
static void           ayyi_client_discover_launch_info ();

enum {
    PLUGIN_TYPE_1 = 1,
    PLUGIN_TYPE_MAX
};
#define PLUGIN_API_VERSION 1


/**
 *  ayyi_client_init:
 *
 *  Returns: (transfer none) AyyiClient
 */
const AyyiClient*
ayyi_client_init ()
{
	if(ayyi.priv){ pwarn("already initialised!"); return &ayyi; }

	ayyi_log_init();

	ayyi.got_shm = 0; // before this is set, we must have _all_ required shm pointers.

	ayyi.priv = AYYI_NEW(AyyiClientPrivate,
		.trans_id = 1,
		.responders = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL)
	);

	ayyi_shm_init ();

	known_services[0] = (AyyiService*)&ardourd_song;
	known_services[1] = (AyyiService*)&ardourd_mixer;

	ayyi.services = g_malloc0(3 * sizeof(gpointer));

	ayyi_client_discover_launch_info();

	return &ayyi;
}


#ifdef HAVE_INTROSPECTION
void
ayyi_client_init_2 ()
{
	ayyi_client_init();
}
#endif


/**
 * ayyi_client_add_services
 * @arg0: (nullable)
 */
void
ayyi_client_add_services (AyyiService** services)
{
	ayyi.service = known_services[AYYI_SERVICE_AYYID1];

	if(services){
		ayyi.services = services;
		for(int i=0;services[i];i++){
			AyyiService* service = services[i];
			service->priv = g_new0(AyyiServicePrivate, 1);
			service->ping = ayyi_client_ping;

			ayyi_shm_seg_new(service, ((AyyiSongService*)service)->segments[0]);
		}
	}else{
		// default service setup

		ayyi.services[0] = known_services[AYYI_SERVICE_AYYID1];
		ayyi.services[0]->priv = g_new0(AyyiServicePrivate, 1);
		ayyi.services[0]->ping = ayyi_client_ping;

		ayyi.services[1] = known_services[AYYI_SERVICE_MIXER];
		ayyi.services[1]->priv = g_new0(AyyiServicePrivate, 1);
		ayyi.services[1]->ping = ayyi_client_ping;

		// note that addition of shm segments is now handled internally
		ayyi_shm_seg_new(known_services[AYYI_SERVICE_AYYID1], SEG_TYPE_SONG);
		ayyi_shm_seg_new(known_services[AYYI_SERVICE_MIXER], SEG_TYPE_MIXER);
	}
}


/**
 * ayyi_client_connect:
 * @arg1: (scope async) (closure arg2)
 */
bool
ayyi_client_connect (AyyiService* service, AyyiHandler2 callback, gpointer user_data)
{
	PF;
	bool ok = true;

	void temp_copy_shared_proxy ()
	{
		if(!known_services[AYYI_SERVICE_MIXER]->priv->proxy)
			known_services[AYYI_SERVICE_MIXER]->priv->proxy = known_services[AYYI_SERVICE_AYYID1]->priv->proxy;
	}

	void on_connected (const GError* error, gpointer user_data)
	{
		dbg(1, "connected!");
		temp_copy_shared_proxy();
		HandlerData* c = user_data;
		call((AyyiHandler2)c->callback, error, c->user_data);
		g_free(c);
	}

	if(service == ayyi.services[0] || service != known_services[AYYI_SERVICE_MIXER]){ // dbus connection is temporarily shared

		ayyi_client__dbus_connect(service, on_connected, AYYI_NEW(HandlerData,
			.callback = (AyyiHandler)callback,
			.user_data = user_data
		));
	}else{
		dbg(1, "NOT connecting... service=%p (0=%p)", service, ayyi.services[0]);
		temp_copy_shared_proxy();
		callback(NULL, user_data);
	}

	return ok;
}


void
ayyi_client_disconnect ()
{
	for(int i=0;i<G_N_ELEMENTS(known_services);i++){
		AyyiService* service = known_services[i];
		if(service){
			if(i == 0){
				g_object_unref0(service->priv->proxy);
			}else{
				service->priv->proxy = NULL;
			}
			g_clear_pointer(&service->priv, g_free);
		}
	}

	if(ayyi.log.loggers){
		g_list_free(ayyi.log.loggers);
		ayyi.log.loggers = NULL;
	}
}


static void
ayyi_client_ping (void (*pong)(const char* s))
{
	ayyi_dbus_ping(ayyi.service, pong);
}


void
ayyi_client_load_plugins ()
{
	if(!g_module_supported()) g_error("Modules not supported! (%s)", g_module_error());

	typedef struct _c {
		int     i;
		int     found;
		gchar** paths;
		GDir*   dir;
		gchar*  filename;
		void    (*next_dir)(struct _c*);
		void    (*next_plugin)(struct _c*);
	} C;

	void plugin_on_load (AyyiPluginPtr plugin, const GError* error, gpointer user_data)
	{
		C* c = user_data;

		if(error){
			dbg(1, "'%s' failed to load.", c->filename);
			return;
		}

		c->found++;
		ayyi.priv->plugins = g_slist_append(ayyi.priv->plugins, plugin);

		c->next_plugin(c);
	}

	void done (C* c)
	{
		ayyi_log_print(0, "ayyi plugins loaded: %i.", c->found);

		if(c->filename) g_free(c->filename);
		g_strfreev(c->paths);
		g_free(c);
	}

	void next_plugin (C* c)
	{
		const gchar* filename = g_dir_read_name(c->dir);
		if (filename) {
			dbg(1, "testing %s...", filename);
			char* path = c->paths[c->i];
			gchar* filepath = g_build_filename(path, filename, NULL);
			// filter files with correct library suffix
			if(!strncmp(G_MODULE_SUFFIX, filename + strlen(filename) - strlen(G_MODULE_SUFFIX), strlen(G_MODULE_SUFFIX))) {
				// If we find one, try to load plugin info and if this was successful try to invoke the specific plugin
				// type loader. If the second loading went well add the plugin to the plugin list.
				if(c->filename) g_free(c->filename);
				c->filename = g_strdup(filename);
				ayyi_plugin_load(filepath, plugin_on_load, c);
			} else {
				dbg(2, "-> no library suffix");
				c->next_plugin(c);
			}
			g_free(filepath);
		} else {
			c->next_dir(c);
		}
	}

	void next_dir (C* c)
	{
		char* path = c->paths[++c->i];
		dbg(2, "%i: path=%s", c->i, path);
		if(!path){
			done(c);
			return;
		}
		if(!g_file_test(path, G_FILE_TEST_EXISTS)){
			c->next_dir(c);
			return;
		}

		GError* error = NULL;
		c->dir = g_dir_open(path, 0, &error);
		if (!error) {
			next_plugin(c);
		} else {
			pwarn("dir='%s' failed. %s", path, error->message);
			g_error_free(error);
			error = NULL;
		}
	}

	next_dir(AYYI_NEW(C,
		.i = -1,
		.next_plugin = next_plugin,
		.next_dir = next_dir,
		.paths = g_strsplit(plugin_path, ":", 0)
	));
}


/**
 *  ayyi_client_get_plugin: (skip)
 *  @arg0: name
 *
 *  Lookup the plugin pointer for a loaded plugin.
 */
AyyiPluginPtr
ayyi_client_get_plugin (const char* name)
{
	static int count = -1;
	count++;

	GSList* l = ayyi.priv->plugins;
	for(;l;l=l->next){
		AyyiPluginPtr plugin = l->data;
		if(!count) dbg(2, "  %s", plugin->name);
		if(!strcmp(name, plugin->name)) return plugin;
	}
	return NULL;
}



/**
 *  ayyi_plugin_load:
 *  @arg1: (scope async): description
 */
void
ayyi_plugin_load (const gchar* filepath, AyyiPluginCallback callback, gpointer user_data)
{
	AyyiPluginPtr plugin = NULL;
	bool success = false;

#if GLIB_CHECK_VERSION(2,3,3)
	GModule* handle = g_module_open(filepath, G_MODULE_BIND_LOCAL);
#else
	GModule* handle = g_module_open(filepath, 0);
#endif

	if(!handle) {
		pwarn("cannot open %s (%s)!", filepath, g_module_error());
		return;
	}

	infoFunc plugin_get_info;
	if(g_module_symbol(handle, "plugin_get_info", (void*)&plugin_get_info)) {
		// load generic plugin info
		if(NULL != (plugin = (*plugin_get_info)())) {
			// check plugin version
			if(PLUGIN_API_VERSION != plugin->api_version){
				dbg(0, "API version mismatch: \"%s\" (%s, type=%d) has version %d should be %d", plugin->name, filepath, plugin->type, plugin->api_version, PLUGIN_API_VERSION);
			}

			// try to load specific plugin type symbols
			switch(plugin->type) {
				default:
					if(plugin->type >= PLUGIN_TYPE_MAX) {
						dbg(0, "Unknown or unsupported plugin type: %s (%s, type=%d)", plugin->name, filepath, plugin->type);
					} else {
						dbg(1, "name='%s' %s", plugin->name, plugin->service_name);

						AyyiService pservice = {
							plugin->service_name,
							plugin->app_path,
							plugin->interface,
							.priv = g_new0(AyyiServicePrivate, 1)
						};

						typedef struct {
							AyyiService        pservice;
							AyyiPluginPtr      plugin;
							AyyiPluginCallback callback;
							gpointer           user_data;
						} C;

						void on_plugin_connected (const GError* error, gpointer user_data)
						{
							C* c = user_data;
							AyyiPluginPtr plugin = c->plugin;
							AyyiService* pservice = &c->pservice;

							if(error){
								switch(error->code){
									case 98742:
										ayyi_log_print(0, "plugin service not available: %s", plugin->service_name);
										break;
									default:
										warnprintf("plugin dbus connection failed: %s", error->message);
										break;
								}
								call(c->callback, plugin, error, c->user_data);
								return;
							}
							dbg(1, "plugin dbus connection ok.");

							//dbus_register_signals();
							//dbus_server_get_shm(&engine);  //.....err shouldnt we be using this?

							// get shm address

							plugin->client_data = (struct _spec_shm*)NULL;

							AyyiCShmSeg* seg = ayyi_shm_seg_new(pservice, AYYI_SEG_TYPE_PLUGIN);

							if(dbus_g_proxy_call(pservice->priv->proxy, "GetShmSingle", (GError**)&error, G_TYPE_STRING, "", G_TYPE_INVALID, G_TYPE_UINT, &seg->id, G_TYPE_INVALID)){
								ayyi_shm_import(ayyi.service);
								plugin->client_data = seg->address; //TODO does it need to be translated? no, dont think so.
								dbg(2, "client_data=%p offset=%i", plugin->client_data, (uintptr_t)plugin->client_data - (uintptr_t)plugin->client_data);
							}

							call(c->callback, plugin, error, c->user_data);
						}

						ayyi_client__dbus_connect(&pservice, on_plugin_connected, AYYI_NEW(C,
							.pservice = pservice,
							.plugin = plugin
						));
					}
					break;
			}
		}
	} else {
		pwarn("File '%s' is not a valid Ayyi plugin!", filepath);
	}

	if(!success) {
		g_module_close(handle);
		return;
	}
}


static void
ayyi_client_discover_launch_info ()
{
	#define ayyi_application_path "/usr/bin/ayyi/clients/:/usr/local/bin/ayyi/clients/"

	gchar** paths = g_strsplit(ayyi_application_path, ":", 0);
	GError* error = NULL;
	char* path;
	int i = 0;
	while((path = paths[i++])){
		if(!g_file_test(path, G_FILE_TEST_EXISTS)) continue;

		dbg(2, "scanning for ayyi applications (%s) ...", path);
		GDir* dir = g_dir_open(path, 0, &error);
		if (!error) {
			gchar* filename;
			while((filename = (gchar*)g_dir_read_name(dir))){
				dbg(2, "testing %s...", filename);
				if(!g_strrstr(filename, ".desktop")) continue;

				gchar* filepath = g_build_filename(path, filename, NULL);

				GError* error = NULL;
				GKeyFile* key_file = g_key_file_new();
				if(g_key_file_load_from_file(key_file, filepath, G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, &error)){
					gchar* groupname = g_key_file_get_start_group(key_file);
					if(!strcmp(groupname, "Desktop Entry")){
						AyyiLaunchInfo* info = g_new0(AyyiLaunchInfo, 1);
						gchar* keyval;
						if((keyval = g_key_file_get_string(key_file, groupname, "Name", &error))){
							info->name = keyval;
							if((keyval = g_key_file_get_string(key_file, groupname, "Exec", &error))){
								info->exec = keyval;
							}
						}
						if(true){
							ayyi.launch_info = g_list_append(ayyi.launch_info, info);
						} else g_free(info);
					}
					g_free(groupname);
				}else{
					printf("unable to load desktop file: %s.\n", error->message);
					g_error_free(error);
					error = NULL;
				}

				g_free(filepath);
				g_key_file_free (key_file);
			}
			g_dir_close(dir);
		} else {
			pwarn("dir='%s' failed. %s", path, error->message);
			g_error_free(error);
			error = NULL;
		}
	}
	g_strfreev(paths);
}


void
ayyi_client_set_signal_reg (int signals)
{
#ifdef USE_DBUS
	ayyi_dbus__register_signals(signals);
#endif
}


void
ayyi_object_set_bool (AyyiObjType object_type, AyyiIdx obj_idx, AyyiPropType prop, gboolean val, AyyiAction* action)
{
#ifdef USE_DBUS
	dbus_set_prop_bool(action, object_type, prop, obj_idx, val);
#endif
}


gboolean
ayyi_object_is_deleted (AyyiItem* object)
{
	dbg(3, "deleted=%i (%i)", (object->flags & deleted), object->flags);
	return (object->flags & deleted);
}


#define MAKE_RESPONDER_KEY(object_type, op_type, object_idx) g_strdup_printf("%i-%i-%i", object_type, op_type, object_idx);


/**
 *  ayyi_client_watch:
 *  @arg2: (scope async)
 */
void
ayyi_client_watch (AyyiIdent object, AyyiOpType op_type, AyyiPropHandler callback, gpointer user_data)
{
	char* key = MAKE_RESPONDER_KEY(object.type, op_type, object.idx); // ownership of the key is transferred to the hash table
	dbg(2, "key=%s", key);

	GList* responders = g_list_append(g_hash_table_lookup(ayyi.priv->responders, key), AYYI_NEW(Responder,
		.callback = callback,
		.user_data = user_data
	));

	g_hash_table_insert(ayyi.priv->responders, key, responders);
}


/**
 *  ayyi_client_watch_once:
 *  @arg2: (nullable) (scope async)
 *
 *  Add a watch but remove it after first response.
 */
void
ayyi_client_watch_once (AyyiIdent id, AyyiOpType op_type, AyyiHandler user_handler, gpointer user_data)
{
	typedef struct
	{
		AyyiOpType      op_type;
		AyyiPropHandler handler;
		AyyiHandler     user_handler;
		void*           user_data;
	} C;

	void onetime_responder_callback(AyyiIdent obj, GError** error, AyyiPropType _prop, gpointer _data)
	{
		C* c = _data;

		call(c->user_handler, obj, error, c->user_data);

		if(!ayyi_client_remove_watch(obj.type, c->op_type, obj.idx, c->handler)){
			pwarn("responder not removed.");
		}

		g_free(c);
	}

	ayyi_client_watch(id, op_type, onetime_responder_callback, AYYI_NEW(C,
		.op_type = op_type,
		.handler = onetime_responder_callback,
		.user_handler = user_handler,
		.user_data = user_data
	));
}


/*
 *  ayyi_client_get_watches:
 *
 *  Returns: (element-type utf8)
 *
 *  The returned list should NOT be freed.
 */
const GList*
ayyi_client_get_watches (AyyiObjType object_type, AyyiOpType op_type, AyyiIdx object_index)
{
#if 0
	{
		GHashTableIter iter;
		gpointer key, value;
		g_hash_table_iter_init (&iter, ayyi.priv->responders);
		while (g_hash_table_iter_next (&iter, &key, &value)){
			dbg(0, "  %s=%p", (char*)key, value);
		}
	}
#endif

	char* key = MAKE_RESPONDER_KEY(object_type, op_type, object_index);
	GList* list = g_hash_table_lookup(ayyi.priv->responders, key);
	dbg(2, "key=%s %s %s found=%i", key, ayyi_print_object_type(object_type), ayyi_print_optype(op_type), g_list_length(list));

#if 0
	if(!g_list_length(list)){
		pwarn("no watches found for: %s %s %s", key, ayyi_print_object_type(object_type), ayyi_print_optype(op_type));
		//ayyi_client_print_watches();
	}
#endif
	g_free(key);

	return list;
}


/**
 *  ayyi_client_remove_watch:
 *  @arg3: (scope call)
 */
bool
ayyi_client_remove_watch (AyyiObjType object_type, AyyiOpType op_type, AyyiIdx object_idx, AyyiPropHandler callback)
{
	//remove the responder that matches AyyiObjType, AyyiOpType, and callback.
	//-if callback is NULL, all responders that match the other parameters will be removed

	gboolean found = false;

	char* key = MAKE_RESPONDER_KEY(object_type, op_type, object_idx);
	GList* responders = g_hash_table_lookup(ayyi.priv->responders, key);
	if(responders){
		int length = g_list_length(responders);

		GList* l = responders;
		for(;l;l=l->next){
			Responder* responder = l->data;
			if(!callback || responder->callback == callback){
				g_free(responder);
				responders = g_list_remove(responders, responder);
				g_hash_table_replace(ayyi.priv->responders, g_strdup(key), responders);
				found = true;
				break;
			}
		}
		dbg(2, "key=%s found=%i n_handlers=%i", key, found, g_list_length(responders));
		if(g_list_length(responders) > length - 1) pwarn("handler not removed?");

		if(!g_list_length(responders)){
			dbg(2, "no more handlers - removing responder... %s", ayyi_print_object_type(object_type), ayyi_print_optype(op_type));
			g_list_free(responders); // not needed?
			g_hash_table_remove(ayyi.priv->responders, key);
		}
	}else{
		pwarn("not found: %i %i %i", object_type, op_type, object_idx);
	}

	g_free(key);
	return found;
}


#ifdef DEBUG
void
ayyi_client_print_watches ()
{
	char* find_first (char* key, char** pos)
	{
		char* s = key + 1;
		char* e = strstr(s, "-");
		*pos = e + 1;
		char* a = g_strndup(key, e - key);
		return a;
	}

	GHashTableIter iter;
	gpointer _key, value;
	g_hash_table_iter_init (&iter, ayyi.priv->responders);
	while (g_hash_table_iter_next (&iter, &_key, &value)){
		char* key = _key;
		char* pos;
		char* a = find_first(key, &pos);
		int object_type = atoi(a);

		char* b = find_first(pos, &pos);
		AyyiOp optype = atoi(b);

		dbg(0, "  %s %s %s", (char*)key, object_type > -1 ? ayyi_print_object_type(object_type) : "ALL", ayyi_print_optype(optype));

		g_free(a);
		g_free(b);
	}
}
#endif


void
ayyi_discover_clients ()
{
	PF;
}


AyyiIdx
ayyi_ident_get_idx (AyyiIdent ident)
{
	return ident.idx;
}


AyyiObjType
ayyi_ident_get_type (AyyiIdent ident)
{
	return ident.type;
}


/**
 *  ayyi_launch:
 *  @argv: use NULL to launch the app without extra arguments.
 */
void
ayyi_launch (AyyiLaunchInfo* info, gchar** argv)
{
	g_return_if_fail(info);
	dbg(0, "launching application: %s", info->name);

	int len = argv ? g_strv_length(argv) : 0;
	gchar** v = g_new0(gchar*, len + 2);
	v[0] = info->exec;

	if(argv){
		int i; for(i=0;i<len;i++){
			v[i+1] = argv[i];
		}
#ifdef DEBUG
		char* a = g_strjoinv(" ", v);
		dbg(1, "'%s'", a);
		g_free(a);
#endif
	}

	GError* error = NULL;
	if(!g_spawn_async(NULL, v, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, &error)){
		ayyi_log_print(LOG_FAIL, "launch %s", info->name);
		GERR_WARN;
	}
	else ayyi_log_print(0, "launched: %s", v[0]);

	g_free(v);
}


void
ayyi_launch_by_name (const char* name, gchar** argv)
{
	GList* l = ayyi.launch_info;
	for(;l;l=l->next){
		AyyiLaunchInfo* info = l->data;
		if(!strcmp(info->name, name)){
			ayyi_launch(info, argv);
			return;
		}
	}
	pwarn("not found");
}


const char*
ayyi_print_object_type (AyyiObjType object_type)
{
	static char unk[32];

	switch (object_type){
#define CASE(x) case AYYI_OBJECT_##x: return "AYYI_OBJECT_"#x
		CASE (EMPTY);
		CASE (TRACK);
		CASE (AUDIO_TRACK);
		CASE (MIDI_TRACK);
		CASE (CHAN);
		CASE (AUX);
		CASE (PART);
		CASE (AUDIO_PART);
		CASE (MIDI_PART);
		CASE (EVENT);
		CASE (RAW);
		CASE (STRING);
		CASE (ROUTE);
		CASE (FILE);
		CASE (LIST);
		CASE (MIDI_NOTE);
		CASE (SONG);
		CASE (TRANSPORT);
		CASE (LOCATORS);
		CASE (AUTO);
		CASE (PROGRESS);
		CASE (METRONOME);
		CASE (UNSUPPORTED);
		CASE (ALL);
#undef CASE
		default:
			snprintf (unk, 31, "UNKNOWN OBJECT TYPE (%d)", object_type);
			return unk;
	}
	return NULL;
}


const char*
ayyi_print_optype (AyyiOp op)
{
	static char* ops[] = {"", "NEW", "DEL", "GET", "SET", "UNDO", "BAD OPTYPE"};
	return ops[MIN(op, 6)];
}


const char*
ayyi_print_prop_type (AyyiPropType prop_type)
{
	static char unk[32];

	switch (prop_type){
#define CASE(x) case AYYI_##x: return "AYYI_"#x
		CASE (NO_PROP);
		CASE (NAME);
		CASE (STIME);
		CASE (LENGTH);
		CASE (LEVEL);
		CASE (PAN);
		CASE (PAN_ENABLE);
		CASE (DELAY);
		CASE (HEIGHT);
		CASE (COLOUR);
		CASE (END);
		CASE (TRACK);
		CASE (MUTE);
		CASE (ARM);
		CASE (SOLO);
		CASE (SDEF);
		CASE (INSET);
		CASE (FADEIN);
		CASE (FADEOUT);
		CASE (INPUT);
		CASE (OUTPUT);
		CASE (PREPOST);
		CASE (SPLIT);
		CASE (PLUGIN_SEL);
		CASE (PLUGIN_BYPASS);
		CASE (PB_LEVEL);
		CASE (PB_PAN);
		CASE (PB_DELAY);
		CASE (TRANSPORT_PLAY);
		CASE (TRANSPORT_STOP);
		CASE (TRANSPORT_REW);
		CASE (TRANSPORT_FF);
		CASE (TRANSPORT_REC);
		CASE (TRANSPORT_LOCATE);
		CASE (TRANSPORT_CYCLE);
		CASE (TRANSPORT_LOCATOR);
		CASE (AUTO_PT);
		CASE (ADD_POINT);
		CASE (DEL_POINT);
		CASE (TEMPO);
		CASE (HISTORY);
		CASE (LOAD_SONG);
		CASE (SAVE);
		CASE (NEW_SONG);
#undef CASE
		default:
			snprintf (unk, 31, "UNKNOWN PROPERTY (%d)", prop_type);
	}
	return unk;
}


const char*
ayyi_print_media_type (AyyiMediaType type)
{
	static char* types[] = {"[Media type not set]", "AUDIO", "MIDI"};
	g_assert(G_N_ELEMENTS(types) == AYYI_MIDI + 1);
	return types[type];
}
