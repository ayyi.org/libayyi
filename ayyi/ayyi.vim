" Vim syntax file
" Language:     C ayyi


syn keyword ayyiType		AyyiClient
syn keyword ayyiType		AyyiHandler
syn keyword ayyiType		AyyiActionCallback
syn keyword ayyiType		AyyiAction
syn keyword ayyiType		AyyiContainer
syn keyword ayyiType		AyyiList
syn keyword ayyiType		AyyiItem
syn keyword ayyiType		AyyiRegion
syn keyword ayyiType		AyyiAudioRegion
syn keyword ayyiType		AyyiMidiRegion
syn keyword ayyiType		AyyiRegionBase
syn keyword ayyiType		AyyiFilesource
syn keyword ayyiType		AyyiChannel
syn keyword ayyiType		AyyiPlaylist
syn keyword ayyiType		AyyiAux
syn keyword ayyiType		AyyiConnection
syn keyword ayyiType		AyyiTrack
syn keyword ayyiType		AyyiMidiTrack
syn keyword ayyiType		AyyiControl
syn keyword ayyiType		AyyiObjIdx
syn keyword ayyiType		AyyiIdx
syn keyword ayyiType		AyyiId
syn keyword ayyiType		AyyiObjType
syn keyword ayyiType		AyyiOpType
syn keyword ayyiType		AyyiPropType
syn keyword ayyiType		AyyiIdent
syn keyword ayyiType		AyyiMediaType
syn keyword ayyiType		AyyiSongPos
syn keyword ayyiType		AyyiMidiNote
syn keyword ayyiType		MidiNote
syn keyword ayyiType		MidiPart
syn keyword ayyiType		TransitionalNote
syn keyword ayyiType		AyyiAutoType
syn keyword ayyiType		ReadyType
syn keyword ayyiType		AMPoolItem
syn keyword ayyiType		TrackType
syn keyword ayyiType		FeatureType
syn keyword ayyiType		NoteSelectionListItem
syn keyword ayyiType		SongPool
syn keyword ayyiType		AyyiFinish
syn keyword ayyiType		AyyiHandler5
syn keyword ayyiType		HandlerData

syn keyword ayyiType		AMPos
syn keyword ayyiType		AMChangeType
syn keyword ayyiType		AMVal
syn keyword ayyiType		AMItem
syn keyword ayyiType		AMCollection
syn keyword ayyiType		AMTrack
syn keyword ayyiType		AMTrackNum
syn keyword ayyiType		AMPart
syn keyword ayyiType		AMPalette
syn keyword ayyiType		AMAccel
syn keyword ayyiType		AMiRange
syn keyword ayyiType		AMCurve

syn keyword ayyiConstant    AYYI_OBJECT_TRACK
syn keyword ayyiConstant    AYYI_OBJECT_AUDIO_TRACK
syn keyword ayyiConstant    AYYI_OBJECT_MIDI_TRACK
syn keyword ayyiConstant    AYYI_OBJECT_TRANSPORT
syn keyword ayyiConstant    AYYI_OBJECT_SONG
syn keyword ayyiConstant    AYYI_OBJECT_PART
syn keyword ayyiConstant    AYYI_OBJECT_AUDIO_PART
syn keyword ayyiConstant    AYYI_OBJECT_MIDI_PART
syn keyword ayyiConstant    AYYI_OBJECT_FILE
syn keyword ayyiConstant    AYYI_OBJECT_ROUTE
syn keyword ayyiConstant    AYYI_OBJECT_UNSUPPORTED
syn keyword ayyiConstant    AYYI_NEW
syn keyword ayyiConstant    AYYI_DEL
syn keyword ayyiConstant    AYYI_SET
syn keyword ayyiConstant    AYYI_NULL_IDENT
syn keyword ayyiConstant    APP_STATE_ENGINE_OK
syn keyword ayyiConstant    APP_STATE_ABORT
syn keyword ayyiConstant    HAS_ALPHA_TRUE
syn keyword ayyiConstant    HAS_ALPHA_FALSE
syn keyword ayyiConstant    FILL_TRUE
syn keyword ayyiConstant    FILL_FALSE
syn keyword ayyiConstant    EXPAND_TRUE
syn keyword ayyiConstant    EXPAND_FALSE
syn keyword ayyiConstant    HANDLED
syn keyword ayyiConstant    NOT_HANDLED
syn keyword ayyiConstant    AM_CHANGE_ZOOM_V
syn keyword ayyiConstant    AM_CHANGE_ARMED
syn keyword ayyiConstant    AM_CHANGE_MUTE
syn keyword ayyiConstant    AM_CHANGE_SOLO
syn keyword ayyiConstant    AM_CHANGE_NAME
syn keyword ayyiConstant    AM_CHANGE_OUTPUT
syn keyword ayyiConstant    AM_CHANGE_COLOUR
syn keyword ayyiConstant    AM_CHANGE_LEN
syn keyword ayyiConstant    AM_CHANGE_INSET
syn keyword ayyiConstant    AM_CHANGE_WIDTH
syn keyword ayyiConstant    AM_CHANGE_HEIGHT
syn keyword ayyiConstant    AM_CHANGE_FADES
syn keyword ayyiConstant    AM_REGION_FULL
syn keyword ayyiConstant    AM_POOL_ITEM_PENDING
syn keyword ayyiConstant    AM_POOL_ITEM_OK
syn keyword ayyiConstant    AM_MAX_COLOUR
syn keyword ayyiMacro       AYYI_TRACK_IS_MASTER
syn keyword ayyiMacro       CH_IS_MASTER
syn keyword ayyiMacro       AM_TRK_IS_AUDIO
syn keyword ayyiMacro       AM_TRK_IS_MIDI
syn keyword ayyiMacro       START_TEST
syn keyword ayyiMacro       FINISH_TEST
syn keyword ayyiMacro       FAIL_TEST
syn keyword ayyiMacro       FAIL_IF_ERROR
syn keyword ayyiMacro       FAIL_MODULE_IF_ERROR
syn keyword ayyiMacro       START_MODULE
syn keyword ayyiMacro       FINISH_MODULE
syn keyword ayyiMacro       assert
syn keyword ayyiFunction    pwarn
syn keyword ayyiFunction    perr
syn keyword ayyiDebug       PF
syn keyword ayyiDebug       PF2
syn keyword ayyiDebug       dbg
syn keyword ayyiFunction    call

syn keyword ayyiType        vec4
syn keyword ayyiType        sampler2D
syn keyword ayyiType        uniform
syn keyword ayyiType        attribute

" ayyi_song.h
syn keyword ayyiFunction ayyi_song__translate_address
syn keyword ayyiFunction ayyi_song__print_automation_list
syn keyword ayyiFunction ayyi_song__track_lookup_by_id
syn keyword ayyiFunction ayyi_song__is_shm
syn keyword ayyiFunction ayyi_song__get_info
syn keyword ayyiFunction ayyi_song__get_audio_part_list
syn keyword ayyiFunction ayyi_song__get_midi_part_list
syn keyword ayyiFunction ayyi_song__get_file_path
syn keyword ayyiFunction ayyi_song__get_audiodir
syn keyword ayyiFunction ayyi_song__print_pool
syn keyword ayyiFunction ayyi_song__print_part_list
syn keyword ayyiFunction ayyi_song_container_get_item
syn keyword ayyiFunction ayyi_song_container_next_item
syn keyword ayyiFunction ayyi_song_container_prev_item
syn keyword ayyiFunction ayyi_song_container_count_items
syn keyword ayyiFunction ayyi_song_container_find
syn keyword ayyiFunction ayyi_song_container_verify
syn keyword ayyiFunction ayyi_song_container_next_name
syn keyword ayyiFunction ayyi_song_container_lookup_by_name
syn keyword ayyiFunction ayyi_song_container_make_name_unique
syn keyword ayyiFunction ayyi_song_container_delete_item
syn keyword ayyiFunction ayyi_song__get_item_by_ident
syn keyword ayyiFunction ayyi_region__index_ok
syn keyword ayyiFunction ayyi_region__delete_async
syn keyword ayyiFunction ayyi_region__make_name_unique
syn keyword ayyiFunction ayyi_region__get_pod_index
syn keyword ayyiFunction ayyi_region__get_start_offset
syn keyword ayyiFunction ayyi_region__get_from_id
syn keyword ayyiFunction ayyi_track__next_armed
syn keyword ayyiFunction ayyi_track__get_output
syn keyword ayyiFunction ayyi_track__get_output_name
syn keyword ayyiFunction ayyi_track__has_output
syn keyword ayyiFunction ayyi_track__index_ok
syn keyword ayyiFunction ayyi_track__get_channel
syn keyword ayyiFunction ayyi_track__make_name_unique
syn keyword ayyiFunction ayyi_song__get_track_count
syn keyword ayyiFunction ayyi_song__get_track_by_playlist
syn keyword ayyiFunction ayyi_song__get_playlist_by_track
syn keyword ayyiFunction ayyi_file_get_other_channel
syn keyword ayyiFunction ayyi_filesource_get_from_id
syn keyword ayyiFunction ayyi_filesource_get_by_name
syn keyword ayyiFunction ayyi_file_is_stereo
syn keyword ayyiFunction ayyi_file_get_linked
syn keyword ayyiFunction ayyi_connection__is_input
syn keyword ayyiFunction ayyi_connection__next_input
syn keyword ayyiFunction ayyi_connection__next_output
syn keyword ayyiFunction ayyi_connection__parse_string
syn keyword ayyiFunction ayyi_song__get_files_list
syn keyword ayyiFunction ayyi_song__have_file
syn keyword ayyiFunction ayyi_song__verify
syn keyword ayyiFunction ayyi_verify_playlists
syn keyword ayyiFunction ayyi_midi_note_new
syn keyword ayyiFunction ayyi_song__print_playlists
syn keyword ayyiFunction ayyi_song__print_tracks
syn keyword ayyiFunction ayyi_song__print_midi_tracks
syn keyword ayyiFunction ayyi_song__print_connections

"model/track.h
syn keyword ayyiFunction am_track_get_type
syn keyword ayyiFunction am_track__new
syn keyword ayyiFunction am_track__get_channel_label
syn keyword ayyiFunction am_track__arm
syn keyword ayyiFunction am_track__mute
syn keyword ayyiFunction am_track__solo
syn keyword ayyiFunction am_track__lookup_channel
syn keyword ayyiFunction am_track__lookup_channel_num
syn keyword ayyiFunction am_track__is_armed
syn keyword ayyiFunction am_track__is_solod
syn keyword ayyiFunction am_track__is_muted
syn keyword ayyiFunction am_track__is_master
syn keyword ayyiFunction am_track__is_not_master
syn keyword ayyiFunction am_track__get_connections_string
syn keyword ayyiFunction am_track__get_name
syn keyword ayyiFunction am_track__set_name
syn keyword ayyiFunction am_track__get_colour
syn keyword ayyiFunction am_track__set_colour
syn keyword ayyiFunction am_track__set_input
syn keyword ayyiFunction am_track__set_output
syn keyword ayyiFunction am_track__get_input_name
syn keyword ayyiFunction am_track__get_output_name
syn keyword ayyiFunction am_track__get_meterlevel
syn keyword ayyiFunction am_track__get_meterlevel_master
syn keyword ayyiFunction am_track__get_input_num
syn keyword ayyiFunction am_track__get_output_num
syn keyword ayyiFunction am_track__get_output
syn keyword ayyiFunction am_track__get_start_end
syn keyword ayyiFunction am_track__get_automation_range
syn keyword ayyiFunction am_track__lookup_curve
syn keyword ayyiFunction am_track__clear_automation
syn keyword ayyiFunction am_track__enable_metering
syn keyword ayyiFunction am_track__is_empty
syn keyword ayyiFunction am_track__is_mono
syn keyword ayyiFunction am_track__is_midi
syn keyword ayyiFunction am_track__is_valid
syn keyword ayyiFunction am_track__print_type

"model/channel.h
syn keyword ayyiType		AMChannel

" model/automation.h
syn keyword ayyiFunction am_automation_point_add
syn keyword ayyiFunction am_automation_point_remove
syn keyword ayyiFunction am_automation_point_change
syn keyword ayyiFunction am_automation_make_empty
syn keyword ayyiFunction am_automation_get_value

" Default highlighting
if version >= 508 || !exists("did_ayyi_syntax_inits")
  if version < 508
    let did_ayyi_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif
  HiLink ayyiType               Type
  HiLink ayyiFunction           Function
  HiLink ayyiMacro              Macro
  HiLink ayyiConstant           Constant
  HiLink ayyiBoolean            Boolean
  HiLink ayyiDebug              Debug
  delcommand HiLink
endif


