/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __ayyi_transport_h__
#define __ayyi_transport_h__

#include <stdbool.h>

void     ayyi_transport_play            ();
void     ayyi_transport_rec             ();
void     ayyi_transport_stop            ();
void     ayyi_transport_rew             ();
void     ayyi_transport_ff              ();

bool     ayyi_transport_is_stopped      ();
bool     ayyi_transport_is_rew          ();
bool     ayyi_transport_is_ff           ();
bool     ayyi_transport_is_playing      ();
bool     ayyi_transport_is_cycling      ();
bool     ayyi_transport_is_rec_enabled  ();
bool     ayyi_transport_is_recording    ();

uint32_t ayyi_transport__get_frame      ();
void     ayyi_transport_get_spp_string  (char* spp);

#endif
