/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_mixer_h__
#define __ayyi_mixer_h__

#include "ayyi/ayyi_typedefs.h"

//#define ayyi_mixer__get_plugin_controls(A) ayyi_container_get_item(&ayyi.mixer->plugins, A)
#define ayyi_mixer__channel_at(A)    ayyi_mixer_container_get_item   (&((AyyiMixerService*)ayyi.service)->amixer->tracks, A)
#define ayyi_mixer__channel_next(A)  (AyyiChannel*)ayyi_mixer__container_next_item (&((AyyiMixerService*)ayyi.service)->amixer->tracks, ((AyyiItem*)A))
#define ayyi_mixer__count_channels() ayyi_mixer_container_count_items(&((AyyiMixerService*)ayyi.service)->amixer->tracks)
#define ayyi_mixer__plugin_next(A)   (AyyiPlugin*)ayyi_mixer__container_next_item (&((AyyiMixerService*)ayyi.service)->amixer->plugins, (AyyiItem*)A)

bool           ayyi_mixer__verify              ();

AyyiChannel*   ayyi_mixer__channel_at_quiet    (AyyiIdx);
int            ayyi_channel__count_plugins     (AyyiChannel*);
AyyiPlugin*    ayyi_plugin_at                  (AyyiIdx);
bool           ayyi_plugin_pod_index_ok        (AyyiIdx);
AyyiContainer* ayyi_plugin_get_controls        (AyyiIdx plugin_slot);
AyyiControl*   ayyi_plugin_get_control         (AyyiPlugin*, AyyiIdx control_idx);

void*          ayyi_mixer_container_get_item   (AyyiContainer*, AyyiIdx);
void*          ayyi_mixer_container_get_quiet  (AyyiContainer*, AyyiIdx);
#if 0
AyyiControl*   ayyi_mixer__get_plugin_control  (route_shared* route, int slot);
#endif
AyyiControl*   ayyi_mixer__plugin_control_next (AyyiContainer*, AyyiControl*);
AyyiItem*      ayyi_mixer__container_next_item (AyyiContainer*, AyyiItem*);
#define        ayyi_mixer__container_next(C, A) ayyi_container_next_item(C, (AyyiItem*)A, (AyyiCShmSeg*)ayyi.services[1]->segs->data, ayyi_mixer_translate_address)
#define        ayyi_mixer__container_prev(C, A) ayyi_container_prev_item(C, (AyyiItem*)A, (AyyiCShmSeg*)ayyi.services[1]->segs->data, ayyi_mixer_translate_address)
int            ayyi_mixer_container_count_items(AyyiContainer*);
gboolean       ayyi_mixer_container_verify     (AyyiContainer*);

AyyiAux*       ayyi_mixer__aux_get             (AyyiChannel*, AyyiIdx);
AyyiAux*       ayyi_mixer__aux_get_            (AyyiIdx channel, AyyiIdx);
int            ayyi_mixer__aux_count           (AyyiIdx channel);

AyyiTrack*     ayyi_channel__get_track         (AyyiChannel*);
AyyiShmEvent*  ayyi_channel__get_auto_event    (AyyiChannel*, int i, int auto_type);
void           ayyi_channel__set_float         (AyyiIdx chan, int prop, double val);
void           ayyi_channel__set_bool          (AyyiIdx chan, int prop, gboolean val);
AyyiList*      ayyi_channel__get_routing       (AyyiChannel*);

#ifdef DEBUG
void           ayyi_print_mixer                ();
#endif
void           ayyi_automation_print           (AyyiChannel*);

void*          ayyi_mixer_translate_address    (void*);

#endif
