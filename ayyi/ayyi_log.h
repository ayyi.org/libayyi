/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_log_h__
#define __ayyi_log_h__

#include <stdbool.h>
#include <glib.h>

typedef enum {
    LOG_NONE = 0,
    LOG_OK,
    LOG_FAIL,
    LOG_WARN,
} AyyiLogType;

/*
 *  Clients can create one or more AyyiLogImpl instances
 *  and enable them by calling ayyi_log_add_logger().
 *
 *  Logging to stdout is enabled if the debug variable is set.
 */
typedef struct
{
   void (*print)      (AyyiLogType, const char*);
   void (*print_ok)   ();
   void (*print_fail) ();
   void (*print_warn) ();

} AyyiLogImpl;

typedef struct
{
   bool   to_stdout;
   GList* loggers;

} AyyiLog;

void ayyi_log_init       ();
void ayyi_log_print      (AyyiLogType, const char* format, ...);
void ayyi_log_print_ok   ();
void ayyi_log_print_fail ();
void ayyi_log_print_warn ();

#define ayyi_log_add_logger(LOGGER) \
	ayyi.log.loggers = g_list_prepend(ayyi.log.loggers, LOGGER);

#endif
