/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __ayyi_action_c__
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include <ayyi/ayyi_action.h>

#if 0
static gboolean ayyi_check_actions (gpointer data);
#endif

extern int debug;


GType
ayyi_action_get_type ()
{
	gpointer ayyi_action_copy (gpointer boxed)
	{
		return boxed;
	}

	static GType type = 0;

	if (G_UNLIKELY (!type))
		type = g_boxed_type_register_static ("AyyiAction", ayyi_action_copy, (GBoxedFreeFunc)ayyi_action_free);

	return type;
}


AyyiAction*
ayyi_action_new (char* format, ...)
{
	va_list argp;
	va_start(argp, format);

	AyyiAction* a = g_new0(AyyiAction, 1);
	a->id = ayyi.priv->trans_id++;
	a->op = AYYI_SET;

	vsnprintf(a->label, 63, format, argp);
	gettimeofday(&a->time_stamp, NULL);

	ayyi.priv->actionlist = g_list_append(ayyi.priv->actionlist, a);
	AYYI_DEBUG if(ayyi.log.to_stdout) printf("%saction%s: %s%s%s\n", yellow_r, white, bold, a->label, white);

	va_end(argp);

	return a;
}


#if 0
AyyiAction*
ayyi_action_new_pending (GList* pending_list)
{
	// create an action that is dependant on other actions being completed first.
	PF;
	AyyiAction* a = ayyi_action_new("");
	a->pending = pending_list;
	return a;
}
#endif


void
ayyi_action_free (AyyiAction* action)
{
	g_return_if_fail(action);
	g_return_if_fail(ayyi.priv->actionlist);
	dbg (2, "list len: %i", g_list_length(ayyi.priv->actionlist));
	dbg (3, "id=%i %s%s%s", action->id, bold, strlen(action->label) ? action->label : "<unnamed>", white);

	ayyi.priv->actionlist = g_list_remove(ayyi.priv->actionlist, action);
#if 0
	if(action->pending) g_list_free(action->pending);
#endif
	if(action->app_data) g_free(action->app_data);
	g_free(action);
	dbg (2, "new list len: %i", g_list_length(ayyi.priv->actionlist));
}


void
ayyi_action_execute (AyyiAction* a)
{
	switch(a->op){
		case AYYI_OP_NEW:
			pwarn("AYYI_NEW: FIXME not implemented");
			break;
		case AYYI_GET:
			pwarn("AYYI_GET: FIXME not implemented");
			break;
		case AYYI_SET:
			switch(a->prop){
				case AYYI_LENGTH:
				case AYYI_AUTO_PT:
				case AYYI_HEIGHT:
					pwarn("AYYI_SET: FIXME not implemented");
					break;
				case AYYI_TRANSPORT_REC:
				case AYYI_ARM:
				case AYYI_SOLO:
					#if USE_DBUS
					dbus_set_prop_bool(a, a->obj.type, a->prop, a->obj_idx.idx1, a->i_val);
					#endif
					break;
				default:
					//integer properties:
					dbus_set_prop_int(a, a->obj.type, a->prop, a->obj_idx.idx1, a->i_val);
					return;
			}
			break;

		case AYYI_DEL:
			#ifdef USE_DBUS
			ayyi_dbus_object_del(a, a->obj.type, a->obj_idx.idx1);
			return;
			#endif
			break;

		default:
			perr ("unsupported op type: %i", a->op);
			break;
	}
}


/*
 *  Actions are freed here and (except for timeouts, and non-action errors) only here.
 */
void
ayyi_action_complete (AyyiAction* action)
{
	g_return_if_fail(action);

	if(action->callback) (*(action->callback))(action);

	// if the callback has not already handled any errors...
	GError* error = action->ret.error;
	if(error){
		if(error->domain == ayyi_msg_error_quark()){
			//error was reported upstream
		}else{
			dbg(1, "client has not handled error...");
			//note: error code is always 32 (DBUS_GERROR_REMOTE_EXCEPTION)
			ayyi_log_print(LOG_FAIL, "%s: %s", action ? action->label : "", error->message);
		}
		action->ret.error = NULL;
	}

	ayyi_action_free(action);
}


/*
 *  Returns the AyyiAction if the given action id id is in the actionlist, or NULL if not.
 */
AyyiAction*
ayyi_action_lookup_by_id (unsigned trid)
{
	if(ayyi.priv->actionlist){
		GList* l = ayyi.priv->actionlist;
		for(;l;l=l->next){
			AyyiAction* a = l->data;
			if(a->id == trid){
				return a;
			}
		}
		perr ("trid not found! (%i)", trid);
	}

	return NULL;
}


/*
 *  Check that message requests are not timing out.
 */
#if 0
static gboolean
ayyi_check_actions (gpointer data)
{
	struct timeval now;
	gettimeofday(&now, NULL);

	GList* l = ayyi.priv->actionlist;
	if(!l) return TRUE;
	for(;l;l=l->next){
		AyyiAction* action = l->data;

		if(now.tv_sec - action->time_stamp.tv_sec > 5){
			//Timeout. call the normal callback. The callback should handle the error itself.

			if(_debug_>-1) if(!action->id || action->id > ayyi.priv->trans_id){ perr ("bad trid? %i", action->id); continue; }
			dbg(0, "timeout! calling callback... trid=%u '%s'", action->id, action->label);
			if(action->callback){
				action->ret.error = g_error_new(G_FILE_ERROR, 3533, "timeout"); //TODO free
				(*(action->callback))(action);
			}
			/*else */ayyi_action_free(action);

			break; // only do one at a time to avoid problems with iterating over a changing list.
		}
	}

	return TRUE;
}
#endif


void
ayyi_print_action_status ()
{
	ayyi_log_print(0, "outstanding requests: %i", g_list_length(ayyi.priv->actionlist));
}


void
ayyi_set_length (AyyiAction* action, uint64_t len)
{
	// set length of object
	// @param _obj_id should *not* have any masks applied to it (except perhaps for container/block indexing)

#ifdef USE_DBUS
	dbus_set_prop_int64(action, action->obj.type, AYYI_LENGTH, action->obj_idx.idx1, len);
#endif
}


void
ayyi_set_float (AyyiAction* action, uint32_t object_type, AyyiIdx obj_idx, int prop, double _val)
{
#ifdef USE_DBUS
	dbus_set_prop_float(action, object_type, prop, obj_idx, _val);
#endif
}


void
ayyi_set_obj (AyyiAction* action, AyyiObjType object_type, AyyiIdx obj_id, int prop, uint64_t val)
{
	action->obj.type = object_type; //check
#ifdef USE_DBUS
	switch(prop){
		case AYYI_MUTE:
		case AYYI_TRACK:
		case AYYI_LOAD_SONG:
		case AYYI_SAVE:
			dbus_set_prop_int(action, object_type, prop, obj_id, val);
			break;
		case AYYI_SPLIT:
		case AYYI_INSET:
			dbus_set_prop_int64(action, AYYI_OBJECT_AUDIO_PART, prop, obj_id, val);
			break;
		default:
			pwarn ("unexpected property: %s (%i)", ayyi_print_prop_type(prop), prop);
	}
#endif
}


void
ayyi_set_string (AyyiAction* action, uint32_t object_type, AyyiIdx idx, const char* _string)
{
#ifdef USE_DBUS
	dbus_set_prop_string(action, object_type, AYYI_NAME, idx, _string);
#endif
}


HandlerData*
ayyi_handler_data_new (AyyiHandler callback, gpointer data)
{
	return AYYI_NEW(HandlerData,
		.callback = callback,
		.user_data = data,
	);
}


void
ayyi_handler_simple (AyyiAction* a)
{
	HandlerData* d = a->app_data;
	if(d){
		call(d->callback, (AyyiIdent){a->obj.type, a->ret.idx}, &a->ret.error, d->user_data);
	}
}


