#ifndef __ayyi_h__
#define __ayyi_h__

#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_log.h>
#include <ayyi/ayyi_action.h>

#endif
