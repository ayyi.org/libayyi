/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */
#ifndef __ayyi_typedefs_h__
#define __ayyi_typedefs_h__

#include <stdint.h>
#include <stdbool.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _ayyi_server         AyyiServer;
typedef struct _ayyi_shm_seg        AyyiShmSeg;
typedef struct _shm_song            AyyiShmSong;
typedef struct _shm_seg_mixer       AyyiShmMixer;

typedef struct _ayyi_action         AyyiAction;
typedef struct _ayyi_obj_idx        AyyiObjIdx;
typedef enum   _ayyi_op_type        AyyiOpType;
typedef enum   _ayyi_prop_type      AyyiPropType;
typedef struct _AyyiIdent           AyyiIdent;
typedef struct _AyyiService         AyyiService;
typedef struct _ardourd_service     AyyiSongService;
typedef struct _ArdourdMixerService AyyiMixerService;
typedef struct _AyyiCShmSeg         AyyiCShmSeg;
typedef enum   _seg_type            SegType;
typedef int64_t                     AyyiMu;
typedef struct _AyyiSongPos         AyyiSongPos;
typedef struct _container           AyyiContainer;
typedef struct _AyyiBlock           block;
typedef struct _AyyiBlock           AyyiBlock;
typedef struct _ayyi_base_item      AyyiItem;
typedef struct _route_shared        AyyiTrack;
typedef struct _ayyi_channel        AyyiChannel;
typedef struct _region_base_shared  AyyiRegionBase;
typedef struct _AyyiAudioRegion     AyyiRegion;
typedef struct _AyyiAudioRegion     AyyiAudioRegion;
typedef struct _midi_region_shared  AyyiMidiRegion;
typedef struct _filesource_shared   AyyiFilesource;
typedef struct _playlist_shared     AyyiPlaylist;
typedef struct _ayyi_connection     AyyiConnection;
typedef struct _midi_track_shared   AyyiMidiTrack;
typedef struct _ayyi_aux            AyyiAux;
typedef struct _ayyi_control        AyyiControl;
typedef struct _plugin_shared       AyyiPlugin;
typedef struct _AyyiMidiNote        AyyiMidiNote;
typedef struct _launch_info         AyyiLaunchInfo;
typedef struct _HandlerData         HandlerData;
typedef struct _AyyiShmCallbackData AyyiShmCallbackData;
typedef struct _ShmEvent            AyyiShmEvent;
typedef struct _ayyi_list           AyyiList;

typedef int                         AyyiIdx;
typedef uint32_t                    AyyiObjType;
typedef uint64_t                    AyyiId;

typedef gboolean (*AyyiShmCallback) (AyyiCShmSeg*);
typedef void   (*AyyiCallback)      (gpointer);
typedef void   (*AyyiFinish)        (AyyiIdent, gpointer);
typedef void   (*AyyiHandler2)      (const GError* error, void*);
typedef void   (*AyyiHandler)       (AyyiIdent, GError**, void*);
typedef void   (*AyyiHandler3)      (AyyiIdent, const GError*, void*);
typedef void   (*AyyiPropHandler)   (AyyiIdent, GError**, AyyiPropType, void*);
typedef void   (*AyyiValHandler)    (AyyiIdent, GError**, double, gpointer);
typedef void   (*AyyiActionCallback)(AyyiAction*);

#ifndef true
#define true  1
#define false 0
#endif

#if INTPTR_MAX == INT64_MAX
	#define boolp int64_t
#else
	#define boolp int32_t
#endif

#ifdef __cplusplus
}
#endif

#endif
