// Manually created - do not delete
[CCode(
	cheader_filename = "ayyi/ayyi.h",
	cheader_filename = "model/ayyi_model.h",
	lower_case_cprefix = "", cprefix = "")]

namespace Ayyi {
	public delegate void AyyiCallback (void* user_data);

	//callback signatures for vala:

	public delegate void AyyiHandler3   (Ident ident, GLib.Error error, void* user_data);
	public delegate void AyyiHandler2a  (GLib.Error error);

	// GLib.Error* here gets translated to GLib.Error** in the c file
	// -the user_data argument is missing, as vala uses it internally for the 'target' (this).
	public delegate void AyyiHandler3a  (Ident ident, GLib.Error* error);
	//public delegate void AyyiHandler3b  (Ident ident) throws GLib.Error;

	[CCode (has_target = false)]
	public delegate void AyyiHandler2   (GLib.Error error, void* user_data);
	[CCode (has_target = false)]
	public delegate void AyyiHandler4   (Ident ident, GLib.Error error, void* user_data);
	[CCode (has_target = false)]
	public delegate void AyyiHandler5   (Ident ident, GLib.Error** error, void* user_data);
	[CCode (has_target = false)]
	public delegate void AyyiActionCallback (AyyiAction* action);

	[CCode(cprefix = "AYYI_", cname = "AyyiMediaType")]
	public enum MediaType
	{
		AUDIO = 1,
		MIDI
	}

	[CCode (cname = "BLOCK_SIZE",
		cheader_filename = "stdint.h",
		cheader_filename = "ayyi/interface.h"
	)]
	private const int BLOCK_SIZE;

	[CCode(cname = "struct block",
		cheader_filename = "ayyi/interface.h",
		cheader_filename = "ayyi/ayyi.h"
	)]
	public struct block
	{
		public void**      slot;
		public int         last;
		public int         full;
		public void*       next;
	}
	[CCode(cname = "struct _container",
		cheader_filename = "stdint.h",
		cheader_filename = "ayyi/interface.h"
	)]
	public struct container {
		public block**       block;
		public int           last;
		public int           full;
		public char*         signature;
	}

	[CCode (cname = "int", cprefix = "AYYI_", has_type_id = false)]
	public enum Loc
	{
		MAX_LOC = 20,
	}

	[CCode(cname = "struct _AyyiSongPos",
		cheader_filename = "ayyi/ayyi.h"
	)]
	public struct SongPos
	{
		int    beat;
		uint16 sub;
		uint16 mu;
	}

	[CCode(cname = "struct _shm_song")]
	public struct ShmSong
	{
		public char*       service_name;
		public char*       service_type;
		public int         version;
		public void*       owner_shm;
		public int         num_pages;
		public void*       tlsf_pool;

		public char*       path;
		public char*       snapshot;
		public uint        sample_rate;
		public SongPos     start;
		public SongPos     end;
		public SongPos*    locators;
		public double      bpm;
		public uint        transport_frame;
		public float       transport_speed;
		public int         play_loop;         // boolean
		public int         rec_enabled;       // boolean

		public container   audio_regions;
		public container   midi_regions;
		public container   filesources;
		public container   connections;
		public container   audio_tracks;
		public container   midi_tracks;
		public container   playlists;
		public block       ports;
		public container   vst_plugin_info;
	}
	[CCode(cname = "struct _shm_seg_mixer")]
	public struct ShmSegMixer {
		public char*         service_name;
		public char*         service_type;
		public int           version;
		public void*         owner_shm;
		public int           num_pages;
		public void*         tlsf_pool;

		public AyyiChannel   master;
		public container     tracks;
		public container     plugins;
	}

	[CCode(cname = "struct _ayyi_list")]
	public struct AyyiList
	{
		public container*    data;
		public void*         next;
		public int           id;
		public char*         name;
	}

	[CCode(cname = "struct _ayyi_base_item")]
	public struct Item {
		public int            shm_idx; // slot_num | (block_num / BLOCK_SIZE)
		public char*          name;
		public uint64         id;
		public void*          server_object;
		public int            flags;
	}

	[CCode(cname = "struct _region_base_shared")]
	public struct BaseRegion {
		int            shm_idx;
		char*          name;
		uint64         id;
		void*          server_object;
		public int     flags;
		int            playlist;
		uint32         position;
		uint32         length;
	}

	[CCode(cname = "struct _AyyiAudioRegion")]
	public struct AyyiRegion {
		public int            shm_idx;
		public char*          name;
		public uint64         id;
		public void*          server_object;
		public int            playlist;
		public uint32         position;
		public uint32         length;
		// end base.
		public uint32         start;
		public int            flags;
		public uint64         source0;
		public char           channels;
		public uint32         fade_in_length;
		public uint32         fade_out_length;
		public float          level;
	}

	[CCode(cname = "struct _midi_region_shared")]
	public struct AyyiMidiRegion {
		public int            shm_idx;
		public char*          name;
		public uint64         id;
		public void*          server_object;
		public int            flags;
		public int            playlist;
		public uint32         position;
		public uint32         length;
		// end base.
		public container      events;
	}

	[CCode(cname = "struct _filesource_shared")]
	public struct AyyiFilesource {
		public int            shm_idx;
		public char*          name;
		public char*          original_name;
		public uint64         id;
		public void*          object;
		public uint32         length;
	}

	[CCode(cname = "struct _route_shared")]
	public struct AyyiTrack {
		public int            shm_idx;
		public char*          name;
		public uint64         id;
		public void*          object;
		public int            flags;
		public int32          colour;
		public char*          input_name;
		public int            input_idx;
		public int            input_minimum;
		public int            input_maximum;
		public int            output_minimum;
		public int            output_maximum;
		public AyyiList*      input_routing;
		public AyyiList*      output_routing;
		public float*         visible_peak_power;
	}

	[CCode(cname = "struct _midi_track_shared")]
	public struct AyyiMidiTrack {
		public int            shm_idx;
		public char*          name;
		public uint64         id;
		public void*          object;
		public int            flags;
		public int32          colour;
	}

	[CCode(cname = "struct _ayyi_connection")]
	public struct AyyiConnection {
		public int            shm_idx;
		public char*          name;
		public uint64         id;
		public void*          object;
		public int            flags;
		public string         device;
		public uint32         nports;
		public char           io;
	}

	[CCode(cname = "struct _ayyi_aux")]
	public struct AyyiAux {
		public int           idx;
		public float         level;
		public float         pan;
		public int           flags;
		public int           bus_num;
	}

	[CCode(cname = "struct I")]
	public struct I {
		public int           idx;
		public char          bypassed;
		public char          active;
	}
	[CCode(cname = "struct _ayyi_channel")]
	public struct AyyiChannel {
		public int            shm_idx;
		public char*          name;
		public uint64         id;
		public void*          object;
		public int            n_in;
		public int            n_out;
		public double         level;
		public int            has_pan;
		public float          pan;
		public float*         visible_peak_power;
		public I**            plugin;
		public AyyiAux*       aux;
		public container*     automation;
		public AyyiList*      automation_list;
	}
	[CCode(cname = "AYYI_PLUGINS_PER_CHANNEL")]
	public int PLUGINS_PER_CHANNEL;

	[CCode(cname = "struct _plugin_shared")]
	public struct AyyiPlugin {
		public int            idx;
		public char*          name;
		public char*          category;
		public uint32         n_inputs;
		public uint32         n_outputs;
		public uint32         latency;
		public container*     controls;
	}

	[CCode(cname = "struct _playlist_shared")]
	public struct Playlist {
		public int            shm_idx;
		public char*          name;
		public uint64         id;
		public void*          object;
		public int            flags;
		public int            track;
	}

	[CCode(cname = "struct _MidiNote")]
	public struct MidiNote {
		public int           idx;
		public int           note;
		public int           velocity;
		public uint          start;
		public int           length;
	}

	[CCode(cname = "enum _seg_type",
		cheader_filename = "ayyi/ayyi_shm.h"
	)]
	public enum SegType
	{
		NOT_SET,
		SONG,
		MIXER,
		PLUGIN,
		LAST
	}
	[CCode(cname = "struct _shm_seg",
		cheader_filename = "ayyi/ayyi.h"
	)]
	public struct ShmSeg
	{
		public int      id;
		public int      type;
		public int      size;
		public bool     attached;
		public bool     invalid;
		public void*    address;
	}

	public ShmSeg* ayyi_shm_seg_new(Service* service, int type);

	[CCode(cname = "struct _AyyiClient")]
	public struct AyyiClient
	{
		public Service*                service; // deprecated
		public Service**               services;
		public GLib.List<ShmSeg>       segs;
		public bool*                   on_shm(ShmSeg* seg);
		public bool                    got_shm;
		public bool                    offline;
		public int                     debug;
		public Log                     log;
		public GLib.List<AyyiAction>*  actionlist;
		public uint                    trans_id;
	}
	AyyiClient ayyi;

	[CCode(cname = "struct _service")]
	public struct Service
	{
		public char*          service;
		public char*          path;
		public char*          interface;
		[CCode(cname = "ardourd->on_shm")] //FIXME
		public void on_shm    ();
		public void on_new    (int object_type, int object_idx);
		public void on_delete (int object_type, int object_idx);
		public void on_change (int object_type, int object_idx);
		public DBusGProxy*    proxy;
	}
	[CCode(cname = "struct _ardourd_service")]
	public struct SongService
	{
		//Service             ayyi_service;
		public ShmSong*       song;
		public ShmSegMixer*   amixer;
		//int                     segments[1];
		//AyyiServicePrivate*     priv;
	}
	[CCode(cname = "enum AYYI_SERVICE")]
	public enum AyyiService
	{
		AYYID1
	}
	[CCode(cname = "known_services")]
	public Service**  services;
	public bool       ayyi_client__dbus_connect (Service* s, AyyiHandler2 callback, void* user_data);
	public bool       ayyi_client__dbus_get_shm (Service* s);

	[CCode(cname = "struct _log")]
	public struct Log
	{
		public bool           to_stdout;
	}

	[CCode (cname = "AyyiIdx", default_value = "-1")]
	[IntegerType (rank = 7)]
	public struct Idx {
	}

	[CCode (cname = "AyyiObjType", default_value = "-1")]
	[IntegerType (rank = 7)]
	public struct ObjType {
	}

	[CCode(cname = "struct _ayyi_obj_idx")]
	public struct AyyiObjIdx
	{
		public uint idx1;
		public uint idx2;
		public uint idx3;
	}

	[SimpleType]
	[CCode(cname = "struct _AyyiIdent")]
	public struct Ident
	{
		public ObjType type;
		public Ayyi.Idx idx;
	}

	[CCode(cprefix = "AYYI_")]
	public enum AyyiOp {
		NEW = 1,
		DEL,
		GET,
		SET,
		UNDO
	}

	[CCode(cprefix = "AYYI_OBJECT_", cname = "AyyiObjType")]
	public enum ObjectType {
		EMPTY = 0,
		TRACK,
		AUDIO_TRACK,
		MIDI_TRACK,
		CHAN,
		AUX,
		PART,
		AUDIO_PART,
		MIDI_PART,
		EVENT,
		RAW,
		STRING,
		ROUTE,
		FILE,
		LIST,
		MIDI_NOTE,
		SONG,
		TRANSPORT,
		AUTO,
		UNSUPPORTED,
		ALL
	}

	[CCode(cname = "struct _ayyi_action")]
	public struct AyyiAction
	{
		public uint             trid;
		public TimeValue        time_stamp;
		public char*            label;

		public int              obj_type;
		public AyyiObjIdx       obj_idx;
		public AyyiOp           op;
		public int              prop;
		public int              i_val;
		public double           d_val;
		public double           d_val2;

		public void             callback();
		//data to forward to callback:
		public int              val1;
		public int              val2;
		public void*            app_data;

		public GLib.List<void>* pending;

		//return values:
		public int              gid;
		public int              idx;
		public char             err;
		public void*            return_val_1;
	}
	[CCode(cname = "ayyi_action_execute")]
	public void        ayyi_action_execute      (AyyiAction* action);

	namespace List {
		public int       ayyi_list__length     (AyyiList* list);

		[CCode(cname = "ayyi_list__first")]
		public AyyiList* first                 (AyyiList* list);

		public AyyiList* ayyi_list__next       (AyyiList* list);
		public void*     ayyi_list__first_data (AyyiList** list);
		public void*     ayyi_list__next_data  (AyyiList** list);
		public AyyiList* ayyi_list__find       (AyyiList* list, uint32 i);
	}

	[CCode(cname = "ayyi_song__audio_region_at")]
	public AyyiRegion*     song__audio_region_get(Ayyi.Idx index);

	public AyyiRegion*     ayyi_song__audio_region_next(AyyiRegion* region);
	public AyyiMidiRegion* ayyi_song__midi_region_next(AyyiMidiRegion* region);
	namespace __Song {
	}
	namespace AyyiSong {
		[CCode(cname = "ayyi_track__get_channel")]
		public AyyiChannel* track__get_channel            (AyyiTrack* track);

		[CCode(cname = "ayyi_song__filesource_at")]
		public AyyiFilesource* get_filesource             (Ayyi.Idx index);

		[CCode(cname = "ayyi_song__audio_track_at")]
		public AyyiTrack*      get_audio_track            (Ayyi.Idx index);

		[CCode(cname = "ayyi_song__midi_track_at")]
		public AyyiTrack*      midi_track_at              (Ayyi.Idx index);

		[CCode(cname = "ayyi_song__connection_at")]
		public AyyiConnection* get_connection             (Ayyi.Idx index);

		[CCode(cname = "ayyi_song__playlist_at")]
		public Ayyi.Playlist*  playlist_at                (Ayyi.Idx index);

		[CCode(cname = "ayyi_song_container_next_item")]
		public Ayyi.Item*      container_next_item        (Ayyi.container* container, void* prev);

		[CCode(cname = "ayyi_song__audio_connection_next")]
		public AyyiConnection* audio_connection_next(AyyiConnection* conn);

		[CCode(cname = "ayyi_song__filesource_next")]
		public AyyiFilesource* filesource_next(AyyiFilesource* file);

		[CCode(cname = "ayyi_song__audio_track_next")]
		public AyyiTrack*      audio_track_next           (AyyiTrack* track);

		[CCode(cname = "ayyi_song__midi_track_next")]
		public AyyiMidiTrack*  midi_track_next            (AyyiMidiTrack* track);

		[CCode(cname = "ayyi_song__playlist_next")]
		public Ayyi.Playlist*   playlist_next             (Ayyi.Playlist* playlist);

		[CCode(cname = "ayyi_song__get_audio_track_count")]
		public int             get_audio_track_count      ();

		[CCode(cname = "ayyi_song__get_playlist_count")]
		public int             get_playlist_count      ();

		[CCode(cname = "ayyi_song_container_count_items")]
		public int             container_count_items      (container* container);
		[CCode(cname = "ayyi_song_container_next_name")]
		public char*           container_next_name        (container* container, char* basename);

		[CCode(cname = "ayyi_song__get_midi_part_list")]
		public GLib.List<AyyiMidiRegion*> get_midi_part_list ();

		[CCode(cname = "AYYI_TRACK_IS_MASTER")]
		public bool            track_is_master            (AyyiTrack* track);

		[CCode(cname = "ayyi_song_container_delete_item")]
		public AyyiAction*     delete_item                (container* container, Ayyi.Item* item, AyyiHandler3a handler);

		[CCode(cname = "ayyi_song__delete_audio_track")]
		public AyyiAction*     track_delete_async         (AyyiTrack* track, AyyiHandler4 handler, void* data);

		[CCode(cname = "ayyi_connection__next_input")]
		public AyyiConnection* next_input_connection      (AyyiConnection* c, int n_chans);

		[CCode(cname = "ayyi_connection__next_output")]
		public AyyiConnection* next_output_connection     (AyyiConnection* c, int n_chans);

		[CCode(cname = "ayyi_connection__parse_string")]
		public bool            connection__parse_string   (AyyiConnection* c, char* label);

		[CCode(cname = "AYYI_PLAYLIST_IS_MIDI")]
		public bool            playlist_is_midi           (Ayyi.Playlist* playlist);
	}

	namespace AyyiMixer {
		[CCode(cname = "ayyi_mixer__channel_at")]
		public AyyiChannel*    channel_at                 (Ayyi.Idx index);

		[CCode(cname = "ayyi_mixer__channel_next")]
		public AyyiChannel*    channel_next               (AyyiChannel* channel);

		[CCode(cname = "ayyi_mixer__plugin_next")]
		public AyyiPlugin*     plugin_next                (AyyiPlugin* plugin);

		[CCode(cname = "ayyi_channel__get_routing")]
		public AyyiList* channel__get_routing             (AyyiChannel* channel);
	}

	namespace Mixer {
		[CCode(cname = "ayyi_mixer__aux_get")]
		public AyyiAux*  get_aux                          (AyyiChannel* ch, Ayyi.Idx aux_idx);

		[CCode(cname = "ayyi_mixer__aux_count")]
		public int       aux_count                        (Ayyi.Idx channel);
	}

	GLib.List<AyyiRegion*>* ayyi_song__get_audio_part_list();

	[CCode(cname = "ayyi_region__delete_async")]
	public AyyiAction*     region__delete                 (AyyiRegion* region, AyyiHandler3a? handler);

	[CCode (cname = "LogType", cprefix = "LOG_", has_type_id = false)]
	public enum LogType {
		OK = 1,
		FAIL,
		WARN,
	}

	[CCode(cname = "ayyi_pos_add")]
	public void        ayyi_pos_add                       (SongPos* pos1, SongPos* pos2);

	[CCode(cname = "ayyi_midi_note_new")]
	public MidiNote*   midi_note_new();

	[CCode(cname = "ayyi_log_print")]
	public void        log_print     (int type, char* format, ...);

	public string yellow;
	public string grey;
	public string white;

	//---------------------------------------------------------------

	[CCode (cheader_filename = "model/ayyi_model.h", cname = "TrackNum", default_value = "-1")]
	[IntegerType (rank = 7)]
	public struct TrackNum {
	}

	const int AM_REGION_FULL;

	[CCode(cname = "struct _GPos")]
	public struct GPos
	{
		public int                beat;
		public uint16             sub;
		public uint16             tick;
	}

	[CCode(cname = "struct AudioData")]
	public struct AudioData {
		public float**    buf;
		public int        n_blocks;
		public int        n_tiers_present;
	}

	[CCode(cname = "enum PartStatus")]
	public enum PartStatus
	{
		PART_OK = 0,
		PART_PENDING,
		PART_BAD,
	}

	[CCode(cname = "struct _midi_part")]
	public struct MidiPart
	{
		//public AM.Part            gpart;
	}

	[CCode (cname = "ChannelType", cprefix = "CHAN_", has_type_id = false)]
	public enum ChannelType {
		INPUT,
		BUS,
		GROUP,
		MASTER
	}

	[CCode(cname = "struct _Channel")]
	public struct Channel {
		ChannelType    type;
		int            nchans;
		Ayyi.Idx       core_index;
		//Gtk.Object*    aux_level;
		//Gtk.TreeModel* plugin_list;
	}

	//[CCode(cname = "struct _curve")]
	//public struct Curve {
	//	public void*           path;
		//size_t          size;
		//size_t          len;
		//size_t          item_size;
		//size_t          grow_size;

		//char*           name;
		//int             track_num;
		//plug*           plugin;
		//int             auto_type;
		//void*           g_path;
		//pthread_mutex_t   mutex;
	//}

	[CCode(cprefix = "CHANGE_")]
	public enum ChangeType
	{
		POS_X,
		LEN,
		INSET,
		WIDTH,
		HEIGHT,
		OUTPUT,
		FADES,
		NAME,
		TRACK,
		ZOOM_H,
		COLOUR,
		ZOOM_V,
		ARMED,
		MUTE,
		SOLO,
		ALL,
	}

	uint64             mu2samples                           (uint64 mu);
	[CCode(cname = "ayyi_pos_is_after")]
	bool               pos_is_after                         (SongPos* pos1, SongPos* pos2);
	[CCode(cname = "ayyi_samples2bbst")]
	void               samples2bbst                         (uint64 samples, char* str);

	[CCode(cname = "struct timeval")]
	public struct TimeValue {
		[CCode(cname = "tv_sec")]
		public int seconds;
		[CCode(cname = "tv_usec")]
		public int microseconds;
	}

	[CCode(cheader_filename = "dbus-1.0/dbus/dbus-glib.h", cname = "struct _DBusGProxy")]
	public struct DBusGProxy
	{
		GObject parent;
	}
	public struct GObject
	{
		void* dummy;
	}
	public void dbus_g_proxy_add_signal     (DBusGProxy *proxy, char *signal_name, int first_type, ...);
	public void dbus_g_proxy_connect_signal (DBusGProxy *proxy, char *signal_name, void* handler, void *data, void* free_data_func);
}

