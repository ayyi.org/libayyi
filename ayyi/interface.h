/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2004-2017 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#ifndef __ayyi_interface_h__
#define __ayyi_interface_h__

#include "stdint.h"
#ifdef HAVE_FST
#include <fst.h>
#endif
#include "ayyi_types.h"

#ifdef __cplusplus
namespace Ayi {
using namespace Ayi;
#endif //__cplusplus

#define AYYI_SHM_VERSION 7
#define CONTAINER_SIZE 128
#define AYYI_CONTAINER_SIZE 128
#define AYYI_BLOCK_SIZE 128
#define AYYI_MAX_LOC 10
#define AYYI_FILENAME_MAX 256
#define AYYI_SHORT_NAME_MAX 32
#define AYYI_PLUGINS_PER_CHANNEL 3
#define AYYI_AUX_PER_CHANNEL 3

typedef uint32_t AyyiNFrames;

#ifndef __GI_SCANNER__
struct _container {
	AyyiBlock*    block[AYYI_CONTAINER_SIZE];
	int           last;
	int           full;
	char          signature[16];
	AyyiObjType   obj_type;
};
#endif

struct _AyyiBlock
{
	void*       slot[AYYI_BLOCK_SIZE];
	int         last;
	int         full;
	void*       next;
};

struct _ayyi_list
{
	struct _container* data;
	void*              next;
	int                id;
	char               name[32];
};

struct _shm_virtual
{
	char        service_name[16];
	char        service_type[16];
	int         version;
	void*       owner_shm;
	int         num_pages;

	void*       tlsf_pool;
};
typedef struct _shm_virtual shm_virtual;

struct _shm_song
{
	char        service_name[16];
	char        service_type[16];
	int         version;
	void*       owner_shm;         //clients need this so they can calculate address offsets.
	int         num_pages;
	void*       tlsf_pool;

	char        path[256];
	char        snapshot[256];
	char        peaks_dir[128];
	uint32_t    sample_rate;       //TODO move to global segment
	AyyiSongPos start;
	AyyiSongPos end;
	AyyiSongPos locators[AYYI_MAX_LOC];
	double      bpm;
	uint32_t    transport_frame;
	float       transport_speed;
	int         play_loop;         //boolean
	int         rec_enabled;       //boolean

	struct _container audio_regions;
	struct _container midi_regions;
	struct _container filesources;
	struct _container connections;
	struct _container audio_tracks;
	struct _container midi_tracks;
	struct _container playlists;
	struct _AyyiBlock ports;
	struct _container vst_plugin_info;
};

struct _region_base_shared {
	int            shm_idx; // slot_num | (block_num / AYYI_BLOCK_SIZE)
	char           name[AYYI_NAME_MAX];
	AyyiId         id;
	void*          server_object;
	int            flags;
	int            playlist;
	AyyiNFrames position;
	AyyiNFrames length;
};

struct _AyyiAudioRegion {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	AyyiId         id;
	void*          server_object;
	int            flags;
	int            playlist;
	AyyiNFrames position;
	AyyiNFrames length;
	// end base.
	AyyiNFrames start;
	uint64_t       source0;
	char           channels;
	uint32_t       fade_in_length;
	uint32_t       fade_out_length;
	float          level;
};

struct _midi_region_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	AyyiId         id;
	void*          server_object;
	int            flags;
	int            playlist;
    AyyiNFrames    position;
    AyyiNFrames    length;
	// end base.
	struct _container events;
};

struct _filesource_shared {
	int            shm_idx;
	char           name[AYYI_FILENAME_MAX]; //can be either absolute path, or relative to current song path.
	char           original_name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	uint32_t       length;
};

struct _ayyi_connection {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	char           device[32];
	uint32_t       nports;
	char           io;     // 0=output 1=input
};

struct _route_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	ChanFlags      flags;
	int            colour;
	char           input_name[128];
	int            input_idx;
	struct _ayyi_list* input_routing;
	struct _ayyi_list* output_routing;
	float          visible_peak_power[2];
};

struct _midi_track_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	uint32_t       colour;
};
typedef struct _midi_track_shared midi_track_shared;

struct _playlist_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	int            track;
};

struct _port_shared {
	char           name[AYYI_NAME_MAX];
	void*          object;
};

struct _ayyi_channel {
	AyyiIdx        shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	char           n_in;
	char           n_out;
	double         level;
	int            has_pan;
	float          pan;
	float          visible_peak_power[2];
	struct I {
		int        idx;
		char       active;
		char       bypassed;
	}              plugin          [AYYI_PLUGINS_PER_CHANNEL];
	struct _ayyi_aux* aux          [AYYI_AUX_PER_CHANNEL];
	struct _container automation[2];
	struct _ayyi_list* automation_list;
};

struct _plugin_shared {
	int            idx;
	char           name[AYYI_FILENAME_MAX];
	char           category[64];
	uint32_t       n_inputs;
	uint32_t       n_outputs;
	uint32_t       latency;
	struct _container controls;
};

struct _ayyi_control {
	char           name[32];
};

#ifdef HAVE_FST
struct _shm_vst_info {
	FSTInfo        fst_info;
};
typedef struct _shm_vst_info shm_vst_info;
#endif

struct _ShmEvent {
    double         when;
    double         value;
};
typedef struct _ShmEvent shm_event;

struct _AyyiMidiNote {
#if 0
    uint16_t       idx;
#else
    uint32_t       idx;
#endif
    uint8_t        note;
    uint8_t        velocity;
    AyyiNFrames    start;
    AyyiNFrames    length;
};

struct _ayyi_aux {
	int           idx;
	float         level;
	float         pan;
	int           flags;
	int           bus_num;
};

struct _shm_seg_mixer {
	char          service_name[16];
	char          service_type[16];
	int           version;
	void*         owner_shm;         //clients need this so they can calculate address offsets.
	int           num_pages;
	void*         tlsf_pool;

	struct _ayyi_channel master;
	struct _container tracks;
	struct _container plugins;
};

#ifdef __cplusplus
}      //namspace Ayi
#endif //__cplusplus
#endif //__ayyi_interface_h__
