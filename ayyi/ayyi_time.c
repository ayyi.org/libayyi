/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __ayyi_time_c__
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <inttypes.h>
#include <glib-object.h>
#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_client.h>
#include <services/ardourd.h>

extern AyyiClient ayyi;


GType
ayyi_song_pos_get_type ()
{
	gpointer ayyi_pos_copy (gpointer boxed)
	{
		gpointer a = g_new(AyyiSongPos, 1);
		memcpy(a, boxed, sizeof(AyyiSongPos));
		return a;
	}

	static GType type = 0;

	if (G_UNLIKELY (!type))
		type = g_boxed_type_register_static ("AyyiSongPos", ayyi_pos_copy, g_free);

	return type;
}


/**
 * ayyi_song_pos_get_value
 *
 * Return value: (transfer none)
 */
AyyiSongPos*
ayyi_song_pos_get_value (const GValue* value)
{
	return g_value_get_boxed(value);
}


/**
 * ayyi_song_pos_set_value:
 * @arg0: a valid #GValue of type %G_TYPE_PARAM
 * @arg1: (nullable): the SongPos to be set
 */
void
ayyi_song_pos_set_value (GValue* value, AyyiSongPos* pos)
{
	value->data[0].v_pointer = pos;
}


/*
 *  Converts a song position to a zero-indexed display string in bars, beats, subbeats, ticks.
 *  The string must have at least 18 bytes allocated.
 */
void
ayyi_pos2bbst (AyyiSongPos* pos, char* str)
{
	sprintf(str, AYYI_BBST_FORMAT2,
		pos->beat / 4,
		pos->beat % 4,
		pos->sub,
		pos->mu
	);
}


/*
 *  Converts a song position to a display string in bars, beats, subbeats, ticks.
 *  Values start at 1, not 0
 */
const char*
ayyi_pos2bbst_1 (AyyiSongPos* pos, char* str)
{
	sprintf(str, AYYI_BBST_FORMAT2,
						pos->beat / 4 + 1,
						pos->beat % 4 + 1,
						pos->sub + 1,
						pos->mu + 1);
	return str;
}


/*
 *  Converts a song position to a display string in bars, beats, subbeats.
 *  The string must have at least 14 bytes allocated (AYYI_BBST_MAX).
 */
const char*
ayyi_pos2bbss (AyyiSongPos* pos, char* str)
{
	// correct behaviour is defined in test_bbss in test/unit_test.c

	// TODO change 192 to 384
	// use names instead of 5

	int beat = pos->beat < 0
		? (pos->beat + 8 + 4) / 4 - 8 / 4
		: (pos->beat + 8) / 4 + 1 - 8 / 4;

	sprintf(str, AYYI_BBST_FORMAT_USER,
		beat,
		(pos->beat + (1<<4)) % 4 + 1,
		(pos->sub / 5) / 192 + 1,
		(pos->sub / 5) % 192 + 1
	);
	return str;
}


int64_t
ayyi_pos2mu (AyyiSongPos* pos)
{
	return ((int64_t)pos->mu) + ((int64_t)((int64_t)pos->sub) * ((int64_t)AYYI_MU_PER_SUB)) + ((int64_t)(((int64_t)pos->beat) * ((int64_t)AYYI_MU_PER_BEAT)));
}


void
ayyi_pos_add_mu (AyyiSongPos* pos, uint64_t mu)
{
	mu += pos->mu;
	unsigned long subs = mu / AYYI_MU_PER_SUB; // carry overflow into the subs field.
	pos->mu            = mu % AYYI_MU_PER_SUB; // use the remainder.
	dbg (1, "mu=%Li remainder=%Li", mu, mu % AYYI_MU_PER_SUB);

	subs += pos->sub;
	long beats         = subs / AYYI_SUBS_PER_BEAT;
	pos->sub           = subs % AYYI_SUBS_PER_BEAT;

	dbg (1, "overflow=%li pos->beat=%i", beats, pos->beat);
	pos->beat += beats; //FIXME too high

	dbg (1, "%i:%i:%i", pos->beat, pos->sub, pos->mu);
}


bool
ayyi_pos_is_valid (AyyiSongPos* pos)
{
	return (
		pos &&
		pos->beat < AYYI_MAX_BEATS &&
		pos->sub < AYYI_SUBS_PER_BEAT &&
		pos->mu < AYYI_MU_PER_SUB
	);
}


void
ayyi_samples2pos (uint32_t samples, AyyiSongPos* pos)
{
	int64_t mu = ayyi_samples2mu(samples);
	ayyi_mu2pos(mu, pos);
}


uint32_t
ayyi_pos2samples (AyyiSongPos* pos)
{
	float bpm = ((AyyiSongService*)ayyi.service)->song->bpm;

	float length_of_1_beat_in_secs = 60.0/bpm;
	float b = ((float)AYYI_SUBS_PER_BEAT) * ((float)AYYI_MU_PER_SUB);
	float sub = pos->sub;
	float samples = length_of_1_beat_in_secs * ((float)((AyyiSongService*)ayyi.service)->song->sample_rate) *
		(
			((float)pos->beat) +
			sub / ((float)AYYI_SUBS_PER_BEAT) +
			((float)pos->mu) / b
		);

	return (uint32_t)(samples + 0.5);
}


long long
ayyi_samples2mu (unsigned long long samples)
{
	//returns the length in mu, of the given number of audio samples.

	//normally, the ratio is of the order of 1924

	int sample_rate = ((AyyiSongService*)ayyi.service)->song->sample_rate;
	if(!sample_rate){ perr ("samplerate zero!"); sample_rate=44100; }

	float secs = (float)samples / (float)sample_rate;

	float bpm = ((AyyiSongService*)ayyi.service)->song->bpm;
	float length_of_1_beat_in_secs = 60.0 / bpm;

	float beats = secs / length_of_1_beat_in_secs;

	return ayyi_beats2mu(beats);
}


long long
ayyi_mu2samples (int64_t mu)
{
	// note that double precision is essential here

	double mu_f = (double)mu;
	double beats = mu_f / (AYYI_SUBS_PER_BEAT * AYYI_MU_PER_SUB);

	dbg(3, "mu=%Li (%.4f) beats=%.4f", mu, mu_f, beats);

	return ayyi_beats2samples_float(beats);
}


void
ayyi_mu2pos (int64_t mu, AyyiSongPos* pos)
{
	g_return_if_fail(pos);

	int32_t beats = mu / (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT);

	int32_t remainder = mu % (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT);
	int32_t subs = remainder / AYYI_MU_PER_SUB;
	remainder = remainder % AYYI_MU_PER_SUB;

	*pos = (AyyiSongPos){beats, subs, remainder};
}


double
ayyi_pos2beats (AyyiSongPos* pos)
{
	return ((double)pos->beat) + (double)pos->sub / ((double)AYYI_SUBS_PER_BEAT) + (double)pos->mu / ((double)AYYI_SUBS_PER_BEAT * AYYI_MU_PER_SUB);
}


int64_t
ayyi_beats2mu (double beats)
{
	//note beats is float.

	return beats * (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT); //this is the definition of a mu.
}


int64_t
ayyi_beats2samples (int beats)
{
	//returns the number of audio samples equivalent to the given number of beats.

	float bpm = ((AyyiSongService*)ayyi.service)->song->bpm;

	float length_of_1_beat_in_secs = 60.0 / bpm;
	float samples = length_of_1_beat_in_secs * ((AyyiSongService*)ayyi.service)->song->sample_rate * beats;

	return (int64_t)samples;
}


long long
ayyi_beats2samples_float (float beats)
{
	//returns the number of audio samples equivalent to the given number of beats.

	float bpm = ((AyyiSongService*)ayyi.service)->song->bpm;

	float length_of_1_beat_in_secs = 60.0 / bpm;
	float samples = length_of_1_beat_in_secs * ((AyyiSongService*)ayyi.service)->song->sample_rate * beats;

	dbg (3, "%.4fbeats = %.4fsamples. length_of_1_beat_in_secs=%.4f", beats, samples, length_of_1_beat_in_secs);

	return (int)(samples + 0.5);
}


bool
ayyi_pos_cmp (const AyyiSongPos* a, const AyyiSongPos* b)
{
	//return TRUE if the two positions are different.
	return (a->beat != b->beat) || (a->sub != b->sub) || (a->mu != b->mu);
}


void
ayyi_pos_add (AyyiSongPos* pos, const AyyiSongPos* addition)
{
	ayyi_mu2pos(ayyi_pos2mu(pos) + ayyi_pos2mu((AyyiSongPos*)addition), pos);
}


void
ayyi_pos_sub (AyyiSongPos* pos, const AyyiSongPos* addition)
{
	g_return_if_fail(ayyi_pos_is_valid((AyyiSongPos*)addition));

	ayyi_mu2pos(ayyi_pos2mu(pos) - ayyi_pos2mu((AyyiSongPos*)addition), pos);
}


void
ayyi_pos_divide (AyyiSongPos* pos, int i)
{
	ayyi_mu2pos(ayyi_pos2mu(pos) / i, pos);
}


void
ayyi_pos_min (AyyiSongPos* a, const AyyiSongPos* b)
{
	// used to find the earliest in a list of parts.
	// if b is earlier than a, a is set to b, else a is unchanged.

	if (b->beat > a->beat) return;
	if (b->beat < a->beat){ *a = *b; return; }
	if (b->sub  > a->sub ) return;
	if (b->sub  < a->sub ){ *a = *b; return; }
	if (b->mu   > a->mu  ) return;
	*a = *b;
}


void
ayyi_pos_max (AyyiSongPos* a, const AyyiSongPos* b)
{
	// the value returned in a is the later of a and b.

	if (b->beat > a->beat){ *a = *b; return; }
	if (b->beat < a->beat) return;
	if (b->sub  > a->sub ){ *a = *b; return; }
	if (b->sub  < a->sub ) return;
	if (b->mu   > a->mu  ){ *a = *b; return; }
}


/*
 *  Return true if pos1 is after pos2.
 */
bool
ayyi_pos_is_after (const AyyiSongPos* pos1, const AyyiSongPos* pos2)
{
	if(pos2->beat < pos1->beat) return true;
	if(pos2->beat > pos1->beat) return false;
	if(pos2->sub  < pos1->sub ) return true;
	if(pos2->sub  > pos1->sub ) return false;
	if(pos2->mu   < pos1->mu  ) return true;

	return false;
}


void
ayyi_samples2bbst (uint64_t samples, char* str)
{
	g_return_if_fail(samples < ayyi_beats2samples(AYYI_MAX_BEATS));

	AyyiShmSong* song = ((AyyiSongService*)ayyi.service)->song;

	float beats_per_second = song->bpm / 60.0;

	float secs = (float)samples / (float)((AyyiSongService*)ayyi.service)->song->sample_rate;
	int ticks = secs * beats_per_second * ayyi_ticks_per_beat;

	float beats2bar = 4.0;
	int bar = ticks / (ayyi_ticks_per_beat * (int)beats2bar);
	int ticks_remaining = ticks % (ayyi_ticks_per_beat * (int)beats2bar);

	int beat = 0;
	if     (ticks_remaining < 1 * ayyi_ticks_per_beat) beat = 0;
	else if(ticks_remaining < 2 * ayyi_ticks_per_beat) beat = 1;
	else if(ticks_remaining < 3 * ayyi_ticks_per_beat) beat = 2;
	else if(ticks_remaining < 4 * ayyi_ticks_per_beat) beat = 3;
	ticks_remaining = ticks_remaining - beat * ayyi_ticks_per_beat;

	int subbeat = ticks_remaining / AYYI_TICKS_PER_SUBBEAT;
	ticks_remaining = ticks_remaining % AYYI_TICKS_PER_SUBBEAT;

	sprintf(str, AYYI_BBST_FORMAT_SHORT, bar + 1, beat + 1, subbeat + 1, ticks_remaining);
}


/*
 *  Construct a zero-based time string for length type use cases.
 */
void
ayyi_samples2bbst_0 (uint64_t samples, char* bbst)
{
	AyyiSongPos pos;

	ayyi_samples2pos(samples, &pos);
	ayyi_pos2bbst(&pos, bbst);
}


void
ayyi_mu2bbst (uint64_t len, char *str)
{
	uint64_t beats = len / ayyi_mu_per_beat;

	uint32_t beats2bar = 4;
	dbg(2, "  beats=%Lu beats2bar=%u mu_per_beat=%Lu mu_per_sub=%Lu", beats, beats2bar, ayyi_mu_per_beat, ayyi_mu_per_sub);

	uint32_t bar  = beats / beats2bar;
	uint32_t beat = beats - beats2bar * bar;

	uint32_t rem_mu = len - beats * ayyi_mu_per_beat;
	//FIXME?
	uint32_t subbeat = rem_mu / (ayyi_mu_per_sub);
	//FIXME?
	rem_mu = rem_mu - subbeat * ayyi_mu_per_sub;
	uint32_t tick = rem_mu / AYYI_MU_PER_TICK;

	sprintf(str, AYYI_BBST_FORMAT_SHORT, bar, beat+1, subbeat+1, tick);
}


