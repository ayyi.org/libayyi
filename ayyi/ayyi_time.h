/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
  Ayyi uses 2 time formats; one sample based and one musical-time based.

  The musical formats are high precision in order to allow conversion to
  and from other formats.

*/

#ifndef __ayyi_time_h__
#define __ayyi_time_h__

#ifdef __cplusplus
extern "C" {
#endif
#include "stdbool.h"
#include <glib-object.h>
#include "ayyi/ayyi_types.h"

// internal time format
#define AYYI_MU_PER_SUB 11025LL
#define AYYI_SUBS_PER_BEAT 3840
#define AYYI_MAX_BEATS 6400      // 120bpm => 7200 beats/hr => 172800 beats/day.

// display format
#define AYYI_TICKS_PER_SUBBEAT 384
#define AYYI_TICKS_PER_BEAT (4 * AYYI_TICKS_PER_SUBBEAT)
#define AYYI_BBST_FORMAT_USER "%03i:%01i:%01i:%03i"
#define AYYI_BBST_FORMAT_SHORT "%03i:%02i:%02i:%03i"    // limited resolution for user interaction
#define AYYI_BBST_FORMAT2 "%03i:%02i:%04i:%04i"         // TODO AYYI_MU_PER_SUB requires 5 digits, not 4 (total 17 bytes)
#define AYYI_BBST_MAX 16

#define AYYI_MU_PER_TICK (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT / AYYI_TICKS_PER_BEAT)
#define AYYI_MU_PER_BEAT (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT)

#ifdef __ayyi_time_c__
const uint64_t ayyi_mu_per_beat = (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT);
const uint64_t ayyi_mu_per_sub  = (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT) / 4;
const uint32_t ayyi_ticks_per_beat = 4 * AYYI_TICKS_PER_SUBBEAT;
#endif

GType          ayyi_song_pos_get_type  ();
AyyiSongPos*   ayyi_song_pos_get_value (const GValue*);
void           ayyi_song_pos_set_value (GValue*, AyyiSongPos*);

AyyiMu         ayyi_pos2mu            (AyyiSongPos*);
void           ayyi_pos_add_mu        (AyyiSongPos*, uint64_t mu);
bool           ayyi_pos_is_valid      (AyyiSongPos*);
void           ayyi_samples2pos       (uint32_t samples, AyyiSongPos*);
uint32_t       ayyi_pos2samples       (AyyiSongPos*);
long long      ayyi_samples2mu        (unsigned long long samples);
long long      ayyi_mu2samples        (AyyiMu);
void           ayyi_mu2pos            (AyyiMu, AyyiSongPos*);
double         ayyi_pos2beats         (AyyiSongPos*);
AyyiMu         ayyi_beats2mu          (double beats);
int64_t        ayyi_beats2samples     (int beats);
long long      ayyi_beats2samples_float (float beats);
bool           ayyi_pos_cmp           (const AyyiSongPos*, const AyyiSongPos*);
void           ayyi_pos_add           (AyyiSongPos*, const AyyiSongPos*);
void           ayyi_pos_sub           (AyyiSongPos*, const AyyiSongPos*);
void           ayyi_pos_divide        (AyyiSongPos*, int);
void           ayyi_pos_min           (AyyiSongPos*, const AyyiSongPos*);
void           ayyi_pos_max           (AyyiSongPos*, const AyyiSongPos*);
bool           ayyi_pos_is_after      (const AyyiSongPos*, const AyyiSongPos*);

void           ayyi_pos2bbst          (AyyiSongPos*, char* bbst);
const char*    ayyi_pos2bbst_1        (AyyiSongPos*, char* bbst);
const char*    ayyi_pos2bbss          (AyyiSongPos*, char* bbs);
void           ayyi_samples2bbst      (uint64_t samples, char*);
void           ayyi_samples2bbst_0    (uint64_t samples, char*);
void           ayyi_mu2bbst           (uint64_t len, char*);

#ifdef __cplusplus
}
#endif
#endif
