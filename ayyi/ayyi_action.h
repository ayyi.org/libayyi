/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_action_h__
#define __ayyi_action_h__

#include <sys/time.h>
#include <glib-object.h>

struct _ayyi_action
{
	unsigned           id;
	struct timeval     time_stamp;
	char               label[64]; // a description of the transaction. For printing only.

	struct {
		AyyiId         id;
		AyyiObjType    type;
	}                  obj;
	AyyiObjIdx         obj_idx;
	AyyiOp             op;
	AyyiPropType       prop;
	int                i_val;
	double             d_val;
	union {
		int            i;
		double         d;
	}                  val2;

	AyyiActionCallback callback;  // the func to call upon reception of succesful completion of the transaction
	void*              app_data;  // data to forward with the callback

	// return values
	struct _r {
		int            gid;       // the object id number returned by the engine following the request.
		AyyiIdx        idx;       // the object idx returned by the engine.
		GError*        error;
		void*          ptr_1;
	} ret;
};

GType        ayyi_action_get_type     ();
AyyiAction*  ayyi_action_new          (char* format, ...);
void         ayyi_action_free         (AyyiAction*);
void         ayyi_action_execute      (AyyiAction*);
void         ayyi_action_complete     (AyyiAction*);
AyyiAction*  ayyi_action_lookup_by_id (unsigned id);

void         ayyi_print_action_status ();

void         ayyi_set_length          (AyyiAction*, uint64_t len);
void         ayyi_set_float           (AyyiAction*, AyyiObjType, AyyiIdx, int prop, double val);
void         ayyi_set_obj             (AyyiAction*, AyyiObjType, AyyiIdx, int prop, uint64_t val);
void         ayyi_set_string          (AyyiAction*, AyyiObjType, AyyiIdx, const char* string);

HandlerData* ayyi_handler_data_new    (AyyiHandler, gpointer);
void         ayyi_handler_simple      (AyyiAction*);

#endif
