/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_utils_h__
#define __ayyi_utils_h__

#include "glib.h"
#include "ayyi/ayyi_typedefs.h"
#include "ayyi/ayyi_client.h"
#include "debug/debug.h"

#define P_GERR if(error){ perr("%s\n", error->message); g_error_free(error); error = NULL; }
#define GERR_INFO if(error){ printf("%s\n", error->message); g_error_free(error); error = NULL; }
#define GERR_WARN if(error){ pwarn("%s", error->message); g_error_free(error); error = NULL; }
#define UNDERLINE printf("-----------------------------------------------------\n")
#define call(FN, A, ...) (FN ? (FN)(A, ##__VA_ARGS__) : NULL)
#define call_i(FN, A, ...) (FN ? (FN)(A, ##__VA_ARGS__) : 0)

#ifndef G_SOURCE_CONTINUE // glib > 2.32
#define G_SOURCE_CONTINUE TRUE
#define G_SOURCE_REMOVE FALSE
#endif
#define AYYI_NEW(T, ...) ({T* obj = g_new0(T, 1); *obj = (T){__VA_ARGS__}; obj;})

void        warn_gerror              (const char* msg, GError**);
void        info_gerror              (const char* msg, GError**);

gchar*      path_from_utf8           (const gchar* utf8);

GList*      get_dirlist              (const char*);

void        string_increment_suffix  (char* newstr, const char* orig, int new_max);

int         get_terminal_width       ();

bool        audio_path_get_leaf      (const char* path, char* leaf);
gchar*      audio_path_get_base      (const char*);
bool        audio_path_get_wav_leaf  (char* leaf, const char* path, int len);
char*       audio_path_truncate      (char*, char);

#ifdef __ayyi_utils_c__
char white    [16] = "\x1b[0;39m"; // 0 = normal
char bold     [16] = "\x1b[1;39m"; // 1 = bright
char grey     [16] = "\x1b[2;39m"; // 2 = dim
char yellow   [16] = "\x1b[1;33m";
char yellow_r [16] = "\x1b[30;43m";
char white__r [16] = "\x1b[30;47m";
char cyan___r [16] = "\x1b[30;46m";
char magent_r [16] = "\x1b[30;45m";
char ayyi_blue     [16] = "\x1b[1;34m";
char ayyi_blue_r   [16] = "\x1b[30;44m";
char ayyi_red      [16] = "\x1b[1;31m";
char ayyi_red_r    [16] = "\x1b[30;41m";
char ayyi_green    [16] = "\x1b[1;32m";
char ayyi_green_r  [16] = "\x1b[30;42m";
char go_rhs   [32] = "\x1b[A\x1b[50C"; // go up one line, then goto column 60
char ok       [32] = " [ \x1b[1;32mok\x1b[0;39m ]";
char fail     [32] = " [\x1b[1;31mFAIL\x1b[0;39m]";
#else
extern char white    [16];
extern char bold     [16];
extern char grey     [16];
extern char yellow   [16];
extern char yellow_r [16];
extern char white__r [16];
extern char cyan___r [16];
extern char magent_r [16];
extern char ayyi_blue     [16];
extern char ayyi_blue_r   [16];
extern char ayyi_red      [16];
extern char ayyi_red_r    [16];
extern char ayyi_green    [16];
extern char ayyi_green_r  [16];
extern char go_rhs   [32];
extern char ok       [];
extern char fail     [];
#endif

#endif
