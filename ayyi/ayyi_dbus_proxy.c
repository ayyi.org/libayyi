/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __dbus_proxy_c__
#include "config.h"
#include <glib.h>
#include <dbus/dbus-glib-bindings.h>
#include <ayyi/dbus_marshallers.h>
#include <ayyi/dbus_marshallers.c>
#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_dbus.h>
#include <ayyi/ayyi_log.h>

extern int debug;

DBusGProxy* proxy = NULL;

static void  ayyi_create_notify   (DBusGProxy*, DBusGProxyCall*, gpointer data);
static void  ayyi_delete_notify   (DBusGProxy*, DBusGProxyCall*, gpointer data);
static void  dbus_set_prop_notify (DBusGProxy*, DBusGProxyCall*, gpointer data);
static void  dbus_get_prop_notify (DBusGProxy*, DBusGProxyCall*, gpointer data);
static void  report_dbus_error    (const char* func, GError*);


GQuark
ayyi_msg_error_quark ()
{
	static GQuark quark = 0;
	if (!quark) quark = g_quark_from_static_string ("ayyi_comms_error");
	return quark;
}


void
ayyi_dbus_object_new (AyyiAction* action, const char* name, gboolean from_source, AyyiIdx src_idx, guint32 parent_idx, AyyiSongPos* stime, guint64 len, guint32 inset)
{
	unsigned char type = action->obj.type;
	SIG_OUT;
	guint64 stime64 = 0; //TODO send the struct directly.
	if (stime) memcpy(&stime64, stime, 8);

	gpointer user_data = action;
	dbus_g_proxy_begin_call(proxy,
		"CreateObject",
		ayyi_create_notify,
		user_data, NULL,
		G_TYPE_INT, type,
		G_TYPE_STRING, name,
		G_TYPE_BOOLEAN, from_source,
		G_TYPE_UINT, src_idx,
		G_TYPE_UINT, parent_idx,
		G_TYPE_UINT64, stime64,
		G_TYPE_UINT64, len,
		G_TYPE_UINT, inset,
		G_TYPE_INVALID
	);
}


void
ayyi_dbus_part_new (AyyiAction* action, const char* name, uint64_t id, gboolean from_source, AyyiIdx src_idx, guint32 parent_idx, AyyiSongPos* stime, guint64 len, guint32 inset)
{
	unsigned char type = action->obj.type;
	SIG_OUT;
	guint64 stime64 = 0; //TODO send the struct directly.
	if (stime) memcpy(&stime64, stime, 8);

	dbus_g_proxy_begin_call(proxy, "CreatePart", ayyi_create_notify, action, NULL,
		G_TYPE_INT, type, G_TYPE_STRING, name, G_TYPE_UINT64, id, G_TYPE_BOOLEAN, from_source, G_TYPE_UINT, src_idx,
		G_TYPE_UINT, parent_idx, G_TYPE_UINT64, stime64, G_TYPE_UINT64, len, G_TYPE_UINT, inset, G_TYPE_INVALID
	);
}


static GError*
ayyi_communication_error ()
{
	return g_error_new(ayyi_msg_error_quark(), 3539, "communication error");
}


static void
ayyi_dbus_process_error (const char* func, GError** _error)
{
	GError* error = *_error;

	if(error->domain != DBUS_GERROR){
		pwarn("unexpected error domain !!! %s", g_quark_to_string(error->domain));
	}
	if(error->code == DBUS_GERROR_REMOTE_EXCEPTION){
		// this is a remote application error rather than a dbus error.
		AYYI_DEBUG pwarn("dbus remote exception error: name=%s", dbus_g_error_get_name(error));
	}
	if(error->code < DBUS_GERROR_REMOTE_EXCEPTION){
		report_dbus_error(func, error);
		g_error_free(error);

		// set a generic communication error. callers should not normally report this.
		*_error = ayyi_communication_error();
	}
}


static void
ayyi_create_notify (DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	P_IN;
	PF2;
	AyyiAction* action = (AyyiAction*)data;
#if DEBUG
	if(!action) pwarn ("no action!");
#endif

	guint idx;
	GError* error = NULL;
	if (!dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_UINT, &idx, G_TYPE_INVALID)){
		if(error){
			ayyi_dbus_process_error(__func__, &error);

			if(action) action->ret.error = error;
		}
	}else{
		if(action) action->ret.idx = idx;
	}

	if(action){
		dbg (2, "request done. idx=%u", idx);
		ayyi_action_complete(action);
	}
}


void
ayyi_dbus_object_del (AyyiAction *action, int object_type, uint32_t object_idx)
{
	gpointer user_data = action;
	dbus_g_proxy_begin_call(proxy, "DeleteObject", ayyi_delete_notify, user_data, NULL, 
		G_TYPE_INT, object_type,
		G_TYPE_UINT, object_idx,
		G_TYPE_INVALID
	);
}


static void
ayyi_delete_notify (DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	P_IN;
	PF2;
	AyyiAction *action = (AyyiAction*)data;
	if(!action) pwarn ("no action!");

	action->ret.idx = action->obj_idx.idx1;

	GError *error = NULL;
	if (!dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID)){
		if(error){
			ayyi_dbus_process_error(__func__, &error);

			if(action) action->ret.error = error;
		}
	}

	ayyi_action_complete(action);
}


DBusGProxyCall*
dbus_get_prop_string (AyyiAction *action, int object_type, int property_type)
{
	SIG_OUT;
	gpointer user_data = action;
	return dbus_g_proxy_begin_call(proxy, "GetPropString", dbus_get_prop_notify, user_data, NULL, G_TYPE_INT, object_type, G_TYPE_INT, property_type, G_TYPE_INVALID); 
}


static void
dbus_get_prop_notify (DBusGProxy *proxy, DBusGProxyCall *call, gpointer _action)
{
	P_IN;
	PF;
	AyyiAction* action = (AyyiAction*)_action;

	char* val = NULL;
	GError* error = NULL;
	if (!dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_STRING, &val, G_TYPE_INVALID)){

		// where possible, the AyyiAction handles the error
		if(action){
			action->ret.error = error;
		}else{
			report_dbus_error(__func__, error);
			g_error_free(error);
			error = NULL;
		}
	}

	if(action){
		action->ret.ptr_1 = val;
		ayyi_action_complete(action);
	}
}


DBusGProxyCall*
dbus_set_prop_int (AyyiAction* action, int object_type, int property_type, AyyiIdx object_idx, int val)
{
	PF2;
	return dbus_g_proxy_begin_call(proxy, "SetPropInt", dbus_set_prop_notify, action, NULL, G_TYPE_INT, object_type, G_TYPE_INT, property_type, G_TYPE_UINT, object_idx, G_TYPE_INT, val, G_TYPE_INVALID); 
}


DBusGProxyCall*
dbus_set_prop_int64 (AyyiAction* action, int object_type, int property_type, AyyiIdx object_idx, int64_t val)
{
	SIG_OUT;
	return dbus_g_proxy_begin_call(proxy, "SetPropInt64", dbus_set_prop_notify, action, NULL, G_TYPE_INT, object_type, G_TYPE_INT, property_type, G_TYPE_UINT, object_idx, G_TYPE_INT64, val, G_TYPE_INVALID); 
}


DBusGProxyCall*
dbus_set_prop_float (AyyiAction* action, int object_type, int property_type, AyyiIdx object_idx, double val)
{
	PF2;
	return dbus_g_proxy_begin_call(proxy, "SetPropFloat", dbus_set_prop_notify, action, NULL, G_TYPE_INT, object_type, G_TYPE_INT, property_type, G_TYPE_UINT, object_idx, G_TYPE_DOUBLE, val, G_TYPE_INVALID); 
}


DBusGProxyCall*
dbus_set_prop_bool (AyyiAction* action, int object_type, int property_type, AyyiIdx object_idx, gboolean val)
{
	PF2;
	gpointer user_data = action;
	return dbus_g_proxy_begin_call(proxy, "SetPropBool", dbus_set_prop_notify, user_data, NULL, G_TYPE_INT, object_type, G_TYPE_INT, property_type, G_TYPE_UINT, object_idx, G_TYPE_BOOLEAN, val, G_TYPE_INVALID); 
}


DBusGProxyCall*
dbus_set_prop_string (AyyiAction* action, int object_type, int property_type, uint32_t object_idx, const char* val)
{
	PF2;
	action->ret.idx = object_idx;
	gpointer user_data = action;
	return dbus_g_proxy_begin_call(proxy, "SetPropString", dbus_set_prop_notify, user_data, NULL, G_TYPE_INT, object_type, G_TYPE_INT, property_type, G_TYPE_UINT, object_idx, G_TYPE_STRING, val, G_TYPE_INVALID); 
}


DBusGProxyCall*
dbus_set_prop_f_pair (AyyiAction* action, int object_type, int property_type, GArray* object_idx, double val1, double val2)
{
	SIG_OUT;
	return dbus_g_proxy_begin_call(proxy, "SetPropFloatPair", dbus_set_prop_notify, action, NULL, G_TYPE_INT, object_type, G_TYPE_INT, property_type, DBUS_TYPE_G_UINT_ARRAY, object_idx, G_TYPE_DOUBLE, val1, G_TYPE_DOUBLE, val2, G_TYPE_INVALID); 
}


//#define DBUS_GVAL_ARRAY (dbus_g_type_get_struct ("GValueArray", DBUS_STRUCT_BYTE_BYTE_UINT_UINT, G_TYPE_INVALID))

DBusGProxyCall*
dbus_set_notelist (AyyiAction* action, uint32_t part_idx, const GPtrArray* notes)
{
	//the notes are sent as a ptr_array of type GValue.

	if(!notes) return NULL;

	SIG_OUT;
	return dbus_g_proxy_begin_call(proxy, "SetNotelist", dbus_set_prop_notify, action, NULL, G_TYPE_UINT, part_idx, dbus_g_type_get_collection ("GPtrArray", DBUS_STRUCT_UINT_BYTE_BYTE_UINT_UINT), notes, G_TYPE_INVALID);
}


static void
dbus_set_prop_notify (DBusGProxy* proxy, DBusGProxyCall* call, gpointer data)
{
	P_IN;

	AyyiAction* action = (AyyiAction*)data;

	if(action) dbg (1, "%s%s%s", bold, action->label, white);
	else       dbg (2, "no action!"); //some msgs, such as mixer events, dont need actions.

	gboolean ok;
	GError *error = NULL;
	if (!dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_BOOLEAN, &ok, G_TYPE_INVALID)){
		if(action){
			if(error){
				ayyi_dbus_process_error(__func__, &error);

				action->ret.error = error;
			}
			ayyi_action_complete(action);
		}
		return;
	}

	if(action) ayyi_action_complete(action);
}


void
dbus_undo (AyyiService* service, int n)
{
	SIG_OUT;
	gboolean ok;
	GError *error = NULL;
	if(!dbus_g_proxy_call(service->priv->proxy, "Undo", &error, G_TYPE_UINT, n, G_TYPE_INVALID, G_TYPE_BOOLEAN, &ok, G_TYPE_INVALID)){
		pwarn("%s", error->message);
		g_error_free(error);
	}
}


static void
report_dbus_error (const char* func, GError* error)
{
	if(error->code == DBUS_GERROR_NO_REPLY){
		g_warning("%s: server timeout", func);
		ayyi_log_print(LOG_FAIL, "server timeout");
	}else if(error->code == DBUS_GERROR_SERVICE_UNKNOWN){
		ayyi_log_print(LOG_FAIL, "service not running");
	}else{
		g_warning ("%s: %i: %s", func, error->code, error->message);
		ayyi_log_print(LOG_FAIL, "%s", error->message);
	}
}


/*
 *  Caller must free the result
 */
GArray*
ayyi_index_array_new (guint a, guint b, guint c)
{
	GArray* idx = g_array_sized_new(/*ZERO_TERMINATED*/false, true, sizeof(guint), 3);
	g_array_set_size(idx, 3);

	guint* i;
	i = &g_array_index(idx, guint, 0); *i = a;
	i = &g_array_index(idx, guint, 1); *i = b;
	i = &g_array_index(idx, guint, 2); *i = c;

	return idx;
}

