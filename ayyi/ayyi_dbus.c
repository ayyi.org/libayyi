/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __ayyi_dbus_c__
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <dbus/dbus-glib-bindings.h>
#include <ayyi/dbus_marshallers.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_server.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include <ayyi/ayyi_dbus.h>

#define AYYI_DBUS_ERROR ayyi_dbus_error_quark()

extern DBusGProxy* proxy;

static void   ayyi_dbus_shm_notify    (DBusGProxy*, DBusGProxyCall*, gpointer);
static GQuark ayyi_dbus_error_quark   ();
static void   ayyi_dbus__rx_deleted   (DBusGProxy*, int object_type, int object_idx, gpointer);
static void   ayyi_dbus__rx_property  (DBusGProxy*, int object_type, int object_idx, int property, gpointer);
static void   ayyi_handle             (AyyiIdent, AyyiPropType, const GList* _handlers);
static void   ayyi_handle_value       (AyyiIdent, float val, const GList* _handlers);
#ifdef DEBUG
static char*  ayyi_dbus_introspect    (AyyiService*, DBusGConnection*);
#endif


static gboolean
dbus_service_test (AyyiService* service, GError** error)
{
	//fn is used speculatively - failing is not neccesarily an error.

	if(!service->priv->proxy) return FALSE;

	int id;
	if(!dbus_g_proxy_call(service->priv->proxy, "GetShmSingle", error, G_TYPE_STRING, seg_strs[SEG_TYPE_SONG], G_TYPE_INVALID, G_TYPE_UINT, &id, G_TYPE_INVALID)){
		if(error){
			dbg (0, "GetShm: %s\n", (*error)->message);
			g_error_free(*error);
		}
		return FALSE;
	}
	return TRUE;
}


bool
ayyi_client__dbus_connect (AyyiService* service, AyyiHandler2 callback, gpointer user_data)
{
	// first we try DBUS_BUS_SESSION, then DBUS_BUS_SYSTEM.
	// app can work with either bus under the right conditions.

	dbg(1, "service=%s", service->service);

	struct {
		gchar* address;
		gchar* pid;
	} personal_bus = {NULL, NULL};

	gchar* contents;
	gsize len;
	GError* _error = NULL;
	if(g_file_get_contents("/tmp/ayyi/dbus", &contents, &len, &_error)){
		dbg(2, "found dbus info:\n%s", contents);
		char* s0 = strstr(contents, "='");
		if(s0){
			s0 += 2;
			char* s1 = strstr(s0, "'\n");
			if(s1){
				personal_bus.address = g_strndup(s0, s1 - s0);
				dbg(0, "%s", personal_bus.address);
				char* s2 = strstr(s1, "=");
				if(s2){
					s2 += 1;
					char* s3 = strstr(s2, "\n");
					if(s3){
						personal_bus.pid = g_strndup(s2, s3 - s2);
						dbg(0, "%s", personal_bus.pid);

						g_setenv("DBUS_SESSION_BUS_ADDRESS", personal_bus.address, true);
						g_setenv("DBUS_SESSION_BUS_PID", personal_bus.pid, true);
					}
				}
			}
		}
		g_free(contents);
	}
	if(_error) g_error_free(_error);
	if(personal_bus.address) g_free(personal_bus.address);
	if(personal_bus.pid) g_free(personal_bus.pid);

	#define BUS_COUNT 2
	struct _busses {
		int bus;
		char name[64];
	} busses[BUS_COUNT] = {
		{DBUS_BUS_SESSION, "session"},
		{DBUS_BUS_SYSTEM, "system"}
	};
	static DBusGConnection* connection[BUS_COUNT] = {NULL,}; // never unreffed - is shared amongst all processes

	DBusGConnection* get_connection (int i, GError** error)
	{
		if(!connection[i]){
			connection[i] = dbus_g_bus_get(busses[i].bus, error);
		}
		return connection[i];
	}

	bool acquired = false;

	int i = 0;
	if(!connection[0]){
		for(i=0;i<BUS_COUNT;i++){
			GError* error = NULL;
			dbg (2, "trying bus: %s...", busses[i].name);
			if((connection[i] = get_connection(i, &error))){
				if((service->priv->proxy = dbus_g_proxy_new_for_name (connection[i], service->service, service->path, service->interface))){
					//we always get a proxy, even if the requested service isnt running.
					dbg (2, "got proxy ok");
					if(dbus_service_test(service, NULL)){
						acquired = TRUE;
						dbg (1, "using '%s' bus.", busses[i].name);
						break;
					}
					else pwarn("%s bus: service test failed.", busses[i].name);
				}
				else dbg (0, "service not running on this bus? (%s)", busses[i].name); //TODO busname may be wrong if connection made previously.
			}
			else{
				switch(error->code){
					case DBUS_GERROR_FILE_NOT_FOUND:
						printf("%s dbus not running?\n", busses[i].name);
						break;
					default:
						printf("failed to open connection to %s%s%s bus", bold, busses[i].name, white);
						if(error->code != 0) printf(": %i %s\n", error->code, error->message);
						printf("\n");
						break;
				}
				g_error_free (error);
				continue;
			}

		}
	} else { //this just means that the fn is being run for the second time. FIXME we should be able to move this back into the above block
		g_return_val_if_fail(service->priv, false);
		if((service->priv->proxy = dbus_g_proxy_new_for_name (connection[i], service->service, service->path, service->interface))){
			//we always get a proxy, even if the requested service isnt running.
			if(dbus_service_test(service, NULL)){
				acquired = TRUE;
			}
		}
		else dbg (0, "service not running on this bus? (%s)", busses[i].name); //TODO busname may be wrong if connection made previously.
	}

	GError* error = NULL;
	if(!connection[0] && !connection[1]){
		if(!error) g_set_error (&error, AYYI_DBUS_ERROR, 93642, "no connections made to any dbus.");

	}else if(!acquired){
		g_set_error (&error, AYYI_DBUS_ERROR, 98742, "service not found: %s. have bus connection? session_bus: %s, system_bus: %s", service->service, connection[0]?"y":"n", connection[1]?"y":"n");

		const char* service_file = "/usr/share/dbus-1/services/ardourd.service";
		if(!g_file_test(service_file, G_FILE_TEST_EXISTS)){
			pwarn("service file not found: %s", service_file);
		}
	}else{
		if(!proxy) proxy = service->priv->proxy; //temp

		dbus_g_object_register_marshaller (_ad_dbus_marshal_VOID__STRING_INT, G_TYPE_NONE, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INVALID);
		dbus_g_object_register_marshaller (_ad_dbus_marshal_VOID__INT_INT, G_TYPE_NONE, G_TYPE_INT, G_TYPE_INT, G_TYPE_INVALID);
		dbus_g_object_register_marshaller (_ad_dbus_marshal_VOID__INT_INT_INT, G_TYPE_NONE, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INVALID);

		if(0) dbg(0, "%s", ayyi_dbus_introspect(service, connection[0]));
	}

	call(callback, error, user_data);

	g_clear_pointer(&error, g_error_free);

	return true;
}


gboolean
ayyi_client__dbus_get_shm (AyyiService* service, void (*on_shm)(AyyiShmCallbackData*), gpointer user_data)
{
	PF2;

	g_return_val_if_fail(service->priv->proxy, FALSE);

	service->priv->on_shm = on_shm;

	if(!service->segs){ pwarn("no shm segs have been configured."); return FALSE; }

	GList* l = service->segs;
	for(;l;l=l->next){
#define DBUS_SYNC
#ifdef DBUS_SYNC
		AyyiCShmSeg* seg = l->data;

		GError* error = NULL;
		if(!dbus_g_proxy_call(service->priv->proxy, "GetShmSingle", &error, G_TYPE_STRING, seg_strs[seg->type], G_TYPE_INVALID, G_TYPE_UINT, &seg->id, G_TYPE_INVALID)){

			// just to demonstrate remote exceptions versus regular GError
			if(error->domain == DBUS_GERROR && error->code == DBUS_GERROR_REMOTE_EXCEPTION){
				perr ("caught remote method exception %s: %s", dbus_g_error_get_name(error), error->message);
			}else{
				perr ("GetShm: %s", error->message);
			}
			g_error_free(error);

			return FALSE;
		}
		if(!seg->id){
			pwarn("failed to get shm id");
			return FALSE;
		}

		AyyiShmCallbackData* c = g_new0(AyyiShmCallbackData, 1);
		c->shm_seg = seg;
		c->user_data = user_data;
		c->service = service;
		ayyi_dbus_shm_notify(service->priv->proxy, NULL, c);
#else
		dbus_g_proxy_begin_call(service->priv->proxy, "GetShmSingle", ayyi_dbus_shm_notify, c, NULL, G_TYPE_STRING, segs[i], G_TYPE_INVALID);
#endif
	}
	return TRUE;
}


/*
 *  Attempt to import all shm segments listed in ayyi->foreign_shm_segs
 */
gboolean
dbus_server_get_shm__server (AyyiServer* ayyi)
{
	// FIXME refactor to use client version instead: dbus_server_get_shm()

	PF;

	GList* l = ayyi->foreign_shm_segs;
	for (;l;l=l->next) {
#define DBUS_SYNC
#ifdef DBUS_SYNC
		AyyiShmSeg* seg = l->data;

		GError* error = NULL;
		if (!dbus_g_proxy_call(proxy, "GetShmSingle", &error, G_TYPE_STRING, seg_strs[seg->type], G_TYPE_INVALID, G_TYPE_UINT, &seg->id, G_TYPE_INVALID)) {

			// just to demonstrate remote exceptions versus regular GError
			if (error->domain == DBUS_GERROR && error->code == DBUS_GERROR_REMOTE_EXCEPTION) {
				perr ("caught remote method exception %s: %s", dbus_g_error_get_name(error), error->message);
			} else {
				perr ("GetShm: %s", error->message);
			}
			g_error_free(error);

			return FALSE;
		}
		dbg (1, "fd=%i\n", seg->id);

		AyyiShmCallbackData* c = g_new0(AyyiShmCallbackData, 1);
		c->shm_seg = (AyyiCShmSeg*)seg;
		c->user_data = NULL;
		c->service = NULL;

		ayyi_dbus_shm_notify(proxy, NULL, c);

#else
		dbus_g_proxy_begin_call(proxy, "GetShmSingle", ayyi_dbus_shm_notify, c, NULL, G_TYPE_STRING, segs[i], G_TYPE_INVALID);
#endif
	}
	return TRUE;
}


static void
ayyi_dbus_shm_notify (DBusGProxy* proxy, DBusGProxyCall* call, gpointer _shm_callback_data)
{
	// our request for shm address has returned.

	AyyiShmCallbackData* c = _shm_callback_data;

	typedef void (*ShmCallback)(AyyiShmCallbackData*);
	AyyiService* service = c->service;
	g_return_if_fail(service);
	ShmCallback user_callback = service->priv->on_shm;

	void notify_finish (AyyiShmCallbackData* c)
	{
		dbg(2, "freeing shmcallbackdata...");
		g_free(c);
	}

	void ayyi_dbus_do_shm_callback (ShmCallback callback, AyyiShmCallbackData* data)
	{
		if (callback) callback(data);
		else pwarn("no on_shm callback set.");
	}

	if (call) {
		guint id;
		GError* error = NULL;
		if (!dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID, G_TYPE_UINT, &id, G_TYPE_INVALID)) {
			perr ("failed to get shm address: %s", error->message);
			g_error_free(error);
			notify_finish(c);
			return;
		}
	}

	if (!ayyi_shm_import(service)) {
		if(_debug_) pwarn("shm import failed. Calling callback with NULL arg");
		c->shm_seg = NULL;
	}
	ayyi_dbus_do_shm_callback(user_callback, c);

	notify_finish(c);
}


void
ayyi_dbus_ping (AyyiService* server, void (*pong)(const char*))
{
	void ping_done (DBusGProxy* proxy, DBusGProxyCall* call, gpointer _pong)
	{
		void (*pong)(const char*) = _pong;

		gchar* message;
		GError* error = NULL;
		if (!dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID, G_TYPE_STRING, &message, G_TYPE_INVALID)) {
			return;
		}
		call(pong, message);
	}
	dbus_g_proxy_begin_call(proxy, "Ping", ping_done, pong, NULL, G_TYPE_INVALID);
}


/*
 *  Fetch the xml file describing the dbus service.
 */
#ifdef DEBUG
static char*
ayyi_dbus_introspect (AyyiService* service, DBusGConnection* connection)
{
	g_return_val_if_fail (connection != NULL, NULL);

	DBusGProxy* remote_object_introspectable = dbus_g_proxy_new_for_name (connection, service->service, service->path, "org.freedesktop.DBus.Introspectable");

	GError* err = NULL;
	char* introspect_data = NULL;
	if (!dbus_g_proxy_call (remote_object_introspectable, "Introspect", &err, G_TYPE_INVALID, G_TYPE_STRING, &introspect_data, G_TYPE_INVALID)) {
		pwarn ("failed to complete Introspect: %s", err->message);
		g_error_free (err);
	}

	g_object_unref (G_OBJECT (remote_object_introspectable));

	return introspect_data;
}
#endif


static GQuark
ayyi_dbus_error_quark ()
{
	static GQuark quark = 0;
	if (!quark) quark = g_quark_from_static_string ("ayyi_dbus_error");
	return quark;
}


void
ayyi_dbus__register_signals (int signals)
{
	PF;
	g_return_if_fail(proxy);

	void
	ayyi_dbus_rx_obj_new(DBusGProxy* proxy, int object_type, int object_idx, gpointer data)
	{
		SIG_IN1;
		ayyi_handle((AyyiIdent){object_type, object_idx}, AYYI_NO_PROP, ayyi_client_get_watches(object_type, AYYI_OP_NEW, -1));
	}

	void ayyi_dbus_rx_transport(DBusGProxy* proxy, int type, gpointer user_data)
	{
		SIG_IN1;
		ayyi_handle(AYYI_NULL_IDENT, AYYI_NO_PROP, ayyi_client_get_watches(AYYI_OBJECT_TRANSPORT, AYYI_SET, -1));
	}

	void ayyi_dbus_rx_locators(DBusGProxy* proxy, int type, gpointer user_data)
	{
		SIG_IN1;
		ayyi_handle(AYYI_NULL_IDENT, AYYI_NO_PROP, ayyi_client_get_watches(AYYI_OBJECT_LOCATORS, AYYI_SET, -1));
	}

	void ayyi_dbus_rx_progress(DBusGProxy* proxy, double val, gpointer user_data)
	{
		// the progress signal is unique in that it passes a value in the message itself.

		SIG_IN1;
		ayyi_handle_value(AYYI_NULL_IDENT, val, ayyi_client_get_watches(AYYI_OBJECT_PROGRESS, AYYI_SET, -1));
	}

	static int registered = 0;

	if(signals & NEW){
		dbus_g_proxy_add_signal     (proxy, "ObjnewSignal", G_TYPE_INT, G_TYPE_INT, G_TYPE_INVALID);
		dbus_g_proxy_connect_signal (proxy, "ObjnewSignal", G_CALLBACK(ayyi_dbus_rx_obj_new), NULL, NULL);
		registered |= NEW;
	}else{
		if(registered & NEW){
			pwarn("already registered: NEW");
			/*
			dbg(0, "NEW: TODO need to unregister signal");
			registered &= ~NEW;
			*/
		}
	}

	if(signals & DELETED){
		dbus_g_proxy_add_signal     (proxy, "DeletedSignal", G_TYPE_INT, G_TYPE_INT, G_TYPE_INVALID);
		dbus_g_proxy_connect_signal (proxy, "DeletedSignal", G_CALLBACK (ayyi_dbus__rx_deleted), NULL, NULL);
		registered |= DELETED;
	}else{
		if(registered & DELETED){
			dbg(0, "DELETED: TODO need to unregister signal");
			registered &= ~DELETED;
		}
	}

	if(signals & PROPERTY){
		dbus_g_proxy_add_signal     (proxy, "PropertySignal", G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INVALID);
		dbus_g_proxy_connect_signal (proxy, "PropertySignal", G_CALLBACK (ayyi_dbus__rx_property), NULL, NULL);
		registered |= PROPERTY;
	}else{
		if(registered & PROPERTY){
			dbg(0, "PROPERTY: TODO need to unregister signal");
			registered &= ~PROPERTY;
		}
	}

	if(signals & TRANSPORT){
		dbus_g_proxy_add_signal     (proxy, "TransportSignal", G_TYPE_STRING, G_TYPE_INVALID);
		dbus_g_proxy_connect_signal (proxy, "TransportSignal", G_CALLBACK (ayyi_dbus_rx_transport), NULL, NULL);
		registered |= TRANSPORT;
	}else{
		if(registered & TRANSPORT){
			dbg(0, "PROPERTY: TODO need to unregister signal");
			registered &= ~TRANSPORT;
		}
	}

	if(signals & LOCATORS){
		dbus_g_proxy_add_signal     (proxy, "LocatorsSignal", G_TYPE_STRING, G_TYPE_INVALID);
		dbus_g_proxy_connect_signal (proxy, "LocatorsSignal", G_CALLBACK (ayyi_dbus_rx_locators), NULL, NULL);
		registered |= LOCATORS;
	}else{
		if(registered & LOCATORS){
			dbg(0, "LOCATOR: TODO need to unregister signal");
			registered &= ~LOCATORS;
		}
	}

	if(signals & PROGRESS){
		dbus_g_proxy_add_signal     (proxy, "ProgressbarSignal", G_TYPE_DOUBLE, G_TYPE_INVALID);
		dbus_g_proxy_connect_signal (proxy, "ProgressbarSignal", G_CALLBACK (ayyi_dbus_rx_progress), NULL, NULL);
		registered |= PROGRESS;
	}else{
		if(registered & PROGRESS){
			dbg(0, "PROGRESS: TODO need to unregister signal");
			registered &= ~PROGRESS;
		}
	}
}


static void
ayyi_handle (AyyiIdent obj, AyyiPropType prop, const GList* _handlers)
{
	GList* handlers = g_list_copy((GList*)_handlers); // the list can be modified while iterating, so copy it first
	dbg(2, "%s n_handlers=%i", ayyi_print_object_type(obj.type), g_list_length(handlers));
	if(handlers){
		GList* h = handlers;
		for(;h;h=h->next){
			Responder* responder = h->data;
			dbg(2, "handler=%p", responder);
			dbg(2, "user_data=%p", responder->user_data);
			responder->callback(obj, NULL, prop, responder->user_data);
		}
		g_list_free(handlers);
	}
}


static void
ayyi_handle_value (AyyiIdent obj, float val, const GList* _handlers)
{
	// currently we have one signal (progress) that passes a value in the message

	GList* handlers = g_list_copy((GList*)_handlers); //the list can be modified while iterating, so copy it first
	dbg(2, "%s n_handlers=%i", ayyi_print_object_type(obj.type), g_list_length(handlers));
	if(handlers){
		GList* h = handlers;
		for(;h;h=h->next){
			Responder* responder = h->data;
			dbg(2, "handler=%p", responder);
			dbg(2, "user_data=%p", responder->user_data);
			((AyyiValHandler)responder->callback)(obj, NULL, val, responder->user_data);
		}
		g_list_free(handlers);
	}
}


static void
handle_all (AyyiIdent obj, AyyiPropType prop, AyyiOp op_type)
{
	// instance specific handlers
	ayyi_handle(obj, prop, ayyi_client_get_watches(obj.type, op_type, obj.idx));

	// object-type handlers
	ayyi_handle(obj, prop, ayyi_client_get_watches(obj.type, op_type, -1));

	// generic fallback handlers
	ayyi_handle(obj, prop, ayyi_client_get_watches(-1, op_type, -1));
}


static void
ayyi_dbus__rx_deleted (DBusGProxy* proxy, int object_type, int object_idx, gpointer user_data)
{
	SIG_IN_BLUE;
	handle_all((AyyiIdent){object_type, object_idx}, AYYI_NO_PROP, AYYI_DEL);
}


static void
ayyi_dbus__rx_property (DBusGProxy* proxy, int object_type, int object_idx, int property_type, gpointer user_data)
{
	if(_debug_ > 1) printf("%s--->%s changed: %s.%i %s\n", bold, white, ayyi_print_object_type(object_type), object_idx, ayyi_print_prop_type(property_type));

	handle_all((AyyiIdent){object_type, object_idx}, property_type, AYYI_SET);
}


