/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifdef __cplusplus
extern "C" {
#endif

int       ayyi_list__length     (AyyiList*);
AyyiList* ayyi_list__first      (AyyiList*);
AyyiList* ayyi_list__next       (AyyiList*);
void*     ayyi_list__first_data (AyyiList**);
void*     ayyi_list__next_data  (AyyiList**);
AyyiList* ayyi_list__find       (AyyiList*, uint32_t);

#ifdef __cplusplus
}
#endif
