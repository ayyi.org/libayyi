/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __ayyi_shm_c__
#include "config.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <errno.h>
#include <glib.h>

#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_dbus.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_log.h>
#include <ayyi/interface.h>
#include <services/ardourd.h>

int shm_page_size = 0;
char nodes_dir[32] = "/tmp/ayyi/nodes/";
char tmp_dir[32] = "/tmp/ayyi/";

extern void*        ayyi_song__translate_address (void*);
extern void*        ayyi_mixer_translate_address (void*);
extern AyyiCShmSeg* ayyi_song__get_info          ();

extern gboolean     is_song_shm_local_quiet      (void*); // tmp
extern gboolean     is_mixer_shm_local           (void*); // tmp

static gboolean     is_shm_seg_local             (AyyiCShmSeg*, void*);
static gboolean     ayyi_shm_is_complete         ();
static gboolean     engine_on_shm                (AyyiCShmSeg*);

#define GET_TRANSLATOR \
	gboolean is_song = is_song_shm_local_quiet(container); \
	void* (*translator)(void* address); \
	if(is_song) translator = ayyi_song__translate_address; \
	else        translator = ayyi_mixer_translate_address; \


bool
ayyi_client_container_verify (AyyiContainer* container, void* (*translator)(void* address))
{
	g_return_val_if_fail(container, false);
	AyyiContainer* c = container;

	gboolean is_song = is_song_shm_local_quiet(container);

	if(!is_song && !is_mixer_shm_local(c)){ perr("bad address: %p", c); return FALSE; }

	if(c->last < -1 || c->last >= AYYI_CONTAINER_SIZE) { pwarn("%s: last out of range: %i", c->signature, c->last); return FALSE; }
	if(c->full <  0 || c->full >  1                  ) { pwarn("full out of range: %i", c->full); return FALSE; }
	if(!c->signature[0]) { pwarn("container has no signature. container=%p", c); return FALSE; }

	// check blocks above the last flag are not allocated.
	if(c->last + 1 < AYYI_CONTAINER_SIZE){
		if(container->block[c->last + 1]) { pwarn("%s: last flag incorrectly set: %i", c->signature, c->last); return FALSE; }
	}

	if(c->last == -1) { dbg(2, "'%s': no blocks allocated.", c->signature); return TRUE; }

	int b; for(b=0;b<AYYI_CONTAINER_SIZE;b++){
		if(!container->block[b]) break;
		AyyiBlock* blk = translator(container->block[b]);
		void** table = blk->slot;
		if(blk->last < -1 || blk->last >= AYYI_BLOCK_SIZE) { pwarn("%s: block %i: last out of range: %i", c->signature, b, blk->last); return FALSE; }
		if(blk->full <  0 || blk->full > 1) { pwarn("block %i: full out of range: %i", b, blk->full); return FALSE; }
		int i; for(i=0;i<AYYI_BLOCK_SIZE;i++){
			if(!table[i]) continue;
			AyyiRegion* item = translator(table[i]);
			if(!item) continue;
			if(is_song){ if(!is_song_shm_local_quiet(item)){ pwarn("%s: slot=%i.%i: bad song address: %p", c->signature, b, i, item); continue; }}
			else if(!is_mixer_shm_local(item)){ pwarn("slot=%i: bad mixer address.", i); continue; }
			uint16_t idx = item->shm_idx; // FIXME midi event items have a different size index
			if(idx != b * AYYI_BLOCK_SIZE + i){ pwarn("bad idx (%i) at %i.%i container='%s'", idx, b, i, c->signature); return FALSE; }
		}
	}

	return TRUE;
}


/*
 *  Used to iterate through the shm data. Returns the item following the one given, or NULL.
 *  -if arg is NULL, the first region is returned.
 */
void*
ayyi_container_next_item (AyyiContainer* container, AyyiItem* prev, AyyiCShmSeg* seg_info, void* (*translator)(void* address))
{
	g_return_val_if_fail(container, NULL);

	if(container->last < 0) return NULL;

	int not_first = 0;
	uint16_t idx = prev ? prev->shm_idx : 0;
	int first_block = prev ? idx / AYYI_BLOCK_SIZE : 0;

	int b; for(b=first_block;b<=container->last;b++){

		AyyiBlock* blk = translator(container->block[b]);
		void** table = blk->slot;

		int i = (not_first++ || !prev) ? 0 : (ayyi_container_find(container, prev, translator) % AYYI_BLOCK_SIZE + 1);
		if(i < 0) { pwarn("cannot find starting point"); return NULL; }
		for(;i<=blk->last;i++){
			dbg (3, " b=%i i=%i blk->last=%i", b, i, blk->last);
			AyyiItem* next = translator(table[i]);
			if(!next) continue;
			if(!is_shm_seg_local(seg_info, next)){ perr ("bad shm pointer. table[%u]=%p\n", i, table[i]); return NULL; }
			g_return_val_if_fail(next != prev, NULL);
			return next;
		}
	}
	return NULL;
}


/*
 *  Used to iterate backwards through the shm data. Returns the item before the one given, or NULL.
 *	-if arg is NULL, the last region is returned.
 */
AyyiItem*
ayyi_container_prev_item (AyyiContainer* container, AyyiItem* old, AyyiCShmSeg* seg_info, void* (*translator)(void* address))
{
	if(container->last < 0) return NULL;

	int not_first_block = 0;

	for(int b=container->last;b>=0;b--){
		AyyiBlock* blk = translator(container->block[b]);
		void** table = blk->slot;

		if(!old){
			//return end item
			dbg (2, "last! item=%p", translator(table[blk->last]));
			return translator(table[blk->last]);
		}

		int i = (not_first_block++) ? blk->last : ayyi_container_find(container, old, translator) - 1;
		if(i == -2) { pwarn("cannot find orig item in container."); return NULL; }
		if(i < 0) return NULL; // no more items.
		for(;i>=0;i--){
			void* next = translator(table[i]);
			if(!next) continue; // unused slot.
			if(!is_shm_seg_local(seg_info, next)){ perr ("bad shm pointer. table[%u]=%p", i, table[i]); return NULL; }
			return next;
		}
	}
	return NULL;
}


AyyiItem*
ayyi_container_get_item (AyyiContainer* container, AyyiIdx shm_idx, void* (*translator)(void* address))
{
	//this _will_ return an item that has been marked as deleted.

	g_return_val_if_fail(container, NULL);
	if(shm_idx < 0 || shm_idx >= CONTAINER_SIZE * AYYI_BLOCK_SIZE){ perr("shm_idx out of range: %i", shm_idx); return NULL; }

	int b = shm_idx / AYYI_BLOCK_SIZE;
	int i = shm_idx % AYYI_BLOCK_SIZE;
	AyyiBlock* blk = translator(container->block[b]);
	void** table = blk->slot;
	void* item = (table && table[i]) ? translator(table[i]) : NULL;
#ifdef DEBUG
	if(!item && _debug_) pwarn("no such item. idx=0x%x container=%s", shm_idx, container->signature);
#endif

	return item;
}


void*
ayyi_container_get_item_quiet (AyyiContainer* container, AyyiIdx shm_idx, void* (*translator)(void* address))
{
	g_return_val_if_fail(shm_idx < CONTAINER_SIZE * AYYI_BLOCK_SIZE, NULL);

	int b = shm_idx / AYYI_BLOCK_SIZE;
	int i = shm_idx % AYYI_BLOCK_SIZE;
	AyyiBlock* blk = translator(container->block[b]);
	void** table = blk->slot;
	void* item = (table && table[i]) ? translator(table[i]) : NULL;
	return item;
}


int
ayyi_container_find (AyyiContainer* container, void* content, void* (*translator)(void* address))
{
	//return the index number for the given struct.

	g_return_val_if_fail(container, -1);
	g_return_val_if_fail(content, -1);

	if(container->last < 0) return -1;

	int b; for(b=0;b<=container->last;b++){
		AyyiBlock* block = translator(container->block[b]);

		if(block->last < 0) break;

		void** table = block->slot;
		int i; for(i=0;i<=block->last;i++){
			void* item = translator(table[i]);
			if(item == content) return b * AYYI_BLOCK_SIZE + i;
		}
	}
	dbg (1, "content not found. '%s' content=%p", container->signature, content);
	return -1;
}


/*
 *  Return the number of items that are allocated in the given shm container.
 */
int
ayyi_container_count_items (AyyiContainer* container, void* (*translator)(void* address))
{
	int count = 0;
	if(container->last < 0) return count;

	for(int b=0;b<=container->last;b++){
		AyyiBlock* block = translator(container->block[b]);
		if(block->full){
			pwarn ("block %i full.", b);
			count += AYYI_BLOCK_SIZE;
		}else{
			if(block->last < 0) break;

			void** table = block->slot;
			for(int i=0;i<=block->last;i++){
				if(!table[i]) continue;
				void* item = translator(table[i]);
				if(item) count++;
			}
		}
	}
	dbg (2, "count=%i", count);

	return count;
}


/*
 *  For song containers, use ayyi_song__container_index_ok() instead.
 */
gboolean
ayyi_container_index_ok (AyyiContainer* container, AyyiIdx shm_idx)
{
	GET_TRANSLATOR

	int b = shm_idx / AYYI_BLOCK_SIZE;
	int i = shm_idx % AYYI_BLOCK_SIZE;
	AyyiBlock* block = translator(container->block[b]);
	return (shm_idx > -1 && i <= block->last);
}


gboolean
ayyi_container_index_is_used (AyyiContainer* container, AyyiIdx shm_idx)
{
	GET_TRANSLATOR

	int b = shm_idx / AYYI_BLOCK_SIZE;
	int i = shm_idx % AYYI_BLOCK_SIZE;
	AyyiBlock* block = translator(container->block[b]);
	if (shm_idx < 0 || i > block->last) return false;
	return (block->slot[i] != NULL);
}


/*
 *  Returns true if the pointer is in the local shm segment.
 *  Returns false if the pointer is in a foreign shm segment.
 */
static gboolean
is_shm_seg_local (AyyiCShmSeg* seg_info, void* p)
{
	uintptr_t segment_end = (uintptr_t)seg_info->address + seg_info->size;
	if(((uintptr_t)p > (uintptr_t)seg_info->address) && ((uintptr_t)p < segment_end)) return true;

	perr ("address is not in shm segment: %p (expected %p - %p)", p, seg_info->address, (void*)segment_end);
#ifdef DEBUG
	if((uintptr_t)p > (uintptr_t)seg_info->address) perr ("shm offset=%u", GPOINTER_TO_INT((uintptr_t)p - (uintptr_t)((AyyiSongService*)ayyi.service)->song));
#endif
	return false;
}


void
ayyi_container_print (AyyiContainer* container)
{
	AyyiContainer* c = container;
	GET_TRANSLATOR
	printf("---------------------------------------\n");
	printf("outer: last=%i full=%i sig='%s'\n", c->last, c->full, c->signature);

	// look at the first block:
	int b = 0;
	AyyiBlock* blk = translator(container->block[b]);
	if(blk){
		void** table = blk->slot;
		printf("block %i: last=%i full=%i\n", b, blk->last, blk->full);
		int i; for(i=0;i<10;i++){
			void* item = table[i] ? translator(table[i]) : NULL;
			printf("  %i: %p\n", i, item);
		}
	}
	else pwarn("block %i not allocated.", b);
	printf("---------------------------------------\n");
}


void
ayyi_shm_init()
{
	shm_page_size = sysconf(_SC_PAGESIZE);

	ayyi.priv->on_shm = engine_on_shm;
}


AyyiCShmSeg*
ayyi_shm_seg_new(AyyiService* service, int type)
{
	g_return_val_if_fail(service, NULL);

	if(type >= AYYI_SEG_TYPE_LAST){ perr ("bad segment type. %i", type); return NULL; }

	AyyiCShmSeg* shm_seg = AYYI_NEW(AyyiCShmSeg,
		.type = type,
		.size = 4096 * AYYI_SHM_BLOCK_SIZE //FIXME
	);
	service->segs = g_list_append(service->segs, shm_seg);

	return shm_seg;
}


/*
 *  Attach all available shm segments.
 */
gboolean
ayyi_shm_import(AyyiService* service)
{
	gboolean good = TRUE;

	GList* l = service->segs;
	for(;l;l=l->next){
		AyyiCShmSeg* shm_seg = l->data;
		if(shm_seg->id){
			if(shm_seg->attached) continue;

			void* shmptr;
			if((shmptr = shmat(shm_seg->id, 0, 0)) == (void *)-1){
				perr ("shmat() %s", fail);
				ayyi_log_print_fail("shmat() error. id=%i", shm_seg->id);
				return FALSE;
			}

			ayyi_log_print(LOG_OK, "ayyi data import: %s", seg_strs[shm_seg->type]);
			shm_seg->attached = true;
			shm_seg->address = shmptr;

			switch(shm_seg->type){
				case SEG_TYPE_NOT_SET:
					perr ("unexpected shm segment type %i", shm_seg->type);
					good = false;
					break;
				case SEG_TYPE_SONG:
					((AyyiSongService*)service)->song = shmptr;
					if(ayyi.priv->on_shm){
						if(!(good = ayyi.priv->on_shm(shm_seg))){
							ayyi_log_print(LOG_FAIL, "imported song data invalid.");
//TODO make this an option
//if(false)
							((AyyiSongService*)service)->song = NULL;
//good = true;
//shm_seg->invalid = false;
						}
					}
					break;
				case SEG_TYPE_MIXER:
					((AyyiMixerService*)service)->amixer = shmptr;
					((AyyiMixerService*)ayyi.service)->amixer = shmptr; //tmp
					if(ayyi.priv->on_shm){
						if(!(good = ayyi.priv->on_shm(shm_seg))){
							ayyi_log_print(LOG_FAIL, "imported mixer data invalid.");
							((AyyiMixerService*)service)->amixer = NULL;
						}
					}
					break;
				default:
					//perr ("unexpected shm segment type %i", shm_seg->type);
					break;
			}

			if(good && ayyi_shm_is_complete()){
				ayyi.got_shm = TRUE;

				//remove any handlers for events that strictly speaking should not have been added until the service is connected.
				if(!((AyyiSongService*)ayyi.service)->song){
					if(!ayyi_client_remove_watch(AYYI_OBJECT_TRANSPORT, AYYI_SET, -1, NULL)){
						pwarn("responder not removed.");
					}
				}
			}
		}
	}

	return good;
}


void
ayyi_client__reattach_shm(AyyiService* service, AyyiHandler2 _callback, gpointer _user_data)
{
	//static AyyiCallback on_shm; on_shm = ayyi.service->priv->on_shm;
	static void(*on_shm)(AyyiShmCallbackData*); on_shm = ayyi.service->priv->on_shm;
	static AyyiHandler2 callback; callback = _callback;
	static gpointer user_data; user_data = _user_data;
	/* TODO fix callback signature so we can use user_data
	typedef struct {
		AyyiHandler2* callback;
		gpointer      user_data;
	} C;
	C* c = g_new0(C, 1);
	c->callback = _callback;
	c->user_data = user_data;
	*/

	void ayyi_on_reattach_shm(AyyiShmCallbackData* shm_data)
	{
		if(!((((AyyiMixerService*)ayyi.service)->song) && (((AyyiMixerService*)ayyi.service)->amixer))) return; //not got all segments

		ayyi.service->priv->on_shm = on_shm; //restore the original callback.

		call(callback, NULL, user_data);
		//g_free(c);
	}

	ayyi_shm_unattach();
	dbg(1, "updating shm info...");
	int i; for(i=0;ayyi.services[i];i++){
		ayyi_client__dbus_get_shm(ayyi.services[i], ayyi_on_reattach_shm, _user_data);
	}
}


void
ayyi_shm_unattach()
{
	int i; for(i=0;ayyi.services[i];i++){
		GList* l = ayyi.services[i]->segs;
		for(;l;l=l->next){
			AyyiCShmSeg* seg = l->data;
			if(seg->id){
				seg->attached = false;
				seg->id = 0;
			}
		}
	}
	((AyyiSongService*)ayyi.service)->song = NULL;
	((AyyiMixerService*)ayyi.service)->amixer = NULL;
}


#include "ayyi/ayyi_song.h"  //tmp. make the verify callback part of the service.
#include "ayyi/ayyi_mixer.h"
static gboolean
engine_on_shm (AyyiCShmSeg* seg)
{
	gboolean ok = false;

	shm_virtual* shmv = seg->address;
	if(!strlen(shmv->service_name)) ok = FALSE;
	if(!shmv->owner_shm){ perr("bad shm. owner_shm not set."); ok = FALSE; }

	switch(seg->type){
		case SEG_TYPE_SONG:
			ok = ayyi_song__verify(seg);
			break;
		case SEG_TYPE_MIXER:
			dbg(2, "mixer_size=%i", sizeof(struct _shm_seg_mixer));
			ok = ayyi_mixer__verify(/*seg*/);
			break;
		default:
			pwarn("!!");
			break;
	}
	return ok;
}


static gboolean
ayyi_shm_is_complete()
{
	int i; for(i=0;ayyi.services[i];i++){
		AyyiService* service = ayyi.services[i];
		GList* l = service->segs;
		for(;l;l=l->next){
			AyyiCShmSeg* shm_seg = l->data;
			if(!shm_seg->attached || shm_seg->invalid){ dbg(2, "not complete."); return FALSE; }
		}
	}
	return TRUE;
}


gboolean
shm_seg__attach (AyyiShmSeg* seg)
{
	if(!g_file_test(tmp_dir, G_FILE_TEST_IS_DIR)){
		if(mkdir(tmp_dir, O_RDWR) == -1){
			perr("cannot create tmp dir '%s'", tmp_dir);
			return false;
		}
	}

	if(!g_file_test(nodes_dir, G_FILE_TEST_IS_DIR)){
		if(mkdir(nodes_dir, O_RDWR) == -1){
			perr("cannot create tmp dir '%s'", nodes_dir);
			return false;
		}
	}

	char node_name[256];
	snprintf(node_name, 255, "%s%s-%d", nodes_dir, seg->name, (int)getpid());
	if(!g_file_test(node_name, G_FILE_TEST_EXISTS))
		creat(node_name, O_RDWR);

	if((seg->_shmkey = ftok(node_name, 'D')) == -1){
		perr("ftok failed: node_name=%s %s", node_name, fail);
		return false;
	}

	if((seg->_shmfd = shmget(seg->_shmkey, seg->size, IPC_CREAT|0400)) == -1){
		perr("shmget %s", fail);
		unlink(node_name);
		return false;
	}

	if((seg->address = (char*)shmat(seg->_shmfd, 0, 0)) == (char*)-1){
		switch(errno){
			case EACCES:
				perr("shmat EACCES");
				break;
			case EINVAL:
				perr("shmat EINVAL");
				break;
			default:
				perr("shmat err=%i", errno);
				break;
		}
		unlink(node_name);
		return false;
	}

	return true;
}


gboolean
shm_seg__validate()
{
	return TRUE;
}


/*
 *  For testing only
 *  The normal test for this would be (container->last == -1)
 */
gboolean
ayyi_container_is_empty(AyyiContainer* container)
{
	if(!((container->last == -1) || (container->last == 0))){ dbg(1, "0: not empty. c->last=%i (not -1)", container->last); return false; }

	GET_TRANSLATOR

	dbg(2, "block0 allocated? %p", container->block[0]);
	int b; for(b=0;b<2;b++){
		AyyiBlock* blk = NULL;
		if((blk = translator(container->block[b]))){
			if(blk){
				if(!(blk->last == -1)){
					dbg(2, "1: not empty. c->last=%i (not -1)", container->last);
					return false;
				}
			}
		}
	}
	dbg(2, "container is now empty");
	return true;
}


/*
 *  Return true if 1st block is full and there are no other items.
 */
gboolean
ayyi_container_is_one_block_used(AyyiContainer* container)
{
	if(ayyi_song__get_audio_region_count() != AYYI_BLOCK_SIZE){
		dbg(0, "region_count=%i (want %i)", ayyi_song_container_count_items(container), AYYI_BLOCK_SIZE);
		return false;
	}
	if(!((container->last == -1) || (container->last == 0))){
		dbg(0, "%s: c->last=%i (want -1)", container->signature, container->last);
		return false;
	}

	GET_TRANSLATOR
	AyyiBlock* blk = translator(container->block[0]);
	if(!blk || !blk->full){
		dbg(0, "block is not full");
		return false;
	}

	if(blk->last != AYYI_BLOCK_SIZE - 1){
		perr("block->last incorrect: %i", blk->last);
		return false;
	}

	int n = 0;
	if(blk){
		int i; for(i=0;i<AYYI_BLOCK_SIZE;i++){
			AyyiRegion* r = (AyyiAudioRegion*)ayyi_song_container_get_item(container, i);
			if(r){
				n++;
			}
		}
	}
	else dbg(0, "block0 deallocated");
	if(n != AYYI_BLOCK_SIZE){
		dbg(0, "n=%i", n);
		return false;
	}

	block* blk2 = translator(container->block[1]);
	if(blk2){
		pwarn("expected block 2 to be de-allocated");
		if(blk2->last != -1){
			dbg(0, "block2 last: %i", blk2->last);
		}
	}

	return true;
}


