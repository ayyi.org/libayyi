/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <glib.h>

#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_dbus.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include <services/ardourd.h>

static AyyiBlock* ayyi_plugin_get_block     (int block_num);
static AyyiBlock* ayyi_automation_get_block (AyyiChannel*, int block_num, AyyiAutoType);
/*static*/ gboolean is_mixer_shm_local      (void*);

#ifdef NOT_USED
static void   ayyi_mixer__block_print     (AyyiContainer*, int s1);
#endif


/*
 *  Return the index number for the given struct.
 */
#if 0
static AyyiIdx
ayyi_mixer__container_find (AyyiContainer* container, void* content)
{
	g_return_val_if_fail(container, FALSE);
	g_return_val_if_fail(content, FALSE);

	if(container->last < 0) return -1;

	int b; for(b=0;b<=container->last;b++){
		block* block = ayyi_mixer_translate_address(container->block[b]);
		if(block->last < 0) break;

		void** table = block->slot;
		int i; for(i=0;i<=block->last;i++){
			if(!table[i]) continue;
			void* item = ayyi_mixer_translate_address(table[i]);
			if(item == content) return b * AYYI_BLOCK_SIZE + i;
		}
	}
	dbg (0, "not found.");
	return -1;
}
#endif


bool
ayyi_mixer__verify ()
{
	//g_return_val_if_fail(seg->type == SEG_TYPE_MIXER, false);

	// check channels
	if(!ayyi_mixer_container_verify(&((AyyiMixerService*)ayyi.service)->amixer->tracks)){
		return FALSE;
	}

	if(!ayyi_client_container_verify(&((AyyiMixerService*)ayyi.service)->amixer->plugins, ayyi_mixer_translate_address)){
		return FALSE;
	}

	return TRUE;
}


AyyiChannel*
ayyi_mixer__channel_at_quiet (AyyiIdx slot)
{
	return ayyi_mixer_container_get_quiet(&((AyyiMixerService*)ayyi.service)->amixer->tracks, slot);
}


int
ayyi_channel__count_plugins (AyyiChannel* ch)
{
	int count = 0;
	int i; for(i=0;i<AYYI_PLUGINS_PER_CHANNEL;i++){
		if(ch->plugin[i].active) count ++;
	}
	return count;
}


/*
 *  Returns the shm data for a plugin with the given slot number.
 */
AyyiPlugin*
ayyi_plugin_at (int slot)
{
	return ayyi_mixer_container_get_item(&((AyyiMixerService*)ayyi.service)->amixer->plugins, slot);
}


bool
ayyi_plugin_pod_index_ok (int pod_index)
{
	//check that the given plugin slot number is valid.
	//-we use this to ensure pointer dereferencing is ok, so dont return TRUE for any magic index numbers.

	if(pod_index >= AYYI_BLOCK_SIZE) return FALSE;

	int block_num = pod_index / AYYI_BLOCK_SIZE;
	int slot_num = pod_index % AYYI_BLOCK_SIZE;
	AyyiBlock* block = ayyi_plugin_get_block(block_num);

	return (pod_index > -1 && slot_num <= block->last);
}


AyyiContainer*
ayyi_plugin_get_controls (int plugin_slot)
{
	AyyiPlugin* plugin = ayyi_plugin_at(plugin_slot);
	if(!plugin) return NULL;

	return &plugin->controls;
}


AyyiControl*
ayyi_plugin_get_control (AyyiPlugin* plugin, int control_idx)
{
	AyyiControl* control = ayyi_mixer_container_get_item(&plugin->controls, control_idx);
	dbg(0, "idx=%i --> control='%s'", control_idx, control ? control->name : "null");
	return control;
}


AyyiControl*
ayyi_mixer__get_plugin_control (AyyiTrack* route, int i)
{
	return NULL;
}


AyyiControl*
ayyi_mixer__plugin_control_next (AyyiContainer* plugin_container, AyyiControl* control)
{
	return (AyyiControl*)ayyi_mixer__container_next_item(plugin_container, (AyyiItem*)control);
}


static AyyiBlock*
ayyi_plugin_get_block (int block_num)
{
    return ayyi_mixer_translate_address(((AyyiMixerService*)ayyi.service)->amixer->plugins.block[block_num]);
}


void*
ayyi_mixer_container_get_item (AyyiContainer* container, int idx)
{
	if(idx >= CONTAINER_SIZE * AYYI_BLOCK_SIZE){ perr("shm_idx out of range: %i", idx); return NULL; }

	int b = idx / AYYI_BLOCK_SIZE;
	int i = idx % AYYI_BLOCK_SIZE;
	AyyiBlock* blk = ayyi_mixer_translate_address(container->block[b]);
	void** table = blk->slot;
	void* item = ayyi_mixer_translate_address(table[i]);
	if(!item) pwarn("empty item. idx=0x%x", idx);
	return item;
}


void*
ayyi_mixer_container_get_quiet (AyyiContainer* container, int idx)
{
	if(idx >= CONTAINER_SIZE * AYYI_BLOCK_SIZE){ perr("shm_idx out of range: %i", idx); return NULL; }

	int b = idx / AYYI_BLOCK_SIZE;
	int i = idx % AYYI_BLOCK_SIZE;
	AyyiBlock* blk = ayyi_mixer_translate_address(container->block[b]);
	void** table = blk->slot;
	if(!table || !table[i]) return NULL;
	void* item = ayyi_mixer_translate_address(table[i]);
	return item;
}


/*
 *	 Used to iterate through the shm data. Returns the item following the one given, or NULL.
 *   -if arg is NULL, the first region is returned.
 */
AyyiItem*
ayyi_mixer__container_next_item (AyyiContainer* container, AyyiItem* item)
{
	return ayyi_container_next_item(container, item, (AyyiCShmSeg*)ayyi.services[AYYI_SERVICE_MIXER]->segs->data, ayyi_mixer_translate_address);
}


/*
 *  Return the number of items that are allocated in the given shm container.
 */
int
ayyi_mixer_container_count_items (AyyiContainer* container)
{
	int count = 0;
	if(container->last < 0) return count;

	for(int b=0;b<=container->last;b++){
		AyyiBlock* block = ayyi_mixer_translate_address(container->block[b]);
		if(block->full){
			pwarn ("block %i full.", b);
			count += AYYI_BLOCK_SIZE;
		}else{
			if(block->last < 0) break;

			void** table = block->slot;
			int i; for(i=0;i<=block->last;i++){
				if(!table[i]) continue;
				void* item = ayyi_mixer_translate_address(table[i]);
				if(item) count++;
			}
		}
	}
	dbg (2, "count=%i", count);
	return count;
}


gboolean
ayyi_mixer_container_verify (AyyiContainer* container)
{
	return ayyi_client_container_verify(container, ayyi_mixer_translate_address);
}


AyyiAux*
ayyi_mixer__aux_get (AyyiChannel* chan, int idx)
{
	g_return_val_if_fail(chan, NULL);

	if(idx >= AYYI_AUX_PER_CHANNEL) return NULL;
	if(!chan->aux[idx]) return NULL;
	AyyiAux* aux = ayyi_mixer_translate_address(chan->aux[idx]);
	if(!is_mixer_shm_local(aux)){ perr ("bad shm pointer. chan=%i aux_idx=%i\n", chan->shm_idx, idx); return NULL; }

	return aux;
}


AyyiAux*
ayyi_mixer__aux_get_ (AyyiIdx channel_idx, int idx)
{
	AyyiChannel* chan = ayyi_mixer__channel_at_quiet(channel_idx);
	if(!chan){ pwarn("no channel for channel_idx=%i", channel_idx); return NULL; }
	return ayyi_mixer__aux_get(chan, idx);
}


int
ayyi_mixer__aux_count (AyyiIdx channel_idx)
{
	AyyiChannel* chan = ayyi_mixer__channel_at_quiet(channel_idx);
	if (!chan) { pwarn("no channel for channel_idx=%i", channel_idx); return -1; }

	int count = 0;
	for (int i=0;i<AYYI_AUX_PER_CHANNEL;i++) {
		if (chan->aux[i]) count++;
	}
	return count;
}


/*
 *  Sends a msg to the engine for the simple case where a strip has a property with a single float value.
 */
void
ayyi_channel__set_float (AyyiIdx ch, int ch_prop, double val)
{
	PF;
#if USE_DBUS
	dbus_set_prop_float (NULL, AYYI_OBJECT_CHAN, ch_prop, ch, val);
#endif
}


/*
 * Sends a msg to the engine for the simple case where a strip has a property with a single boolean value.
 */
void
ayyi_channel__set_bool (AyyiIdx chan, int ch_prop, gboolean val)
{
#if USE_DBUS
	dbus_set_prop_bool (NULL, AYYI_OBJECT_CHAN, ch_prop, chan, val);
#endif
}


AyyiList*
ayyi_channel__get_routing (AyyiChannel* channel)
{
	g_return_val_if_fail(channel, NULL);

	//AyyiTrack* track = ayyi_song__audio_track_get(channel->shm_idx);
	AyyiTrack* track = (AyyiTrack*)ayyi_song_container_get_item(&((AyyiSongService*)ayyi.service)->song->audio_tracks, channel->shm_idx);
	if(track){
		struct _ayyi_list* routing = track->output_routing;
		return routing;
	}
	return NULL;
}


#ifdef NOT_USED
static void
ayyi_mixer__block_print (AyyiContainer* container, int s1)
{
	g_return_if_fail(container);

	block* block = ayyi_mixer_translate_address(container->block[s1]);
	if (!is_mixer_shm_local(block)){ perr ("bad block! not initialised?"); }

	printf("%s():\n", __func__);
	printf("  last=%i full=%i, next=%p\n", block->last, block->full, block->next);
	int i; for(i=0;i<4;i++) printf("  %i: %p\n", i, block->slot[i]);
}
#endif


/*
 *  Returns true if the pointer is in the local shm segment.
 *  Returns false if the pointer is in a foreign shm segment.
 */
/*static*/ gboolean
is_mixer_shm_local (void* p)
{
	AyyiShmMixer* amixer = ((AyyiMixerService*)ayyi.service)->amixer;
	g_return_val_if_fail(amixer, FALSE);
	void* segment_end = (void*)amixer + ((AyyiMixerService*)ayyi.service)->amixer->num_pages * AYYI_SHM_BLOCK_SIZE; //TODO should use seg_info->size here?
	if(((void*)p > (void*)((AyyiMixerService*)ayyi.service)->amixer) && ((void*)p<segment_end)) return TRUE;

	perr ("address is not in mixer shm: %p (expected %p - %p)\n", p, amixer, (void*)segment_end);
	if((uintptr_t)p > (uintptr_t)amixer) perr ("shm offset=%zu seg_size=%u\n", (uintptr_t)p - (uintptr_t)amixer, 512*AYYI_SHM_BLOCK_SIZE);

	return FALSE;
}


/*
 *  Pointers in shm have to be translated to the address range of the imported segment.
 */
void*
ayyi_mixer_translate_address (void* address)
{
	// temporarily used in ayyi_shm
	// unfortunately we cannot make this static as it is used by all mixer shm data types, eg ayyi_list.

	g_return_val_if_fail(address, NULL);

	void* distance_from_base = (void*)((uintptr_t)address - (uintptr_t)((AyyiMixerService*)ayyi.service)->amixer->owner_shm);
	void* translated = distance_from_base + (uintptr_t)((AyyiMixerService*)ayyi.service)->amixer;

#if 0
	printf("%s(): ((AyyiSongService*)ayyi.service)->amixer=%p ((AyyiSongService*)ayyi.service)->amixer->owner_shm=%p diff=%x distance_from_base=%x\n", __func__, ((AyyiSongService*)ayyi.service)->amixer, ((AyyiSongService*)ayyi.service)->amixer->owner_shm, (uint32_t)((AyyiSongService*)ayyi.service)->amixer - (uint32_t)((AyyiSongService*)ayyi.service)->amixer->owner_shm, (uint32_t)distance_from_base);

	if(!is_song_shm(translated)){ errprintf("sml_translate_address(): address is not in shm!\n"); return NULL; }
	printf("%s(): owner %p -> local %p\n", __func__, address, (void*)translated);
#endif

	return translated;
}


static AyyiBlock*
ayyi_automation_get_block (AyyiChannel* ch, int block_num, AyyiAutoType auto_type)
{
	g_return_val_if_fail(auto_type < AUTO_MAX, NULL);
	if(block_num > ch->automation[auto_type].last) { perr ("block_num out of range: %i (last=%i)", block_num, ch->automation[auto_type].last); return NULL; }

	static char sigs[2][32] = {"vol automation", "pan automation"};
	g_return_val_if_fail(ch, NULL);
	if(strcmp(ch->automation[auto_type].signature, sigs[auto_type])){ perr ("bad container signature: %s", ch->automation[auto_type].signature); return NULL; }

	dbg (3, "route=%p b=%i block=%p block[0]=%p last=%i", ch, block_num, ch->automation[auto_type].block, ch->automation[auto_type].block[0], ch->automation[auto_type].last);
	return ayyi_mixer_translate_address(ch->automation[auto_type].block[block_num]);
}


/*
 *  The current model implements a 1:1 relationship between channels and tracks
 */
AyyiTrack*
ayyi_channel__get_track (AyyiChannel* ch)
{
	return ayyi_song__audio_track_at(ch->shm_idx);
}


AyyiShmEvent*
ayyi_channel__get_auto_event (AyyiChannel* ch, int i, int auto_type)
{
	AyyiBlock* block = ayyi_automation_get_block(ch, 0, auto_type);
	if(block->last < 0) dbg (0, "no events. last=%i", block->last);

	void** event_table = block->slot;
	if(!is_mixer_shm_local(event_table)){ perr ("bad song data. block_table=%p", event_table); return NULL; }
	shm_event* event = ayyi_mixer_translate_address(event_table[i]);
	if(!event) return NULL;
	if(!is_mixer_shm_local(event)){ perr ("failed to get shared event struct. event_index[%u]=%p auto_type=%i", i, event_table[i], auto_type); return NULL; }
	return event;
}


#if 0
static void*
distance_from_base (void* address)
{
	return (void*)((uintptr_t)address - (uintptr_t)((AyyiMixerService*)ayyi.service)->amixer->owner_shm);
}
#endif


#ifdef DEBUG
static void*
distance_from_base_local (void* address)
{
	return (void*)((uintptr_t)address - (uintptr_t)((AyyiMixerService*)ayyi.service)->amixer);
}
#endif


inline static void*
address_add (void* a, void* b)
{
	return (void*)((uintptr_t)a + (uintptr_t)b);
}


#ifdef DEBUG
void
ayyi_print_mixer ()
{
	printf("\n%s\n", __func__);
	if(_debug_ > -1){
		AyyiCShmSeg* segment = ayyi.services[AYYI_SERVICE_MIXER]->segs->data;
		AyyiShmMixer* mixer = ((AyyiMixerService*)ayyi.service)->amixer;

		printf("segment: type=%i size=%i (%p)\n", segment->type, segment->size, GINT_TO_POINTER(segment->size));
		printf("name=%s\n", mixer->service_name);
		printf("mixer:   %p %p\n", mixer->owner_shm, mixer);
		printf("top:     %p %p %p\n",
			address_add(mixer->owner_shm, GINT_TO_POINTER(segment->size)),
			(void*)((uintptr_t)mixer + (uintptr_t)segment->size),
			distance_from_base_local((void*)((uintptr_t)mixer + GINT_TO_POINTER(segment->size)))
		);
		printf("tracks:                 %p %p\n", &mixer->tracks, distance_from_base_local(&mixer->tracks));
		printf("plugins:                %p %p\n\n", &mixer->plugins, distance_from_base_local(&mixer->plugins));
	}

	printf("Channels:");
#if 1 // iteratate using Ayyi iterator
	printf("  %2s %16s %6s %6s %s %s\n", "", "", "level", "pan", "m", "n");
	AyyiChannel* ch = NULL;
	while((ch = ayyi_mixer__channel_next(ch))){
		AyyiTrack* track = ayyi_channel__get_track(ch);
		printf("  %2i %16s %2.4f %2.4f %i %i\n", ch->shm_idx, ch->name, ch->level, ch->pan, track->flags & muted, ch->n_in);
	}
#else
	AMIter i;
	channel_iter_init(&i);
	Channel* channel = NULL;
	while((channel = channel_next(&i))){
		printf("  %2i %15s\n", channel->core_index, am_channel__get_name(channel));
	}
#endif
}
#endif


void
ayyi_automation_print (AyyiChannel* route)
{
	PF;

	if(!route->automation[VOL].block[0]){ dbg (0, "track has no automation events."); return; }

	int count = 0;
	for(int b=0;b<CONTAINER_SIZE;b++){
		AyyiBlock* block = ayyi_automation_get_block(route, b, VOL);
		if(!block){ dbg (2, "end found (empty block). stopping..."); break; }

		void** auto_table = block->slot;
		for(int i=0;i<=block->last;i++){
			if(!auto_table[i]) continue;
			shm_event* shared = ayyi_channel__get_auto_event(route, i, VOL);
			if(shared){
				printf("  %9.2f: %8.2f\n", shared->when, shared->value);
				count++;
			}
		}
	}

	dbg (0, "event_count=%i", count);
}


