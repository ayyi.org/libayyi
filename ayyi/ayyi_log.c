/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <string.h>
#include <stdio.h>
#include "ayyi/ayyi_types.h"
#include "ayyi/ayyi_utils.h"
#include "ayyi/ayyi_client.h"
#include "ayyi/ayyi_log.h"


static void
term_print (AyyiLogType type, const char* str)
{
	char goto_rhs[32];
	sprintf(goto_rhs, "\x1b[A\x1b[%iC", MIN(get_terminal_width() - 12, 80)); // go up one line, then goto rhs

	switch(type){
		case LOG_WARN: printf("%s ", ayyi_warn);
			break;
		case LOG_NONE:
			fputs(bold, stdout);
			break;
		default:
			break;
	}

	printf("%s", str);

	switch(type){
		case LOG_OK:   printf("\n%s%s", goto_rhs, ok);
			break;
		case LOG_FAIL: printf("\n%s%s", goto_rhs, fail);
			break;
		case LOG_NONE: printf("%s", white);
			break;
		default:
			break;
	}
	printf("\n");
}


AyyiLogImpl term = {
	term_print,
};


void
ayyi_log_init ()
{
	g_return_if_fail(!ayyi.log.loggers);
	ayyi.log.loggers = g_list_prepend(NULL, &term);
}


static void
log_append (AyyiLogType type, const char* str)
{
	//text is appended to the text buffer of each open log window.
	//text is also output to stdout and (if print_to_ui is set) the gui (statusbar or other notification).

	GList* l = ayyi.log.loggers;
	for(;l;l=l->next){
		AyyiLogImpl* i = l->data;
		i->print(type, str);
	}
}


void 
ayyi_log_print (AyyiLogType type, const char* format, ...)
{
	char str[256];

	va_list argp;
	va_start(argp, format);
	vsnprintf(str, 255, format, argp);
	va_end(argp);

	log_append(type, str);
}


void
ayyi_log_print_ok ()
{
	// append coloured "[ ok ]"

	GList* l = ayyi.log.loggers;
	for(;l;l=l->next){
		AyyiLogImpl* i = l->data;
		if(i->print_ok) i->print_ok();
	}
}


void
ayyi_log_print_fail ()
{
	GList* l = ayyi.log.loggers;
	for(;l;l=l->next){
		AyyiLogImpl* i = l->data;
		if(i->print_fail) i->print_fail();
	}
}


void
ayyi_log_print_warn ()
{
	GList* l = ayyi.log.loggers;
	for(;l;l=l->next){
		AyyiLogImpl* i = l->data;
		if(i->print_warn) i->print_warn();
	}
}

