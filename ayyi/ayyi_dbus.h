/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 }
 */

#pragma once

#include "ayyi/ayyi_client.h"
#include <dbus/dbus-glib-bindings.h>

#define DBUS_STRUCT_UINT_BYTE_BYTE_UINT_UINT (dbus_g_type_get_struct ("GValueArray", G_TYPE_UINT, G_TYPE_UCHAR, G_TYPE_UCHAR, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_INVALID))

#define P_IN {if(ayyi.log.to_stdout) printf("%s--->%s ", ayyi_green, white); }
#define SIG_IN {if(ayyi.log.to_stdout) printf("%s--->%s %s\n", bold, white, __func__); }
#ifdef DEBUG
#define SIG_IN1 {if(_debug_) printf("%s--->%s ", bold, white); }
#define SIG_IN_BLUE {if(_debug_) printf("%s--->%s ", ayyi_blue, white); }
#define SIG_OUT {if(_debug_) printf("%s(): %s--->%s\n", __func__, yellow, white); }
#else
#define SIG_IN1
#define SIG_IN_BLUE
#define SIG_OUT
#endif

#ifdef __ayyi_client_c__
bool            ayyi_client__dbus_connect   (AyyiService*, AyyiHandler2, gpointer);
void            ayyi_dbus_ping              (AyyiService*, void (*pong)(const char* s));
#endif
gboolean        ayyi_client__dbus_get_shm   (AyyiService*, void (*on_shm)(AyyiShmCallbackData*), gpointer);
#ifdef __ayyi_private__
void            ayyi_dbus__register_signals (int);
#endif

DBusGProxyCall* dbus_set_notelist           (AyyiAction*, uint32_t part_idx, const GPtrArray* notes);
