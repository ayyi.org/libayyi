/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib.h>
#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_dbus.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_time.h>
#include "ayyi/ayyi_transport.h"
#include <services/ardourd.h>

void
ayyi_transport_stop ()
{
	//send a transport stop request.

	if(!ayyi_transport_is_stopped()){
#if USE_DBUS
		dbus_set_prop_bool(NULL, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_STOP, 0, TRUE);
#endif
	}else{
		dbg(1, "already stopped - going to 00:00...");
		//already stopped - go to start.
#if USE_DBUS
		dbus_set_prop_int(NULL, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_LOCATE, 0, 0);
#endif
	}
}


void
ayyi_transport_play ()
{
#if USE_DBUS
	dbus_set_prop_bool(NULL, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_PLAY, 0, TRUE);
#endif
}


void
ayyi_transport_rec ()
{
	AyyiAction* a = ayyi_action_new("rec enable toggle");
	a->obj.type   = AYYI_OBJECT_TRANSPORT;
	a->prop       = AYYI_TRANSPORT_REC;
	a->i_val      = !ayyi_transport_is_rec_enabled();
	ayyi_action_execute(a);
}


void
ayyi_transport_ff ()
{
	#if USE_DBUS
	dbus_set_prop_bool(NULL, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_FF, 0, TRUE);
	#endif
}


bool
ayyi_transport_is_stopped ()
{
	if(!((AyyiSongService*)ayyi.service)->song) return TRUE;
	return (((AyyiSongService*)ayyi.service)->song->transport_speed < 0.1 && ((AyyiSongService*)ayyi.service)->song->transport_speed > -0.1);
}


bool
ayyi_transport_is_rew ()
{
	return (((AyyiSongService*)ayyi.service)->song->transport_speed < 0.0);
}


bool
ayyi_transport_is_ff ()
{
	return (((AyyiSongService*)ayyi.service)->song->transport_speed > 1.2);
}


bool
ayyi_transport_is_playing ()
{
	if(!((AyyiSongService*)ayyi.service)->song) return FALSE;
	return (((AyyiSongService*)ayyi.service)->song->transport_speed > 0.1 || ((AyyiSongService*)ayyi.service)->song->transport_speed < -0.1);
}


bool
ayyi_transport_is_cycling ()
{
	dbg (3, "cycle_mode=%i", ((AyyiSongService*)ayyi.service)->song->play_loop);
	return ((AyyiSongService*)ayyi.service)->song->play_loop;
}


bool
ayyi_transport_is_rec_enabled ()
{
	g_return_val_if_fail(ayyi.got_shm, false);
	dbg (2, "rec_enabled=%i", ((AyyiSongService*)ayyi.service)->song->rec_enabled);
	return ((AyyiSongService*)ayyi.service)->song->rec_enabled;
}


bool
ayyi_transport_is_recording ()
{
	dbg (5, "rec_enabled=%i", ((AyyiSongService*)ayyi.service)->song->rec_enabled);
	return ((AyyiSongService*)ayyi.service)->song->rec_enabled && (((AyyiSongService*)ayyi.service)->song->transport_speed > 0.1);
}


uint32_t
ayyi_transport__get_frame ()
{
	return ((AyyiSongService*)ayyi.service)->song->transport_frame;
}


void
ayyi_transport_get_spp_string (char* spp)
{
	uint64_t samples = MIN(((AyyiSongService*)ayyi.service)->song->transport_frame, ayyi_beats2samples(AYYI_MAX_BEATS));
	ayyi_samples2bbst(samples, spp);
}


