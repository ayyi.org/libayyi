/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __ayyi_types_h__
#define __ayyi_types_h__

#include <stdint.h>
#include <ayyi/ayyi_typedefs.h>

#ifdef __cplusplus
namespace Ayi {
#endif

#define AYYI_NAME_MAX 64

struct _ayyi_base_item {
	int        shm_idx; // slot_num | (block_num / AYYI_BLOCK_SIZE)
	char       name[AYYI_NAME_MAX];
	AyyiId     id;
	void*      server_object;
	int        flags;
};

typedef enum {
	AYYI_AUDIO = 1,
	AYYI_MIDI,
	AYYI_MEDIA_TYPE_MAX
} AyyiMediaType;

//op types
typedef enum _ayyi_op_type {
	AYYI_OP_NEW = 1,
	AYYI_DEL,
	AYYI_GET,
	AYYI_SET,
	AYYI_UNDO
} AyyiOp;

//object types
enum {
	AYYI_OBJECT_EMPTY = 0,
	AYYI_OBJECT_TRACK,
	AYYI_OBJECT_AUDIO_TRACK,
	AYYI_OBJECT_MIDI_TRACK,
	AYYI_OBJECT_CHAN,
	AYYI_OBJECT_AUX,
	AYYI_OBJECT_PART,
	AYYI_OBJECT_AUDIO_PART,
	AYYI_OBJECT_MIDI_PART,
	AYYI_OBJECT_EVENT,
	AYYI_OBJECT_RAW,
	AYYI_OBJECT_STRING,
	AYYI_OBJECT_ROUTE,
	AYYI_OBJECT_FILE,
	AYYI_OBJECT_LIST,
	AYYI_OBJECT_MIDI_NOTE,
	AYYI_OBJECT_SONG,
	AYYI_OBJECT_TRANSPORT,
	AYYI_OBJECT_LOCATORS,
	AYYI_OBJECT_AUTO,
	AYYI_OBJECT_PROGRESS,
	AYYI_OBJECT_METRONOME,
	AYYI_OBJECT_UNSUPPORTED,
	AYYI_OBJECT_ALL
};

// properties
// 20130824: now doubling as changetypes, though as there are too many for 32bit int, perhaps a separate ChangeType needs to be added.
enum _ayyi_prop_type {
	AYYI_NO_PROP         = 0,
	AYYI_NAME            = 1 <<  0,
	AYYI_STIME           = 1 <<  1,
	AYYI_LENGTH          = 1 <<  2,
	AYYI_LEVEL           = 1 <<  3,
	AYYI_PAN             = 1 <<  4,
	AYYI_PAN_ENABLE      = 1 <<  5,
	AYYI_DELAY           = 1 <<  6,
	AYYI_HEIGHT          = 1 <<  7,
	AYYI_COLOUR          = 1 <<  8,
	AYYI_END             = 1 <<  9,
	AYYI_TRACK           = 1 << 10,
	AYYI_MUTE            = 1 << 11,
	AYYI_ARM             = 1 << 12,
	AYYI_SOLO            = 1 << 13,
	AYYI_SDEF            = 1 << 14,
	AYYI_INSET           = 1 << 15,
	AYYI_FADEIN          = 1 << 16,
	AYYI_FADEOUT         = 1 << 17,
	AYYI_INPUT           = 1 << 18,
	AYYI_OUTPUT          = 1 << 19,
	AYYI_PREPOST         = 1 << 20,
	AYYI_SPLIT           = 1 << 21,

	AYYI_PLUGIN_SEL      = 1 << 22,
	AYYI_PLUGIN_BYPASS   = 1 << 23,

	AYYI_PB_LEVEL        = 1 << 24,
	AYYI_PB_PAN,
	AYYI_PB_DELAY,

	AYYI_TRANSPORT_PLAY,
	AYYI_TRANSPORT_STOP,
	AYYI_TRANSPORT_REW,
	AYYI_TRANSPORT_FF,
	AYYI_TRANSPORT_REC,
	AYYI_TRANSPORT_LOCATE,
	AYYI_TRANSPORT_CYCLE,
	AYYI_TRANSPORT_LOCATOR,

	AYYI_AUTO_PT, // not really a property. We specify the complete point. Perhaps remove?

	AYYI_ADD_POINT,
	AYYI_DEL_POINT,

	AYYI_TEMPO,
	AYYI_HISTORY,

	AYYI_LOAD_SONG,
	AYYI_SAVE,
	AYYI_NEW_SONG
};


#define AYYI_TYPE_SONG_POS (ayyi_song_pos_get_type ())

/**
 * AyyiSongPos: (set-value-func ayyi_pos_set_value) (get-value-func ayyi_pos_get_value)
 */
struct _AyyiSongPos
{
	int beat;
	int16_t sub;
	int16_t mu;
};


struct _ayyi_obj_idx
{
	uint32_t idx1;
	uint32_t idx2;
	uint32_t idx3;
};


struct _AyyiIdent
{
	AyyiObjType type;
	AyyiIdx     idx;
};


struct _launch_info
{
	char* name;
	char* exec;
};

struct _HandlerData {
	AyyiHandler callback;
	gpointer    user_data;
};

typedef enum {
	VOL = 0,
	PAN,
	AUTO_MAX
} AyyiAutoType;

typedef enum // channel/track/aux flags
{
	muted  = 1 << 0,
	solod  = 1 << 1,
	armed  = 1 << 2,
	master = 1 << 3,
	deleted= 1 << 4,
	FLAGS_MAX
} ChanFlags;

typedef enum
{
	Midi = 1 << 0,
} AyyiChannelFlags;

typedef enum
{
	PLAYLIST_FLAG_MIDI = 1 << 0,
} PlaylistFlags;

typedef enum
{
	NEW       = 1 << 0,
	DELETED   = 1 << 1,
	CHANGED   = 1 << 2,
	TRANSPORT = 1 << 3,
	LOCATORS  = 1 << 4,
	PROPERTY  = 1 << 5,
	PROGRESS  = 1 << 6,
	SIGNAL_MASK_MAX
} SignalMask;

#ifdef __cplusplus
}      // namspace Ayi
#endif

#endif
