/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_shm_h__
#define __ayyi_shm_h__

#define AYYI_SHM_BLOCK_SIZE 4096

enum _seg_type
{
	SEG_TYPE_NOT_SET,
	SEG_TYPE_SONG,
	SEG_TYPE_MIXER,
	AYYI_SEG_TYPE_PLUGIN, //TODO
	AYYI_SEG_TYPE_BUFFER,
	AYYI_SEG_TYPE_LAST
};

#ifdef __ayyi_shm_c__
char seg_strs[AYYI_SEG_TYPE_LAST][16] = {"", "song", "mixer", "plugin", "buffer"};
#else
extern char seg_strs[5][16];
#endif

struct _AyyiCShmSeg
{
	int      id;
	int      type;
	int      size; //bytes
	gboolean attached;
	void*    address;
	gboolean invalid;
};

//server version of _shm_seg - probably should be merged with client version.
struct _ayyi_shm_seg
{
	int      id;
	int      type;
	int      size; //bytes
	gboolean attached;
	void*    address;
	gboolean invalid;

	char     name[64];

	long     _shmkey;
	int      _shmfd;
};

typedef void* (*Translator) (void*);

bool     ayyi_client_container_verify   (AyyiContainer*, Translator);
void*    ayyi_container_next_item       (AyyiContainer*, AyyiItem*, AyyiCShmSeg*, Translator);
AyyiItem*ayyi_container_prev_item       (AyyiContainer*, AyyiItem*, AyyiCShmSeg*, Translator);
AyyiItem*ayyi_container_get_item        (AyyiContainer*, AyyiIdx, Translator);
void*    ayyi_container_get_item_quiet  (AyyiContainer*, AyyiIdx, Translator);
int      ayyi_container_find            (AyyiContainer*, void* content, Translator);
int      ayyi_container_count_items     (AyyiContainer*, Translator);
gboolean ayyi_container_index_ok        (AyyiContainer*, AyyiIdx);
gboolean ayyi_container_index_is_used   (AyyiContainer*, AyyiIdx);
void     ayyi_container_print           (AyyiContainer*);

void     ayyi_shm_init                  ();
AyyiCShmSeg*
         ayyi_shm_seg_new               (AyyiService*, int type);
gboolean ayyi_shm_import                (AyyiService*);
void     ayyi_shm_unattach              ();
void     ayyi_client__reattach_shm      (AyyiService*, AyyiHandler2, gpointer);

gboolean shm_seg__attach                (AyyiShmSeg*);
gboolean shm_seg__validate              ();

#ifdef __ayyi_private__
//for testing only
gboolean ayyi_container_is_empty          (AyyiContainer*);
gboolean ayyi_container_is_one_block_used (AyyiContainer*);
#endif

#endif
