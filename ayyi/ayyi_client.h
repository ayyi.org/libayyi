/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __ayyi_client_h__
#define __ayyi_client_h__

#include <stdbool.h>
#include "ayyi/ayyi_typedefs.h"
#include "ayyi/ayyi_types.h"
#include "ayyi/ayyi_log.h"

#define ASSERT_SONG_SHM  if(!ayyi.got_shm){ perr("no song shm"); return; }
#define ASSERT_SONG_SHM_RET_FALSE  if(!ayyi.got_shm){ perr("no song shm"); return FALSE; }
#define UNEXPECTED_PROPERTY(T) pwarn("unexpected property: obj_type=%s prop=%s", ayyi_print_object_type(T), ayyi_print_prop_type(prop));
#define UNEXPECTED_OBJECT_TYPE(T) pwarn("unexpected object type: %i %s", T, ayyi_print_object_type(T));

#define AYYI_NULL_IDENT ((AyyiIdent){-1, -1})

#ifndef __ayyi_client_c__
extern AyyiService* known_services[];
#endif

#if (defined __ayyi_client_c__ || defined __ayyi_action_c__ || defined __ayyi_shm_c__ || defined __am_message_c__ )
struct _AyyiClientPrivate
{
	GSList*                 plugins;            // loaded Ayyi plugins

	GList*                  actionlist;         // list of AyyiAction*
	unsigned                trans_id;

	gboolean                (*on_shm)(AyyiCShmSeg*); // used for segment verification.

	GHashTable*             responders;
};
#endif

#if (defined __ayyi_dbus_c__ || defined __ayyi_client_c__ || defined __dbus_proxy_c__ || defined __ayyi_shm_c__ )
#include <dbus/dbus-glib-bindings.h>
struct _AyyiServicePrivate
{
	DBusGProxy*             proxy;

	void                    (*on_shm)(AyyiShmCallbackData*); // application level callback
};
#endif

typedef struct _AyyiClientPrivate AyyiClientPrivate;
typedef struct _AyyiServicePrivate AyyiServicePrivate;

typedef struct _AyyiClient
{
    AyyiService*            service;     // deprecated - use the array below instead.
    AyyiService**           services;    // null terminated.

    int                     got_shm;     // TRUE when we must have _all_ required shm pointers.
    gboolean                offline;     // TRUE if we are not connected and should not attempt to connect to remote service.

    int                     signal_mask; // defines which signals we wish to receive.

    AyyiLog                 log;

    GList*                  launch_info; // type AyyiLaunchInfo

    AyyiClientPrivate*      priv;
} AyyiClient;

struct _AyyiService
{
	char*                   service;
	char*                   path;
	char*                   interface;

	//instance values (service is singleton, so class and instance are the same):

	void                    (*ping)      (void (*)(const char*));

	GList*                  segs;        // list of type shm_seg*

	AyyiServicePrivate*     priv;
};

typedef enum {
	AYYI_SERVICE_AYYID1 = 0,
	AYYI_SERVICE_MIXER,
} AyyiServiceType;

typedef struct _AyyiPlugin {
	guint       api_version;
	gchar*      name;
	guint       type;          // plugin type (currently only PLUGIN_TYPE_1)

	gchar*      service_name;
	gchar*      app_path;
	gchar*      interface;

	void*       symbols;       // plugin type specific symbol table

	void*       client_data;   // currently used to store the local shm address
} *AyyiPluginPtr;

typedef struct
{
	AyyiPropHandler callback;
	gpointer        user_data;
} Responder;

struct _AyyiShmCallbackData
{
	AyyiCShmSeg* shm_seg;
	gpointer     user_data;
	AyyiService* service;
};


const AyyiClient*
              ayyi_client_init              ();
void          ayyi_client_add_services      (AyyiService**);
bool          ayyi_client_connect           (AyyiService*, AyyiHandler2, gpointer);
void          ayyi_client_disconnect        ();
void          ayyi_client_load_plugins      ();
AyyiPluginPtr ayyi_client_get_plugin        (const char*);
void          ayyi_client_set_signal_reg    (int);

void          ayyi_object_set_bool          (AyyiObjType, AyyiIdx, AyyiPropType, gboolean val, AyyiAction*);
gboolean      ayyi_object_is_deleted        (AyyiItem*);

void          ayyi_client_watch             (AyyiIdent, AyyiOpType, AyyiPropHandler, gpointer);
void          ayyi_client_watch_once        (AyyiIdent, AyyiOpType, AyyiHandler, gpointer);
bool          ayyi_client_remove_watch      (AyyiObjType, AyyiOpType, AyyiIdx, AyyiPropHandler);
const GList*  ayyi_client_get_watches       (AyyiObjType, AyyiOpType, AyyiIdx);
#ifdef DEBUG
void          ayyi_client_print_watches     ();
#endif

void          ayyi_discover_clients         ();

AyyiIdx       ayyi_ident_get_idx            (AyyiIdent);
AyyiObjType   ayyi_ident_get_type           (AyyiIdent);

void          ayyi_launch                   (AyyiLaunchInfo*, gchar** argv);
void          ayyi_launch_by_name           (const char*, gchar** argv);

const char*   ayyi_print_object_type        (AyyiObjType);
const char*   ayyi_print_optype             (AyyiOpType);
const char*   ayyi_print_prop_type          (AyyiPropType);
const char*   ayyi_print_media_type         (AyyiMediaType);

#ifdef __ayyi_client_c__
AyyiClient ayyi;
#else
extern AyyiClient ayyi;
#endif

#ifdef HAVE_INTROSPECTION
void         ayyi_client_init_2             ();
#endif

#endif
