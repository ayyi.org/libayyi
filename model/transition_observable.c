/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2020-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "config.h"
#include "math.h"
#include <glib.h>
#include "ayyi/ayyi_utils.h"
#include "model/transition_observable.h"


TransitionObservable*
transition_observable (Observable* observable)
{
	void _on_frame (WfAnimation* animation, int time)
	{
		TransitionObservable* to = animation->user_data;

		for(GList* l=animation->members;l;l=l->next){
			WfAnimActor* actor = l->data;
			for(GList* k=actor->transitions;k;k=k->next){
				WfAnimatable* animatable = k->data;
				observable_set((Observable*)to, (AMVal){ .b = *animatable->val.b});
			}
		}
	}

	TransitionObservable* to = AYYI_NEW(TransitionObservable,
		.observable.value = observable->value,
		.source = observable,
		.animatable = {
			.target_val.pt = (AGlPtf){.x = observable->value.pt.x, .y = observable->value.pt.y},
			.type = WF_PT,
		},
		.value = observable->value
	);
	to->animatable.val.b = &to->value.b;

	void observe (Observable* source, AMVal val, gpointer _to)
	{
		TransitionObservable* to = (TransitionObservable*)_to;

		to->animatable.start_val.b = *to->animatable.val.b; // may not be needed
		to->animatable.target_val.b = val.b;

		void to_on_finish (WfAnimation* transition, gpointer to)
		{
			((TransitionObservable*)to)->transition = NULL;
		}

		if(to->transition){
			wf_animation_remove(to->transition);
		}

		to->transition = wf_animation_new(to_on_finish, to);
		to->transition->on_frame = _on_frame;
		wf_transition_add_member(to->transition, g_list_append(NULL, &to->animatable));
		wf_animation_start(to->transition);
	}

	observable_subscribe(observable, observe, to);

	return to;
}


void
transition_observable_free (Observable* observable)
{
	TransitionObservable* to = (TransitionObservable*)observable;

	observable_unsubscribe(to->source, NULL, to);
	g_clear_pointer(&observable, observable_free);
}
