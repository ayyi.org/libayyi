/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  Test client for Ayyi Plugin loading
 |  -----------------------------------
 |
 */
#include "config.h"
#include "model/ayyi_model.h"
#include "model/test/runner.h"
#include "model/test/common.h"
#ifdef USE_SPECTROGRAM
#include "spectrogram/client.h"
#endif
#include "runner.h"

TestFn test_one;

gpointer tests[] = {
	test_one,
};


void
setup (int argc, char* argv[])
{
	TEST.n_tests = G_N_ELEMENTS(tests);

	am_init(NULL);
	ayyi.log.to_stdout = true;

	gboolean connect(gpointer user_data)
	{
		_debug_ = 1;

		am_connect_all(app_on_all_shm, NULL);

		return G_SOURCE_REMOVE;
	}
	g_idle_add(connect, NULL);
}


void
teardown ()
{
#ifdef WITH_VALGRIND
	am_uninit();
#endif
}


void
test_one ()
{
	START_TEST;

#ifdef USE_SPECTROGRAM
	AyyiPluginPtr plugin;
	if ((plugin = ayyi_client_get_plugin("Ayyi Spectrogram Plugin"))) {
		SpectrogramSymbols* spectrogram = plugin->symbols;
		//spectrogram->on_expose(NULL, NULL, NULL, plugin);
		dbg(0, "hello=%s", spectrogram->get_hello());
		dbg(0, "meter_level=%.2f", spectrogram->get_meterlevel(plugin));
		assert(!strcmp("hello", spectrogram->get_hello()), "unexpected signature");
	}
	else FAIL_TEST("couldnt get plugin");
#else
	assert(false, "USE_SPECTROGRAM not defined - install the spectrogram plugin and rerun configure");
#endif

	FINISH_TEST;
}
