/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __runner_c__

#include "config.h"
#include <glib.h>
#include "common.h"
#include "runner.h"

/*
 *  The test must provide the list of tests and the setup and teardown functions
 */
extern gpointer tests[];
extern void setup(char* argv[]);
extern void teardown();

static void next_test ();

static GMainLoop* mainloop;


int
main (int argc, char* argv[])
{
	set_log_handlers();

	setup (argv);

	gboolean fn(gpointer user_data) { next_test(); return G_SOURCE_REMOVE; }

	g_idle_add(fn, NULL);

	g_main_loop_run (mainloop = g_main_loop_new (NULL, 0));

	exit(1);
}


static gboolean
__exit ()
{
	g_main_loop_unref(mainloop);
	exit(TEST.n_failed ? EXIT_FAILURE : EXIT_SUCCESS);
	return G_SOURCE_REMOVE;
}


static gboolean
run_test (gpointer test)
{
	((Test)test)();
	return G_SOURCE_REMOVE;
}


static gboolean
on_test_timeout (gpointer _user_data)
{
	FAIL_TEST_TIMER("TEST TIMEOUT\n");
	return G_SOURCE_REMOVE;
}


static void
next_test ()
{
	printf("\n");

	TEST.current.test++;
	if (TEST.timeout) g_source_remove (TEST.timeout);
	if (TEST.current.test < TEST.n_tests) {
		TEST.current.finished = false;
		gboolean (*test)() = tests[TEST.current.test];
		dbg(2, "test %i of %i.", TEST.current.test + 1, TEST.n_tests);

		if (TEST.before_each) {
			void ready ()
			{
				g_timeout_add(1, run_test, tests[TEST.current.test]);
			}
			wait_for(TEST.before_each, ready, NULL);
		} else {
			g_timeout_add(300, run_test, test);
		}

		TEST.timeout = g_timeout_add(20000, on_test_timeout, NULL);
	} else {
		printf("finished all. passed=%s %i %s failed=%s %i %s\n", ayyi_green, TEST.n_passed, ayyi_white, (TEST.n_failed ? ayyi_red : ayyi_white), TEST.n_failed, ayyi_white);
		teardown();
		g_timeout_add(TEST.n_failed ? 4000 : 1000, __exit, NULL);
	}
}


void
test_finish ()
{
	dbg(2, "... passed=%i", TEST.passed);

	for (GList* l = TEST.current.timers; l; l = l->next) {
		g_source_remove(GPOINTER_TO_INT(l->data));
	}
	g_clear_pointer(&TEST.current.timers, g_list_free);

	if (TEST.passed) TEST.n_passed++; else TEST.n_failed++;
	if (!TEST.passed && abort_on_fail) TEST.current.test = 1000;

	next_test();
}


void
test_log_start (const char* func)
{
	printf("%srunning %i of %i: %s%s ...\n", ayyi_bold, TEST.current.test + 1, TEST.n_tests, func, ayyi_white);
}


void
test_errprintf (char* format, ...)
{
	char str[256];

	va_list argp;
	va_start(argp, format);
	vsprintf(str, format, argp);
	va_end(argp);

	printf("%s%s%s\n", ayyi_red, str, ayyi_white);
}


void
reset_timeout (int ms)
{
	if(TEST.timeout) g_source_remove (TEST.timeout);

	gboolean on_test_timeout (gpointer _user_data)
	{
		FAIL_TEST_TIMER("TEST TIMEOUT\n");
		return G_SOURCE_REMOVE;
	}
	TEST.timeout = g_timeout_add(ms, on_test_timeout, NULL);
}


void
wait_for (ReadyTest test, WaitCallback on_ready, gpointer user_data)
{
	typedef struct {
		ReadyTest    test;
		int          i;
		WaitCallback on_ready;
		gpointer     user_data;
	} C;

	gboolean _check (C* c)
	{
		if(c->test(c->user_data)){
			TEST.current.timers = g_list_remove(TEST.current.timers, GINT_TO_POINTER(g_source_get_id(g_main_current_source())));
			c->on_ready(c->user_data);
			g_free(c);
			return G_SOURCE_REMOVE;
		}

		if(c->i++ > 100){
			TEST.current.timers = g_list_remove(TEST.current.timers, GINT_TO_POINTER(g_source_get_id(g_main_current_source())));
			g_free(c);
			return G_SOURCE_REMOVE;
		}

		return G_SOURCE_CONTINUE;
	}

	TEST.current.timers = g_list_prepend(TEST.current.timers, GINT_TO_POINTER(
		g_timeout_add(100, (GSourceFunc)_check, AYYI_NEW(C,
			.test = test,
			.on_ready = on_ready,
			.user_data = user_data
		)
	)));
}
