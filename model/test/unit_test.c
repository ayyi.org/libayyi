/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 *  Unit Tests for Ayyi client model libary
 *  ---------------------------------------
 *
 *  These tests dont require connecting to any Ayyi services.
 *  All tests run synchronously without callbacks.
 *
 */

#define __main_c__

#include "config.h"
#include <getopt.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>
#include <glib.h>
#include <ayyi/ayyi_typedefs.h>
#include "model/ayyi_model.h"
#include "model/track_list.h"
#include "runner.h"

static void app_init  ();

TestFn test_ayyi_list, test_time_conversions,
	test_mu_samples_conversion,
	test_mu2pos,
	test_pos_mu_conversion,
	test_pos_sample_conversion,
	test_q_to_prev_next,
	test_pos_add,
	test_pos_subtract,
	test_bbss,
	test_bbss2pos,
	test_bbst_increment,
	test_fader_conversion,
	test_observable,
	test_filter_iterator,
	test_part_input,
	test_automation,
	test_container;

#define OK 0

gpointer tests[] = {
	test_time_conversions,
	test_ayyi_list,
	test_mu_samples_conversion,
	test_mu2pos,
	test_pos_mu_conversion,
	test_pos_sample_conversion,
	test_pos_add,
	test_pos_subtract,
	test_bbss,
	test_bbss2pos,
	test_bbst_increment,
	test_q_to_prev_next,
	test_fader_conversion,
	test_observable,
	test_filter_iterator,
	test_part_input,
	test_automation,
	test_container,
};

#define MAX_RECENT_SONGS 4
typedef struct _canvas_op canvas_op;

typedef struct
{
  int            state;

  gboolean       dbus;

  char           config_filename[256];
  GKeyFile*      key_file;             //config file data.

  char           recent_songs[MAX_RECENT_SONGS][256];

} App;

App app;


void
setup (int argc, char *argv[])
{
#if 0
	g_log_set_handler (NULL, G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gtk", G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gdk", G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("GLib", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("GLib-GObject", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gdl", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Ayyi", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
#endif
	TEST.n_tests = G_N_ELEMENTS(tests);

	am_init(NULL);
	app_init();
}


void
teardown ()
{
	am_uninit();
}


static void
app_init ()
{
	ayyi.got_shm = 0;   // is set when all required shm pointers are set
	app.dbus = false;

	for(int i=0;i<MAX_RECENT_SONGS;i++)
		*app.recent_songs[i] = 0;

	if (!((AyyiSongService*)ayyi.service)->song) {
		((AyyiSongService*)ayyi.service)->song = AYYI_NEW(struct _shm_song,
			.bpm = 120,
			.sample_rate = 44100
		);
	}

	ayyi.log.to_stdout = true;
}


gboolean
test1 ()
{
	PF;
	/*
	uint64_t mu = px2mu_nz(PX_PER_BEAT);
	if(mu != CORE_SUBS_PER_BEAT * CORE_MU_PER_SUB){ perr("%Lu", mu); return FAIL; }
	*/
	return OK;
}

void
test_time_conversions ()
{
	START_TEST;

	FINISH_TEST;
}


void
test_mu2pos ()
{
	g_assert(sizeof(AyyiSongPos) == 8);

	typedef struct {
		AyyiMu      mu;
		AyyiSongPos result;
	} Case;

	Case cases[] = {
		{0,                                        {0,}},
		{1,                                        {0,0,1}},
		{AYYI_MU_PER_SUB - 1,                      {0,0,11024}},
		{AYYI_MU_PER_SUB,                          {0,1,0}},
		{AYYI_MU_PER_SUB + 1,                      {0,1,1}},
		{AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT - 1, {0,3839,11024}},
		{AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT,     {1,0,0}},
		{AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT + 1, {1,0,1}},
		{-1,                                       {0,0,-1}},
	};

	START_TEST;

	bool pass = true;
	for (int i=0;i<G_N_ELEMENTS(cases);i++) {
		Case* c = &cases[i];
		printf("  %"PRIi64"\n", c->mu);
		AyyiSongPos r; ayyi_mu2pos(c->mu, &r);
		g_assert(ayyi_pos_is_valid(&r));

		char bbst2[32]; ayyi_pos2bbst(&r, bbst2);
		char bbst0[32]; ayyi_pos2bbst(&c->result, bbst0);
		if(ayyi_pos_cmp(&r, &c->result)){
			perr("%s != %s (%i)", bbst2, bbst0, c->result.beat);
			pass = false;
		}
	}

	if (pass) FINISH_TEST;
	FAIL_TEST("");
}


void
test_mu_samples_conversion ()
{
	START_TEST;

	uint32_t s[] = {0, 22050, 22051, 22052, 22053, 44100, 11254665};

	for (int i=0;i<G_N_ELEMENTS(s);i++) {
		long long mu = ayyi_samples2mu(s[i]);
		long long b = ayyi_mu2samples(mu);
#ifdef DEBUG
		long long mpb = AYYI_MU_PER_BEAT;
		dbg(0, "%8i mu=%8Li s=%8Li (%Li)", s[i], mu, b, mpb);
#endif

		assert((b == s[i]), "conversion");
	}

	FINISH_TEST;
}


void
test_ayyi_list ()
{
	// AyyiList's live only in shared memory, so can't be tested here.

	START_TEST;
	FINISH_TEST;
	return;

	AyyiList* ayyi_list_prepend (AyyiList* l, int i)
	{
		AyyiList* new = g_new0(AyyiList, 1);
		new->id = i;
		return new;
	}

	int id = 99;
	AyyiList* l = ayyi_list_prepend(NULL, id);
	if (l->id != id) FAIL_TEST("");
	if (!(ayyi_list__length(l) == 1)) FAIL_TEST("");
}


void
test_q_to_prev_next ()
{
	START_TEST;
	QMap q;
	strncpy(q.name, "exponential", 63);
	q.size = 5;
	q.pos[0] = (GPos){0,0,0};
	q.pos[1] = (GPos){0,0,AYYI_TICKS_PER_SUBBEAT/4};
	q.pos[2] = (GPos){0,1,0};
	q.pos[3] = (GPos){1,0,0};
	q.pos[4] = (GPos){4,0,0};
	q.pos[5] = (GPos){8,0,0};

	GPos cases[] = {{-4, 0, 0}, {0, 0, 0}, {0, 0, 1}, {0, 0, 96}, {0, 0, 97}, {1, 1, 0}, {7, 0, 0}, {9, 0, 0}, {16, 0, 0}};

	int i; for(i=0;i<G_N_ELEMENTS(cases);i++){
		GPos prev = cases[i];
		GPos next = cases[i];
		GPos orig = cases[i];
		q_to_prev(&prev, &q);
		q_to_next(&next, &q);

		char bbst1[64]; pos2bbst(&orig, bbst1);
		char bbst2[64]; pos2bbst(&prev, bbst2);
		char bbst3[64]; pos2bbst(&next, bbst3);
		dbg(1, "orig=%s prev=%s next=%s", bbst1, bbst2, bbst3);
	}
	FINISH_TEST;
}


void
test_pos_mu_conversion ()
{
	START_TEST;

	AyyiSongPos d[] = {{0,0,0}, {0,0,1}, {0,1,0}, {1,0,0}};
	uint64_t result[] = {0, 1, AYYI_MU_PER_SUB, AYYI_MU_PER_BEAT};
	for (int i=0;i<G_N_ELEMENTS(d);i++) {
		AyyiSongPos* pos = &d[i];
		if (ayyi_pos2mu(pos) != result[i]) FAIL_TEST("");
	}

	FINISH_TEST;
}


void
test_pos_sample_conversion ()
{
	START_TEST;
	static int64_t accuracy;

	bool _ayyi_pos_cmp(const AyyiSongPos* a, const AyyiSongPos* b)
	{
		//return TRUE if the two positions are not within sample accuracy.

		int64_t mu1 = ayyi_pos2mu((AyyiSongPos*)a);
		int64_t mu2 = ayyi_pos2mu((AyyiSongPos*)b);
		int64_t diff = ABS(mu1 - mu2);
		//dbg(0, "error=%Li", diff);
		return diff > accuracy;
	}

	float tempo[] = {60.0, 120.0, 240.0};
	int t; for(t=0;t<G_N_ELEMENTS(tempo);t++){
		((AyyiSongService*)ayyi.service)->song->bpm = tempo[t];
		accuracy = ayyi_samples2mu(1);
		dbg(1, "tempo=%.1f accuracy=%Lu", tempo[t], accuracy);

		AyyiSongPos d[] = {{0,0,0}, {0,0,1}, {0,0,accuracy}, {0, 0, AYYI_MU_PER_SUB/2}, {0,0,AYYI_MU_PER_SUB-1}, {0,1,0}, {0,1,1}, {0,1,2}, {0,1,3}, {0,AYYI_SUBS_PER_BEAT/2,0}, {0,AYYI_SUBS_PER_BEAT-1,AYYI_MU_PER_SUB-1}, {1,0,0}, {3,3,383}, {4,0,1}};
		int i; for(i=0;i<G_N_ELEMENTS(d);i++){
			uint32_t samples = ayyi_pos2samples(&d[i]);
			AyyiSongPos out; ayyi_samples2pos(samples, &out);
			ayyi_pos_is_valid(&out);
			char bbst1[32]; ayyi_pos2bbst(&d[i], bbst1);
			char bbst2[32]; ayyi_pos2bbst(&out, bbst2);
			dbg(1, "%s --> %s samples=%u", bbst1, bbst2, samples);
			if(_ayyi_pos_cmp(&d[i], &out)) ayyi_log_print(LOG_FAIL, "test %i", i);
			if(_ayyi_pos_cmp(&d[i], &out)) pwarn("test %i", i);

			//check that the displayed time is the same:
			GPos g_in, g_out;
			if(!songpos_ayyi2gui(&g_in, &d[i])) return;
			if(!songpos_ayyi2gui(&g_out, &out)) return;
			pos2bbst0(&g_in, bbst1);
			pos2bbst0(&g_out, bbst2);
			if(strcmp(bbst1, bbst2)){
				pwarn("%s --> %s", bbst1, bbst2);
			}
		}
	}
	FINISH_TEST;
}


void
test_pos_add ()
{
	START_TEST;
	bool pass = true;
	AyyiSongPos cases[][2] = {{{0,1,},{0,1,}}, {{1,},{1,}}, {{1,},{2,}}, {{0,AYYI_SUBS_PER_BEAT-2},{0,1}}, {{0,AYYI_SUBS_PER_BEAT-1},{0,1}}, {{0,AYYI_SUBS_PER_BEAT-1},{0,2}}, {{0,0,AYYI_MU_PER_SUB-1},{0,0,1}}};
	AyyiSongPos results[]  = {{0,2},           {2,},        {3,},        {0,AYYI_SUBS_PER_BEAT-1},         {1,},                             {1,1},                            {0,1}};
	int i; for(i=0;i<G_N_ELEMENTS(cases);i++){
		AyyiSongPos a = cases[i][0];
		ayyi_pos_add(&a, &cases[i][1]);
		char bbst0[32]; ayyi_pos2bbst(&cases[i][0], bbst0);
		char bbst1[32]; ayyi_pos2bbst(&cases[i][1], bbst1);
		char bbst2[32]; ayyi_pos2bbst(&a, bbst2);
		char bbst3[32]; ayyi_pos2bbst(&results[i], bbst3);
		dbg(0, "   %s + %s = %s", bbst0, bbst1, bbst2);
		if(ayyi_pos_cmp(&a, &results[i])){
			perr("%s != %s", bbst2, bbst3);
			pass = false;
		}
	}
	if (pass) FINISH_TEST;
	FAIL_TEST("");
}


void
test_pos_subtract ()
{
	typedef struct {
		AyyiSongPos pos1;
		AyyiSongPos pos2;
		AyyiSongPos result;
	} Case;

	Case cases[] = {
		{{0,},    {0,},                {0,}},
		{{1,},    {0,},                {1,}},
		{{1,},    {1,},                {0,}},
		{{2,},    {1,},                {1,}},
		{{1,1},   {1,},                {0,1}},
		{{2,1},   {1,},                {1,1}},
		{{1,1,1}, {0,},                {1,1,1}},
		{{1,1,1}, {1,},                {0,1,1}},
		{{2,},    {0,1},               {1,AYYI_SUBS_PER_BEAT-1}},
		{{2,1},   {0,1},               {2,}},
		{{0},     {1},                 {-1,}},
		{{1},     {2},                 {-1,}},
		{{0},     {0,1},               {0,-1}},
	};

	START_TEST;
	bool pass = true;
	for(int i=0;i<G_N_ELEMENTS(cases);i++){
		Case* c = &cases[i];
		AyyiSongPos a = c->pos1;
		AyyiSongPos b = c->pos2;
		g_assert(ayyi_pos_is_valid(&a));
		g_assert(ayyi_pos_is_valid(&b));
		char bbst1[32]; ayyi_pos2bbst(&a, bbst1);
		char bbst2[32]; ayyi_pos2bbst(&b, bbst2);
		dbg(0, "%i.%i", a.beat, a.sub);
		dbg(0, "%s - %s", bbst1, bbst2);
		ayyi_pos_sub(&a, &b);
		g_assert(ayyi_pos_is_valid(&a));

		char bbst3[32]; ayyi_pos2bbst(&a, bbst3);
		char bbst0[32]; ayyi_pos2bbst(&c->result, bbst0);
		if(ayyi_pos_cmp(&a, &c->result)){
			perr("%s != %s", bbst2, bbst0);
			pass = false;
		}
	}
	if (pass) FINISH_TEST;
	FAIL_TEST("");
}


void
test_bbss ()
{
	typedef struct {
		AyyiSongPos pos;
		char*       result;
	} Case;

	Case cases[] = {
		{{0,},                          "001:1:1:001"},
		{{0,0,1},                       "001:1:1:001"},
		{{0,1,0},                       "001:1:1:001"},
		{{0,4,0},                       "001:1:1:001"},
		{{0,4,AYYI_MU_PER_SUB-1},       "001:1:1:001"},
		{{0,5,0},                       "001:1:1:002"},
		{{0,6,0},                       "001:1:1:002"},
		{{0,10,0},                      "001:1:1:003"},
		{{0,1*AYYI_SUBS_PER_BEAT/4,},   "001:1:2:001"},
		{{0,2*AYYI_SUBS_PER_BEAT/4,},   "001:1:3:001"},
		{{0,4*AYYI_SUBS_PER_BEAT/4-1,}, "001:1:4:192"},
		{{1,},                          "001:2:1:001"},
		{{-1,0,0},                      "000:4:1:001"},
		{{-2,0,0},                      "000:3:1:001"},
		{{-3,0,0},                      "000:2:1:001"},
		{{-4,0,0},                      "000:1:1:001"},
		{{-5,0,0},                      "-01:4:1:001"},
		{{-6,0,0},                      "-01:3:1:001"},
		{{-7,0,0},                      "-01:2:1:001"},
		{{-8,0,0},                      "-01:1:1:001"},
		{{-9,0,0},                      "-02:4:1:001"},
		{{-12,0,0},                     "-02:1:1:001"},
		{{-16,0,0},                     "-03:1:1:001"},
		{{-32,0,0},                     "-07:1:1:001"},
		{{-128,0,0},                    "-31:1:1:001"},
		{{-1024,0,0},                   "-255:1:1:001"},
		{{-8192,0,0},                   "-2047:1:1:001"},
		{{-32768,0,0},                  "-8191:1:1:001"},
		{{-131072,0,0},                 "-32767:1:1:001"},
		{{-1048576,0,0},                "-262143:1:1:001"},
		{{-1,1,0},                      "000:4:1:001"},
		{{-1,6,0},                      "000:4:1:002"},
	};

	START_TEST;

	bool pass = true;
	for(int i=0;i<G_N_ELEMENTS(cases);i++){
		Case* c = &cases[i];
		AyyiSongPos a = c->pos;
		g_assert(ayyi_pos_is_valid(&a));
		char bbss[16]; ayyi_pos2bbss(&a, bbss);
		dbg(0, "%8i,%4i %s %s", a.beat, a.sub, bbss, c->result);
		if(strcmp(bbss, c->result)){
			perr("%s != %s", bbss, c->result);
			pass = false;
		}
	}
	if (pass) FINISH_TEST;
	FAIL_TEST("");
}


void
test_bbss2pos ()
{
	typedef struct {
		char*       bbss;
		AyyiSongPos result;
	} Case;

	Case cases[] = {
		{"001:1:1:001", {0,}},
		{"001:2:1:001", {1,}},
		{"002:1:1:001", {4,}},
		{"001:1:1:002", {0,5}},
		{"001:1:1:003", {0,10}},
		{"001:1:2:001", {0,1*AYYI_SUBS_PER_BEAT/4}},
		{"001:1:3:001", {0,2*AYYI_SUBS_PER_BEAT/4}},
		{"001:1:4:001", {0,3*AYYI_SUBS_PER_BEAT/4}},
		{"001:1:4:192", {0,AYYI_SUBS_PER_BEAT - 5}},
		{"001:1:4:191", {0,AYYI_SUBS_PER_BEAT - 10}},
	};

	START_TEST;

	bool pass = true;
	int i; for(i=0;i<G_N_ELEMENTS(cases);i++){
		Case* c = &cases[i];

		AyyiSongPos out;
		bbss2pos(c->bbss, &out);
		g_return_if_fail(ayyi_pos_is_valid(&out));

		char bbss [32]; ayyi_pos2bbss(&out, bbss);
		char bbss0[32]; ayyi_pos2bbss(&c->result, bbss0);
		char bbst1[32]; ayyi_pos2bbst(&c->result, bbst1);
		char bbst2[32]; ayyi_pos2bbst(&out, bbst2);
		if(ayyi_pos_cmp(&out, &c->result)){
			perr("%i: %s != %s orig=%s (%s != %s)", i, bbss, c->bbss, bbss0, bbst2, bbst1);
			pass = false;
		}
	}
	if (pass) FINISH_TEST;
	FAIL_TEST("");
}


void
test_bbst_increment ()
{
	START_TEST;

	// TODO check if it is intentional that reaching the maximum does not carry
	char* in[] = {"001:01:01:001", "001:01:01:383", "001:01:01:384"};
	char* out[] = {"001:01:01:002", "001:01:01:384", "001:01:01:000"};

	int i; for(i=0;i<G_N_ELEMENTS(in);i++){
		char a[32] = {'\0'};
		g_strlcpy(a, in[i], 32);
		bbst_increment(a);
		assert(!strcmp(a, out[i]), "%i: %s (expected %s)", i, a, out[i]);
	}

	FINISH_TEST;
}


void
test_fader_conversion ()
{
	START_TEST;

	double in[] = {1.0, 0.999,   0.5,  0.25, 0.001};
	//double r[]  = {0.0, -0.009, -0.6, -12.0, -60}; // expected db values (not currently checked)
	int i; for(i=0;i<G_N_ELEMENTS(in);i++){
		float db = am_gain2db(in[i]);
		float out = am_db2gain(db);
		dbg(0, "in=%.3f out=%.3f dB=%.3f", in[i], out, db);
		if(fabs(in[i] - out) > 0.1) pwarn("fail!");
	}

	FINISH_TEST;
}


void
test_observable ()
{
	START_TEST;

	static bool got_callback = false;
	Observable* observable = observable_new();
	void fn(Observable* o, AMVal val, gpointer _)
	{
		if(val.i == 3)
			got_callback = true;
	}
	observable_subscribe(observable, fn, NULL);
	observable_set(observable, (AMVal){.i=3});

	assert(observable->value.i == 3, "wrong value");
	assert(got_callback, "callback not called");

	observable_free(observable);

	{
		Observable* observable = observable_new_point(0, 0);
		observable->min.pt = (Ptf){0.0, 0.0};
		observable->max.pt = (Ptf){100.0, 100.0};
		Ptf pt = {100, 0};
		observable_point_set(observable, &pt.x, NULL);
		assert(observable->value.pt.x == 100.0, "wrong value (expected 100.0)");
		pt.x = 101.0;
		observable_point_set(observable, &pt.x, NULL);
		assert(observable->value.pt.x == 100.0, "(max) wrong value %f (expected 100.0)", observable->value.pt.x);
		pt.y = 101.0;
		observable_point_set(observable, NULL, &pt.y);
		assert(observable->value.pt.y == 100.0, "(max) wrong value %f (expected 100.0)", observable->value.pt.y);
		pt.x = -1.0;
		observable_point_set(observable, &pt.x, NULL);
		assert(observable->value.pt.x == 0.0, "(min) wrong value %f (expected 0.0)", observable->value.pt.x);
		pt.y = -1.0;
		observable_point_set(observable, NULL, &pt.y);
		assert(observable->value.pt.y == 0.0, "(min) wrong value %f (expected 0.0)", observable->value.pt.y);

		observable_free(observable);
	}

	FINISH_TEST;
}


void
test_filter_iterator ()
{
	START_TEST;

	{
		bool filter (void* item, gpointer user_data)
		{
			AMPart* part = item;
			return part->ident.idx == 1;
		}

		AMCollection* collection = (AMCollection*)am_list_new(0, NULL);
		assert(am_collection_length(collection) == 0, "len not 0");
		am_collection_append(collection, AYYI_NEW(AMPart, .ident.idx = 1));
		assert(am_collection_length(collection) == 1, "len not 1");

		FilterIterator* fi = filter_iterator_new (0, NULL, NULL, collection, filter, NULL);
		assert(fi, "filter not created");
		AMPart* part = (AMPart*)filter_iterator_next(fi);
		assert(part->ident.idx == 1, "1st item not correct");
		filter_iterator_unref(fi);

		am_collection_append(collection, AYYI_NEW(AMPart, .ident.idx = 2));
		fi = filter_iterator_new (0, NULL, NULL, collection, filter, NULL);
		part = (AMPart*)filter_iterator_next(fi);
		assert(part->ident.idx == 1, "1st item not correct");
		part = (AMPart*)filter_iterator_next(fi);
		assert(!part, "part iterator returned too many items");

		filter_iterator_unref(fi);
	}

	{
		bool filter (void* item, gpointer user_data)
		{
			AMTrack* track = item;
			return track->ident.idx == 1;
		}

		AMCollection* collection = (AMCollection*)am_track_list_new(0, NULL, NULL);
		assert(am_collection_length(collection) == 0, "len not 0");
		am_collection_append(collection, AYYI_NEW(AMTrack, .type = TRK_TYPE_AUDIO, .ident.idx = 1));
		assert(am_collection_length(collection) == 1, "len not 1");

		FilterIterator* fi = filter_iterator_new (0, NULL, NULL, collection, filter, NULL);
		assert(fi, "filter not created");
		AMTrack* track = (AMTrack*)filter_iterator_next(fi);
		assert(track, "1st track item not found");
		assert(track->ident.idx == 1, "1st item not correct");
		track = (AMTrack*)filter_iterator_next(fi);
		assert(!track, "track iterator returned too many items");
		filter_iterator_unref(fi);
	}

	FINISH_TEST;
}


void
test_part_input ()
{
	START_TEST;

	{
		AMPartInput* input = am_part_input_new();
		am_part_input_set_name(input, g_strdup("Name"));
		const gchar* name = am_part_input_get_name(input);
		assert(!strcmp(name, "Name"), "Incorrect name");
	}
	{
		AMPartInput* input = g_object_new(AM_TYPE_PART_INPUT, "name", g_strdup("Name"), NULL);
		const gchar* name = am_part_input_get_name(input);
		assert(!strcmp(name, "Name"), "Incorrect name");
	}

	FINISH_TEST;
}

void
test_automation ()
{
	START_TEST;

	AMTrack track = {0,};
	AMCurve* curve = curve_new(&track, VOL, NULL);

	curve_setsize(curve, 3);
	curve_setlength(curve, 3);

	BPath* path = (BPath*)curve->path;

	path[0] = (BPath){
		.code = MOVETO_OPEN,
		.x3 = 0.0,
		.y3 = 30.,
	};

	path[1] = (BPath){
		.code = LINETO,
		.x3 = 44100.,
		.y3 = 40.,
	};

	path[2] = (BPath){
		.code = BP_END,
		.x3 = 44100.,
		.y3 = 40.,
	};

	assert(curve->len == 3, "Curve length");

	float value = am_automation_get_value (curve, 0, 0);
	assert(value == 30., "value");

	value = am_automation_get_value (curve, 0, 22050);
	assert(value == 35., "value: %f", value);

	value = am_automation_get_value (curve, 1, 44100);
	assert(value == 40., "value %f", value);

	FINISH_TEST;
}


void
test_container ()
{
	START_TEST;

	AMArray* list = am_array_new(0, NULL, NULL, 0);
	assert(am_collection_length((AMCollection*)list) == 0, "length not zero");

	am_collection_append((AMCollection*)list, AYYI_NEW(AMItem,
		.ident = {
			.idx = 5,
		}
	));
	assert(am_collection_length((AMCollection*)list) == 1, "expected length 1, got %i", am_collection_length((AMCollection*)list));

	AMItem* found = am_collection_find_by_idx((AMCollection*)list, 5);
	assert(found && found->ident.idx == 5, "not found");

	FINISH_TEST;
}
