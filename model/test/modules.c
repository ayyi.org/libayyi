/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "ayyi/ayyi_shm.h"

typedef struct {
	const char* module_name;
	gpointer    user_data;
} TimeoutData;

typedef struct ModuleClosure {
	const char* module_name;
	AyyiFinish  finish;
	gpointer    user_data;
	int         timer;
	void*       module;
} MC;

#define _g_free0(var) (var = (g_free (var), NULL))

// for concurrent module usage, use START_MODULE_B instead (does not use static variables)
#define START_MODULE \
	printf("starting module: %s%s%s ...\n", ayyi_green, __func__, white); \
	g_return_if_fail(on_finish); \
	static gpointer __user_data; __user_data = _user_data; \
	static AyyiFinish __finish = NULL; \
	__finish = on_finish; \
	static int timer; \
	\
	static TimeoutData* __d; \
	__d = AYYI_NEW(TimeoutData, .module_name = __func__); \
	__d->user_data = _user_data; \
	\
	gboolean module_timeout (gpointer _d) \
	{ \
		printf("MODULE TIMEOUT! %s\n", ((TimeoutData*)_d)->module_name); \
		FAIL_TEST_TIMER("MODULE TIMEOUT!\n"); \
		g_clear_pointer(&__d, g_free); \
		return G_SOURCE_REMOVE; \
	} \
	timer = g_timeout_add(40000, module_timeout, __d);

#define FINISH_MODULE2 \
	{ g_source_remove(timer); \
	if(d){ \
		if(d->on_finish) d->on_finish(d->user_data); \
		g_free(d); \
	}}

#define FINISH_MODULE(OBJ) \
	{ \
		g_source_remove(timer); \
		g_clear_pointer(&__d, g_free); \
		__finish(OBJ, __user_data); \
	}

#define FAIL_MODULE_IF_ERROR \
	if(error && *error){ g_source_remove(timer); FAIL_TEST("> %s", (*error)->message); }

#define FAIL_MODULE_IF_ERROR_2 \
	if(error){ g_source_remove(timer); FAIL_TEST("> %s", error->message); }

//-----------------------------------------------------------------------

#define START_MODULE_B \
	printf("starting module: %s%s%s ...\n", ayyi_green, __func__, white); \
	g_return_if_fail(on_finish); \
	gboolean module_timeout(gpointer _mc) \
	{ \
		MC* mc = _mc; \
		printf("MODULE TIMEOUT! %s\n", mc->module_name); \
		FAIL_TEST_TIMER("MODULE TIMEOUT!\n"); \
		g_free(mc); \
		return G_SOURCE_REMOVE; \
	} \
	MC* mc = AYYI_NEW(MC, \
		.module_name = __func__, \
	); \
	MC* _mc = mc; /* needed by FINISH_MODULE */ \
	_mc->finish = on_finish; \
	mc->user_data = _user_data; \
	mc->timer = g_timeout_add(40000, module_timeout, mc);

#define FINISH_MODULE_B(OBJ) \
	{ \
	MC* mc = _mc; \
	if(mc->timer) g_source_remove(mc->timer); \
	if(mc->module) _g_free0(mc->module); \
	mc->finish(OBJ, mc->user_data); \
	g_free(mc); \
	}

#define FAIL_MODULE_IF_ERROR_B \
	MC* mc = _mc; \
	if(error && *error){ g_source_remove(mc->timer); FAIL_TEST("> %s", (*error)->message); }

//-----------------------------------------------------------------------

/*
 *  Modules are called by test functions.
 *  -they can do anything they like as long as they call the callback when finished.
 *  -on failure, the test is aborted imediately via FAIL_TEST()
 */

void
module__save_song(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE_B;

	void module__on_save(AyyiIdent id, GError** error, gpointer _mc)
	{
		FINISH_MODULE_B(id);
	}

	am_song__save(module__on_save, mc);
}


void
module__load_song_with_delay(AyyiFinish on_finish, gpointer _user_data, const char* _path)
{
	// does not save the current song.

	START_MODULE;

	dbg(0, "loading song: %s", _path);

	typedef struct
	{
		GCallback   on_unload;
		bool        unload_done;
		const char* path;
	} C;
	static C* cc;

	void module_on_unload (GObject* object, gpointer _c)
	{
		C* c = _c;
		assert((c == cc), "bad closure data");
		PF0;
		c->unload_done = true;
		g_signal_handlers_disconnect_by_func (song, c->on_unload, c);
	}

	void module_load__on_load__(AyyiIdent obj, GError** error, gpointer _c)
	{
		FAIL_MODULE_IF_ERROR;
		C* c = _c;
		assert((c == cc), "bad closure data");
		assert(song->loaded, "loaded flag not set");
		g_free((char*)c->path);
		g_free(c);
		FINISH_MODULE(AYYI_NULL_IDENT);
		PF;
	}

	C* c = cc = g_new0(C, 1);
	c->path = g_strdup(_path);
	c->on_unload = (GCallback)module_on_unload;

	am_song__connect("song-unload", G_CALLBACK(module_on_unload), c);
	am_song__load_new(c->path, module_load__on_load__, c);
}


void
module__save_and_load_song_with_delay(AyyiFinish on_finish, gpointer _user_data, const char* path)
{
	START_MODULE;
	/*
	static gboolean unloaded_done = false;

	void on_load(GObject* object, gpointer user_data)
	{
		gboolean load_pause_done(gpointer data)
		{
			PF;
			FINISH_MODULE(AYYI_NULL_IDENT);
			return STOP;
		}

		PF;
		if(!unloaded_done) pwarn("no unload signal received");
		assert(song->loaded, "");
		g_signal_handlers_disconnect_by_func (song, user_data, user_data);
		g_timeout_add(4000, load_pause_done, NULL);
	}
	*/
	void _load_finished(AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_MODULE(AYYI_NULL_IDENT);
	}

	void save_done(AyyiIdent obj, GError** error, gpointer data)
	{
		char* path = data;
		dbg(0, "path=%s", path);
		/*
		g_signal_connect(song, "song-load", G_CALLBACK(on_load), on_load);
		am_song__load_new(path, NULL, NULL);
		*/

		module__load_song_with_delay(_load_finished, NULL, path);
	}

	am_song__save(save_done, (void*)path);
}


#define remove_handler(OBJ, ID) {if(ID){ g_signal_handler_disconnect((gpointer)OBJ, ID); ID = 0; } else pwarn("disconnect: no handler id! %i", ID); }

/*
 *  Module to add an audio Part.
 *  -the part counting test has been removed because it doesnt work with concurrent tests.
 */
void
module__add_audio_part(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE_B;

	typedef struct {
		int handler_id;
		int check;
		bool got_signal;
	} C;

	mc->module = AYYI_NEW(C,
		.check = 5
	);

	void
	module__add_audio_part_done (AyyiIdent obj, GError** error, gpointer _mc)
	{
		FAIL_MODULE_IF_ERROR_B;
		C* c = mc->module;

		AyyiRegion* region = ayyi_song__audio_region_at(obj.idx);
		dbg(0, "idx=%i name='%s'", obj.idx, region ? region->name : NULL);
		assert(region, "region");

		AMPart* part = am_partmanager__get_by_id(region->id);
		assert(part, "cannot get AMPart");

		AMPoolItem* pool_item = part->pool_item;
		assert(pool_item, "PoolItem not set");

		assert(region->length > 1, "length not set");
		assert(region->length == ((Waveform*)pool_item)->n_frames, "part length not correct: %i %"PRIi64"i", region->length, ((Waveform*)pool_item)->n_frames);

		assert(part->track, "part has no track");
		AMTrack* track = part->track;

		AyyiSongPos part_end;
		am_part__end_pos(part, &part_end);

		AyyiSongPos track_start, track_end;
		am_track__get_start_end (track, &track_start, &track_end);

		char bbst1[64];
		ayyi_pos2bbst(&part->start, bbst1);
		char bbst2[64];
		ayyi_pos2bbst(&part_end, bbst2);

		char bbst3[64];
		ayyi_pos2bbst(&track_start, bbst3);
		char bbst4[64];
		ayyi_pos2bbst(&track_end, bbst4);

		assert(!ayyi_pos_is_after(&part_end, &track_end), "end not after");
		assert(!ayyi_pos_is_after(&track_start, &part->start), "start not before");

		assert(c->got_signal, "signal not received");

		if(c->handler_id) remove_handler(song->parts, c->handler_id); //useless as should have been already done below
		FINISH_MODULE_B(obj);
	}

	void module__add_audio_part__on_have_pool_item(AyyiIdent obj, gpointer _mc)
	{
		void module__add_audio_part__on_have_track(AyyiIdent obj, gpointer _mc)
		{
			MC* mc = _mc;
			C* c = mc->module;

			assert(ayyi_song__get_file_count(), "no files");

			int n_tracks = ayyi_song__get_audio_track_count();
			int i = 0;
			int t = g_random_int_range(0, n_tracks); //FIXME not random
			AyyiTrack* track = NULL;
			while((track = ayyi_song__audio_track_next(track))){
				if(i >= t){
					if(strcmp(track->name, "master") && strcmp(track->name, "Master")) break;
				}
				i++;
			}
			if(!track) FAIL_TEST("no audio tracks!")
			dbg(2, "track=%i", track->shm_idx);

			char name[AYYI_NAME_MAX];
			am_partmanager__make_name_unique(name, "New Part");

			void* ___user_data = NULL;
			FilterIterator* filter_iter = filter_iterator_new (0, NULL, NULL, am_pool, (Filter)pool_item_is_nonempty, ___user_data);
			AMPoolItem* pool_item = (AMPoolItem*)filter_iterator_next(filter_iter);
			filter_iterator_unref(filter_iter);
			assert(pool_item, "pool_item");

			void got_add_signal(GObject* _parts, AMPart* new_part, gpointer c)
			{
				assert((((C*)c)->check == 5), "closure check failed");
				((C*)c)->got_signal = true;
				remove_handler(song->parts, ((C*)c)->handler_id);
			}
			c->handler_id = g_signal_connect(song->parts, "add", G_CALLBACK(got_add_signal), c);
			dbg(2, "handler=%i", c->handler_id);

			uint32_t region_idx = AM_REGION_FULL;
			uint64_t len = ayyi_samples2mu(((Waveform*)pool_item)->n_frames);
			AyyiSongPos start = {get_random_int(240), get_random_int(4) * AYYI_SUBS_PER_BEAT / 4, 0};
			assert(ayyi_pos_is_valid(&start), "start position valid");
			int n = 0;
			bool position_is_ok = false;
			while(true){
				uint32_t samples = ayyi_pos2samples(&start);
				FilterIterator* filter_iter = filter_iterator_new (0, NULL, NULL, am_parts, (Filter)am_part__is_at_position_samples, GUINT_TO_POINTER(samples));
				AMPart* part = (AMPart*)filter_iterator_next(filter_iter);
				if(!part){
					GList* l = ((AMCollection*)song->parts)->pending;
					for(;l;l=l->next){
						AMPart* p = l->data;
						if(am_part__is_at_position(p, &start)){
							dbg(0, "found part in pending list");
							part = p;
							break;
						}
					}
				}
				if(part){
					// there is a part here already
					start = (AyyiSongPos){get_random_int(240), get_random_int(4) * AYYI_SUBS_PER_BEAT / 4};
					dbg(0, "new start=%i", start.beat);
					assert(ayyi_pos_is_valid(&start), "modified start position valid");
				}else{
					// there are no parts here.
					dbg(2, "no parts here");
					position_is_ok = true;
				}
				filter_iterator_unref(filter_iter);
				if(position_is_ok) break;
				n++;
				if(n > 511){
					FAIL_TEST("cannot find position");
					break;
				}
			}
			{
				GPos s;
				songpos_ayyi2gui(&s, &start);
				char bbst1[64]; pos2bbst(&s, bbst1); dbg(1, "going with: %s (%i) %i", bbst1, pos2samples(&s), ayyi_pos2samples(&start));
			}
			am_song__add_part(AYYI_AUDIO, region_idx, pool_item, track->shm_idx, &start, len, 0, (char*)name, 0, module__add_audio_part_done, mc);
		}

		module__ensure_audio_track(module__add_audio_part__on_have_track, _mc);
	}
	module__ensure_pool_item(module__add_audio_part__on_have_pool_item, mc);
}


void
module__add_n_audio_parts(AyyiFinish on_finish, int n, gpointer _user_data)
{
	START_MODULE_B;

	typedef struct _c
	{
		int target_n_regions;
		void (*callback)(AyyiIdent, MC*);
	} C;

	void _add_part_finished(AyyiIdent obj, MC* _mc)
	{
		C* c = _mc->module;
		assert(c->callback, "");
		if (TEST.current.finished) return; // aborted elsewhere
		dbg(0, "%i > %i ?", ayyi_song__get_audio_region_count(), c->target_n_regions);
		if(ayyi_song__get_audio_region_count() >= c->target_n_regions){
			dbg(0, "target reached. Finishing...");
			FINISH_MODULE_B(obj);
			return;
		}

		module__add_audio_part((gpointer)c->callback, _mc);
	}

	mc->module = AYYI_NEW(C,
		.callback = _add_part_finished,
		.target_n_regions = n
	);

	module__add_audio_part((gpointer)_add_part_finished, mc);
}


void
module__delete_audio_part(AyyiFinish on_finish, AMPart* part, gpointer _user_data)
{
	START_MODULE_B;
	typedef struct _closure {
		int   handler;
		void* callback;
		bool  got_signal; // dont really need this, instead check handler is 0
	} C;

	static int n_parts; n_parts = am_collection_length(am_parts);
	g_return_if_fail(part);

	void module__delete_audio_part_done(AyyiIdent id, GError** error, gpointer _mc)
	{
		C* c = ((MC*)_mc)->module;
		dbg(0, "n_parts=%i-->%i", n_parts, am_collection_length(am_parts));
		if(n_parts - 1 != am_collection_length(am_parts)) FAIL_TEST("wrong part count");
		if(c->handler) remove_handler(song->parts, c->handler);
#ifdef DEBUG
		gulong n = g_signal_handler_find(song->parts, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, c->callback, NULL);
		dbg(0, "handler found: %lu (should be 0)", n);
#endif
		FINISH_MODULE_B(id);
	}

	void got_del_signal(GObject* _parts, AMPart* new_part, gpointer user_data)
	{
		PF;
		C* c = user_data;
		c->got_signal = true;
		remove_handler(song->parts, c->handler);
		// will this generate a warning?
		gulong n = g_signal_handler_find(song->parts, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, c->callback, NULL);
		assert(!n, "handler found: %lu (should be 0)", n);
	}

	mc->module = AYYI_NEW(C,
		.callback = got_del_signal
	);
	((C*)mc->module)->handler = g_signal_connect(song->parts, "delete", G_CALLBACK(got_del_signal), mc->module);
#ifdef DEBUG
	gulong n = g_signal_handler_find(song->parts, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, got_del_signal, NULL);
	dbg(0, "handler found: %lu", n);
#endif

	am_song__remove_part(part, module__delete_audio_part_done, mc);
}


void
module__delete_random_part(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	void module__delete_random_part_done(AyyiIdent id, GError** error, gpointer user_data)
	{
		FAIL_MODULE_IF_ERROR;
		FINISH_MODULE(id);
	}

	GRand* rand = g_rand_new();
	int r = g_rand_int_range(rand, 0, am_collection_length(am_parts) - 1);

	AMPart* part = g_list_nth_data(song->parts->list, r);
	assert(part, "part");

	am_song__remove_part(part, module__delete_random_part_done, NULL);
}


void
module__delete_all_parts(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	typedef struct {
		int handler_id;
		int n_deleted;
		int n_to_delete;
		bool finished;
	} C;
	C* c = g_new0(C, 1);

	void _finish(C* c)
	{
		remove_handler(am_parts, c->handler_id);
		assert(!c->finished, "module already finished!");
		if(am_collection_length(am_parts)) am_partmanager__print_list();
		assert(am_collection_length(am_parts) == 0, "n_parts == 0");
		c->finished = true;
		g_free(c);
		FINISH_MODULE(AYYI_NULL_IDENT);
	}

#ifdef DELETE_PARTS_BY_MODEL_LIST //this will _not_ delete regions that are not currently assigned to tracks.
	void _delete_all_audio_parts__on_delete(GObject* _song, AMPart* part, gpointer user_data)
	{
		C* c = user_data;
		c->n_deleted++;
		dbg(0, "%i parts deleted", c->n_deleted);
#ifdef DELETE_PARTS_BY_MODEL_LIST
		if(!am_collection_length(am_parts)){
#else
		if(!ayyi_song__get_audio_region_count() && !ayyi_song__get_midi_region_count()){
#endif

#if 0
			remove_handler(am_parts, c->handler_id);
			assert(!c->finished, "module already finished!");
			c->finished = true;
			g_free(c);
			FINISH_MODULE(AYYI_NULL_IDENT);
#endif
			_finish(c);
		}
	}

	c->handler_id = g_signal_connect(am_parts, "delete", G_CALLBACK(_delete_all_audio_parts__on_delete), c);

	GList* l = song->parts->list;
	c->n_to_delete = g_list_length(l);
	for(;l;l=l->next){
		AMPart* part = l->data;
		dbg(0, "  removing: %i", part->ayyi->shm_idx);
		if(PART_IS_AUDIO(part))
			ayyi_song__delete_audio_region((AyyiAudioRegion*)part->ayyi);
		else
			ayyi_song__delete_midi_region((AyyiMidiRegion*)part->ayyi);
	}
	dbg(0, "parts found: %i", g_list_length(song->parts->list));
#else
	c->n_to_delete = 0;
	AyyiRegion* part = NULL;
	while((part = ayyi_song__audio_region_next(part))){
		if(!(part->flags & deleted)){
			c->n_to_delete++;
			ayyi_song__delete_audio_region(part);
		}
	}
	{
	AyyiMidiRegion* part = NULL;
	while((part = ayyi_song__midi_region_next(part))){
		if(!(part->flags & deleted)){
			c->n_to_delete++;
			ayyi_song__delete_midi_region((AyyiMidiRegion*)part);
		}
	}
	}
#endif

	gboolean container_is_empty(gpointer _c)
	{
		dbg(2, "n_regions=%i+%i", ayyi_song__get_audio_region_count(), ayyi_song__get_midi_region_count());
		return ayyi_container_is_empty(&((AyyiSongService*)ayyi.service)->song->audio_regions) && ayyi_container_is_empty(&((AyyiSongService*)ayyi.service)->song->midi_regions);
	}

	void wait_done(AyyiIdent id, GError** e, gpointer _c)
	{
		PF0;
		_finish((C*)_c);
	}

	//reset_timeout(30000);
	if(c->n_to_delete)
		am_wait_for(container_is_empty, wait_done, c);
	else
		_finish(c);
}


void
module__select_part(AyyiFinish on_finish, AMPart* part, gpointer _user_data)
{
	//if part is NULL, use first available part.

	START_MODULE;
	static int handler; handler = 0;

	assert(song->parts->list, "no parts");

	if(!part){
		part = song->parts->list->data;
	}

	void got_add_signal(GObject* _parts, AMPart* new_part, gpointer user_data)
	{
		remove_handler(song->parts, handler);
		FINISH_MODULE(AYYI_NULL_IDENT);
	}
	handler = g_signal_connect(song->parts, "selection-change", G_CALLBACK(got_add_signal), NULL);

	am_collection_selection_replace (am_parts, g_list_append(NULL, part), NULL);
}


void
module__split_part(AyyiFinish on_finish, AMPart* part, gpointer _user_data)
{
	//split a part in half.

	START_MODULE;

	assert(part, "part");
	dbg(0, "%s: len=%i", part->name, ayyi_pos2samples(&part->length));
	assert(ayyi_pos2samples(&part->length) > 1, "part too short to split");

	static int n_parts; n_parts = g_list_length(song->parts->list);

	void _module_split_part_finished(AyyiIdent obj, GError** error, gpointer data)
	{
		FAIL_MODULE_IF_ERROR;
		assert(g_list_length(song->parts->list) == n_parts + 1, "incorrect number of parts. %i != %i", g_list_length(song->parts->list), n_parts + 1);
		PF;
		FINISH_MODULE(obj);
	}

	AyyiSongPos pos = part->length;
	ayyi_pos_divide(&pos, 2);
	ayyi_pos_add(&pos, &part->start);

	char start[32];
	char len[32];
	char end[32];
	ayyi_pos2bbst(&part->start, start);
	ayyi_pos2bbst(&part->length, len);
	ayyi_pos2bbst(&pos, end);
	dbg(0, "start=%s length=%s cut=%s", start, len, end);

	assert(ayyi_pos_is_after(&pos, &part->start), "cut at start");

	am_part_split(part, &pos, _module_split_part_finished, NULL);
}


void
module__select_track(AyyiFinish on_finish, AMTrack* track, gpointer _user_data)
{
	//if part is NULL, use first available part.

	START_MODULE;
	static int handler; handler = 0;

	assert(am_track_list_at(song->tracks, 0), "no tracks");

	if(!track){
		track = am_track_list_at(song->tracks, 0);
	}

	void got_selection_signal(GObject* _parts, AMTrack* new_track, gpointer user_data)
	{
		remove_handler(song->tracks, handler);
		FINISH_MODULE(AYYI_NULL_IDENT);
	}
	handler = g_signal_connect(song->tracks, "selection-change", G_CALLBACK(got_selection_signal), NULL);

	am_collection_selection_replace (am_tracks, g_list_append(NULL, track), NULL);
}


void
module__add_audio_track(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;
	static bool got_signal = false;
	static int handler_id;
	static int count; count = am_track_list_count(song->tracks);

	void
	module_on_track_create (AyyiIdent id, const GError* error, gpointer user_data)
	{
		FAIL_MODULE_IF_ERROR_2

		assert(got_signal, "got signal");
		remove_handler(song->channels, handler_id);

#ifdef DEBUG
		AyyiIdx new_object_idx = id.idx;
		AyyiTrack* track = ayyi_song__audio_track_at(new_object_idx);
		dbg(0, "idx=%i name='%s'", new_object_idx, track ? track->name : NULL);
#endif

		assert(am_track_list_count(song->tracks) == count + 1, "track count: %i (expected %i)", am_track_list_count(song->tracks), count + 1);

		assert(ayyi_verify_playlists(), "playlist verify");

		FINISH_MODULE(id)
	}

	void got_add_signal(GObject* _song, AMChannel* new_channel, gpointer user_data)
	{
		got_signal = true;
		remove_handler(song->channels, handler_id);
	}

	handler_id = g_signal_connect(song->channels, "add", G_CALLBACK(got_add_signal), NULL);

	am_song_add_track(TRK_TYPE_AUDIO, "New Track", 1, module_on_track_create, _user_data);
}


void
module__delete_audio_track (AyyiFinish on_finish, gpointer _user_data, AMTrack* trk)
{
	START_MODULE;

	typedef struct {
		int      handler_id;
		bool     got_signal;
		gpointer user_data;
		int      count;
	} C;

	void module__on_track_delete (AyyiIdent id, GError** error, gpointer user_data)
	{
		PF;
		C* c = user_data;
		FAIL_MODULE_IF_ERROR

		assert(c->got_signal, "got signal");
		remove_handler(song->channels, c->handler_id);

		assert(am_track_list_count(song->tracks) == c->count - 1, "track count %i, expected %i", am_track_list_count(song->tracks), c->count - 1);

		assert(ayyi_verify_playlists(), "playlist verify");

		//iterating over the tracks will check that the ayyi route exists
		AMTrack* trk = NULL;
		AMIter i;
		am_collection_iter_init (am_tracks, &i);
		while((trk = am_collection_iter_next(am_tracks, &i))){
		}

		g_free(c);
		FINISH_MODULE(id);
	}

	void got_remove_signal (GObject* _song, AMChannel* new_channel, gpointer user_data)
	{
		PF;
		C* c = user_data;
		c->got_signal = true;
		remove_handler(song->channels, c->handler_id);
	}

	C* c = AYYI_NEW(C,
		.user_data = _user_data,
		.count = am_track_list_count(song->tracks)
	);

	assert(am_track_list_count(song->tracks) > -1, "track count %i", am_track_list_count(song->tracks));

	//this is an additional test, to make sure the mixer is properly updated.
	c->handler_id = g_signal_connect(song->channels, "delete", G_CALLBACK(got_remove_signal), c);

	am_song_remove_track(trk, module__on_track_delete, c);
}


void
module__delete_tracks_by_name(AyyiFinish on_finish, gpointer _user_data, char** track_names)
{
	START_MODULE;
	FINISH_MODULE(AYYI_NULL_IDENT);
}


void
module__delete_all_tracks (AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	void module__on_delete (AyyiIdent id, GError** error, gpointer data)
	{
		FAIL_MODULE_IF_ERROR
		assert(ayyi_verify_playlists(), "playlist verify");
		assert(am_track_list_get_visible_count(song->tracks) == MIN_TRACKS, "tracks not cleared: %i", am_track_list_get_visible_count(song->tracks));
		assert(ayyi_song__get_playlist_count() == MIN_TRACKS, "playlists not cleared (%i != %i)", ayyi_song__get_playlist_count(), MIN_TRACKS);
		FINISH_MODULE(id);
	}

	void module__delete_all_tracks__on_parts_deleted (AyyiIdent id, gpointer data)
	{
		assert(ayyi_verify_playlists(), "playlist verify");
		assert(am_track_list_get_visible_count(song->tracks) == ayyi_song__get_playlist_count(), "playlist count: %i (expected %i)", ayyi_song__get_playlist_count(), am_track_list_get_visible_count(song->tracks));

		am_song__clear_tracks(module__on_delete, data);
	}

	assert(ayyi_verify_playlists(), "playlists not verified");

	//its not clear whether removing a tracks parts is neccesary.
	module__delete_all_parts(module__delete_all_tracks__on_parts_deleted, _user_data);
}


void
module__add_midi_track (AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;
	assert(ayyi_verify_playlists(), "playlist verify");

	void test_add_midi_track_done (AyyiIdent id, const GError* error, gpointer user_data)
	{
		FAIL_MODULE_IF_ERROR_2;
		assert(ayyi_song__get_midi_track_count(), "no midi tracks found");
		dbg(0, "n_midi_tracks=%i", ayyi_song__get_midi_track_count());
		AMTrack* track = am_track_list_find_by_ident(song->tracks, id);
		assert(track, "track not found");
		assert(track->type == TRK_TYPE_MIDI, "wrong track type")
		gboolean playlists_ok = ayyi_verify_playlists();
		if(!playlists_ok) ayyi_song__print_playlists();
		assert(playlists_ok, "playlist verify");
		FINISH_MODULE(id);
	}

	char unique[256];
	ayyi_track__make_name_unique(unique, "Test Midi Track");

	am_song_add_track(TRK_TYPE_MIDI, unique, 1, test_add_midi_track_done, _user_data);
}


void
module__add_midi_part (AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	typedef struct {
		int handler_id;
		bool got_signal;
	} C;

	void module__add_midi_part__on_have_track (AyyiIdent obj, gpointer _c)
	{
		C* c = _c;

		AyyiMidiTrack* track = ayyi_song__midi_track_next (NULL);
		assert(track, "no tracks");

		void got_add_signal (GObject* _parts, AMPart* new_part, gpointer _c)
		{
			C* c = _c;
			c->got_signal = true;
			remove_handler(song->parts, c->handler_id);
			c->handler_id = 0;
		}
		c->handler_id = g_signal_connect(song->parts, "add", G_CALLBACK(got_add_signal), c);
		dbg(2, "handler=%i", c->handler_id);

		void module__add_midi_part_done (AyyiIdent obj, GError** error, gpointer _c)
		{
			PF;
			C* c = _c;
			assert(c->got_signal, "signal not received");

			if(c->handler_id) remove_handler(song->parts, c->handler_id);
			g_free(c);
			FINISH_MODULE(obj);
		}

		char* name = ayyi_song_container_next_name (&((AyyiSongService*)ayyi.service)->song->midi_regions, "Midi Part");
		GPos l = {4, 0, 0};
		uint64_t len = pos2mu (&l);
		AyyiSongPos pos = {1, 0, 0};

		am_song__add_part(AYYI_MIDI, -1, NULL, track->shm_idx, &pos, len, 0, name, 0, module__add_midi_part_done, c);

		g_free(name);
	}

	module__ensure_midi_track(module__add_midi_part__on_have_track, g_new0(C, 1));
}


void
module__delete_midi_part(AyyiFinish on_finish, AMPart* part, gpointer _user_data)
{
	START_MODULE;
	assert(part, "no part arg");

	void module__delete_midi_part_done(AyyiIdent obj, GError** error, gpointer data)
	{
		FAIL_MODULE_IF_ERROR;
		AMPart* part = data;
		FINISH_MODULE(part->ident);
	}
	am_song__remove_part(part, module__delete_midi_part_done, part);
}


void
module__add_midi_notes(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE_B;

	static int max = 4;
	typedef struct {
		int      i;
		AMPart*  part;
		int      n_events;
		AyyiMidiNote note;
	} C;

	void module__add_midi_notes_all_done(AyyiIdent id, GError** error, gpointer _mc)
	{
		FINISH_MODULE_B(id);
	}

	void module__add_midi_note_done(AyyiIdent id, GError** error, gpointer _mc)
	{
		PF;
		FAIL_MODULE_IF_ERROR_B;
		C* c = mc->module;

		AMPart* part = am_list_find_by_ident(song->parts, id);
		assert(part, "cannot find part %i", id.idx);
		//am_midi_part__print_events(part);

		AyyiMidiRegion* region = (AyyiMidiRegion*)c->part->ayyi; //ayyi_song__midi_region_at(part->ayyi->shm_idx);
		int n_events = ayyi_song_container_count_items(&region->events);
		assert(n_events == c->n_events + c->i + 1, "wrong event count: %i (expected %i)", n_events, c->n_events + c->i + 1);

		// we dont have a reference to the note, so we check all the note events
		bool found = false;
		{
			if(n_events){
				printf("      events (%i):\n", n_events);
				int n = 0;
				AyyiMidiNote* note = NULL;
				while((note = (AyyiMidiNote*)ayyi_song_container_next_item(&region->events, note))){
					printf("        %i: %i %i %i %i\n", n, note->note, note->velocity, note->start, note->length);
					if(note->note == c->note.note && note->velocity == c->note.velocity && note->start == c->note.start && note->length == c->note.length){
						found = true;
						break;
					}
					n++;
				}

			}
			else printf("      no events\n");
		}
		assert(found, "note not found");

		if(++c->i < max){
			c->note.note++;
			c->note.velocity++;
			c->note.start += (int)pos2samples(&(GPos){0, 1, 0});

			AyyiMidiNote* note = ayyi_midi_note_new();
			*note = c->note;
			am_song__midi_note_add(c->part, note, module__add_midi_note_done, mc);
		}else{
			module__add_midi_notes_all_done(id, error, _mc);
		}
	}

	AMPart* part = NULL;
	GList* l = song->parts->list;
	for(;l;l=l->next){
		if(PART_IS_MIDI((AMPart*)l->data)){
			part = l->data;
			break;
		}
	}
	assert(part, "part");
	AyyiMidiRegion* region = (AyyiMidiRegion*)part->ayyi;

	mc->module = AYYI_NEW(C,
		.part = part,
		.i = 0,
		.n_events = ayyi_song_container_count_items(&region->events),
		.note = {
			.note = 60,
			.velocity = 100,
			.length = (int)pos2samples(&(GPos){0, 1, 0}),
		}
	);

	AyyiMidiNote* note = ayyi_midi_note_new();
	*note = ((C*)mc->module)->note;
	am_song__midi_note_add(part, note, module__add_midi_note_done, mc);
}


static bool
am_part__has_notes(void* part, gpointer user_data)
{
	AyyiMidiRegion* region = (AyyiMidiRegion*)((AMPart*)part)->ayyi;

	return ayyi_song_container_count_items(&region->events);
}


void
module__modify_midi_notes(AyyiFinish on_finish, gpointer _user_data)
{
	#define MAX_NOTES 4
	START_MODULE;

	typedef struct _C C;
	struct _C {
		AMPart*  part;
		int      i;
		int      n_notes;
		AyyiMidiNote note;
		void     (*next)(C*);
	};

	void module__modify_midi_note_done(AyyiIdent id, GError** error, gpointer _mc)
	{
		PF;
		FAIL_MODULE_IF_ERROR_B;
		C* c = _mc;

		AyyiMidiRegion* region = (AyyiMidiRegion*)c->part->ayyi;
		assert(ayyi_song_container_count_items(&region->events) == c->n_notes, "note count: %i (expected %i)", ayyi_song_container_count_items(&region->events), c->n_notes);

		AyyiMidiNote* note = am_midi_part__get_note_by_idx(c->part, c->i);
		assert(c->note.note == note->note, "note mismatch %i (expected %i)", note->note, c->note.note);
		assert(c->note.velocity == note->velocity, "incorrect velocity: %i (expected %i)", note->velocity, c->note.velocity);
		assert(c->note.start == note->start, "incorrect start: %i (expected %i)", note->start, c->note.start);
		assert(c->note.length == note->length, "incorrect length: %i (expected %i)", note->length, c->note.length);

		if(c->i++ < MAX_NOTES){
			c->next(c);
		}else{
			g_free(c);
			FINISH_MODULE(id);
		}
	}

	FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_parts, am_part__has_notes, NULL);
	AMPart* part = (AMPart*)filter_iterator_next(i);
	filter_iterator_unref(i);
	assert(part, "part");

	AyyiMidiRegion* region = (AyyiMidiRegion*)((AMPart*)part)->ayyi;

	void next_note(C* c)
	{
		assert(c->n_notes >= MAX_NOTES, "not enough notes");

		AMPartMidi* m = (AMPartMidi*)c->part;
		g_clear_pointer(&m->note_selection, g_list_free);
		m->note_selection = g_list_append(m->note_selection, am_midi_part__get_note_by_idx(c->part, c->i));

		// copy the note selection to a new list
		GList* note_list = NULL;
		note_selection_list__new(&note_list, c->part);

		NoteSelectionListItem* item = note_list->data;
		assert(item->notes, "item has notes");

		if(item->notes){
			TransitionalNote* n = item->notes->data;
			n->transient.note += 1;
			n->transient.velocity = n->transient.velocity + get_random_int(10) * (n->transient.velocity > 100 ? -1 : 1);
			note_selection_list__print(&note_list);
			c->note = n->transient;

			am_midi_part__set_notes(c->part, item->notes, module__modify_midi_note_done, c);
		}
	}

	next_note(AYYI_NEW(C,
		.part = part,
		.n_notes = ayyi_song_container_count_items(&region->events),
		.next = next_note
	));
}


void
module__delete_midi_note (AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;
	static int n_notes;
	static int to_delete[] = {1, 0, -1};
	static int i = 0;

	void module__delete_midi_note_done(AyyiIdent id, GError** error, gpointer _)
	{
		FAIL_MODULE_IF_ERROR;
		AMPart* part = am_song__get_part_by_ident(id);
		assert(part, "cannot get part from ident");

		AyyiMidiRegion* region = (AyyiMidiRegion*)part->ayyi;
		int n_notes2 = ayyi_song_container_count_items(&region->events);
		assert(n_notes2 == n_notes - 1, "Note not deleted");
		n_notes = n_notes2;

		if(++i < G_N_ELEMENTS(to_delete)){
			GList* notes = g_list_append(NULL, am_midi_part__get_note_by_order(part, to_delete[i]));
			am_midi_part__remove_notes ((MidiPart*)part, notes, module__delete_midi_note_done, NULL);
		}else{
			FINISH_MODULE(id);
		}
	}

	FilterIterator* f = filter_iterator_new (0, NULL, NULL, am_parts, am_part__has_notes, NULL);
	AMPart* part = (AMPart*)filter_iterator_next(f);
	filter_iterator_unref(f);
	assert(part, "part");

	AyyiMidiRegion* region = (AyyiMidiRegion*)part->ayyi;
	n_notes = ayyi_song_container_count_items(&region->events);
	assert(n_notes > 0, "part has no notes");
	assert(n_notes > 2, "this test requires a part with more than 2 notes");

	GList* notes = g_list_append(NULL, am_midi_part__get_note_by_order(part, to_delete[i]));
	am_midi_part__remove_notes ((MidiPart*)part, notes, module__delete_midi_note_done, NULL);
}


	typedef struct _module_data {
		AyyiFinish  on_finish;
		gpointer    user_data;
		const char* name;
	} ModuleData;


/*
 *  filename can be either full path, or just leaf-name.
 *  If leaf-name is used, a set of known directories will be searched.
 */
void
module__load_wav_fixture (AyyiFinish on_finish, gpointer _user_data, const char* _filename)
{
	START_MODULE;

	char* filename = NULL;
	filename = g_path_is_absolute(_filename)
		? g_strdup(_filename)
		: find_wav(_filename);

	void module__load_wav_fixture_done (AyyiIdent id, GError** error, gpointer user_data)
	{
		PF;
		FAIL_MODULE_IF_ERROR;
		ModuleData* d = user_data;

		assert(ayyi_song__have_file(d->name), "file not loaded");

		dbg(0, "n_files=%i", g_list_length(song->pool->list));
		assert(g_list_length(song->pool->list) > 0, "pool empty");

		const AMPoolItem* item = am_pool__get_item_from_idx(id.idx);
		assert(item, "cannot find pool item: id=%i.%i", id.type, id.idx);
		AMPoolItem* pool_item = (AMPoolItem*)item;
		assert(pool_item->state == AM_POOL_ITEM_OK, "pool item bad state: %i", pool_item->state);
		assert(pool_item->channels, "pool item has no channels");

		g_free((char*)d->name);
		g_free(d);
		FINISH_MODULE(id)
	}

	am_song__add_file (filename, module__load_wav_fixture_done, AYYI_NEW(ModuleData,
		.on_finish = on_finish,
		.user_data = _user_data,
		.name = filename
	));
}


void
module__delete_audio_file (AyyiFinish on_finish, gpointer _user_data, const char* filename)
{
	START_MODULE;
	assert(filename && strlen(filename), "filename not specified");
	dbg(0, "%s", filename);

	if(!g_list_length(song->pool->list)) FINISH_MODULE(AYYI_NULL_IDENT);

	typedef struct {
		const char* filename;
	} Closure;

	void module__delete_audio_file_done (AyyiIdent id, GError** error, gpointer data)
	{
		PF;
		Closure* c = data;
		FAIL_MODULE_IF_ERROR;

		AyyiFilesource* file = ayyi_filesource_get_by_name(((Closure*)data)->filename);
		assert(!file, "file not cleared");

		g_free((char*)c->filename);
		g_free(c);
		FINISH_MODULE(id);
	}

	AMPoolItem* p = am_pool__get_item_by_orig_name (filename);
	assert(p, "cannot find pool_item");
	assert(p->channels, "pool item has no channels");

	am_song__remove_file(p, module__delete_audio_file_done, AYYI_NEW(Closure,
		.filename = find_wav(filename),
	));
}


void
module__delete_all_audio_files (AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	dbg(0, "n_files=%i", am_collection_length(am_pool));
	if(!am_collection_length(am_pool)) FINISH_MODULE(AYYI_NULL_IDENT);

	void module__delete_all_audio_files_done(AyyiIdent id, GError** error, gpointer data)
	{
		PF;
		int n_files = g_list_length(song->pool->list);
		dbg(0, "n_files=%i", n_files);
		if(!n_files){
			FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_parts, am_part__is_audio, NULL);
			AMPart* part = (AMPart*)filter_iterator_next(i);
			filter_iterator_unref(i);

			assert(!part, "n audio parts not zero");
			FINISH_MODULE(id)
		}
	}

	GList* l = song->pool->list;
	for(;l;l=l->next){ //warning! iterating over a list that is changing.
		am_song__remove_file((AMPoolItem*)l->data, module__delete_all_audio_files_done, NULL);
	}
}


void
module__delete_parts_by_audio_file (AyyiFinish on_finish, gpointer _user_data, AMPoolItem* pool_item)
{
	START_MODULE;

	void module__delete_parts_by_audio_file_done(AyyiIdent id, GError** error, gpointer data)
	{
		FAIL_MODULE_IF_ERROR;
		FINISH_MODULE(id)
	}

	am_song__remove_parts_by_file(pool_item, module__delete_parts_by_audio_file_done, NULL);
}


void
module__add_plugin(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	void module__add_plugin_done(AyyiIdent id, GError** error, gpointer data)
	{
		PF;
		//dbg(0, "error=%p", error ? *error : NULL);
		FAIL_MODULE_IF_ERROR;
		FINISH_MODULE(id)
	}

	//AMIter i;
	//channel_iter_init(&i);
	//AMChannel* channel = channel_next(&i);
	FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_channels, (Filter)am_channel__is_type, (gpointer)AM_CHAN_INPUT);
	AMChannel* channel = (AMChannel*)filter_iterator_next(i);
	filter_iterator_unref(i);

	AyyiIdx plugin_slot = 0;
	const char* plugin_name = vst_plugin_fixture;

	am_plugin__change(channel, plugin_slot, plugin_name, module__add_plugin_done, NULL);
}


void
module__ensure_pool_item(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;
	if(ayyi_song__get_file_count()){
		FINISH_MODULE(AYYI_NULL_IDENT)
		return;
	}

	dbg(0, "wav fixture being loading...");
	void on_wav_added(AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_MODULE(obj)
	}
	module__load_wav_fixture(on_wav_added, NULL, mono_wav_fixture);
}


void
module__ensure_audio_track(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE_B;

	AMTrack* track = am_track_list_get_first_visible(song->tracks, TRK_TYPE_AUDIO);
	if(track){
		FINISH_MODULE_B(track->ident)
	}else{

		void module__ensure_audio_track_done(AyyiIdent obj, gpointer _mc)
		{
			FINISH_MODULE_B(obj)
		}

		module__add_audio_track(module__ensure_audio_track_done, mc);
	}
}


void
module__ensure_mono_audio_track(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_tracks, (Filter)am_track__is_mono, NULL);
	AMTrack* track = (AMTrack*)filter_iterator_next(i);
	filter_iterator_unref(i);

	if(track){
		FINISH_MODULE(track->ident)
	}else{
		void module__ensure_mono_audio_track_done(AyyiIdent obj, gpointer data)
		{
			FINISH_MODULE(obj)
		}

		module__add_audio_track(module__ensure_mono_audio_track_done, NULL);
	}
}


void
module__ensure_pannable_track(AyyiFinish on_finish, gpointer _user_data)
{
	//currently only mono tracks are pannable.
	//the first mono track will be used, and a panner will be added to it if necessary.

	START_MODULE_B;

	FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_tracks, (Filter)am_track__is_mono, NULL);
	AMTrack* track = (AMTrack*)filter_iterator_next(i);
	filter_iterator_unref(i);

	void module__ensure_mono_audio_track_done(AyyiIdent obj, gpointer _mc)
	{
		AMChannel* ch = channels__get_by_index(obj.idx);
		assert(ch, "channel not found");
		AyyiChannel* ac = ayyi_mixer__channel_at(obj.idx);
		assert(ac, "ayyi channel not found");

		void module__ensure_pannable_track_done(AyyiIdent obj, GError** error, gpointer _mc)
		{
			PF0;
			dbg(0, "idx=%i", obj.idx);
			FAIL_MODULE_IF_ERROR_B;
			FINISH_MODULE_B(obj)
		}

		dbg(0, "ch=%i has_pan=%i", obj.idx, ac->has_pan);
		if(ac->has_pan){
			FINISH_MODULE_B(obj);
		}else{
			/*  //can delete
			AMTrack* track = am_track_list_find_audio_by_idx(song->tracks, obj.idx);
			AyyiConnection* connection = NULL;
			while((connection = ayyi_connection__next_input(connection, 2))){
				break;
			}
			dbg(0, "connection=%s", connection->name);

			void done(AyyiIdent obj, GError** error, gpointer data) {}
			am_track__set_output(track, connection, done, NULL);
			*/

			am_channel__set_enable_pan(ch, true, module__ensure_pannable_track_done, _mc);
		}
	}

	if(track){
		module__ensure_mono_audio_track_done(track->ident, mc);
	}else{
		module__add_audio_track(module__ensure_mono_audio_track_done, mc);
	}
}


void
module__ensure_midi_track(AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;

	AMTrack* midi_track = am_track_list_get_first_visible(song->tracks, TRK_TYPE_MIDI);
	if(midi_track){
		FINISH_MODULE(midi_track->ident)
	}else{

		void module__ensure_midi_track_done(AyyiIdent obj, gpointer data)
		{
			FINISH_MODULE(obj)
		}

		module__add_midi_track(module__ensure_midi_track_done, NULL);
	}
}


void
module__ensure_track_has_part (AyyiFinish on_finish, AMTrack* track, gpointer _user_data)
{
	START_MODULE;
	void module__ensure_track_has_part_done(gpointer data)
	{
		AMPart* part = data;
		FINISH_MODULE(part->ident);
	}

	GList* parts = am_partmanager__get_by_track(track);
	if(parts){
		AMPart* part = parts->data;
		g_list_free(parts);
		module__ensure_track_has_part_done(part);
	}else{
		void module__ensure_track_has_part__part_added (AyyiIdent id, GError** error, gpointer data)
		{
			AMPart* part = am_collection_find_by_idx((AMCollection*)song->parts, id.idx);
			assert(part, "part not found for idx=%i", id.idx);
			module__ensure_track_has_part_done(part);
		}
		AMPoolItem* pool_item = am_list_first (song->pool);
		assert(pool_item, "no pool_item");
		uint64_t len = ayyi_samples2mu(((Waveform*)pool_item)->n_frames);
		AyyiSongPos start = {get_random_int(40), 0, 0};
		am_song__add_part(AYYI_AUDIO, AM_REGION_FULL, pool_item, track->ident.idx, &start, len, 0, track->name, 0, module__ensure_track_has_part__part_added, track);
	}
}


void
module__ensure_part (AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE;
	void module__ensure_part_done (gpointer data)
	{
		AMPart* part = data;
		FINISH_MODULE(part->ident);
	}

	AMPart* part = am_list_first(song->parts);
	if(part){
		module__ensure_part_done(part);
	}else{
		void module__ensure_part_have_poolitem(AyyiIdent id, gpointer data)
		{
			void module__ensure_part__part_added (AyyiIdent id, GError** error, gpointer data)
			{
				AMPart* part = am_collection_find_by_idx((AMCollection*)song->parts, id.idx);
				assert(part, "part not found for idx=%i", id.idx);
				module__ensure_part_done(part);
			}
			AMTrack* track = am_track_list_get_first_visible(song->tracks, TRK_TYPE_AUDIO);
			assert(track, "no tracks")
			AMPoolItem* pool_item = am_list_first (song->pool);
			assert(pool_item, "no pool_item");
			uint64_t len = ayyi_samples2mu(((Waveform*)pool_item)->n_frames);
			AyyiSongPos start = {get_random_int(40), 0, 0};
			am_song__add_part(AYYI_AUDIO, AM_REGION_FULL, pool_item, track->ident.idx, &start, len, 0, track->name, 0, module__ensure_part__part_added, track);
		}

		module__ensure_pool_item(module__ensure_part_have_poolitem, NULL);
	}
}


void
module__ensure_aux (AyyiFinish on_finish, AMTrack* track, gpointer _user_data)
{
	//if track is NULL use first audio track.

	START_MODULE_B;

	void on_have_track (AyyiIdent t, gpointer _mc)
	{
		void on_have_aux (AyyiIdent trk, GError** error, gpointer _mc)
		{
			FAIL_MODULE_IF_ERROR_B;
			FINISH_MODULE_B(trk);
		}

		AMTrack* track = am_track_list_find_by_ident(song->tracks, t);
		assert(track, "track not found");
		const AMChannel* ch = am_track__lookup_channel(track);

		AyyiChannel* chan = ayyi_mixer__channel_at_quiet(ch->ident.idx);
		AyyiAux* aux = NULL;//ayyi_mixer__aux_get_(ch->ident.idx, aux_num);
		int i; for(i=0;i<AYYI_AUX_PER_CHANNEL;i++){
			if(!chan->aux[i]) continue;
			aux = chan->aux[i];
			break;
		}
		if(aux)
			on_have_aux(t, NULL, _mc);
		else{
			am_aux__add(ch, 0, on_have_aux, _mc);
		}
	}

	module__ensure_audio_track(on_have_track, mc);
}


void
module__reset_track(AyyiFinish on_finish, AMTrack* track, gpointer _user_data)
{
	//currently just disables solo.

	START_MODULE;

	void module_on_unsolo(AyyiIdent t, GError** error, gpointer user_data)
	{
		PF;
		FAIL_MODULE_IF_ERROR;
		AMTrack* track = user_data;
		assert(track, "track");
		assert(!am_track__is_solod(track), "track solod");

		FINISH_MODULE(t);
	}

	am_track__solo(track, FALSE, module_on_unsolo, track);
}


void
module__clear_auxs(AyyiFinish on_finish, AMTrack* track, gpointer _user_data)
{
	START_MODULE_B;

	typedef struct {
		const AMChannel* ch;
		int              n_outstanding;
	} C;

	void module_clear_aux_on_clear(AyyiIdent t, GError** error, gpointer _mc)
	{
		PF0;
		C* c = ((MC*)_mc)->module;
		FAIL_MODULE_IF_ERROR_B;
		int n_auxs = ayyi_mixer__aux_count(c->ch->ident.idx);
		assert(n_auxs > -1, "shm error");
		c->n_outstanding --;
		dbg(0, "n_auxs=%i outstanding=%i", n_auxs, c->n_outstanding);
		if(!n_auxs && !c->n_outstanding){
			FINISH_MODULE_B(t);
		}
	}

	const AMChannel* ch = am_track__lookup_channel(track);
	mc->module = AYYI_NEW(C,
		.ch = ch
	);

	((C*)mc->module)->n_outstanding = AYYI_AUX_PER_CHANNEL;
	int i; for(i=0;i<AYYI_AUX_PER_CHANNEL;i++){
		am_aux__remove(ch, i, module_clear_aux_on_clear, mc);
	}
}


void
module__add_aux(AyyiFinish on_finish, AMTrack* track, gpointer _user_data)
{
	START_MODULE_B;

	typedef struct {
		AyyiChannel* chan;
		int          n_auxs;
	} C;

	const AMChannel* ch = am_track__lookup_channel(track);
	AyyiChannel* chan = ayyi_mixer__channel_at_quiet(ch->ident.idx);
	assert(chan, "chan");
	assert(!chan->aux[AYYI_AUX_PER_CHANNEL - 1], "aux's full");

	void on_have_aux(AyyiIdent trk, GError** error, gpointer _mc)
	{
		FAIL_MODULE_IF_ERROR_B;
		C* c = ((MC*)_mc)->module;

		assert(ayyi_mixer__aux_count(c->chan->shm_idx) == c->n_auxs + 1, "wrong aux count: %i (expected %i)", ayyi_mixer__aux_count(c->chan->shm_idx), c->n_auxs + 1);

		FINISH_MODULE_B(trk);
	}

	mc->module = AYYI_NEW(C,
		.chan = chan,
		.n_auxs = ayyi_mixer__aux_count(ch->ident.idx)
	);

	am_aux__add(ch, ayyi_mixer__aux_count(chan->shm_idx), on_have_aux, mc);
}


void
module__remove_aux(AyyiFinish on_finish, AMTrack* track, int aux_num, gpointer _user_data)
{
	START_MODULE_B;

	typedef struct {
		AyyiChannel* chan;
		int          n_auxs;
	} C;

	const AMChannel* ch = am_track__lookup_channel(track);
	AyyiChannel* chan = ayyi_mixer__channel_at_quiet(ch->ident.idx);
	assert(chan, "chan");
	assert(chan->aux[aux_num], "aux slot already empty");

	void on_remove(AyyiIdent trk, GError** error, gpointer _mc)
	{
		FAIL_MODULE_IF_ERROR_B;
		C* c = ((MC*)_mc)->module;

		assert(ayyi_mixer__aux_count(c->chan->shm_idx) == c->n_auxs - 1, "wrong aux count: %i (expected %i)", ayyi_mixer__aux_count(c->chan->shm_idx), c->n_auxs - 1);

		FINISH_MODULE_B(trk);
	}

	mc->module = AYYI_NEW(C,
		.chan = chan,
		.n_auxs = ayyi_mixer__aux_count(ch->ident.idx)
	);

	am_aux__remove(ch, aux_num, on_remove, mc);
}


void
module__clear_automation (AyyiFinish on_finish, AMTrack* track, gpointer _user_data)
{
	START_MODULE_B;

	typedef struct {
		AMTrack* track;
		void (*next)(gpointer);
		int count;
		int i;
	} C;

	void on_remove(AyyiIdent trk, GError** error, gpointer _mc)
	{
		FAIL_MODULE_IF_ERROR_B;
		C* c = ((MC*)_mc)->module;

		AyyiChannel* ch = ayyi_mixer__channel_at_quiet(c->track->ident.idx);
		int count = ayyi_mixer_container_count_items(&ch->automation[VOL]);
		assert((count == c->count - 1), "count did not decrement: %i", count);

		if(count){
			c->next(_mc);
		}else{
			FINISH_MODULE_B(trk);
		}
	}

	void next_point(gpointer _mc)
	{
		C* c = ((MC*)_mc)->module;
		Curve* curve = c->track->bezier.vol;
		c->i++;

		AyyiChannel* ch = ayyi_mixer__channel_at_quiet(c->track->ident.idx);
		c->count = ayyi_mixer_container_count_items(&ch->automation[VOL]);

		if(c->count)
			am_automation_point_remove(curve, 0, on_remove, _mc);
		else
			FINISH_MODULE_B(AYYI_NULL_IDENT);
	}

	mc->module = AYYI_NEW(C,
		.track = track,
		.next = next_point,
		.i = -1
	);

	next_point(mc);
}


void
module__add_automation_points (AyyiFinish on_finish, AMTrack* track, int n_points, gpointer _user_data)
{
	START_MODULE_B;

	assert(AM_TRK_IS_AUDIO(track), "Track must be AUDIO for automation tests");

	typedef struct {
		AMTrack* track;
		int      n_points;
		int      done_count;
		void (*next)(gpointer);
	} C;

	void on_add (AyyiIdent trk, GError** error, gpointer _mc)
	{
		FAIL_MODULE_IF_ERROR_B;
		C* c = ((MC*)_mc)->module;

		c->done_count++;

		AyyiChannel* ch = ayyi_mixer__channel_at_quiet(c->track->ident.idx);
		AyyiContainer* events = &ch->automation[VOL];
		int count = ayyi_mixer_container_count_items(events);
		assert((count == c->done_count), "length=%i (expected %i)", count, c->done_count);

		if(c->done_count < c->n_points){
			c->next(_mc);
		}else{
			FINISH_MODULE_B(trk);
		}
	}

	void next_point (gpointer _mc)
	{
		C* c = ((MC*)_mc)->module;

		Curve* curve = c->track->bezier.vol;
		if (!curve->len) {
			BPath* end = curve_append(curve);
			end->code = BP_END;
			end->x3 = 100000;
		}
		int ins_pos = c->done_count;
		BPath* point = curve_ins(curve, ins_pos);
		point->code = LINETO;
		point->x1 = point->x2 = point->x3 = 10000 * c->done_count;
		point->y1 = point->y2 = point->y3 = c->done_count;

		am_automation_point_add(curve, ins_pos, on_add, _mc);
	}

	mc->module = AYYI_NEW(C,
		.track = track,
		.n_points = n_points,
		.next = next_point
	);

	next_point(mc);
}


