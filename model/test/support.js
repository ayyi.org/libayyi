/*
 *  Wrap AM functions to provide named arguments and promises
 */

var AM = imports.gi.AM

const add_track = AM.Song.add_track

AM.Song.add_track = ({type, name, channels}) => new Promise(resolve => add_track(
	type,
	name,
	channels,
	resolve,
	null
))

AM.Song.add_part = input => new Promise(resolve => AM.Song._add_part(
	new AM.PartInput(input),
	resolve
))

AM.init(null)
