/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "config.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <glib.h>
#include "model/ayyi_model.h"
#include "queue.h"


AMQueue*
am_queue_new(gpointer user_data)
{
	AMQueue* q = g_new0(AMQueue, 1);
	q->context = user_data;
	return q;
}


void
am_queue_add(AMQueue* q, AMTask* tasks)
{
	AMTask task;
	int i=0; for(;(task = tasks[i]);i++){
		AMTask task = tasks[i];
		if(!task) break;
		q->tasks = g_list_append(q->tasks, task);
	}
}


void
am_queue_next(AMQueue* q)
{
	g_return_if_fail(q);

	if(q->tasks){
		AMTask task = q->tasks->data;
		q->tasks = g_list_delete_link(q->tasks, q->tasks);
		q->i++;
		task(q);
	}else{
		am_queue_finish(q);
	}
}


void
am_queue_execute_action(AMQueue* q, AyyiAction* a)
{
	g_return_if_fail(q);

	void
	am_queue_action_done(AyyiAction* a)
	{
		AMQueue* q = a->app_data;
		q->id.type = -1;
		q->id.idx = a->ret.idx;
		am_queue_next((AMQueue*)a->app_data);
	}

	a->callback = am_queue_action_done;
	a->app_data = q;
	ayyi_action_execute(a);
}


void
am_queue_finish(AMQueue* q)
{
	if(q->context){
		g_free(q->context);
		q->context = NULL;
	}
	q->finished();
}


