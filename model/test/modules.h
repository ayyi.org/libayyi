/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2007-2018 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

void module__save_song                     (AyyiFinish, gpointer);

void module__ensure_pool_item              (AyyiFinish, gpointer);
void module__ensure_audio_track            (AyyiFinish, gpointer);
void module__ensure_mono_audio_track       (AyyiFinish, gpointer);
void module__ensure_pannable_track         (AyyiFinish, gpointer);
void module__ensure_midi_track             (AyyiFinish, gpointer);
void module__ensure_track_has_part         (AyyiFinish, AMTrack*, gpointer);
void module__ensure_part                   (AyyiFinish, gpointer);
void module__ensure_aux                    (AyyiFinish, AMTrack*, gpointer);

void module__load_song_with_delay          (AyyiFinish, gpointer, const char*);
void module__save_and_load_song_with_delay (AyyiFinish, gpointer, const char*);
void module__add_audio_part                (AyyiFinish, gpointer);
void module__add_n_audio_parts             (AyyiFinish, int, gpointer);
void module__delete_all_parts              (AyyiFinish, gpointer);
void module__delete_audio_part             (AyyiFinish, AMPart*, gpointer);
void module__delete_random_part            (AyyiFinish, gpointer);
void module__split_part                    (AyyiFinish, AMPart*, gpointer);
void module__select_part                   (AyyiFinish, AMPart*, gpointer);
void module__select_track                  (AyyiFinish, AMTrack*, gpointer);
void module__add_audio_track               (AyyiFinish, gpointer);
void module__delete_audio_track            (AyyiFinish, gpointer, AMTrack*);
void module__delete_tracks_by_name         (AyyiFinish, gpointer, char**);
void module__delete_all_tracks             (AyyiFinish, gpointer);
void module__add_midi_track                (AyyiFinish, gpointer);
void module__add_midi_part                 (AyyiFinish, gpointer);
void module__delete_midi_part              (AyyiFinish, AMPart*, gpointer);
void module__add_midi_notes                (AyyiFinish, gpointer);
void module__modify_midi_notes             (AyyiFinish, gpointer);
void module__delete_midi_note              (AyyiFinish, gpointer);
void module__load_wav_fixture              (AyyiFinish, gpointer, const char*);
void module__delete_audio_file             (AyyiFinish, gpointer, const char*);
void module__delete_all_audio_files        (AyyiFinish, gpointer);
void module__delete_parts_by_audio_file    (AyyiFinish, gpointer, AMPoolItem*);
void module__add_plugin                    (AyyiFinish, gpointer);
void module__reset_track                   (AyyiFinish, AMTrack*, gpointer);
void module__clear_auxs                    (AyyiFinish, AMTrack*, gpointer);
void module__add_aux                       (AyyiFinish, AMTrack*, gpointer);
void module__remove_aux                    (AyyiFinish, AMTrack*, int, gpointer);
void module__clear_automation              (AyyiFinish, AMTrack*, gpointer);
void module__add_automation_points         (AyyiFinish, AMTrack*, int n, gpointer);

