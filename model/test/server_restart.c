/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  Tests survival of a server restart
 |
 |  Usage: once the program has connected, stop then restart the server.
 |
 */

#define __ayyi_private__

#include "config.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <inttypes.h>
#include <glib.h>
#include "model/ayyi_model.h"
#include "model/track_list.h"
#include "model/test/runner.h"
#include "model/test/common.h"

#include "modules.c"

static void test__wait_for_server_restart ();

gpointer    tests[] = {
	test__wait_for_server_restart,
};


void
setup (int argc, char* argv[])
{
	am_init(NULL);
	ayyi.log.to_stdout = true;

	am_connect_all(app_on_all_shm, NULL);
}


void
teardown ()
{
#ifdef WITH_VALGRIND
	am_uninit();
#endif
}


static void
test__wait_for_server_restart ()
{
	START_TEST;

	void __on_song_new (AyyiIdent obj, GError** errror, AyyiPropType pt, gpointer user_data)
	{
		dbg (0, "...");

		ayyi_song__print_playlists ();
		am_song__print_tracks      ();
		am_song__channels_print    ();
		am_partmanager__print_list ();

		FINISH_TEST;
	}

	void __on_song_del (AyyiIdent obj, GError** errror, AyyiPropType pt, gpointer user_data)
	{
		//TODO handler order is important. clients may need to run before am_song so that data still exists.

		dbg (0, "OBJECT_SONG");
	}

	AyyiIdent id = {0, -1};
	ayyi_client_watch((id.type = AYYI_OBJECT_SONG, id), AYYI_OP_NEW, __on_song_new, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_SONG, id), AYYI_DEL, __on_song_del, NULL);

	if (TEST.timeout) g_source_remove (TEST.timeout);

	printf("waiting for server restart...\n");
}


void
test_never ()
{
	module__add_midi_track(0, 0);
	module__add_midi_part(0, 0);
	module__delete_parts_by_audio_file(0, NULL, 0);
	module__delete_audio_part(0, 0, 0);
	module__load_song_with_delay(0, 0, 0);
	module__save_and_load_song_with_delay(0, 0, 0);
	module__delete_all_audio_files(0, 0);
}
