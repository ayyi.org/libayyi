/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://ayyi.org              |
 | copyright (C) 2007-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "stdint.h"
#include "ayyi/ayyi_log.h"
#include "model/common.h"

#define mono_wav_fixture "mono_0:10.wav"
#define mono_wav_fixture2 "mono_10:00.wav"
#define stereo_wav_fixture "stereo_0:10.wav"
#define vst_plugin_fixture "SaffireEQMono"
#define MIN_TRACKS 1 //server crashes if no tracks?

void am_test_init       (gpointer tests[], int);
void app_on_all_shm     (const GError*, gpointer);

bool get_random_boolean ();
int  get_random_int     (int max);

void errprintf4         (char* format, ...);

char*find_wav           (const char*);
void am_wait_for        (gboolean (*)(gpointer), AyyiHandler, gpointer);

#include "model/test/modules.h"
