/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
*/
/*
 +----------------------------------------------------------------------+
 | Functional tests for the Ayyi Model c libary                         |
 | ============================================                         |
 | An Ayyi Model based client that tests functionality of the Model     |
 | See also test/unit_test.c for unit tests.                            |
 | See also src/test.c for gui tests.                                   |
 +----------------------------------------------------------------------+
 |
 */

#define __ayyi_private__
#include "config.h"
#include <getopt.h>
#include <time.h>
#include <sys/time.h>
#include <glib.h>
#include "ayyi/ayyi_log.h"
#include "model/ayyi_model.h"
#include "model/test/queue.h"
#include "model/test/runner.h"
#include "model/test/common.h"

static bool tracks_check (AyyiIdx);

TestFn test_ayyi_container_iteration, blank_song, test_song_open, test_song_save, test_song_reload, test_transport, test_locators, test_arm,
	test_record,
	test_connections,
	test_add_audio_track,
	test_add_delete_audio_track,
	test_delete_audio_track2,
	test_delete_audio_track3,
	test_delete_all_tracks,
	test_add_midi_track,
	test_add_remove_midi_part,
	test_delete_midi_track,
	test_stress_audio_track,
	test_add_audio_part,
	test_select_part,
	test_select_track,
	test_split_part,
	test_add_remove_audio_part,
	test_add_n_audio_parts,
	test_add_n_audio_parts_concurrent,
	test_move_part,
	test_copy_part,
	test_part_change_track,
	test_rename_part,
	test_add_lots_of_parts,
	test_stress_audio_parts,
	test_track_add_remove,
	test_note_selection,
	test_add_audio_file,
	test_add_stereo_audio_file,
	test_remove_audio_file,
	test_remove_stereo_audio_file,
	test_remove_all_audio_files,
	test_midi,
	test_rename_track,
	test_add_plugin,
	test_fader,
	test_pan,
	test_mute,
	test_solo,
	test_aux_level,
	test_aux_add_remove,
	test_automation,
	test_tempo,
	test_meters,
	test_909,
	test_ayyi_container;

#define STOP false;
static char* song_fixture1 = "/tmp/test_session1/";
static char* song_fixture2 = "/tmp/test_session2/";
extern char  current_test_name[];

gpointer tests[] = {
	blank_song,
	test_song_open,
//	test_record,
	test_transport,
	test_locators,
	test_arm,
	test_add_audio_track,
	test_add_delete_audio_track,
	test_add_delete_audio_track,
	test_add_delete_audio_track,
	test_delete_audio_track2,
	test_delete_audio_track3,
	test_stress_audio_track,
	test_track_add_remove,
	test_add_audio_file,
	test_add_stereo_audio_file,
	test_remove_stereo_audio_file,
	test_remove_audio_file,
	test_add_n_audio_parts_concurrent,
	test_move_part,
	test_copy_part,
	test_part_change_track,
	test_rename_part,
	test_note_selection,
	test_connections,
	test_add_lots_of_parts,
	test_stress_audio_parts,
	test_remove_all_audio_files,
	test_add_audio_part,
	test_select_part,
	test_select_track,
	test_split_part,
	test_add_remove_audio_part,
	test_rename_track,
	test_add_n_audio_parts,
	test_delete_all_tracks,
	test_add_midi_track,
	test_add_remove_midi_part,
	test_add_remove_midi_part,
	test_delete_midi_track,
	test_midi,
	test_song_save,
	test_song_reload,
	//test_add_plugin, //will fail if vst not enabled on server.
	test_fader,
	test_mute,
	test_solo,
	test_aux_level,
	test_aux_add_remove,
	test_pan,
	test_automation,
	test_tempo,
	test_meters,
	//test_909, // requires plugin to be installed
	test_ayyi_container_iteration,
	test_ayyi_container,
};

gpointer part_tests[] = {
	test_move_part,
	test_copy_part,
	test_add_audio_part,
	test_rename_part,
	test_select_part,
	test_split_part,
	test_add_remove_audio_part,
	test_add_n_audio_parts,
};

#if 1
	#define TESTS tests
#else
	#define TESTS part_tests
#endif

#define NEXT_CALLBACK(A, B, C) \
	step++; \
	void (*callback)() = callbacks[step]; \
	callback(A, B, C);

#define FAIL_IF_ERROR \
	if(error && *error) FAIL_TEST((*error)->message);

#define FAIL_IF_ERROR_2 \
	if(error) FAIL_TEST(error->message);

#include "modules.c"

G_STATIC_ASSERT (sizeof(off_t) == 8);


void
setup (int argc, char* argv[])
{
	TEST.n_tests = G_N_ELEMENTS(tests);

	am_init(NULL);
	ayyi.log.to_stdout = true;

	gboolean connect (gpointer user_data)
	{
		_debug_ = 1;

		am_connect_all(app_on_all_shm, NULL);

		return G_SOURCE_REMOVE;
	}
	g_idle_add(connect, NULL);
}


void
teardown ()
{
}


static AyyiAction*
new_action_for_audio_track_delete (AyyiTrack* track, gpointer callback)
{
	AyyiAction* a   = ayyi_action_new("audio track delete %i", track->shm_idx);

	a->callback     = callback;
	a->op           = AYYI_DEL;
	a->obj.type     = AYYI_OBJECT_AUDIO_TRACK;
	a->obj_idx.idx1 = track->shm_idx;

	return a;
}


void
blank_song ()
{
	START_TEST;

	void on_unload (AyyiIdent obj, gpointer data) {
		//song_fixture1 is now unloaded
		assert(file_delete_recursive(song_fixture1), "failed to remove old test directory");
		reset_timeout(20000);

		void _1_load_finished(AyyiIdent obj, gpointer data) { FINISH_TEST; }

		module__load_song_with_delay(_1_load_finished, NULL, song_fixture1);
	}

	if(!strcmp(song_fixture1, ((AyyiSongService*)ayyi.service)->song->path)){
		module__load_song_with_delay(on_unload, NULL, song_fixture2);
	}
	else //TODO check this - 'else' was added without testing.
	on_unload(AYYI_NULL_IDENT, NULL);
}


void
test_song_open ()
{
	// 1- open two empty sessions and swap between them
	// 2- add a file to the session and retest.
	// 3- add an audio part to the session and retest.
	// 4- add a midi track to the session and retest.

	START_TEST;

	//long test. have to disable timeout.
	am_source_remove0 (TEST.timeout);

	assert(file_delete_recursive(song_fixture1), "failed to remove old test session directory 1");
	assert(file_delete_recursive(song_fixture2), "failed to remove old test session directory 2");

	bool f1 = g_file_test(song_fixture1, G_FILE_TEST_EXISTS);
	bool f2 = g_file_test(song_fixture2, G_FILE_TEST_EXISTS);
	assert(!f1, "session 1 not deleted!");
	assert(!f2, "session 2 not deleted!");

	void _10_load_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_TEST;
	}
	void _9_add_midi_track_done (AyyiIdent id, gpointer data)
	{
		PF;
		module__load_song_with_delay(_10_load_finished, NULL, song_fixture2);
	}
	void _8_load_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		module__add_midi_track(_9_add_midi_track_done, NULL);
	}
	void _7_add_part_done (AyyiIdent obj, gpointer data)
	{
		PF;
		//note that we save the song here as parts are not saved automatically.
		module__save_and_load_song_with_delay(_8_load_finished, NULL, song_fixture2);
	}
	void _6_load_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		module__add_audio_part(_7_add_part_done, NULL);
	}
	void _5_file_add_done (AyyiIdent obj, gpointer data)
	{
		PF;
		module__load_song_with_delay(_6_load_finished, NULL, song_fixture2);
	}
	void _4_load_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		char* wav = find_wav(mono_wav_fixture);
		assert(!ayyi_song__have_file(wav), "mono wav not loaded");

		module__load_wav_fixture(_5_file_add_done, NULL, wav);
		g_free(wav);
	}
	void _3_load_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		module__load_song_with_delay(_4_load_finished, NULL, song_fixture2);
	}
	void _2_load_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		printf("part 2 finished\n");
		printf("_________________________________________________________________\n");
		module__load_song_with_delay(_3_load_finished, NULL, song_fixture1);
	}
	void _1_load_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		printf("part 1 finished\n");
		printf("_________________________________________________________________\n");
		module__load_song_with_delay(_2_load_finished, NULL, song_fixture2);
	}
	module__load_song_with_delay(_1_load_finished, NULL, song_fixture1);
}


void
test_song_save ()
{
	START_TEST;

	void test_save_done(AyyiIdent id, gpointer user_data) { FINISH_TEST; }

	module__save_song(test_save_done, NULL);
}


void
test_song_reload ()
{
	START_TEST;

	typedef struct {
		bool unload_done;
		bool load_done;
	} C;

	void done (AyyiIdent id, GError** error, gpointer _c)
	{
		C* c = _c;

		if(!c->unload_done) FAIL_TEST("no unload received");
		if(!c->load_done) FAIL_TEST("no load received");

		FINISH_TEST;
	}

	void on_unload (GObject* object, gpointer c)
	{
		PF;
		((C*)c)->unload_done = true;
	}

	void on_load (GObject* object, gpointer c)
	{
		PF;
		((C*)c)->load_done = true;
	}

	C* c = g_new0(C, 1);

	am_song__connect("song-load", G_CALLBACK(on_load), c);
	am_song__connect("song-unload", G_CALLBACK(on_unload), c);

	am_song__reload(done, c);
}


void
test_transport ()
{
	START_TEST;
	assert(ayyi_transport_is_stopped(), "transport not stopped");

	void test_on_stop (GObject* object, gpointer self)
	{
		PF;
		g_signal_handlers_disconnect_by_func (song, self, self);
		assert(ayyi_transport_is_stopped(), "transport not stopped");
		FINISH_TEST;
	}

	void test_on_play (GObject* object, gpointer self)
	{
		PF;
		g_signal_handlers_disconnect_by_func (song, self, self);
		assert(ayyi_transport_is_playing(), "transport not playing");

		am_song__connect("transport-stop", G_CALLBACK(test_on_stop), test_on_stop);
		ayyi_transport_stop();
	}

	am_song__connect("transport-play", G_CALLBACK(test_on_play), test_on_play);

	ayyi_transport_play();
}


void
test_locators ()
{
	START_TEST;

	static int l = 1;

	void test_on_locator (GObject* object, gpointer self)
	{
		PF;
		g_signal_handlers_disconnect_by_func (song, self, self);

		assert(!ayyi_pos_cmp(&am_object_val(&song->loc[l]).sp, &(AyyiSongPos){4, 0, 0}), "locator 1 not set correctly");

		FINISH_TEST;
	}

	am_song__connect ("locators_change", G_CALLBACK(test_on_locator), test_on_locator);

	am_transport_set_locator_pos(l, &(AyyiSongPos){4, 0});
}


void
test_arm ()
{
	START_TEST;

	static bool got_signal = false; assert(!got_signal, "got_signal"); got_signal = false;

	void test_on_unarm (AyyiIdent obj, GError** error, gpointer user_data)
	{
		PF;
		FAIL_IF_ERROR;
		AMTrack* track = user_data;
		assert(track, "track");
		assert(!am_track__is_armed(track), "track not armed");

		FINISH_TEST;
	}

	void test_on_arm (AyyiIdent obj, GError** error, gpointer user_data)
	{
		PF;
		AMTrack* track = user_data;
		assert(track, "track");
		assert(am_track__is_armed(track), "track not armed");
		FAIL_IF_ERROR;
		assert(got_signal, "didnt get signal");
		got_signal = false;

		am_track__arm(track, FALSE, test_on_unarm, track);
	}

	void test_arm__on_tracks_change_signal (GObject* _song, AMTrackNum t_num1, AMTrackNum t_num2, int change_type, gpointer self)
	{
		PF;
		got_signal = true;
		g_signal_handlers_disconnect_by_func (song, self, self);
	} 

	void on_have_track (AyyiIdent t, gpointer user_data)
	{
		AMTrack* track = am_track_list_find_by_ident(song->tracks, t);
		assert(track, "track not found");

		am_song__connect("tracks-change", G_CALLBACK(test_arm__on_tracks_change_signal), test_arm__on_tracks_change_signal);

		am_track__arm(track, TRUE, test_on_arm, track);
	}

	module__ensure_audio_track(on_have_track, NULL);
}


void
test_solo ()
{
	START_TEST;

	static bool got_signal = false; assert(!got_signal, "got_signal"); got_signal = false;

	void test_on_unsolo (AyyiIdent obj, GError** error, gpointer user_data)
	{
		PF;
		FAIL_IF_ERROR;
		AMTrack* track = user_data;
		assert(track, "track");
		assert(!am_track__is_solod(track), "track not solod");

		FINISH_TEST;
	}

	void test_on_solo (AyyiIdent obj, GError** error, gpointer user_data)
	{
		PF;
		FAIL_IF_ERROR;
		AMTrack* track = user_data;
		assert(track, "track");
		assert(am_track__is_solod(track), "track not solod");
		assert(got_signal, "didnt get signal");
		got_signal = false;

		am_track__solo(track, FALSE, test_on_unsolo, track);
	}

	void test_solo__on_tracks_change_signal (GObject* _song, AMTrackNum t_num1, AMTrackNum t_num2, AMChangeType change_type, gpointer self)
	{
		PF0;
		dbg(0, "change_type=%i (%i)", change_type, AM_CHANGE_SOLO);
		if((change_type & AM_CHANGE_SOLO) == AM_CHANGE_SOLO){
			dbg(0, "got signal");
			got_signal = true;
			g_signal_handlers_disconnect_by_func (song, self, self);
		}
	} 

	void on_have_track (AyyiIdent t, gpointer user_data)
	{
		AMTrack* track = am_track_list_find_by_ident(song->tracks, t);
		assert(track, "track not found");

		void on_track_reset (AyyiIdent t, gpointer user_data)
		{
			AMTrack* track = user_data;

			am_song__connect("tracks-change", G_CALLBACK(test_solo__on_tracks_change_signal), test_solo__on_tracks_change_signal);

			am_track__solo(track, TRUE, test_on_solo, track);
		}
		module__reset_track(on_track_reset, track, track);
	}

	module__ensure_audio_track(on_have_track, NULL);
}


void
test_aux_level ()
{
	START_TEST;

	static double vals[] = {-60.0, -6.0, -0.1, -0.0, +6.0};
	typedef struct
	{
		FilterIterator* i;
		AMChannel*      ch;
		int             aux_num;
		AyyiHandler     callback;
		int             n;
		bool            got_signal;
		void*           signal_handler;
	} C;
	C* c = g_new0(C, 1);

	void set_aux_level (C* c)
	{
		am_channel__set_aux_level(c->ch, c->aux_num, vals[c->n], c->callback, c);
	}

	bool channel_has_aux (AMChannel* ch, gpointer _c)
	{
		C* c = _c;

		AyyiAux* aux = NULL;
		for (int i=0;i<AYYI_AUX_PER_CHANNEL;i++) {
			aux = ayyi_mixer__aux_get_(ch->ident.idx, i);
			if (aux) {
				c->aux_num = i;
				break;
			}
		}
		dbg(0, "ch=%s [%s] %i", am_channel__get_name(ch), aux ? "y" : "n", ayyi_mixer__aux_count(ch->ident.idx));
		return !!aux;
	}

	void test_on_aux (AyyiIdent obj, GError** error, gpointer _c)
	{
		PF0;
		dbg(0, "obj=%i", obj.idx);
		FAIL_IF_ERROR;
		C* c = _c;

		AyyiAux* aux = ayyi_mixer__aux_get_(c->ch->ident.idx, c->aux_num);
		assert(aux, "channel has no aux: %i", c->aux_num);
		assert((ABS(am_channel__get_aux_level(c->ch, c->aux_num) - vals[c->n]) < 0.0001),
			"wrong aux level: %.2fdB(%0.2f) expected %.2f",
			am_channel__get_aux_level(c->ch, c->aux_num), aux->level, vals[c->n]
		);

		if(++c->n >= G_N_ELEMENTS(vals)){
			if((c->ch = (AMChannel*)filter_iterator_next(c->i))){
				c->n = 0;
			}else{
				filter_iterator_unref(c->i);
				FINISH_TEST;
				return;
			}
		}
		set_aux_level(c);
	}
	c->callback = test_on_aux;

	void test_aux__on_tracks_change_signal (GObject* _song, AMTrackNum t_num1, AMTrackNum t_num2, AMChangeType change_type, gpointer _c)
	{
		// the model doesnt currently track changes to aux levels.

		C* c = _c;

		//if(change_type & CHANGE_SOLO == CHANGE_SOLO){
			c->got_signal = true;
			g_signal_handlers_disconnect_by_func (song, c->signal_handler, c);
		//}
	}
	c->signal_handler = test_aux__on_tracks_change_signal;

	void on_have_aux (AyyiIdent t, gpointer _c)
	{
		C* c = _c;
		assert(c, "closure data");

		am_song__connect("tracks-change", G_CALLBACK(test_aux__on_tracks_change_signal), c);

		c->i = filter_iterator_new (0, NULL, NULL, (AMCollection*)song->channels, (Filter)channel_has_aux, c);
		c->ch = (AMChannel*)filter_iterator_next(c->i);
		assert(c->ch, "first channel not found");

		AyyiAux* aux = ayyi_mixer__aux_get_(c->ch->ident.idx, c->aux_num);
		assert(aux, "no auxs on this channel");

		set_aux_level(c);
	}

	// check basic conversion fns.
	float dbvals[] = {-50.0f, -20.0f, -6.0f, 0.0, 10.0f};
	for (int i=0;i<G_N_ELEMENTS(dbvals);i++) {
		float f = dbvals[i];
		//dbg(0, "%.2f %.2f %.2f", f, am_db2gain(f), am_gain2db(am_db2gain(f)));
		assert((ABS(am_gain2db(am_db2gain(f)) - f) < 0.0001), "dB conversion failed");
	}

	module__ensure_aux(on_have_aux, NULL, c);
}


void
test_aux_add_remove ()
{
	START_TEST;

	typedef struct
	{
		const AMChannel* ch;
		AMTrack*         track;
	} C;

	void test_aux_on_have_track (AyyiIdent t, gpointer _c)
	{
		C* c = _c;
		c->track = am_track_list_find_by_ident(song->tracks, t);
		assert(c->track, "track not found");
		c->ch = am_track__lookup_channel(c->track);

		void test_aux_on_remove(AyyiIdent obj, gpointer _c)
		{
			g_free(_c);
			FINISH_TEST;
		}

		void test_aux_on_add2(AyyiIdent t, gpointer _c)
		{
			dbg(0, "%s.%i", ayyi_print_object_type(t.type), t.idx);
			module__remove_aux(test_aux_on_remove, ((C*)_c)->track, 0, _c);
		}

		void test_aux_on_add1(AyyiIdent t, gpointer _c)
		{
			dbg(0, "%s.%i", ayyi_print_object_type(t.type), t.idx);
			module__add_aux(test_aux_on_add2, ((C*)_c)->track, _c);
		}

		void test_aux_on_clear(AyyiIdent obj, gpointer _c)
		{
			dbg(0, "%s.%i", ayyi_print_object_type(obj.type), obj.idx);
			pwarn("FIXME return type of aux calls");
			module__add_aux(test_aux_on_add1, ((C*)_c)->track, _c);
		}

		module__clear_auxs(test_aux_on_clear, c->track, c);
	}

	C* c = g_new0(C, 1);
	module__ensure_audio_track(test_aux_on_have_track, c);
}


void
test_record ()
{
	START_TEST;

	gboolean check_record (gpointer data)
	{
		PF;
		dbg(0, "playing=%i rec_enabled=%i", ayyi_transport_is_playing(), ayyi_transport_is_rec_enabled());
		bool is_recording = ayyi_transport_is_recording();
		ayyi_transport_stop();
		if (!is_recording) FAIL_TEST_TIMER("is not recording");
		FINISH_TEST_TIMER_STOP;
	}

	void rec_ready (const GError* error, gpointer data)
	{
		if (!ayyi_transport_is_rec_enabled()) {
			FAIL_TEST("not rec-enabled.");
		}

		void on_track_armed (AyyiIdent ident, GError** error, gpointer data)
		{
			PF;
			ayyi_transport_stop();
			ayyi_transport_play();
			ayyi_transport_rec();
			g_timeout_add(500, (gpointer)check_record, NULL);
		}

		AMTrack* track = am_track_list_get_first_visible(song->tracks, TRK_TYPE_AUDIO);
		if (!track) FAIL_TEST("no tracks");
		am_track__arm(track, true, on_track_armed, NULL);

	}

	if (ayyi_transport_is_rec_enabled()) {
		rec_ready(NULL, NULL);
	} else {
		am_transport_set_rec(true, rec_ready, NULL);
	}
}


void
test_connections ()
{
	//connect first track to "output 1 + 2"

	START_TEST;

	typedef struct _c {
		AyyiTrack*      track;
		AyyiConnection* connection;
		void            (*next_connection)(struct _c*);
	} C;

	void on_have_track (AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		void test_connections__on_connect (AyyiIdent track_id, GError** error, gpointer _c)
		{
			FAIL_IF_ERROR;
			C* c = _c;

			if(strcmp(c->connection->device, "system")){ //dont verify system ports, due to issues with duplicates.
#if 0
				AyyiConnection* connection = ayyi_track__get_output(c->track);
				if(connection != c->connection){
					dbg(0, "  !! got:      %s:%s", connection->device, connection->name);
					dbg(0, "  !! expected: %s:%s", c->connection->device, c->connection->name);
				}
				assert((connection == c->connection), "wrong connection: %i (expected %i)", connection->shm_idx, c->connection->shm_idx);
#else
				//this assertion relaxes the requirements above so that the connection must be ONE of possibly several outputs.
				bool has_output = ayyi_track__has_output(c->track, c->connection);
				if(!has_output){
					pwarn("  track does not have expected output: %i: \"%s:%s\"", c->connection->shm_idx, c->connection->device, c->connection->name);
					pwarn("  current track outputs:");
					for (AyyiList* l=ayyi_list__first(c->track->output_routing); l; l=ayyi_list__next(l)) {
						AyyiConnection* connection = ayyi_song__connection_at(l->id);
						printf ("    output_idx=%i name=%s\n", l->id, connection->name);
					}
					pwarn("  other connections:");
					ayyi_song__print_connections();
				}
				assert(ayyi_track__has_output(c->track, c->connection), "wrong connection")
#endif
			}

			c->next_connection(c);
		}

		void next_connection (C* c)
		{
			AyyiChannel* ch = ayyi_mixer__channel_at(c->track->shm_idx);
			if((c->connection = ayyi_connection__next_input(c->connection, ch->n_out))){
				if(!strcmp(c->connection->name, "playback_1{playback_1}") || !strcmp(c->connection->name, "playback_2{playback_2}")){ //seems we have duplicate connections!
					c->next_connection(c);
				}else if(c->connection->flags & Midi){
					dbg(0, "ignoring midi connection");
					c->next_connection(c);
				}else{
					AMTrack* trk = am_track_list_find_by_ident (song->tracks, (AyyiIdent){TRK_TYPE_AUDIO, c->track->shm_idx});
					assert(trk, "no track for %i", c->track->shm_idx);
					dbg(0, "connecting to: %s", c->connection->name);
					am_track__set_output(trk, c->connection, test_connections__on_connect, c);
				}
			}else{
				g_free(c);
				FINISH_TEST;
			}
		}
		c->next_connection = next_connection;

		c->track = ayyi_song__audio_track_at(t.idx);
		AyyiChannel* ch = ayyi_mixer__channel_at(t.idx);
		AMTrack* trk = am_track_list_find_by_ident (song->tracks, (AyyiIdent){TRK_TYPE_AUDIO, c->track->shm_idx});
		dbg(0, "trk=%i channels=%i->%i", trk->ident.idx, ch->n_in, ch->n_out);
		AyyiConnection* current = am_track__get_output(trk);
		AyyiConnection* connection = NULL;
		while((connection = ayyi_connection__next_input(connection, ch->n_out))){
			if(connection != current) break;
		}
		c->connection = connection;
		assert(connection, "no available connections");

		am_track__set_output(trk, connection, test_connections__on_connect, c);
	}

	module__ensure_audio_track(on_have_track, g_new0(C, 1));
}


void
test_add_audio_track()
{
	START_TEST;

	void
	test_on_create (AyyiIdent new_obj, const GError* error, gpointer user_data)
	{
		// this will be called following creation of a new (track) object, so we have a reference to the new object.

		FAIL_IF_ERROR_2;

#if 0
		AyyiTrack* track = ayyi_song__audio_track_at(new_obj.idx);
		dbg(0, "idx=%i name='%s'", new_obj.idx, track ? track->name : NULL);
#endif

		FINISH_TEST;
	}

	am_song_add_track(TRK_TYPE_AUDIO, "New Track", 1, test_on_create, NULL);
}


void
test_add_delete_audio_track()
{
	// create 2 tracks, then delete the first, then the second.
	// this is a good test for other connected clients as it quickly creates and deletes a track.

	static gpointer callbacks[4] = {NULL, NULL, NULL, NULL};
	static int new_track_1 = -1;
	static int new_track_2 = -1;
	static int orig_track_count = -1;

	START_TEST;
	assert(ayyi_verify_playlists(), "playlist verify");

	void
	test_on_delete2(AyyiAction* a)
	{
		if(!tracks_check(-1)) FAIL_TEST("tracks check"); //check track[] array.

		//if(ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks) != orig_track_count) FAIL_TEST("count");
		assert(ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks) == orig_track_count, "count");

		assert(ayyi_verify_playlists(), "playlist verify");
		FINISH_TEST;
	}

	void
	test_on_delete1(AyyiAction* a)
	{
		if(!tracks_check(-1)) FAIL_TEST("track check failed."); //check track[] array.

		//check the slot is empty.
		AyyiTrack* track = ayyi_container_get_item_quiet(&((AyyiSongService*)ayyi.service)->song->audio_tracks, a->ret.idx, ayyi_song__translate_address);
		if(track) perr("track not empty (idx=%i)", a->ret.idx);
		if(track) FAIL_TEST("track not empty");

		if(ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks) != orig_track_count + 1) FAIL_TEST("");

		AyyiTrack* track_1 = ayyi_song__audio_track_at(new_track_2);
		ayyi_action_execute(new_action_for_audio_track_delete(track_1, test_on_delete2));
	}

	void
	on_track2_added(AyyiIdent id, GError** error, gpointer user_data)
	{
		new_track_2 = id.idx;
		dbg(0, "deleting track..");
		AyyiTrack* track = ayyi_song__audio_track_at(new_track_1);
		am_action_execute(new_action_for_audio_track_delete(track, test_on_delete1));
	}

	void
	on_track1_added(AyyiIdent id, GError** error, gpointer user_data)
	{
		PF0;
		//this will be called following creation of a new (track) object, so we have a reference to the new object.
		new_track_1 = id.idx;

		// add a 2nd track
		am_song_add_track(TRK_TYPE_AUDIO, "New Track 2", 1, callbacks[1], NULL);
	}

	if(!callbacks[0]){
		callbacks[0] = &on_track1_added;
		callbacks[1] = &on_track2_added;
		callbacks[2] = test_on_delete1;
		callbacks[3] = test_on_delete2;
	}

	orig_track_count = ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks);

	am_song_add_track(TRK_TYPE_AUDIO, "New Track", 1, callbacks[0], NULL);
}


#if 0
//temp - maps fn sig
void
am_song__add_track_q(AMQueue* q, TrackType type, char* name)
{
	void done(AyyiIdent id, GError** error, gpointer user_data)
	{
		AMQueue* q = user_data;
		g_return_if_fail(q);
		am_queue_next(q);
	}

	am_song_add_track(type, name, 1, done, q);
}
#endif


static void
task_done (AyyiIdent id, const GError* error, gpointer user_data)
{
	FAIL_IF_ERROR_2;
	AMQueue* q = user_data;
	g_return_if_fail(q);
	q->id = id;
	q->error = (GError**)&error; // TODO
	am_queue_next(q);
}


/*
 *  Alternative implementation of test_delete_audio_track using an array of promises
 *
 *  TODO its unfortunate that the fns have AMQueue* as their argument. Change to AMContext* and next() ?
 */
void
test_delete_audio_track2 ()
{
	typedef struct {
		int new_track_1;
		int new_track_2;
		int orig_track_count;
	} C;

	START_TEST;

	void finished()
	{
		FINISH_TEST;
	}

	return finished(); // ----- TODO test is not working currently

	void create1 (AMQueue* q)
	{
		PF0;
		C* c = q->context;
		c->orig_track_count = ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks);

		am_song_add_track(TRK_TYPE_AUDIO, "New Track", 1, task_done, q);
	}

	void
	create2 (AMQueue* q)
	{
		PF0;
		C* c = q->context;

		c->new_track_1 = q->id.idx;

		//add a 2nd track:
		//am_song__add_track_q(q, TRK_TYPE_AUDIO, g_strdup(name));
		am_song_add_track(TRK_TYPE_AUDIO, "New Track 2", 1, task_done, q);
	}

	void
	delete1 (AMQueue* q)
	{
		PF0;
		C* c = q->context;

		c->new_track_2 = q->id.idx;

		dbg(0, "deleting track..");
		AyyiTrack* track = ayyi_song__audio_track_at(c->new_track_1);
		am_queue_execute_action(q, new_action_for_audio_track_delete(track, NULL));
	}

	void delete2 (AMQueue* q)
	{
		C* c = q->context;

		if(!tracks_check(-1)) FAIL_TEST("track check failed."); //check track[] array.

		//check the slot is empty.
		AyyiTrack* track = ayyi_container_get_item_quiet(&((AyyiSongService*)ayyi.service)->song->audio_tracks, q->id.idx, ayyi_song__translate_address);
		assert(track, "track not empty (idx=%i)", q->id.idx);

		assert((ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks) == c->orig_track_count + 1), "track count");

		AyyiTrack* track_1 = ayyi_song__audio_track_at(c->new_track_2);
		am_queue_execute_action(q, new_action_for_audio_track_delete(track_1, NULL));
	}

	void finish (AMQueue* q)
	{
		C* c = q->context;

		assert(tracks_check(-1), "tracks check"); //check track[] array.

		assert((ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks) == c->orig_track_count), "track count");

		FINISH_TEST;
	}

	static AMTask tasks[6];
	tasks[0] = create1;
	tasks[1] = create2;
	tasks[2] = delete1;
	tasks[3] = delete2;
	tasks[4] = finish;
	tasks[5] = NULL;

	C* c = AYYI_NEW(C,
		.new_track_1      = -1,
		.new_track_2      = -1,
		.orig_track_count = -1,
	);

	AMQueue* q = am_queue_new(c);
	q->finished = finished;
	am_queue_add(q, (AMTask*)tasks);
	am_queue_next(q);
}


/*
 *  Alternative implemenation of test_delete_audio_track2 using piped promises
 *  instead of an array of promises
 */
void
test_delete_audio_track3 ()
{
	void am_promise_pipe (AMPromise* p, AMPromise* p2)
	{
		// Allows promises to be chained together. p2 is added after p1 in the chain.

#if 0
		typedef struct {
			AMPromise* promise;
		} Chain;

		Chain* chain = AYYI_NEW(Chain, .promise = p2);
#endif

		void do_chain ()
		{
		}

		p->callbacks = g_list_append(p->callbacks, do_chain); //TODO user_data
	}

	typedef struct {
		AMPromise* p;
		AyyiIdent  new_track_1;
		int        new_track_2;
		int        orig_track_count;
	} C;

	START_TEST;

	void finished (gpointer c)
	{
		g_free(c);
		FINISH_TEST;
	}

	AMPromise* create1 (gpointer user_data)
	{
		PF0;
		C* c = user_data;
		c->orig_track_count = ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks);

		void create1_done (AyyiIdent id, const GError* error, gpointer user_data)
		{
			C* c = user_data;
			c->new_track_1 = id;
			am_promise_resolve(c->p, NULL);
		}

		am_song_add_track(TRK_TYPE_AUDIO, "New Track", 1, create1_done, c);

		return am_promise_new(c);
	}

	void task_done (gpointer obj, gpointer c)
	{
		PF0;
		finished(c);
	}

	AMPromise* create2 (gpointer user_data)
	{
		PF0;
		C* c = user_data;
		return am_promise_new(c);
	}

	C* c = AYYI_NEW(C,
		.new_track_1      = AYYI_NULL_IDENT,
		.new_track_2      = -1,
		.orig_track_count = -1
	);

	return finished(c); // TODO this test needs some work

	c->p = create1(c);
	am_promise_add_callback(c->p, task_done, c);

	am_promise_pipe(c->p, create2(c));
}


void
test_delete_all_tracks()
{
	START_TEST;

	void test_delete_all_tracks_done(AyyiIdent obj, gpointer data)
	{
		assert(ayyi_verify_playlists(), "playlist verify");
		FINISH_TEST;
	}

	module__delete_all_tracks(test_delete_all_tracks_done, NULL);
}


void
test_stress_audio_track ()
{
	#define MASTER_COUNT 1

	START_TEST;

	int get_track_count()
	{
		int n = 0;
		AMTrack* track;
		AMIter i;
		am_collection_iter_init (am_tracks, &i);
		while((track = am_collection_iter_next(am_tracks, &i))){
			n++;
		}
		return n;
	}

	int get_master_track_count()
	{
		int n = 0;
		for(int i=0;i<AM_MAX_TRK;i++){
			AMTrack* track = song->tracks->track[i];
			if(!track) break;
			if(am_track__is_master(track)) n++;
		}
		return n;
	}

	void print_tracks ()
	{
		int empty = 0;
		for (AMTrackNum t=0;t<AM_MAX_TRK;t++) {
			AMTrack* tr = song->tracks->track[t];
			if(!tr){ printf("  %i NULL\n", t); break; }
			if(!tr->type) empty++;
			AyyiTrack* route = ayyi_song__audio_track_at(tr->ident.idx);
			printf("  %i %i flags=%i %s\n", t, tr->type, route ? route->flags : -1, tr->name);
			if(empty > 1) break;
		}
		printf("-----------------------------------------------------\n");
	}

	void _5__delete_track_finished(AyyiIdent obj, gpointer data)
	{
		PF;
		dbg(0, "count=%i (2)", get_track_count());
		assert(get_track_count() == MIN_TRACKS + 2, "");

		am_song__print_tracks();
		print_tracks();

		AMTrackNum first_empty = MIN_TRACKS + 2 + get_master_track_count();
		assert((song->tracks->track[first_empty]     == NULL), "%i not empty! name='%s' type=%i", first_empty, song->tracks->track[first_empty]->name, song->tracks->track[first_empty]->type);
		dbg(0, "2: %i", first_empty - 1);
		assert((song->tracks->track[first_empty - 1] != NULL), "track unexpectedly empty");
		FINISH_TEST;
	}

	void _4__add_track_finished ()
	{
		PF;
		assert(get_track_count() == MIN_TRACKS + 3, "unexpected track count");
		print_tracks();
		module__delete_audio_track(_5__delete_track_finished, NULL, song->tracks->track[1]);
	}

	void _3__add_track_finished ()
	{
		PF;
		int expected_track_count = MIN_TRACKS + 2;
		assert(get_track_count() == expected_track_count, "unexpected track count");
		module__add_audio_track(_4__add_track_finished, NULL);
	}

	void _2__add_track_finished ()
	{
		PF;
		dbg(0, "count=%i (1)", get_track_count());
		int expected_track_count = MIN_TRACKS + 1;
		assert(get_track_count() == expected_track_count, "unexpected track count");

		print_tracks();
		AMTrackNum first_empty = expected_track_count + get_master_track_count();
		assert((song->tracks->track[first_empty] == NULL), "");

		module__add_audio_track(_3__add_track_finished, NULL);
	}

	void _1__delete_all_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		dbg(0, "count=%i (0)", get_track_count());
		int expected_track_count = MIN_TRACKS;
		assert((get_track_count() == expected_track_count), "track count not %i (is %i)", MIN_TRACKS, get_track_count());
		print_tracks();
		AMTrackNum first_empty = expected_track_count + MASTER_COUNT;
		dbg(0, "checking %i...", first_empty);
		assert((song->tracks->track[first_empty] == NULL), "");

		module__add_audio_track(_2__add_track_finished, NULL);
	}

	AyyiChannel* master = &((AyyiSongService*)ayyi.service)->amixer->master;
	assert(master, "master track");

	module__delete_all_tracks (_1__delete_all_finished, NULL);
}


void
test_add_audio_part ()
{
	START_TEST;

	void _2_add_part_done (AyyiIdent obj, gpointer data)
	{
		PF;
		assert(g_list_length(song->parts->list) == 1, "n_parts == 1");
		AMPart* part = song->parts->list->data;
		assert(part->status == PART_OK, "part status");
		assert(part->ident.idx >= 0, "part index");
		FINISH_TEST;
	}

	void _1_delete_all_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		assert(g_list_length(song->parts->list) == 0, "n_parts == 0");
		module__add_audio_part(_2_add_part_done, NULL);
	}

	module__delete_all_parts (_1_delete_all_finished, NULL);
}


void
test_select_part ()
{
	START_TEST;

	void _1_select_part_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_TEST;
	}

	module__select_part (_1_select_part_finished, NULL, NULL);
}


void
test_select_track ()
{
	START_TEST;

	void _1_select_track_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_TEST;
	}

	module__select_track (_1_select_track_finished, NULL, NULL);
}


void
test_split_part ()
{
	START_TEST;

	static AyyiTrack* track = NULL;

	void on_split_2 (AyyiIdent obj, gpointer data)
	{
		FINISH_TEST;
	}
	void on_split_1 (AyyiIdent obj, gpointer data)
	{
		bool _is_correct_track (void* _part, void* user_data)
		{
			AMPart* part = _part;
			AMTrack* t = part->track;
			AyyiTrack* tr = ayyi_song__audio_track_at(t->ident.idx);
			dbg(2, "  %p %p", tr, track);
			return tr == track;
		}

		FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_parts, _is_correct_track, NULL);
		AMPart* part = (AMPart*)filter_iterator_next(i);
		filter_iterator_unref(i);
		assert(part, "part not found on track '%s'", track->name);

		module__split_part(on_split_2, part, NULL);
	}

	void split__move_part_done (AyyiIdent obj, GError** error, gpointer _part)
	{
		assert(_part, "part");
		module__split_part(on_split_1, (AMPart*)_part, NULL);
	}

	void split__add_part_done (AyyiIdent obj, gpointer user_data)
	{
		AMPart* part = am_song__get_part_by_ident(obj);
		assert(part, "part");

		AMTrack* t = am_track_list_find_by_shm_idx(song->tracks, track->shm_idx, TRK_TYPE_AUDIO);
		assert(t, "track");
		am_part_set_track(part, t, split__move_part_done, part);
	}

	void split__add_track_done (AyyiIdent obj, gpointer user_data)
	{
		track = ayyi_song__audio_track_at(obj.idx);
		assert(track, "track");

		module__add_audio_part(split__add_part_done, NULL);
	}

	module__add_audio_track(split__add_track_done, NULL);
}


void
test_add_remove_audio_part ()
{
	START_TEST;
	static bool finished; finished = false;

	void _3_remove_part_done(AyyiIdent obj, gpointer user_data)
	{
		PF;
		assert(am_collection_length(am_parts) == 0, "n_parts == 0");
		assert(!finished, "already finished!");
		finished = true;
		FINISH_TEST;
	}

	void _2_add_part_done (AyyiIdent obj, gpointer data)
	{
		PF;
		assert(am_collection_length(am_parts) == 1, "n_parts == 1 (%i)", am_collection_length(am_parts));

		module__delete_audio_part(_3_remove_part_done, (AMPart*)am_list_first(song->parts), NULL);
	}

	void _1_delete_all_finished (AyyiIdent obj, gpointer data)
	{
		PF;
		assert(am_collection_length(am_parts) == 0, "n_parts == 0");
		module__add_audio_part(_2_add_part_done, NULL);
	}

	module__delete_all_parts(_1_delete_all_finished, NULL);
}


void
test_add_n_audio_parts ()
{
	//as above but dont delete existing parts first.
	START_TEST;
	static int n_to_add = 2;
assert(ayyi_verify_playlists(), "playlist verify");

	static int n_added = 0;
	static int n_parts; n_parts = am_collection_length(am_parts);

	void add_finished (AyyiIdent obj, AyyiFinish cb)
	{
		PF;
		n_added++;
		assert(am_collection_length(am_parts) == n_parts + n_added, "wrong part count");
		dbg(0, "n_added=%i", n_added);
		if(n_added >= n_to_add){
			FINISH_TEST;
		}
		if(cb) module__add_audio_part((AyyiFinish)cb, cb);
	}

	module__add_audio_part ((AyyiFinish)add_finished, add_finished);
}


void
test_add_n_audio_parts_concurrent ()
{
	//as above but dont delete existing parts first.
	START_TEST;
	static int n_to_add = 12;
	static int n_concurrent = 4;

	static int n_added = 0;
	static int n_requested = 0;
	static int n_parts; n_parts = am_collection_length(am_parts);

	assert(ayyi_verify_playlists(), "playlist verify");

	void add_finished (AyyiIdent obj, AyyiFinish cb)
	{
		PF;
		if (TEST.current.finished) return; //in case already failed.

		n_added++;
		assert(n_added <= n_to_add, "too many parts added");
		dbg(0, "n_added=%i", n_added);
		if (n_added == n_to_add) {
			assert(am_collection_length(am_parts) == n_parts + n_added, "part count wrong");
			FINISH_TEST;
		}
		if(cb && (n_requested < n_to_add)){
			module__add_audio_part((AyyiFinish)cb, cb);
			n_requested++;
		}
	}

	void on_have_pool_item (AyyiIdent id, gpointer user_data)
	{
		int i; for(i=0;i<n_concurrent;i++){
			module__add_audio_part ((gpointer)add_finished, add_finished);
		}
		n_requested = n_concurrent;
	}
	module__ensure_pool_item(on_have_pool_item, NULL);
}


void
test_move_part()
{
	START_TEST;

	void move_part__have_part (AyyiIdent p, gpointer _c)
	{
		AMPart* part = am_list_first(song->parts);
		assert(part, "no parts");

		typedef struct
		{
			int           example;
		} C;

		void test_move_part_done (AyyiIdent id, GError** error, gpointer __data)
		{
			C* data = __data;
			assert(data->example == 7, "closure data check failed");

			gboolean finished(gpointer data)
			{
				FINISH_TEST_TIMER_STOP;
			}
			dbg(0, "done! %i", data->example);
			g_free(data);
			g_timeout_add(200, finished, NULL); // allow other callbacks to finish in case any errors occur.
		}

		AyyiSongPos delta = {4,};
		AyyiSongPos* position = &part->start;
		ayyi_pos_add(position, &delta);

		// am_part_move now has a callback argument, so its not strictly neccesary to use a responder.
		ayyi_client_watch_once(part->ident, AYYI_SET, test_move_part_done, AYYI_NEW(C,
			.example = 7
		));

		am_part_move(part, position, NULL, NULL, NULL);
	}

	module__ensure_part(move_part__have_part, NULL);
}


void
test_copy_part ()
{
	START_TEST;

	typedef struct
	{
		AMPart*    part;
		AMTrack*   track;
	} C;

	bool partlist_contains_duplicates ()
	{
		AMPart* part = NULL;
		AMIter i;
		am_collection_iter_init (am_parts, &i);
		while((part = am_collection_iter_next(am_parts, &i))){
			FilterIterator* f = filter_iterator_new (0, NULL, NULL, am_parts, (Filter)am_part__has_id, &part->ayyi->id);
			AMPart* _part = (AMPart*)filter_iterator_next(f);
			assert_and_stop(_part, "cannot find ourself");
			_part = (AMPart*)filter_iterator_next(f);
			assert_and_stop(!_part, "duplicate id found");
			filter_iterator_unref(f);
		}
		return false;
	}

	void test_copy_done (AyyiIdent p, GError** error, gpointer _c)
	{
		FAIL_IF_ERROR;
		C* c = _c;
		assert(p.idx > 0, "index not set");
		AMPart* part = am_list_find_by_ident(song->parts, p);
		assert((part), "cannot find copied part")
		assert((part != c->part), "part is the same")
		AyyiRegionBase* region = part->ayyi;
		assert((region), "ayyi not set");
		assert((region->shm_idx != c->part->ayyi->shm_idx), "part same idx")
		assert((region->id != c->part->ayyi->id), "part same id")
		am_partmanager__print_list();
		assert(!partlist_contains_duplicates(), "part list contains duplicates");
		g_free(c);
		FINISH_TEST;
	}

	void copy_part__have_part (AyyiIdent obj, gpointer _c)
	{
		PF0;
		C* c = _c;
		c->part = am_song__get_part_by_ident(obj);
		assert(c->part, "part");

		AyyiSongPos* position = &c->part->start;
		ayyi_pos_add(position, &(AyyiSongPos){4,}); //TODO there may be a part here already

		AMPoolItem* pi = NULL;
		uint64_t len = 0;
		am_song__add_part(AYYI_AUDIO, c->part->ayyi->shm_idx, pi, c->part->track->ident.idx, position, len, 0, NULL, 0, test_copy_done, c);
	}

	void copy_part__have_track(AyyiIdent new_obj, gpointer _c)
	{
		C* c = _c;
		c->track = am_track_list_find_by_ident (song->tracks, new_obj);
		assert(c->track, "cannot get AMtrack");

		module__ensure_track_has_part(copy_part__have_part, c->track, c);
	}

	module__add_audio_track(copy_part__have_track, g_new0(C, 1));
}


void
test_part_change_track ()
{
	START_TEST;

	static int n_iterations = 5;

	typedef struct _c
	{
		AMPart*  part;
		AMTrack* track;
		AMTrack* old_track;
		int      i;
		void     (*next)(struct _c*);
	} C;

	bool am_track__is_other (AMTrack* track, gpointer other_track)
	{
		return track != (AMTrack*)other_track;
	}

	void test_track_change_done (AyyiIdent id, GError** error, gpointer _c)
	{
		C* c = _c;
		FAIL_IF_ERROR;
		assert((id.idx == c->part->ident.idx), "incorrect part ident returned: %i", id.idx);
		AMPart* part = am_collection_find_by_idx((AMCollection*)song->parts, id.idx);
		assert(part, "part not found for idx=%i", id.idx);
		assert((part->track != c->old_track), "track is unchanged");
		assert((part->track == c->track), "wrong track");

		if(++c->i >= n_iterations){
			g_free(c);
			FINISH_TEST;
		}else{
			//c->track = part->track;
			c->next(c);
		}
	}

	void do_iteration (C* c)
	{
		FilterIterator* i = am_track_iterator_new ((Filter)am_track__is_other, c->part->track);
		c->track = (AMTrack*)filter_iterator_next(i);
		filter_iterator_unref(i);

		if(c->track){
			c->old_track = c->part->track;
			am_part_set_track(c->part, c->track, test_track_change_done, c);
		}else{
			dbg(0, "TODO make new track");
		}
	}

	C* c = AYYI_NEW(C,
		.part = am_list_first(song->parts),
		.next = do_iteration
	);
	assert(c->part, "no parts");

	c->next(c);
}


void
test_rename_part ()
{
	START_TEST;

	typedef struct _c
	{
		AMPart*  part;
		char*    old_name;
		char*    new_name;
		guint    connection;
	} C;
	C* c = g_new0(C, 1);

	c->part = am_list_first(song->parts);
	assert(c->part, "no parts");
	c->old_name = g_strdup(c->part->name);
	c->new_name = g_strdup_printf("%s-renamed", c->part->name);

	void got_signal (GObject* parts, AMPart* part, int change, void* src, gpointer _c)
	{
		C* c = _c;
		assert(c, "bad user_data");
		assert((part == c->part), "bad signal");
		assert(((change & AM_CHANGE_NAME) == AM_CHANGE_NAME), "bad change type: %i (expected %i)", change, AM_CHANGE_NAME);
		g_signal_handler_disconnect(am_parts, c->connection);
		c->connection = 0;
	}
	c->connection = g_signal_connect(am_parts, "item-changed", G_CALLBACK(got_signal), c);

	void test_rename_part_on_rename (AyyiIdent id, GError** error, gpointer _c)
	{
		C* c = _c;
		FAIL_IF_ERROR;
		AMPart* part = am_collection_find_by_idx((AMCollection*)song->parts, id.idx);
		assert(part, "part");
		assert(!strcmp(c->new_name, part->name), "incorrect name");
		assert(!c->connection, "signal not received");
		g_free(c->old_name);
		g_free(c->new_name);
		g_free(c);
		FINISH_TEST;
	}
	am_part_rename(c->part, c->new_name, test_rename_part_on_rename, c);
}


void
test_add_lots_of_parts ()
{
	START_TEST;
	//int n_regions = 129;
	int n_regions = 33;

	void add_n_parts_done(AyyiIdent obj, gpointer data)
	{
		FINISH_TEST;
	}

	module__add_n_audio_parts(add_n_parts_done, n_regions, NULL);
}


void
test_stress_audio_parts ()
{
	START_TEST;
	int n_regions = 129;

	static int n_iterations = 0;
	static int max_iterations = 2;//88;

	static int n_added; n_added = 0;
	static int n_removed; n_removed = 0;

	//long test. have to disable timeout.
	g_source_remove (TEST.timeout);
	TEST.timeout = 0;

	static AyyiFinish done = NULL;
	void iteration_done (AyyiIdent obj, gpointer data)
	{
		dbg(0, "------------------------------------------------");
		if(n_iterations++ >= max_iterations){
			dbg(0, "finished. n_added=%i n_removed=%i", n_added, n_removed);
			FINISH_TEST;
		}

		gboolean next (gpointer data)
		{
			int r = g_random_int_range(0, 1 + 1);
			if(r){
				module__add_audio_part(done, NULL);
				n_added++;
			}else{
				module__delete_random_part(done, NULL);
				n_removed++;
			}
			return G_SOURCE_REMOVE;
		}
		g_timeout_add(50, next, NULL);
	}
	done = iteration_done;

	module__add_n_audio_parts(iteration_done, n_regions, NULL);
}


void
test_add_midi_track ()
{
	START_TEST;
	void test_add_midi_track_done(AyyiIdent id, gpointer data)
	{
		FINISH_TEST;
	}

	module__add_midi_track(test_add_midi_track_done, NULL);
}


void
test_add_remove_midi_part ()
{
	START_TEST;
	void test_remove_midi_part_done(AyyiIdent obj, gpointer data)
	{
		FINISH_TEST;
	}
	void test_add_midi_part_done(AyyiIdent obj, gpointer data)
	{
		AMPart* part = am_list_find_by_ident(song->parts, obj);
		assert(part, "no part");
		module__delete_midi_part(test_remove_midi_part_done, part, NULL);
	}

	module__add_midi_part(test_add_midi_part_done, NULL);
}


void
test_delete_midi_track ()
{
	START_TEST;

	assert(ayyi_verify_playlists(), "playlist verify");
	static int n_tracks; n_tracks = ayyi_song__get_midi_track_count();

	void
	test_delete_midi_track_done(AyyiAction* a)
	{
		assert(!a->ret.error, "%s", a->ret.error->message);
		assert(ayyi_song__get_midi_track_count() == n_tracks - 1, "wrong midi track count: %i", ayyi_song__get_midi_track_count());
		assert(ayyi_verify_playlists(), "playlist verify");
		FINISH_TEST;
	}

	AyyiMidiTrack* track = ayyi_song__midi_track_next(NULL);
	if(!track) FAIL_TEST("no midi tracks to delete");
	dbg(0, "n_tracks=%i", ayyi_song__get_midi_track_count());

	AyyiAction* a   = ayyi_action_new("midi track delete %i", track->shm_idx);
	a->callback     = test_delete_midi_track_done;
	a->op           = AYYI_DEL;
	a->obj.type     = AYYI_OBJECT_MIDI_TRACK;
	a->obj_idx.idx1 = track->shm_idx;
	am_action_execute(a);
}


void
test_track_add_remove()
{
	//adds and deletes an audio track.

	START_TEST;
	static AyyiHandler callbacks[3] = {NULL, NULL, NULL};

	void
	_02_on_track_delete(AyyiIdent obj, gpointer user_data)
	{
		FINISH_TEST;
	}

	void
	_01_start_track_delete(AyyiIdent id, GError** error, gpointer user_data)
	{
		//delete the previously created track.

		FAIL_IF_ERROR;

		AyyiIdx new_object_idx = id.idx;
		if(!new_object_idx) FAIL_TEST("no index");
		AyyiTrack* track = ayyi_song__audio_track_at(new_object_idx);
		if(!track) FAIL_TEST("no track");

		step++;

		AMTrack* trk = am_track_list_find_by_ident (song->tracks, id);
		assert(trk, "cannot get AMtrack");

		module__delete_audio_track((gpointer)callbacks[step], NULL, trk);
	}

	void
	_00_on_track_create(AyyiIdent new_obj, gpointer user_data)
	{
		//this will be called following creation of a new (track) object, so we have a reference to the new object.

#if 0
		AyyiTrack* track = ayyi_song__audio_track_at(new_obj.idx);
		dbg(0, "idx=%i name='%s'", new_obj.idx, track ? track->name : NULL);
#endif

		NEXT_CALLBACK(new_obj, NULL, user_data);
	}

	if(!callbacks[1]){
		step = 0;
		callbacks[0] = (AyyiHandler)_00_on_track_create;
		callbacks[1] = _01_start_track_delete;
		callbacks[2] = (AyyiHandler)_02_on_track_delete;
	}

	module__add_audio_track(_00_on_track_create, NULL);
}


void
test_note_selection ()
{
	//test the note_selection 'object'.

	START_TEST;

	void
	on_new_midi_part (AyyiIdent obj, GError** error, gpointer user_data)
	{
		dbg(0, "...");
		FAIL_IF_ERROR;

		AMPart* part = am_list_find_by_ident(song->parts, obj);
		assert(part, "no part");
		assert(PART_IS_MIDI(part), "part not midi");
		dbg(0, "got new part");

		AyyiMidiNote note = {0, 60, 100, 44100, 960};
		GList* main_selection = g_list_append(NULL, &note);
		am_part__set_note_selection(part, main_selection);

		GList* selection = NULL;
		note_selection_list__new(&selection, part);
		note_selection_list__print(&selection);

		note_selection_list__reset(selection, part);
		note_selection_list__print(&selection);
		if (g_list_length(selection) != 1) FAIL_TEST("");
		NoteSelectionListItem* item = selection->data;
		if (item->notes) FAIL_TEST("");

#if 0
		note_selection_list__free(&selection);
#else
		selection = NULL; //FIXME
#endif
		assert((g_list_length(selection) == 0), "selection not cleared");
		note_selection_list__print(&selection);

		AyyiMidiRegion* mpart = ayyi_song__midi_region_at(obj.idx);
		assert(ayyi_song_container_verify(&mpart->events), "events container corrupted");

		FINISH_TEST;
	}

	void have_midi_track(AyyiIdent obj, gpointer data)
	{
		void
		create_midi_part()
		{
			AMPos length = {4,};
			uint64_t length_mu = pos2mu(&length);
			unsigned colour = 10;
			AMPos start = {step * 4 + 4, 0, 0};
			AyyiSongPos start_; songpos_gui2ayyi(&start_, &start);
			AMTrack* midi_track = am_track_list_get_first_visible(song->tracks, TRK_TYPE_MIDI);

			am_song__add_part(AYYI_MIDI, 0, NULL, midi_track->ident.idx, &start_, length_mu, colour, "test part", 0, on_new_midi_part, NULL);
		}

		create_midi_part();
	}
	module__ensure_midi_track(have_midi_track, NULL);
}


void
test_remove_all_audio_files()
{
	START_TEST;

	void test_remove_all_audio_files_done(AyyiIdent obj, gpointer data)
	{
		PF;
		dbg(0, "n_files=%i", am_collection_length(am_pool));

		FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_parts, am_part__is_audio, NULL);
		AMPart* part = (AMPart*)filter_iterator_next(i);
		filter_iterator_unref(i);

		if(part){
			assert(PART_IS_AUDIO(part), "audio part iterator returned non-audio part!");
		}
		assert(!part, "total audio parts not zero");
		FINISH_TEST;
	}

	module__delete_all_audio_files (test_remove_all_audio_files_done, NULL);
}


void 
test_add_audio_file ()
{
	START_TEST;

	void ready_to_add ()
	{
		void on_wav_added (AyyiIdent obj, gpointer data)
		{
			PF;
			FINISH_TEST;
		}

		char* wav = find_wav(mono_wav_fixture);
		module__load_wav_fixture(on_wav_added, NULL, wav);
		g_free(wav);
	}

	if(ayyi_song__have_file(mono_wav_fixture)){
		void on_wav_removed (AyyiIdent obj, gpointer data)
		{
			PF;
			ready_to_add();
		}
		module__delete_audio_file(on_wav_removed, NULL, mono_wav_fixture);
	}else{
		ready_to_add();
	}
}


void 
test_add_stereo_audio_file ()
{
	START_TEST;

	assert(!ayyi_song__have_file(stereo_wav_fixture), "file already loaded");

	void on_stereo_wav_added (AyyiIdent obj, gpointer data)
	{
		PF;
		AMPoolItem* p = am_pool__get_item_by_orig_name (stereo_wav_fixture);
		assert(p, "pool item for added file not found");
		assert(p->channels == 2, "pool item has wrong channel count");
		assert(pool_item_file_exists(p), "file not found");
#ifdef DEBUG
		if(am_pool__get_new_items()){
			GList* items = am_pool__get_new_items();
			GList* l = items;
			for(;l;l=l->next){
				AMPoolItem* pi = l->data;
				dbg(0, " !! pending: %s", pi->leafname);
			}
		}
#endif
		assert(!am_pool__get_new_items(), "pool has pending items");
		FINISH_TEST;
	}

	module__load_wav_fixture(on_stereo_wav_added, NULL, stereo_wav_fixture);
}


void 
test_remove_audio_file ()
{
	START_TEST;

	char* wav = find_wav(mono_wav_fixture);
	assert(ayyi_song__have_file(wav), "file not loaded (%s)", wav);
	g_free(wav);

	void on_wav_removed (AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_TEST;
	}

	module__delete_audio_file (on_wav_removed, NULL, mono_wav_fixture);
}


void 
test_remove_stereo_audio_file()
{
	START_TEST;

	assert(ayyi_song__have_file(stereo_wav_fixture), "file not loaded");

	void on_wav_removed(AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_TEST;
	}

	module__delete_audio_file(on_wav_removed, NULL, stereo_wav_fixture);
}


void
test_midi()
{
	// add midi track, add midi part, add notes

	START_TEST;

	void _7_delete_note_done(AyyiIdent obj, gpointer data)
	{
		PF;
		FINISH_TEST;
	}
	void _6_modify_notes_done(AyyiIdent obj, gpointer data)
	{
		PF;
		module__delete_midi_note(_7_delete_note_done, NULL);
	}
	void _5_add_notes_done(AyyiIdent obj, gpointer data)
	{
		PF;
		module__modify_midi_notes(_6_modify_notes_done, NULL);
	}
	void _4_add_notes_done(AyyiIdent obj, gpointer data)
	{
		PF;
		module__add_midi_notes(_5_add_notes_done, NULL);
	}
	void _3_add_part_done(AyyiIdent obj, gpointer data)
	{
		PF;
		module__add_midi_notes(_4_add_notes_done, NULL);
	}
	void _2_load_finished(AyyiIdent obj, gpointer data)
	{
		PF;
		module__add_midi_part(_3_add_part_done, NULL);
	}
	void _1_add_midi_track_done(AyyiIdent id, gpointer data)
	{
		PF;
#if 0
		module__save_and_load_song_with_delay(_2_load_finished, NULL, song_fixture1);
#else
		// TODO song loading is currently broken so we bypass it.
		_2_load_finished((AyyiIdent){0,}, NULL);
#endif
	}
	module__add_midi_track(_1_add_midi_track_done, NULL);
}


void
test_rename_track()
{
	START_TEST;
	
	void test_rename_track_done(AyyiIdent id, GError** error, gpointer user_data)
	{
		PF;
		FAIL_IF_ERROR;

		AMTrack* tr = am_track_list_find_by_ident (song->tracks, id);
		assert(tr, "cant find track");

		AyyiTrack* track = (AyyiTrack*)am_track__get_shared(tr);
		assert(track, "cant find ayyi track");
		assert(!strcmp(track->name, tr->name), "AM track name doesnt match Ayyi track name")

		//check for playlist mismatch
		bool playlist_valid(AMTrack* tr)
		{
			bool ok = true;
			GList* parts = am_partmanager__get_by_track(tr);
			if(parts){
				GList* l = parts;
				for(;l;l=l->next){
					AyyiRegionBase* part = ((AMPart*)l->data)->ayyi;

					AyyiPlaylist* playlist = ayyi_song__playlist_at(part->playlist);
					if(!playlist){ ok = false; break; }
					AyyiTrack* t = ayyi_song__get_track_by_playlist(playlist);
					if(t->shm_idx != tr->ident.idx){ ok = false; break; };
				}
				g_list_free(parts);
			}
			else pwarn("no parts. nothing to test.");
			return ok;
		}

		assert(playlist_valid(tr), "playlist invalid for track '%s'", tr->name);

		FINISH_TEST;
	}

	void test_rename_track__on_track_has_part (AyyiIdent obj, gpointer data)
	{
		AMTrack* track = data;
		char* name = g_strdup_printf("%s-renamed", track->name);
		dbg(0, "renaming track: %s --> %s", track->name, name);
		am_track__set_name(track, name, test_rename_track_done, name);
		g_free(name);
	}

	AMTrack* track = am_track_list_get_first_visible(song->tracks, TRK_TYPE_AUDIO);
	module__ensure_track_has_part(test_rename_track__on_track_has_part, track, track);
}


void
test_add_plugin()
{
	START_TEST;

	void on_plugin_added(AyyiIdent obj, gpointer data) { FINISH_TEST; }

	module__add_plugin(on_plugin_added, NULL);
}


void
test_fader ()
{
	START_TEST;

	static double vals[] = {0.01, 0.1, 1.0, 1.9};

	typedef struct
	{
		const AMChannel* ch;
		AyyiChannel*     ac;
		int              n;
	} C;
	C* c = g_new0(C, 1);
	static C* cc; cc = c;

	void on_have_track (AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		AMTrack* track = am_track_list_find_by_ident(song->tracks, t);
		assert(track, "track not found");

		c->ch = am_track__lookup_channel(track);
		assert(track, "failed to get Channel");
		//float val = am_channel__fader_level(c->ch);
		c->ac = ayyi_mixer__channel_at_quiet(c->ch->ident.idx);
		dbg(0, "%i val=%.4f", c->ch->ident.idx, c->ac->level);

		void set_level(C* c)
		{
			ayyi_channel__set_float(c->ch->ident.idx, AYYI_LEVEL, vals[c->n]);
			dbg(0, "set float done");
		}
		set_level(c);

		void finish() { FINISH_TEST; }

		gboolean test_fader_on_timeout (gpointer _c)
		{
			C* c = _c;
			assert_and_stop((c == cc), "bad closure data");
			//double val = am_channel__fader_level(c->ch); //TODO why does this return the wrong value?
			AyyiChannel* ac = ayyi_mixer__channel_at_quiet(c->ch->ident.idx);
			dbg(0, "%i: val=%.4f", c->ch->ident.idx, ac->level);
			//ayyi_print_mixer();
			assert_and_stop((ABS(ac->level - vals[c->n]) < 0.0001), "wrong fader value1 %.3f (expected %.3f)", ac->level, vals[c->n]);

			c->n++;
			if(c->n < G_N_ELEMENTS(vals)){
				set_level(c);
				return G_SOURCE_CONTINUE;
			}else{
				finish();
				return G_SOURCE_REMOVE;
			}
		}

		g_timeout_add(1000, test_fader_on_timeout, c);
	}

	module__ensure_audio_track(on_have_track, c);
}


void
test_pan ()
{
	START_TEST;

	static double vals[] = {-1.0, -0.5, 0.1, 1.0, 1.1, 0.0};
	typedef struct
	{
		AMIter       i;
		AMChannel*   ch;
		AyyiChannel* ac;
		int          n;
	} C;
	C* c = g_new0(C, 1);
	static C* cc; cc = c;

	void on_have_track(AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		channel_iter_init(&c->i);
		while((c->ch = channel_next(&c->i))){
			c->ac = ayyi_mixer__channel_at_quiet(c->ch->ident.idx);
			if(c->ch->type != AM_CHAN_MASTER && c->ac->n_in == 1) break;
		}
		dbg(0, "channel %i:%s", c->ch->ident.idx, c->ac->name);
		assert((c->ac->n_in == 1), "channel %i:%s is not mono", c->ch->ident.idx, c->ac->name);
		assert(c->ac->has_pan, "channel %i:%s has no pan control", c->ch->ident.idx, c->ac->name);
#if 0
		AyyiTrack* track = ayyi_song__audio_track_at(c->ch->ident.idx);
		dbg(0, "%i '%s' has_pan=%i val=%.4f", c->ch->ident.idx, track->name, c->ac->has_pan, c->ac->pan);
#endif

		void set_pan(C* c)
		{
			ayyi_channel__set_float(c->ch->ident.idx, AYYI_PAN, vals[c->n]);
		}
		set_pan(c);

		void finish() { FINISH_TEST; }

		gboolean test_pan_post_check(gpointer _c)
		{
			C* c = _c;
			assert_and_stop((c == cc), "bad closure data");
#ifdef DEBUG
			double val2 = am_channel__pan_value(c->ch, NULL); //TODO does this return the wrong value?
			dbg(0, "%i: val=%.4f %.4f", c->ch->ident.idx, c->ac->pan, val2);
#endif
			if(vals[c->n] <= 1.0){
				assert_and_stop((ABS(c->ac->pan - vals[c->n]) < 0.0001), "wrong pan value: %.3f. expected: %.3f", c->ac->pan, vals[c->n]);
			}else{
				//we specified an out-of-range value, so request should not be respected.
				assert_and_stop((ABS(c->ac->pan - vals[c->n]) > 0.0001), "unexpected pan value: %.3f. expected: != %.3f", c->ac->pan, vals[c->n]);
			}

			if(++c->n >= G_N_ELEMENTS(vals)){
				if((c->ch = channel_next(&c->i))){
					c->ac = ayyi_mixer__channel_at_quiet(c->ch->ident.idx);
					dbg(0, "channel=%s", c->ac->name);
					c->n = 0;
				}else{
					finish();
					return G_SOURCE_REMOVE;
				}
			}
			set_pan(c);
			reset_timeout(3000);
			return G_SOURCE_CONTINUE;
		}

		g_timeout_add(1000, test_pan_post_check, c);
	}

	module__ensure_pannable_track(on_have_track, c);
}


void
test_mute()
{
	START_TEST;

	typedef struct
	{
		AMIter       i;
		AMChannel*   ch;
		AyyiChannel* ac;
		int          n;
	} C;
	C* c = g_new0(C, 1);
	static C* cc; cc = c;

	void on_have_track(AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		channel_iter_init(&c->i);
		c->ch = channel_next(&c->i);
		assert(c->ch, "failed to get Channel");
		c->ac = ayyi_mixer__channel_at_quiet(c->ch->ident.idx);
		dbg(0, "%i val=%.4f", c->ch->ident.idx, c->ac->pan);

		void set_mute(C* c)
		{
			ayyi_channel__set_bool(c->ch->ident.idx, AYYI_MUTE, c->n % 2);
		}
		set_mute(c);

		void finish() { FINISH_TEST; }

		gboolean test_mute_on_timeout(gpointer _c)
		{
			PF0;
			C* c = _c;
			assert_and_stop((c == cc), "bad closure data");

			bool muted = am_channel__is_muted(c->ch);
			dbg(0, "%i: val=%i", c->n % 2, (int)muted);
			assert_and_stop(((c->n % 2) == muted), "wrong mute value %i %i", c->n % 2, muted);

			if(++c->n >= 4){
				if((c->ch = channel_next(&c->i))){
					c->ac = ayyi_mixer__channel_at_quiet(c->ch->ident.idx);
					dbg(0, "channel=%s", c->ac->name);
					c->n = 0;
				}else{
					finish();
					return G_SOURCE_REMOVE;
				}
			}
			set_mute(c);
			return G_SOURCE_CONTINUE;
		}

		g_timeout_add(1000, test_mute_on_timeout, c);
	}

	module__ensure_audio_track(on_have_track, c);
}


void
test_automation ()
{
	#define n_points 10

	START_TEST;

	typedef struct
	{
		AMTrack*     track;
		Curve*       curve;
		AyyiHandler  on_pt_added;
		AyyiHandler  on_pt_removed;
		int          n_added;
		int          automation_type;

	} C;

	char* print_auto_type (int T)
	{
		switch(T){
			case VOL: return "VOL"; break;
			case PAN: return "PAN"; break;
			default: return NULL;
		}
	}

	void add_pt (C* c)
	{
		int ins_pos = 0;
		BPath* inserted = curve_ins(c->curve, ins_pos);
		BPath* bpath = (BPath*)c->curve->path;
		BPath* bp = &bpath[ins_pos];
		assert((bp == inserted), "insert pointer");

		bp->code = curve_get_length(c->curve) > 1 ? LINETO : BP_END;
		bp->x1 = 0.0;                   bp->y1 =  0.0;  
		bp->x2 = 0.0;                   bp->y2 =  0.0;
		bp->x3 = 200 - (c->n_added + 1) * 8;  bp->y3 = 50.0;
		//am_automation_print_path(c->curve);

		c->n_added++;
		am_automation_point_add(c->curve, 0, c->on_pt_added, c);
	}

	void next_automation_type (C* c)
	{
		void test_automation_cleared (AyyiIdent id, GError** error, gpointer _c)
		{
			FAIL_IF_ERROR;
			PF0;
			C* c = _c;

			assert(!curve_get_length(c->curve), "%s automation not cleared", print_auto_type(c->automation_type));

			add_pt(c);
		}

		c->automation_type++;
		dbg(0, "------------------------------------------------");
		dbg(0, "auto_type=%s", print_auto_type(c->automation_type));
		dbg(0, "------------------------------------------------");
		if(c->automation_type < AUTO_MAX){
			if((c->curve = ((Curve**)&c->track->bezier)[c->automation_type])){
				am_track__clear_automation(c->track, c->curve, test_automation_cleared, c);
			}else{
				next_automation_type(c);
			}
		}else{
			g_free(c);
			FINISH_TEST;
		}
	}

	void remove_pt (C* c)
	{
		c->n_added--;
		am_automation_point_remove(c->curve, 0, c->on_pt_removed, c);
	}

	void test_automation_on_pt_removed (AyyiIdent id, GError** error, gpointer _c)
	{
		FAIL_IF_ERROR;
		PF0;
		C* c = _c;

		assert((curve_get_length(c->curve) == c->n_added), "%s automation length %zu. expected 1", print_auto_type(c->automation_type), curve_get_length(c->curve));

		if(curve_get_length(c->curve) > 0){
			remove_pt(c);
		}else{
			next_automation_type(c);
		}
	}

	void test_automation_on_pt_added (AyyiIdent id, GError** error, gpointer _c)
	{
		FAIL_IF_ERROR;
		PF0;
		C* c = _c;

		assert((curve_get_length(c->curve) == c->n_added), "%s automation length %zu. expected 1", print_auto_type(c->automation_type), curve_get_length(c->curve));

		AyyiChannel* channel = ayyi_mixer__channel_at(c->track->ident.idx);
		AyyiContainer* container = &channel->automation[c->automation_type];
		int count = ayyi_mixer_container_count_items(container);
		assert((count == c->n_added || count == c->n_added + 1), "ayyi count! %i (expected %i)", count, c->n_added);

		if(curve_get_length(c->curve) < n_points){
			add_pt(c);
		}else{
			remove_pt(c);
		}
	}

	void test_automation_on_have_track (AyyiIdent t, gpointer _c)
	{
		C* c = _c;
		dbg(1, "t.idx=%i", t.idx);
		t.type = AYYI_OBJECT_AUDIO_TRACK; //FIXME should be done upstream

		c->track = am_track_list_find_by_ident(song->tracks, t);
		assert(c->track, "track not found: %i", t.idx);
		assert((c->track->ident.idx == t.idx), "ident mismatch");

		next_automation_type(c);
	}

	module__ensure_pannable_track(test_automation_on_have_track, AYYI_NEW(C,
		.automation_type = -1,
		.on_pt_removed = test_automation_on_pt_removed,
		.on_pt_added = test_automation_on_pt_added,
	));
}


void
test_tempo ()
{
	START_TEST;

	typedef struct
	{
		int          dummy;
	} C;
	C* c = g_new0(C, 1);

	void test_tempo_done(AyyiIdent t, GError** error, gpointer _c)
	{
		FAIL_IF_ERROR;
		assert((((AyyiSongService*)ayyi.service)->song->bpm == 101.5), "incorrect tempo")
		g_free(_c);
		FINISH_TEST;
	}

	am_song__set_tempo(101.5, test_tempo_done, c);
}


void
test_meters()
{
	START_TEST;

	typedef struct
	{
		int          dummy;
	} C;
	C* c = g_new0(C, 1);

	void on_have_track(AyyiIdent t, gpointer c)
	{
		AMIter iter;
		AMChannel* channel = NULL;
		am_collection_iter_init((AMCollection*)song->channels, &iter);
		while((channel = am_collection_iter_next((AMCollection*)song->channels, &iter))){
			AyyiChannel* ch = ayyi_mixer__channel_at(channel->ident.idx);
			for(int c=0;c<ch->n_in;c++){
				float level = ch->visible_peak_power[c];
										// TODO need to make sure input monitoring is not on
				assert((level < -83.0), "meter level too high: %f (%s)", level, ch->name);
			}
		}

		g_free(c);
		FINISH_TEST;
	}

	module__ensure_audio_track(on_have_track, c);
}


void
test_909()
{
	START_TEST;

	gboolean test_909_post_launch (gpointer _c)
	{
		PF0;
		dbg(0, "TODO post-launch check");
		FINISH_TEST_TIMER_STOP;
		return G_SOURCE_REMOVE;
	}

	void run_plugin ()
	{
		bool found = false;
		GList* l = ayyi.launch_info;
		for(;l;l=l->next){
			AyyiLaunchInfo* info = l->data;

			dbg(0, "plugin=%s", info->name);
			if(!strcmp(info->name, "Ayyi 909")){
				found = true;
				ayyi_launch(info, NULL);

				g_timeout_add(1000, test_909_post_launch, NULL);
				break;
			}
		}
		assert(found, "909 plugin not found")
	}

	void remove_909 ()
	{
		//clean up from any previous runs

		//remove these tracks:
		// 909_1_kik
		// 909_2_snr
		// 909_3_hat
		// 909_4_hat
		void test_909_on_tracks_removed (AyyiIdent t, gpointer _c)
		{
			run_plugin();
		}

		char* track_names[4];
		track_names[0] = "909_1_kik";
		track_names[1] = NULL;
		module__delete_tracks_by_name(test_909_on_tracks_removed, NULL, track_names);
	}

	remove_909();
}


/*
 *  Test ayyi container iterator next and prev functions using 3 parts.
 *  Test ayyi_mixer__container_next and ayyi_mixer__container_prev using automation data.
 */
void
test_ayyi_container_iteration ()
{
	START_TEST;

	typedef struct
	{
		AyyiContainer* parts;
		AMTrack*       track;
	} C;

	gboolean container_is_empty (gpointer _c)
	{
		C* c = _c;
		g_return_val_if_fail(c->parts, FALSE);
		dbg(2, "n_regions=%i+%i", ayyi_song__get_audio_region_count(), ayyi_song__get_midi_region_count());
		return ayyi_container_is_empty(c->parts);
	}

	gboolean container_has_three_parts (gpointer _c)
	{
		C* c = _c;
		g_return_val_if_fail(c->parts, FALSE);
		assert_and_stop((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! 2");
		return ayyi_song__get_audio_region_count() == 3;
	}

	void test_on_automation_added (AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		assert(ayyi_mixer__verify(), "mixer verification failed after adding automation points");

		AyyiChannel* ch = ayyi_mixer__channel_at_quiet(c->track->ident.idx);
		AyyiContainer* events = &ch->automation[VOL];
		//int count = ayyi_mixer_container_count_items(events);
		if(_debug_ > 1) ayyi_container_print(events);
		//Curve* curve = c->track->bezier.vol;

		shm_event* event = NULL;
		int n = 0;
		while ((event = ayyi_mixer__container_next(events, (AyyiItem*)event))) {
			shm_event* ref = ayyi_mixer_container_get_item(events, n);
			assert((event == ref), "next iterator mismatch at %i: %p (ref=%p)", n, event, ref);
			n++;
		}
		assert((n == 3), "next: count %i", n);

		n = 0;
		event = NULL;
		while ((event = (shm_event*)ayyi_mixer__container_prev(events, (AyyiItem*)event))) {
			shm_event* ref = ayyi_mixer_container_get_item(events, 3 - 1 - n);
			assert((event == ref), "prev iterator mismatch at %i (%i): %p (ref=%p)", n, 3 - 1 -n, event, ref);
			n++;
		}
		assert((n == 3), "prev: count %i", n);

		g_free(c);
		FINISH_TEST;
	}

	void test_on_automation_cleared (AyyiIdent t, gpointer _c)
	{
		C* c = _c;
		assert(ayyi_mixer__verify(), "mixer verification failed after clearing existing automation");
		module__add_automation_points (test_on_automation_added, c->track, 3, c);
	}

	void test_on_parts_added (AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		void wait_done(AyyiIdent id, GError** e, gpointer _c)
		{
			C* c = _c;

			AyyiItem* item = NULL;
			int i;
			for (i=0;i<3;i++) {
				AyyiItem* a = ayyi_song_container_get_item(c->parts, i);
				item = ayyi_song_container_next_item(c->parts, item);
				assert((item == a), "next: mismatch %i", i);
			}
			assert((ayyi_song_container_next_item(c->parts, item) == NULL), "next final: mismatch %i", i);

			item = NULL;
			for (i=0;i<3;i++) {
				AyyiItem* a = ayyi_song_container_get_item(c->parts, 3  - 1 - i);
				item = ayyi_song_container_prev_item(c->parts, item);
				assert((item == a), "prev: mismatch %i", i);
			}
			assert((ayyi_song_container_prev_item(c->parts, item) == NULL), "prev final: mismatch %i", i);

			module__clear_automation (test_on_automation_cleared, c->track, c);
		}

		dbg(2, "waiting...");
		assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! -1");
		reset_timeout(20000);
		am_wait_for(container_has_three_parts, wait_done, _c);
	}

	void test_on_parts_deleted(AyyiIdent t, gpointer _c)
	{
		PF0;
		C* c = _c;
		assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! 0");

		void on_delayed(AyyiIdent id, GError** e, gpointer _c)
		{
			dbg(0, "");

			reset_timeout(40000);

			C* c = _c;
			assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! b");
			module__add_n_audio_parts(test_on_parts_added, 3, _c);
		}

		am_wait_for(container_is_empty, on_delayed, c);
	}

	void test_on_have_track (AyyiIdent t, gpointer __)
	{
		AMTrack* track = am_track_list_find_by_ident(song->tracks, t);
		assert(track, "track not found");

		module__delete_all_parts(test_on_parts_deleted, AYYI_NEW(C,
			.parts = &((AyyiSongService*)ayyi.service)->song->audio_regions,
			.track = track
		));
	}

	module__ensure_audio_track(test_on_have_track, NULL);
}


void
test_ayyi_container()
{
	START_TEST;

	typedef struct
	{
		AyyiContainer* parts;
	} C;
	C* c = AYYI_NEW(C,
		.parts = &((AyyiSongService*)ayyi.service)->song->audio_regions,
	);

	gboolean container_is_empty(gpointer _c)
	{
		C* c = _c;
		g_return_val_if_fail(c->parts, FALSE);
		dbg(2, "n_regions=%i+%i", ayyi_song__get_audio_region_count(), ayyi_song__get_midi_region_count());
		return ayyi_container_is_empty(c->parts);
	}

	gboolean container_is_one_block(gpointer _c)
	{
		C* c = _c;
		g_return_val_if_fail(c->parts, FALSE);
		assert_and_stop((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! 2");
		return ayyi_container_is_one_block_used(c->parts);
	}

	//-------------------------------------------------------

	void test_on_parts_added_again(AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		void wait_done(AyyiIdent id, GError** e, gpointer _c)
		{
			C* c = _c;
			PF;
			g_free(c);
			FINISH_TEST;
		}

		dbg(2, "waiting...");
		assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! -1");
		reset_timeout(20000);
		am_wait_for(container_is_one_block, wait_done, _c);
	}

	void test_on_parts_deleted_again(AyyiIdent t, gpointer _c)
	{
		PF0;
		C* c = _c;
		assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! 0");

		void on_container_really_empty(AyyiIdent id, GError** e, gpointer _c)
		{
			PF;
			reset_timeout(40000);
			module__add_n_audio_parts(test_on_parts_added_again, AYYI_BLOCK_SIZE, _c);
		}

		am_wait_for(container_is_empty, on_container_really_empty, _c);
	}

	void test_on_parts_added(AyyiIdent t, gpointer _c)
	{
		C* c = _c;

		void wait_done(AyyiIdent id, GError** e, gpointer _c)
		{
			C* c = _c;

			module__delete_all_parts(test_on_parts_deleted_again, c);
		}

		dbg(2, "waiting...");
		assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! -1");
		reset_timeout(20000);
		am_wait_for(container_is_one_block, wait_done, _c);
	}

	void test_on_parts_deleted(AyyiIdent t, gpointer _c)
	{
		PF0;
		C* c = _c;
		assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! 0");

		void on_delayed(AyyiIdent id, GError** e, gpointer _c)
		{
			dbg(0, "");

			reset_timeout(40000);

			C* c = _c;
			assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! b");
			module__add_n_audio_parts(test_on_parts_added, AYYI_BLOCK_SIZE, _c);
		}

		//assert((c->parts == &((AyyiSongService*)ayyi.service)->song->audio_regions), "c! a");
		am_wait_for(container_is_empty, on_delayed, _c);
	}

	module__delete_all_parts(test_on_parts_deleted, c);
}


/*
 * Check that the given track must be listed once and only once. (set to -1 to ignore this test.)
 */
static bool
tracks_check (AyyiIdx idx)
{
	int found = 0;

	GHashTable* hash = g_hash_table_new_full(g_int_hash, g_int_equal, NULL, g_free);

	AMTrackNum t; for(t=0;t<AM_MAX_TRK;t++){
		AMTrack* tr = song->tracks->track[t];
		if(!tr || !AM_TRK_IS_AUDIO(tr)) continue;
		if(idx > -1 && tr->ident.idx == idx) found ++;
		int* n = g_hash_table_lookup(hash, &tr->ident.idx);
		if(!n) n = g_new0(int, 1);
		*n = *n + 1;
		g_hash_table_insert(hash, &tr->ident.idx, n);
	}

	// check the contents of the hash table
	void print_icon_list(gpointer key, gpointer val, gpointer error)
	{
		int count = *(int*)val;
		dbg (2, "key=%i val=%i", *(int*)key, *(int*)val);
		if(count != 1){
			perr("track count != 1");
		}
	}

	int error = false;
	g_hash_table_foreach(hash, print_icon_list, &error);
	g_hash_table_unref(hash);
	if(error) return false;

	dbg(2, "found=%i", found);
	return (idx > -1) ? (found == 1) : true;
}

