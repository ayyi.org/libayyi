/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __ayyi_private__
#include "config.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <inttypes.h>
#include <signal.h>
#include <glib.h>
#include "model/ayyi_model.h"
#include "model/test/runner.h"
#include "model/test/common.h"

#include "modules.c"

TestFn
	test_delete_audio_part,
	test_add_delete_stress_audio_part,
	test_split_audio_part;

gpointer   tests[] = {
	//test_delete_audio_part,
	//test_split_audio_part,
	test_add_delete_stress_audio_part,
};


void
setup (int argc, char *argv[])
{
	TEST.n_tests = G_N_ELEMENTS(tests);

	am_init(NULL);

	am_connect_all(app_on_all_shm, NULL);
}


void
teardown ()
{
#ifdef WITH_VALGRIND
	am_uninit();
#endif
}


void
test_delete_audio_part ()
{
	START_TEST;

	static int n_parts; n_parts = g_list_length(song->parts->list);
	AMPart* part = song->parts->list->data;

	void test_delete_audio_part_done (AyyiIdent id, GError** error, gpointer user_data)
	{
		dbg(0, "n_parts=%i-->%i", n_parts, g_list_length(song->parts->list));
		if (n_parts - 1 != g_list_length(song->parts->list)) FAIL_TEST("");
		FINISH_TEST;
	}

	am_song__remove_part(part, test_delete_audio_part_done, NULL);
}


void
test_add_delete_stress_audio_part ()
{
	// adds and deletes parts randomly.
	// TODO only 1 action is done simultaneously - should try more.

	START_TEST;
	static int n_ops = 10;
	static int n_added = 0;
	static int n_deleted = 0;

	typedef struct
	{
		void (*on_add)();
		void (*on_delete)();
	} C;

	void delete_done (AyyiIdent obj, gpointer _c)
	{
		PF;
		C* c = _c;
		g_assert(c);
		n_deleted++;

		if (++step >= n_ops) {
			printf("n_added=%i n_deleted=%i\n", n_added, n_deleted);

			// wait for cleanup
			gboolean f(gpointer data)
			{
				FINISH_TEST_TIMER_STOP;
				return G_SOURCE_REMOVE;
			}
			g_timeout_add(10100, f, NULL);
		}

		if (get_random_boolean() && g_list_length(song->parts->list)) {
			AMPart* part = g_list_nth_data(song->parts->list, get_random_int(g_list_length(song->parts->list)));
			module__delete_audio_part(c->on_delete, part, c);
		} else {
			module__add_audio_part(c->on_add, c);
		}
	}

	void add_done (AyyiIdent obj, gpointer _c)
	{
		dbg(0, "%sstep=%i%s", yellow, step, white);
		n_added++;
		C* data = _c;
		if (++step >= n_ops) {
			printf("n_added=%i n_deleted=%i\n", n_added, n_deleted);
			FINISH_TEST;
		}

		if (get_random_boolean() && g_list_length(song->parts->list)) {
			int n = get_random_int(g_list_length(song->parts->list));
			AMPart* part = g_list_nth_data(song->parts->list, n);
			module__delete_audio_part(data->on_delete, part, data);
		} else {
			module__add_audio_part(data->on_add, data);
		}
	}

	module__add_audio_part(add_done, AYYI_NEW(C,
		.on_add = add_done,
		.on_delete = delete_done
	));

	return;
}


void
test_split_audio_part()
{
	START_TEST;

#ifdef DEBUG
	static int n_parts; n_parts = g_list_length(song->parts->list);
#endif

	void test_split_audio_part_done(AyyiIdent id, GError** error, gpointer user_data)
	{
		//TODO wait until the two new parts have been created.
		dbg(0, "n_parts=%i-->%i", n_parts, g_list_length(song->parts->list));
		//if(n_parts - 1 != g_list_length(song->parts->list)) FAIL_TEST;
		FINISH_TEST;
	}

	AMPart* part = song->parts->list->data;
	AyyiSongPos pos = part->length;
	ayyi_pos_divide(&pos, 2);
	ayyi_pos_add(&pos, &part->start);

	am_part_split(part, &pos, test_split_audio_part_done, NULL);

	return;
}


void
test_never()
{
	test_delete_audio_part();
	module__add_midi_track(0, 0);
	module__add_midi_part(0, 0);
	module__delete_parts_by_audio_file(0, NULL, 0);
	module__delete_audio_part(0, 0, 0);
	module__load_song_with_delay(0, 0, 0);
	module__delete_all_audio_files(0, 0);
}


