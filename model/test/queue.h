/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

typedef struct {
	GList*    tasks;
	int       i;
	gpointer  context;
	void      (*finished)();
	AyyiIdent id;
	GError**  error;
} AMQueue;

typedef void (*AMTask) (AMQueue*);


AMQueue* am_queue_new            (gpointer);
void     am_queue_add            (AMQueue*, AMTask*);
void     am_queue_next           (AMQueue*);
void     am_queue_finish         (AMQueue*);
void     am_queue_execute_action (AMQueue*, AyyiAction*);

