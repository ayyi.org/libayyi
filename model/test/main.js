/*
 *  Demonstration of connecting to the server and adding a new Track and Part
 */
imports.searchPath.push("../../gir")
imports.searchPath.push(".")

const GLib = imports.gi.GLib
const Waveform = imports.gi.Waveform
const Ayyi = imports.gi.Ayyi
const AM = imports.support.AM

const loop = new GLib.MainLoop(null, null)

AM.connect_all(async () => {

	// Add a new track
	const track_id = await AM.Song.add_track({
		type: Ayyi.MediaType.AUDIO,
		name: 'New Track',
		channels: Waveform.ChannelCount.MONO
	})

	log(`New Track ${track_id.get_idx()} added`)

	// Add a new Part
	const part_id = await AM.Song.add_part({
		name: 'New part',
		pool_item: AM.pool__get_item_from_idx(0),
		track: track_id.get_idx(),
		start: new Ayyi.SongPos({beat: 4}),
		length: 10000
	})

	log(`New part "${part_id.get_idx()}" added`)

	loop.quit()
}, null)

loop.run()
