/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __am_common_c__

#include "config.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <glib.h>
#include "model/ayyi_model.h"
#include "model/ayyi_model.h"
#include "model/test/runner.h"
#include "model/test/common.h"

#pragma GCC diagnostic ignored "-Wint-in-bool-context"


void
app_on_all_shm (const GError* e, gpointer user_data)
{
	if(!((AyyiSongService*)ayyi.service)->song){
		exit(EXIT_FAILURE);
	}

	if(e){
		printf("failed to connect\n");
#if 0
		if(main_loop){
			g_main_loop_unref(main_loop);
			main_loop = NULL;
		}
#endif
		exit(EXIT_FAILURE);
	}

	if (((AyyiSongService*)ayyi.service)->song->version != AYYI_SHM_VERSION) {
		perr ("wrong shm version: %i - should be %i", ((AyyiSongService*)ayyi.service)->song->version, AYYI_SHM_VERSION);
		return;
	}

	ayyi_log_print(LOG_OK, "connected to server");
}


bool
get_random_boolean ()
{
	int r = rand();
	int s = RAND_MAX / 2;
	int t = r / s;
	return t;
}


int
get_random_int (int max)
{
	int r = rand();
	int s = RAND_MAX / max;
	int t = r / s;
	return t;
}


void
errprintf4 (char* format, ...)
{
	char str[256];

	va_list argp;
	va_start(argp, format);
	vsprintf(str, format, argp);
	va_end(argp);

	printf("%s%s%s\n", ayyi_red, str, white);
}


char*
find_wav (const char* wav)
{
	if (wav[0] == '/') {
		return g_strdup(wav);
	}

	if (g_file_test(wav, G_FILE_TEST_EXISTS)) {
		return g_strdup(wav);
	}

	char* filename = g_build_filename(g_get_current_dir(), wav, NULL);
	if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
		return filename;
	}
	g_free(filename);

	filename = g_build_filename(g_get_current_dir(), "../../lib/waveform/test/data/", wav, NULL);
	if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
		return filename;
	}
	g_free(filename);

	filename = g_build_filename(g_get_current_dir(), "../../../../lib/waveform/test/data/", wav, NULL);
	if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
		return filename;
	}
	g_free(filename);

	return NULL;
}


/*
 *  Periodically call the test fn. wait is finished when test returns true.
 */
void
am_wait_for (gboolean (*test)(gpointer), AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(callback);
	g_return_if_fail(test);

	typedef struct
	{
		AyyiHandler callback;
		gpointer    user_data;
		gboolean    (*test)(gpointer);
		int         wait_count;
	} C;

	bool finish (C* c)
	{
		g_free(c);
		return G_SOURCE_REMOVE;
	}

	gboolean am_wait_for_iter (gpointer _c)
	{
		C* c = _c;
		g_return_val_if_fail(c, finish(c));

		if (!c->test(c->user_data)) {
			dbg(((c->wait_count > 0) && (c->wait_count % 4)) ? 2 : 3, "not yet ready (%i)", c->wait_count);
			c->wait_count = c->wait_count + 1;
			return c->wait_count < 100 ? G_SOURCE_CONTINUE : finish(c);
		}
		dbg(2, "wait done.");
		c->callback(AYYI_NULL_IDENT, NULL, c->user_data);
		return finish(c);
	}

	g_timeout_add(1000, am_wait_for_iter, AYYI_NEW(C,
		.callback = callback,
		.user_data = user_data,
		.test = test
	));
}
