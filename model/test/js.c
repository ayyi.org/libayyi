/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*/

#include "stdio.h"
#include <gjs/gjs.h>

int _debug_ = 0;


int
main (int argc, char* argv[])
{
	int exit_status;
	GError* error = NULL;

	GjsContext* gjs = gjs_context_new ();

	if (!gjs_context_eval_file (gjs, "main.js", &exit_status, &error)) {
		g_print ("couldnt evaluate JavaScript: %i %s\n", error->code, error->message);
		g_clear_error (&error);

		printf("check GI_TYPELIB_PATH\n");
	}

	g_object_unref (gjs);
}
