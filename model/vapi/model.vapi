
// This is manually created - do not delete!
[CCode(
	cheader_filename = "stdint.h",
	cheader_filename = "dbus/dbus.h",
	cheader_filename = "dbus/dbus/dbus-glib-bindings.h",
	cheader_filename = "ayyi/ayyi.h",
	cheader_filename = "model/ayyi_model.h",
	lower_case_cprefix = "", cprefix = "")]

namespace Ayyi {

	public void        am_song__load                        ();
	public AM.Part*    am_song__get_part_by_region_index    (uint32 region_idx, MediaType mediatype);

	namespace AM
	{
		public enum QType
		{
			Q_16,
			Q_8,
			Q_BEAT,
			Q_BAR,
			Q_EXPONENTIAL,
			Q_MAX
		}

		public enum SnapType
		{
			SNAP_OFF = 0,
			SNAP_BAR,
			SNAP_BEAT,
			SNAP_16,
			SNAP_Q,
			MAX_SNAP,
		}

		[CCode (cname = "am_init")]
		AyyiClient*        init                              (Service** service);
		[CCode (cname = "am_connect_all")]
		public bool        connect_all                       (AyyiHandler2a? callback);

		[CCode(cname = "am__connect")]
		public bool        connect                           (Service* service, GLib.Error** error);

		[CCode (has_target = false, cname = "am__get_shm")]
		public bool        get_shm                           (Service* service, owned AyyiCallback on_shm);
		[CCode (cname = "am__get_shm_with_target")]
		public bool        get_shm_with_target               (Service* service, owned AyyiCallback on_shm);

		[CCode(cname = "AM_MAX_TRK")]
		public const int MAX_TRK;

		[CCode (cname = "pos2mu")]
		uint64           pos2mu                              (GPos* pos);
		[CCode (cname = "pos2samples")]
		uint32           pos2samples                         (GPos* pos);
		[CCode (cname = "pos2bbst")]
		public void        pos2bbst                          (GPos* p, char* str);
		[CCode (cname = "pos_is_before")]
		public bool        pos_is_before                     (GPos* pos1, GPos* pos2);
		[CCode (cname = "pos_is_after")]
		public bool        pos_is_after                      (GPos* pos1, GPos* pos2);
		[CCode (cname = "songpos_gui2core")]
		public void        songpos_gui2core                  (SongPos* pos1, GPos* pos2);
		public void        pos_add                           (GPos* p1, GPos* p2);

		namespace Song
		{
			[CCode (cname = "am_song__save")]
			public bool     save                              (AyyiHandler3a? callback);

			[CCode (cname = "am_song__add_file")]
			public void     add_file                          (char* filename, AyyiHandler3a? callback);

			[CCode (cname = "am_song_add_track")]
			public void     add_track                         (TrackType type, char* name, int n_channels, AyyiHandler3a? callback);

			[CCode (cname = "am_song__add_part")]
			public void     add_part                          (MediaType t, Ayyi.Idx region_idx, PoolItem* p, Ayyi.Idx track, SongPos* pos, uint64 len, uint colour, char* name, uint inset, AyyiHandler3a? callback);

			[CCode (cname = "am_song__remove_part")]
			public void     remove_part                       (AM.Part* part, AyyiHandler3a handler);

			[CCode (cname = "am_song__midi_note_add")]
			public void     midi_note_add                     (AM.Part* part, MidiNote* note, AyyiHandler3a? callback);

			// part manager

			[CCode (cname = "am_partmanager__get_by_id")]
			public AM.Part* get_part_by_id                    (uint64 id);

			[CCode (cname = "am_partlist_quantize")]
			public void     partlist_quantize                 (GLib.List<AM.Part*> parts, AyyiHandler3a? callback);
		}

		[CCode(cname = "struct _song_pool")]
		public struct SongPool
		{
			public GLib.List<AM.PoolItem*>* pool;
			public void*                    gui_data;
		}

		[CCode (cname = "am_track__is_master")]
		public bool     track__is_master                     (Track* track);

		[CCode (cname = "am_track__set_output")]
		public void     track__set_output                    (Track* track, AyyiConnection* connection, AyyiHandler3a? callback);

		[CCode (cname = "am_part__move")]
		public void     move_part                            (AM.Part* part, SongPos* p, Track* new_track, AyyiHandler3a? callback);

		[CCode (cname = "am_part__has_pool_item")]
		public bool     part__has_pool_item                  (AM.Part* part, void* pool_item);

		[CCode (cname = "am_midi_part__new")]
		public MidiPart* part_midi__new                      ();

		[CCode (cname = "am_midi_part__print_events")]
		public void     midi_part__print_events              (AM.Part* part);

		[CCode(cprefix = "PART_")]
		public enum PartStatus
		{
			OK = 0,
			LOCAL,
			PENDING,
			BAD,
		}

		[CCode(cname = "struct _AMPart")]
		public struct Part
		{
			public Ayyi.Ident         ident;
			public BaseRegion*        ayyi;
			public char*              name;
			public MediaType          type;
			public Track*             track;
			public PartStatus         status;
			public SongPos            start;
			public uint32             region_start;
			public SongPos            length;

			public PoolItem*          pool_item;

			public int                bg_colour;
			public uint32             fg_colour;
			public bool               fg_is_set;

			public uint64             peak_handler;
		}

		[CCode(cname = "struct _AMPoolItem")]
		public struct PoolItem
		{
			public char*                filename;
			public char*                dirname;
			public char*                leafname;
			public int                  state;
			public bool                 valid;

			public int                  channels;

			public short**              peak_buf;
			public int                  peak_buflen;
			public GLib.PtrArray*       hires_peaks;
			public int                  num_peaks;
			public int                  peak_byte_depth;
			public bool                 peak_float;
			public bool                 peakfile_valid;

			public void*                gui_ref;
			public uint64*              source_id;
			public int*                 pod_index;
		}

		[CCode(cname = "struct _plug")]
		public struct Plugin
		{
			public char* name;
			public int   type;
			public int   shm_num;
		}

		//------------------------------------------

		[CCode(cname = "struct _curve")]
		public struct Curve
		{
			//BPath*          path;
			public size_t          size;
			public size_t          len;
			public size_t          item_size;
			public size_t          grow_size;

			public char*           name;
			public Track*          track;
			public AM.Plugin*      plugin;
			public int             auto_type;
			public void*           g_path;

			//public pthread_mutex_t	mutex;
		}

		[CCode (cname = "curve_get_length")]
		public size_t  curve_get_length(Curve* curve);

		[CCode (cname = "am_pool__get_item_from_idx")]
		public PoolItem*   pool__get_item_from_idx      (Ayyi.Idx idx);

		[CCode (cname = "Palette",
			cheader_filename = "model/am_palette.h"
		)]
		public struct Palette
		{
			uint red[64]; //TODO MAX_COLOUR
			uint blu[64];
			uint grn[64];
		}

		/*
		public union
		{
			int    i;
			float  f;
			char*  c;
			GPos   pos;

		} AMVal;
		*/

		[CCode(cname = "AMObject")]
		public struct Object
		{
			/*
			struct _am_val {
				AMVal    val;
				int      type;
			}**          vals;
			void         (*set)              (AMObject*, int property, AMVal value, AyyiHandler2, gpointer);
			*/
			public void* user_data;
		}
	}

	[CCode (cname = "AMTrackType", cprefix = "TRK_TYPE_", has_type_id = false)]
	public enum TrackType {
		NULL,
		AUDIO,
		MIDI,
		BUS
	}

	public struct Curves
	{
		AM.Curve*           vol;
	}
	[CCode(cname = "struct _AMTrack")]
	public struct Track {
		Ident               ident;
		TrackType           type;
		char*               name;
		bool                visible;
		int                 colour;
		string              user_notes;
		//GtkObject*          meteradj;

		//struct _track_curves {
		//	Curve*              vol;
		//	Curve*              pan;
		//	int                 size : 2;
		//} bezier;
		Curves              bezier;

		//GList*              controls;
	}


	[CCode (cname = "AMSong")]
	public struct Song {
		bool                             loaded;
		public AM.Object                 loc[20];            //locator positions. 1=LEFT, 2=RIGHT
		public int                       start;              //beats. Affects playback and arrange canvas display.
		public uint                      end;                //end of the song measured in beats. Used to size the canvas.
																  //-also playback will stop at this point.
		public bool                      end_auto;           //if true, song->end will follow the song contents.
		public int                       sample_rate;
		public float                     beats2bar;          //float??? useful for canvas ops. useful for strange time sigs?

		public char*                     audiodir;           //audio files are found relative to this directory.
																  //It would normally be relative to the project dir, but also can be absolute.
		public char*                     peaksdir;           //full path to the directory containing audio peak files.

		public QMap                      q_settings[5];
		public AM.SnapType               snap_mode;          //arguably should be in Arrange.

		public AM.TrackList*             tracks;
		public AM.List<AM.PoolItem*>*    pool;
		public AM.Array<Channel*>*       channels;
		public AM.List<AM.Part*>* parts;
		public AM.List*                  map_parts;          //songmap parts that show above the rulerbar.

		public GLib.List<void*>          input_list;         //list of available input channels (currently only used for mixer strip input selection)
		public char*                     input_str;          //one big buffer for all the input channel labels.
		public GLib.List<void*>          bus_list;
		public char*                     bus_strs;

		public GLib.List<void*>          plugins;

		public AM.Palette                palette;
	}

	public struct QMap
	{
		char      name[64];
		int       size;                 //the index of the last valid entry in the pos[] array.
		GPos      pos[33];              //the last entry in pos (pos[size]) should be equal to length.
	}

	public Ayyi.Song*       song;

	namespace Model
	{
		[CCode(cname = "struct _AMIter")]
		public struct Iter {
			public int                   i;
			public weak void*            ptr;
			public weak GLib.List<void*> list;
			public weak GLib.PtrArray    array;
			//public delegate void* next (Iter* iter);
			//public void*          next;                //cannot declare delegate in struct?
		}

	}
}
