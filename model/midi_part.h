/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_midi_part_h__
#define __am_midi_part_h__

MidiPart*     am_midi_part__new                 ();
AyyiMidiNote* am_midi_part__get_note_by_idx     (AMPart*, AyyiIdx);
AyyiMidiNote* am_midi_part__get_note_by_order   (AMPart*, int);
AyyiMidiNote* am_midi_part__get_next_event      (AMPart*, AyyiMidiNote*);
AyyiMidiNote* am_midi_part__get_prev_event      (AMPart*, AyyiMidiNote*);
AyyiMidiNote* am_midi_part__get_next_visible    (AMPart*, AyyiMidiNote*);
int           am_midi_part__find_note           (AyyiMidiRegion*, AyyiMidiNote*);
gboolean      am_midi_part__sync                (MidiPart*);
MidiPart*     am_midi_part__init_from_index     (MidiPart*, AyyiIdx);
void          am_midi_part__set_notes           (AMPart*, const GList* notes, AyyiHandler, gpointer);
void          am_midi_part__set_note_selection  (MidiPart*, GList*, gpointer);
void          am_midi_part__remove_notes        (MidiPart*, GList*, AyyiHandler, gpointer);

void          am_midi_part__print_events        (AMPart*);

#endif
