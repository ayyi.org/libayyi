/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <model/am_part.h>
#include <model/midi_part.h>
#include <model/am_collection.h>

#define am_parts ((AMCollection*)song->parts)
#define am_parts_selection (((AMCollection*)song->parts)->selection)
#define am_parts__emit(...) ({ if(song->loaded) g_signal_emit_by_name (song->parts, __VA_ARGS__); })

#define am_part_iterator_new(F, DATA) filter_iterator_new (0, NULL, NULL, am_parts, F, DATA)

#define part_foreach GList* parts = song->parts->list; if(parts){ for(;parts;parts=parts->next){ AMPart* gpart = (AMPart*)parts->data;
#define end_part_foreach } }

AMPart*        am_partmanager__get_by_id          (uint64_t id);
AMPart*        am_partmanager__get_first          (AMTrack*);
const AMPart*  am_partmanager__get_previous       (AMPart*, AMTrack*);
const AMPart*  am_partmanager__get_next           (AMPart*, AMTrack*);
const AMPart*  am_partmanager__find_by_position   (AMTrack*, AyyiSongPos*);
GList*         am_partmanager__get_by_track       (AMTrack*);
GList*         am_partmanager__get_regions_by_pool_item
                                                  (AMPoolItem*);
bool           am_partmanager__is_selected        (AMPart*);

void           am_partmanager__make_name_unique   (char* name_unique, const char*);

void           am_partmanager__get_pos            (GList*, AyyiSongPos* start, GPos* end);
void           am_partmanager__find_boundary      (GList*, AyyiSongPos* start, AyyiSongPos* end, int* track_top, int* track_bottom);

void           am_partlist_quantize               (GList*, AyyiHandler, gpointer);

void           am_partmanager__print_list         ();
bool           am_partmanager__verify_part        (AMPart*);

#ifdef __am_private__
AMPart*        am_partmanager__add_new_from_index (AyyiIdent);
AMPart*        am_partmanager__add_new_pending    (AyyiIdx, AyyiMediaType);
AMPart*        am_partmanager__add_new_local      (AyyiMediaType);
#endif
