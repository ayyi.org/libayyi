/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __observable_h__
#define __observable_h__

#include "model/utils.h"

typedef struct {
   AMVal  value;
   AMVal  min;
   AMVal  max;
   GList* subscriptions;
} Observable;

typedef enum {
   CHANGE_X = 1,
   CHANGE_Y = 2,
   CHANGE_BOTH = 3,
} PtChangeType;

typedef struct {
   Observable   observable;
   PtChangeType change;
} PtObservable;

typedef void (*ObservableFn) (Observable*, AMVal, gpointer);

Observable* observable_new         ();
void        observable_free        (Observable*);
void        observable_set         (Observable*, AMVal);
void        observable_subscribe   (Observable*, ObservableFn, gpointer);
void        observable_subscribe_with_state
                                   (Observable*, ObservableFn, gpointer);
void        observable_unsubscribe (Observable*, ObservableFn, gpointer);

Observable* observable_float_new   (float val, float min, float max);
void        observable_float_set   (Observable*, float);

Observable* observable_new_point   (float, float);
void        observable_point_set   (Observable*, float*, float*);

/*
 *  ArrayObservable is a GPtrArray that emits when items are added or removed.
 *  The value property contains the last item that was changed.
 */
typedef struct {
    Observable observable;
    GPtrArray* array;
    enum {
       OBSERVABLE_ADD,
       OBSERVABLE_REMOVE,
    }          change;
} ArrayObservable;

Observable* array_observable_new    ();
void        array_observable_add    (Observable*, gpointer);
void        array_observable_remove (Observable*, gpointer);

#define     observable_free0(A)    (observable_free(A), A = NULL)

#endif
