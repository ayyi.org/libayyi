/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __am_private__
#include <model/common.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include "model/song.h"
#include "model/transport.h"
#include "dbus.h"

extern DBusGProxy* proxy;

static void hello_signal_handler (DBusGProxy*, const char *hello_string, gpointer);


/*
 *  Tell the proxy which signals we are interested in.
 */
void
am_dbus__register_signals ()
{
	ayyi_client_set_signal_reg(TRANSPORT | LOCATORS | PROPERTY | PROGRESS);

	dbus_g_proxy_add_signal     (proxy, "HelloSignal", G_TYPE_STRING, G_TYPE_INVALID);
	dbus_g_proxy_connect_signal (proxy, "HelloSignal", G_CALLBACK (hello_signal_handler), NULL, NULL);

	dbus_g_proxy_call_no_reply  (proxy, "emitHelloSignal", G_TYPE_INVALID);

#ifdef USE_dbus_test_notify
	// test hash table (is working):
	dbus_g_proxy_begin_call(proxy, "getPropertiesTest", dbus_test_notify, NULL, NULL, G_TYPE_STRING, "dummy", G_TYPE_INVALID);
#endif
}


static void
hello_signal_handler (DBusGProxy *proxy, const char *hello_string, gpointer user_data)
{
	SIG_IN1;
	AYYI_DEBUG printf ("Core says: %s\n", hello_string);
}


#ifdef USE_dbus_test_notify
static void
dbus_test_notify (DBusGProxy* proxy, DBusGProxyCall* call, gpointer data)
{
	P_IN;
	PF;

	GError* error = NULL;
	GHashTable *hashtable;
	if(!dbus_g_proxy_end_call(proxy, call, &error, dbus_g_type_get_map("GHashTable", G_TYPE_STRING, G_TYPE_VALUE), &hashtable, G_TYPE_INVALID)){
		gerr ("%s", error->message);
		g_error_free(error);
		return;
	}

	GValue* value = NULL;
	if((value = g_hash_table_lookup(hashtable, "saffirea"))){
		const gchar *t = g_value_get_string(value);
		dbg(0, "hashtable lookup succeeded! val=%s", t);
	}
}
#endif


