/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_transport_h__
#define __am_transport_h__

#include "ayyi/ayyi_transport.h"

void           am_transport_sync              ();

void           am_transport_set_rec           (bool, AyyiHandler2, gpointer);

void           am_transport_jump              (AyyiSongPos*);
void           am_transport_get_pos           (GPos*);
AyyiSongPos*   am_transport_get_pos_          (AyyiSongPos*);

void           am_transport_set_locator_pos   (int loc_num, AyyiSongPos*);

void           am_transport_cycle_toggle      ();

const char*    am_locator_name                (int);

#define        am_locator_is_set(L) ((L)->vals[0].val.sp.beat >= 0)

#endif
