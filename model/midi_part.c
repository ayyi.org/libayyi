/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "model/midi_part.h"
#include "part_manager.h"

static void am_note_selection_free(GList**);


MidiPart*
am_midi_part__new ()
{
	return AYYI_NEW(MidiPart,
		.part = {
			.ident.type = AYYI_OBJECT_MIDI_PART,
			.length = (AyyiSongPos){8,},
			.ref_count = 1,
		}
	);
}


AyyiMidiNote*
am_midi_part__get_note_by_idx (AMPart* part, AyyiIdx shm_idx)
{
	AyyiMidiRegion* midi_region = ayyi_song__midi_region_at(part->ayyi->shm_idx);
	AyyiContainer* container = &midi_region->events;
	AyyiMidiNote* note = (AyyiMidiNote*)ayyi_song_container_get_item(container, shm_idx);

	return note;
}


AyyiMidiNote*
am_midi_part__get_note_by_order (AMPart* part, int idx)
{
	AyyiMidiNote* note = NULL;
	if (idx >= 0) {
		for (int i=0;i<idx;i++) {
			if (!(note = am_midi_part__get_next_event(part, note))) return NULL;
		}
	} else {
		// idx is negative - count from end
		for (int i=0;i>idx;i--) {
			if (!(note = am_midi_part__get_prev_event(part, note))) return NULL;
		}
	}

	return note;
}


AyyiMidiNote*
am_midi_part__get_next_event (AMPart* part, AyyiMidiNote* prev)
{
	g_return_val_if_fail(part, NULL);

	AyyiMidiRegion* region = ayyi_song__midi_region_at(part->ayyi->shm_idx);
	AyyiContainer* container = &region->events;
	if (!container || container->last < 0) return NULL;

	return (AyyiMidiNote*)ayyi_midi_note_next((AyyiItem*)prev);
}


AyyiMidiNote*
am_midi_part__get_prev_event (AMPart* part, AyyiMidiNote* prev)
{
	AyyiMidiRegion* region = ayyi_song__midi_region_at(part->ayyi->shm_idx);
	AyyiContainer* container = &region->events;
	return (AyyiMidiNote*)ayyi_midi_note_prev((AyyiItem*)prev);
}


AyyiMidiNote*
am_midi_part__get_next_visible (AMPart* part, AyyiMidiNote* prev)
{
	g_return_val_if_fail(part, NULL);

	AyyiMidiRegion* region = ayyi_song__midi_region_at(part->ayyi->shm_idx);
	AyyiContainer* container = &region->events;
	if (!container || container->last < 0) return NULL;

	uint32_t len = ayyi_pos2samples(&part->length);

	do {
		AyyiMidiNote* note = (AyyiMidiNote*)ayyi_midi_note_next((AyyiItem*)prev);
		if (!note || note->start < len)
			return note;
		prev = note;
	} while (true);

	return NULL;
}


int
am_midi_part__find_note (AyyiMidiRegion* region, AyyiMidiNote* note)
{
	//temp? probably should add shm_idx to the note struct like the others.
	g_return_val_if_fail(region, false);
	g_return_val_if_fail(note, false);

	if (!region->events.block[0]) { pwarn("region has no events"); return -1; }

	return ayyi_container_find(&region->events, note, ayyi_song__translate_address);
}


gboolean
am_midi_part__sync (MidiPart* part)
{
	g_return_val_if_fail(part, false);
	gboolean err = false;
	AMPart* gpart = (AMPart*)part;
	g_return_val_if_fail(gpart->ayyi, false);

	AyyiMidiRegion* shared = ayyi_song__midi_region_at(gpart->ayyi->shm_idx);
	if (!shared->id) { perr ("illegal id 0");/* err = true;*/ }

	_am_part_sync_name(gpart, (AyyiRegionBase*)shared);
	_am_part_sync_pos(gpart);

	am_part__sync_length(gpart);

	return !err;
}


/*
 *   Initialise data for a part which is new to the gui but already exists in core.
 */
MidiPart*
am_midi_part__init_from_index (MidiPart* mpart, AyyiIdx ayyi_index)
{
	AMPart* part = (AMPart*)mpart;

	part->ayyi       = (AyyiRegionBase*)ayyi_song_container_get_item(&((AyyiSongService*)ayyi.service)->song->midi_regions, ayyi_index);
	part->ident      = (AyyiIdent){AYYI_OBJECT_MIDI_PART, ayyi_index};
	part->track      = _am_part_get_ayyi_track(part);
	part->pool_item  = NULL;

	if(!part->track) return NULL;
	if(!am_midi_part__sync(mpart)) perr ("init failed - should abort...\n");

	if(part->length.beat > AM_MAX_PART_LENGTH_BEATS){
		pwarn ("part length too long (%i).\n", part->length.beat);
		part->length = (AyyiSongPos){100, 0, 0};
	}

	am_parts__emit("add", part, NULL);

	return mpart;
}


#define CHECK_NOTE(note) { \
		g_return_if_fail(note->note <= 128); \
		g_return_if_fail(note->length > 0); \
	}

/*
 *  @param notes, list of TransitionalNote*.
 */
void
am_midi_part__set_notes (AMPart* part, const GList* notes, AyyiHandler callback, gpointer user_data)
{
	typedef struct {
		AyyiHandler callback;
		gpointer    user_data;
	} C;

	void
	am_midi_set_notes_done(AyyiAction* action)
	{
		PF;
		C* c = action->app_data;
		call(c->callback, (AyyiIdent){action->obj.type, action->ret.idx}, &action->ret.error, c->user_data);
	}

	AyyiMidiRegion* region = (AyyiMidiRegion*)part->ayyi;

	AyyiAction* a = ayyi_action_new("set notes");
	a->callback = am_midi_set_notes_done;
	a->app_data = AYYI_NEW(C,
		.callback = callback,
		.user_data = user_data
	);

	GPtrArray* array = g_ptr_array_new();

	GList* l = (GList*)notes;
	int i = 0;
	for(;l;l=l->next){
		TransitionalNote* tn = l->data;
		AyyiMidiNote* note = &tn->transient;
		dbg(1, "idx=%i note=%i start=%u v=%i", note->idx, note->note, note->start, note->velocity);
		CHECK_NOTE(note);
		g_return_if_fail(ayyi_container_index_ok(&region->events, note->idx));

		GValue value = {0,};
		g_value_init (&value, DBUS_STRUCT_UINT_BYTE_BYTE_UINT_UINT);
		g_value_take_boxed (&value, dbus_g_type_specialized_construct (DBUS_STRUCT_UINT_BYTE_BYTE_UINT_UINT));

		dbus_g_type_struct_set (&value, 0, note->idx, 1, note->note, 2, note->velocity, 3, note->start, 4, note->length, G_MAXUINT);
		g_ptr_array_add (array, g_value_get_boxed(&value));
		dbg(2, "array_len=%i key=%i", array->len, note->note);

		i++;
	}
	if(array->len) dbus_set_notelist(a, part->ayyi->shm_idx, array);

	g_ptr_array_free(array, TRUE);
}


/*
 *   Ownership of the note list is taken and will be free'd.
 *   The notes in the list are assumed to be references to the model and are not free'd.
 */
void
am_midi_part__remove_notes (MidiPart* part, GList* notes, AyyiHandler callback, gpointer user_data)
{
	AyyiAction* a = ayyi_action_new("delete midi notes");
	a->callback   = ayyi_handler_simple;
	a->app_data   = ayyi_handler_data_new(callback, user_data);
	a->obj.type   = AYYI_OBJECT_MIDI_PART;
	a->ret.idx    = part->part.ident.idx;

	int event_type = 0;
	int note_idx = 0;
	GArray* array = ayyi_index_array_new(part->part.ident.idx, event_type, note_idx);
	dbus_set_prop_f_pair(a, AYYI_OBJECT_MIDI_NOTE, AYYI_DEL_POINT, array, 0.0, 0.0);

	g_array_unref (array);
	g_list_free (notes);
}


/*
 *   Ownership of the selection is taken.
 */
void
am_midi_part__set_note_selection (MidiPart* part, GList* selection, gpointer user_data)
{
	am_note_selection_free(&part->note_selection);
	part->note_selection = selection;

	g_signal_emit_by_name (am_parts, "item-changed", part, AM_CHANGE_SELECTION, user_data);
}


static void
am_note_selection_free (GList** selection)
{
	PF;
	if(*selection){
		g_list_free0(*selection);
	}
}


void
am_midi_part__print_events (AMPart* part)
{
	AyyiMidiRegion* region = (AyyiMidiRegion*)part->ayyi;

	int n_events = ayyi_song_container_count_items(&region->events);
	if(n_events){
		printf("      events (%i):\n", n_events);
		int n = 0;
		AyyiMidiNote* note = NULL;
		while((note = (AyyiMidiNote*)ayyi_song_container_next_item(&region->events, note))){
			printf("        %i: %i %i %i %i\n", n, note->note, note->velocity, note->start, note->length);
			n++;
		}
	}
	else printf("      no events\n");
}
