/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2009-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <model/common.h>
#include <model/am_part.h>
#include <model/note_selection.h>


/*
 *  Copy the selected notes in the current Part to a new list of NoteSelectionListItem*.
 *
 *  Currently the list only contains an entry for *1* Part.
 *
 *  Lifetime: created lazily in _motion or _finish callbacks.
 *            should not be cleared at the end of each op so that in a subsequent
 *            op, the observer knows to clear the previous selection.
 */
GList*
note_selection_list__new (GList** selection, AMPart* part)
{
	if(*selection) pwarn("note_selection not previously reset.");

	GList* copy = NULL;
	GList* l = ((MidiPart*)part)->note_selection;
	for (;l;l=l->next) {
		AyyiMidiNote* src = l->data;
		TransitionalNote* t = transitional_note__new(src);
		copy = g_list_append(copy, t);
	}
	dbg(2, "len=%i", g_list_length(copy));

	GList* list = g_list_append(NULL, AYYI_NEW(NoteSelectionListItem,
		.part = (MidiPart*)part,
		.notes = copy,
	));

	return *selection = list;
}


void
note_selection_list__free (GList** selection)
{
	GList* l = *selection;
	for(;l;l=l->next){
		NoteSelectionListItem* item = l->data;
		GList* k = item->notes;
		for(;k;k=k->next){
			g_free((TransitionalNote*)k->data);
		}
		g_list_free(l->data);
	}
	g_list_clear(*selection);
}


/*
 *  Removes the note list for each part but leaves the part item so the observer know which Parts to update.
 */
void
note_selection_list__reset(GList* selection, AMPart* part)
{
	am_part__set_note_selection(part, NULL);
	GList* l = selection;
	for(;l;l=l->next){
		NoteSelectionListItem* item = l->data;
		GList* k = item->notes;
		for(;k;k=k->next){
			g_free((TransitionalNote*)k->data);
		}
		g_list_clear(item->notes);
	}
	dbg(1, "n_parts=%i", g_list_length(selection));
}


void
note_selection_list__print(GList** selection)
{
	UNDERLINE;
	if(!*selection) dbg(0, "empty.");

	GList* l = *selection;
	for(;l;l=l->next){
		NoteSelectionListItem* item = l->data;
		dbg(0, "part '%s':", ((AMPart*)item->part)->name);
		GList* k = item->notes;
		if(!k) dbg(0, "  no notes selected.");
		for(;k;k=k->next){
			TransitionalNote* n = k->data;
			printf("    note=%i vel=%i\n", n->orig->note, n->orig->velocity);
		}
	}
	UNDERLINE;
}


TransitionalNote*
transitional_note__new (AyyiMidiNote* note)
{
	TransitionalNote* t = AYYI_NEW(TransitionalNote,
		.orig = note,
		.transient = *note
	);
	return t;
}


