/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __am_collection_h__
#define __am_collection_h__

#include <glib.h>
#include <glib-object.h>
#include "model/observable.h"

G_BEGIN_DECLS

#define AM_TYPE_COLLECTION (am_collection_get_type ())
#define AM_COLLECTION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), AM_TYPE_COLLECTION, AMCollection))
#define AM_COLLECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), AM_TYPE_COLLECTION, AMCollectionClass))
#define AM_IS_COLLECTION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AM_TYPE_COLLECTION))
#define AM_IS_COLLECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AM_TYPE_COLLECTION))
#define AM_COLLECTION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), AM_TYPE_COLLECTION, AMCollectionClass))

typedef struct _AMCollection AMCollection;
typedef struct _AMCollectionClass AMCollectionClass;
typedef struct _AMCollectionPrivate AMCollectionPrivate;

#define AM_TYPE_ARRAY (am_array_get_type ())
#define AM_ARRAY(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), AM_TYPE_ARRAY, AMArray))
#define AM_ARRAY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), AM_TYPE_ARRAY, AMArrayClass))
#define AM_IS_ARRAY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AM_TYPE_ARRAY))
#define AM_IS_ARRAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AM_TYPE_ARRAY))
#define AM_ARRAY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), AM_TYPE_ARRAY, AMArrayClass))

typedef struct _AMArrayClass AMArrayClass;
typedef struct _AMArrayPrivate AMArrayPrivate;

#define AM_TYPE_LIST (am_list_get_type ())
#define AM_LIST(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), AM_TYPE_LIST, AMList))
#define AM_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), AM_TYPE_LIST, AMListClass))
#define AM_IS_LIST(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AM_TYPE_LIST))
#define AM_IS_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AM_TYPE_LIST))
#define AM_LIST_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), AM_TYPE_LIST, AMListClass))

typedef struct _AMListClass AMListClass;
typedef struct _AMListPrivate AMListPrivate;

#define TYPE_FILTER_ITERATOR (filter_iterator_get_type ())
#define FILTER_ITERATOR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FILTER_ITERATOR, FilterIterator))
#define FILTER_ITERATOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_FILTER_ITERATOR, FilterIteratorClass))
#define IS_FILTER_ITERATOR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FILTER_ITERATOR))
#define IS_FILTER_ITERATOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_FILTER_ITERATOR))
#define FILTER_ITERATOR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_FILTER_ITERATOR, FilterIteratorClass))

typedef struct _FilterIteratorClass FilterIteratorClass;
typedef struct _FilterIteratorPrivate FilterIteratorPrivate;

struct _AMCollection {
	GObject              parent_instance;
	AMCollectionPrivate* priv;
	GList*               selection;
	Observable*          selection2;
	GList*               pending;
	void*                user_data;
};

typedef struct {
	AyyiIdent       ident;
} AMItem;

typedef void* (*AMCollectionNextFn) (AMCollection*, AMIter*);

struct _AMCollectionClass {
	GObjectClass parent_class;
	guint        (*length)     (AMCollection*);
	bool         (*contains)   (AMCollection*, gconstpointer item);
	const AMItem*(*find_by_id) (AMCollection*, guint64 id);
	gpointer     (*find_by_idx)(AMCollection*, AyyiIdx);
	gpointer     (*append)     (AMCollection*, gconstpointer item);
	void         (*remove)     (AMCollection*, gconstpointer item);
	void         (*iter_init)  (AMCollection*, AMIter*);
	AMCollectionNextFn iter_next;
};

struct _AMArray {
	AMCollection    parent_instance;
	AMArrayPrivate* priv;
	GPtrArray*      array;
};

struct _AMArrayClass {
	AMCollectionClass parent_class;
};

struct _AMList {
	AMCollection    parent_instance;
	AMListPrivate*  priv;
	GList*          list;
};

struct _AMListClass {
	AMCollectionClass parent_class;
};

struct _FilterIterator {
    GTypeInstance          parent_instance;
    volatile int           ref_count;
    FilterIteratorPrivate* priv;
    AMCollection*          list;
    AMIter                 iter;
    Filter                 accept;
    gpointer               accept_target;
    GDestroyNotify         accept_target_destroy_notify;
};

struct _FilterIteratorClass {
	GTypeClass parent_class;
	void (*finalize) (FilterIterator*);
};


GType           am_collection_get_type          (void) G_GNUC_CONST;
guint           am_collection_length            (AMCollection*);
bool            am_collection_contains          (AMCollection*, gconstpointer item);
const AMItem*   am_collection_find_by_id        (AMCollection*, guint64 id);
gpointer        am_collection_find_by_idx       (AMCollection*, AyyiIdx);
gpointer        am_collection_append            (AMCollection*, gconstpointer item);
void            am_collection_remove            (AMCollection*, gconstpointer item);
void            am_collection_iter_init         (AMCollection*, AMIter*);
void*           am_collection_iter_next         (AMCollection*, AMIter*);
void            am_collection_selection_reset   (AMCollection*);
void            am_collection_selection_replace (AMCollection*, GList*, void* sender);
void            am_collection_selection_add     (AMCollection*, GList*, void* sender);
void            am_collection_selection_remove  (AMCollection*, gconstpointer item, void* sender);
gpointer        am_collection_selection_first   (AMCollection*);
AMCollection*   am_collection_construct         (GType object_type, GType t_type);

GType           am_array_get_type               (void) G_GNUC_CONST;
AMArray*        am_array_new                    (GType t_type, GBoxedCopyFunc, GDestroyNotify, gint n);
AMArray*        am_array_construct              (GType object_type, GType t_type, GBoxedCopyFunc, GDestroyNotify, gint n);
gconstpointer*  am_array_at                     (AMArray*, gint index);
gpointer        am_array_find_by_idx            (AMCollection*, AyyiIdx);

GType           am_list_get_type                (void) G_GNUC_CONST;
AMList*         am_list_new                     ();
gpointer        am_list_first                   (AMList*);
gpointer        am_list_find_by_ident           (AMList*, AyyiIdent);
void            am_list_unpend                  (AMList*, gconstpointer item);
bool            am_list_is_pending              (AMList*, AyyiIdx, AyyiMediaType);

FilterIterator* filter_iterator_new             (GType t_type, GBoxedCopyFunc, GDestroyNotify, AMCollection* _list, Filter, void*);
gpointer        filter_iterator_ref             (gpointer);
void            filter_iterator_unref           (gpointer);
GType           filter_iterator_get_type        (void) G_GNUC_CONST;
FilterIterator* filter_iterator_construct       (GType object_type, GType t_type, GBoxedCopyFunc, GDestroyNotify, AMCollection*, Filter, void*);
gconstpointer*  filter_iterator_next            (FilterIterator*);
GParamSpec*     param_spec_filter_iterator      (const gchar* name, const gchar* nick, const gchar* blurb, GType, GParamFlags);
void            value_set_filter_iterator       (GValue*, gpointer v_object);
void            value_take_filter_iterator      (GValue*, gpointer v_object);
gpointer        value_get_filter_iterator       (const GValue*);

G_END_DECLS

#endif
