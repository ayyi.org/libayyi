/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __am_private__
#include <model/common.h>
#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_utils.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <model/model_types.h>
#include <model/pool_item.h>
#include <model/song.h>
#include <model/midi_part.h>
#include <model/am_part.h>
#include <model/time.h>
#include <model/track_list.h>
#include "model/part_manager.h"


AMPart*
am_partmanager__add_new_from_index (AyyiIdent id)
{
	AMPart* part = NULL;
	switch (id.type) {
		case AYYI_OBJECT_AUDIO_PART:
			part = am_part_new();
			if (am_part_init_from_index(part, id.idx)) {
				am_collection_append(am_parts, part);
//				part->ref_count++;
				if (am_list_is_pending(song->parts, id.idx, AYYI_AUDIO)) perr("FIXME is PENDING");

				am_parts__emit("add", part, NULL);
			} else {
				am_part_unref(part);
				return part = NULL;
			}
			return part;
		case AYYI_OBJECT_MIDI_PART:
			part = (AMPart*)am_midi_part__new();
			am_collection_append(am_parts, part);
//			part->ref_count++;
			if (am_list_is_pending(song->parts, id.idx, AYYI_MIDI)) perr("FIXME is PENDING"); //TODO move after init
			AMPart* part_ = (AMPart*)am_midi_part__init_from_index((MidiPart*)part, id.idx);
			if (!part_) {
				song->parts->list = g_list_remove(song->parts->list, part);
				part->ref_count--;
			}
			return part_;
			break;
		default:
			perr("bad media type: %i", id.type);
			return NULL;
	}

	return NULL;
}


AMPart*
am_partmanager__add_new_pending (AyyiIdx ayyi_idx, AyyiMediaType type)
{
	//duplicate of above, but adds to the Pending list, and sets the PART_PENDING flag
	//-used for externally created parts that are not yet complete enough to be useable.

	gboolean pending_on_timeout (gpointer _part)
	{
		AMPart* part = (AMPart*)_part;
		if (am_collection_contains(am_parts, part)) {
			if (part->status == PART_PENDING) {
				am_collection_remove(am_parts, part);
				am_part_unref(part);
			}
		}

		return G_SOURCE_REMOVE;
	}

	AMPart* part = NULL;
	switch (type) {
		case AYYI_AUDIO:
			part = am_part_new();
			part->status = PART_PENDING;
			if (am_part_init_from_index(part, ayyi_idx)) {
				am_collection_append(am_parts, part);
				g_timeout_add(10000, pending_on_timeout, part);
			} else {
				am_part_unref(part);
				return part = NULL;
			}
			return part;
		case AYYI_MIDI:
			part = (AMPart*)am_midi_part__new();
			part->status = PART_PENDING;
			am_collection_append(am_parts, part);
			return (AMPart*)am_midi_part__init_from_index((MidiPart*)part, ayyi_idx);
			break;
		default:
			perr("bad media type: %i", type);
			return NULL;
	}
	return NULL;
}


/*
 *  Duplicate of above, but adds to the Pending list, and sets the PART_LOCAL flag
 *  - used for locally created parts that are not yet on the backend.
 */
AMPart*
am_partmanager__add_new_local (AyyiMediaType type)
{
#if 0
	gboolean pending_on_timeout(gpointer _part)
	{
		AMPart* part = (AMPart*)_part;
		if(am_collection_contains(am_parts, part)){
			if(part->status == PART_PENDING){
				am_collection_remove(am_parts, part);
				am_part__unref(part);
			}
		}
		//if(g_list_length(song->priv->parts_pending)) dbg(0, "n_pending=%i", g_list_length(song->priv->parts_pending));

		return TIMER_STOP;
	}
#endif

	AMPart* part = NULL;
	switch(type){
		case AYYI_AUDIO:
			part = am_part_new();
#if 0
			g_timeout_add(10000, pending_on_timeout, part);
#endif
			break;
		case AYYI_MIDI:
			part = (AMPart*)am_midi_part__new();
			break;
		default:
			perr("bad media type: %i", type);
			return NULL;
	}
	part->status = PART_LOCAL;
	am_collection_append(am_parts, part);
	return part;
}


/*
 *  Deprecated, use am_collection_find_by_id(song->parts, guint64 id) instead
 *
 *  Lookup failure is not considered to be an error
 */
AMPart*
am_partmanager__get_by_id (uint64_t id)
{
	GList* l = song->parts->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		AyyiRegionBase* region = am_part_get_shared(part);
		if(region && region->id == id) return part;
	}
	dbg (2, "part not found. idx=%Lu", id);

	return NULL;
}


bool
am_partmanager__is_selected (AMPart* part)
{
	g_return_val_if_fail(part, false);

	GList* found = g_list_find(am_parts_selection, part);

	return (boolp)found;
}


void
am_partlist_quantize (GList* parts, AyyiHandler callback, gpointer user_data)
{
	dbg (1, "selected parts: %i", g_list_length(parts));

	GList* p = parts;
	for(;p;p=p->next){
		AMPart* part = p->data;
		GPos new_pos; songpos_ayyi2gui(&new_pos, &part->start);
		if(q_to_nearest(&new_pos, &song->q_settings[Q_16])){
			AyyiSongPos p; songpos_gui2ayyi(&p, &new_pos);
			am_part_move(part, &p, part->track, NULL, NULL);
		}
	}
}


void
am_partmanager__print_list ()
{
	PF;
	UNDERLINE;

	ayyi_song__print_part_list();

	printf("am partlist:\n");
	GList* i = song->parts->list;
	if(i){
		printf("       %s %8s %14s %s %s %s\n", "part", "podidx", "id", "s", "t", "track");
		for(;i;i=i->next){
			AMPart* part = i->data;
			g_return_if_fail(part);
			char* status = part->status > PART_BAD
				? "!"
				: part->status == PART_LOCAL ? "L" : "-";
			char* media = PART_IS_AUDIO(part) ? "A" : "M";
			printf("  %p  %06i %14"PRIu64" %s %s %p %s\n", part, part->ayyi->shm_idx, part->ayyi->id, status, media, part->track, part->name);
		}
	} else printf("  partlist is empty.\n");

	UNDERLINE;
}


bool
am_partmanager__verify_part (AMPart* part)
{
	GList* l = song->parts->list;
	bool found = false;
	for(;l;l=l->next){
		if((AMPart*)l->data == part){ found = true; break; }
	}
	if(!found){ perr("part not in song. %p", part); return false; }

	g_return_val_if_fail(am_track_list_verify_track(song->tracks, part->track), false);

	return true;
}


const AMPart*
am_partmanager__get_previous (AMPart* orig, AMTrack* track)
{
	g_return_val_if_fail(orig, NULL);

	AMPart* closest = NULL;

	GList* l = song->parts->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		if(part->track != track) continue;

		if(!ayyi_pos_is_after(&orig->start, &part->start)) continue; // too late.
		if(!closest) closest = part;
		if(ayyi_pos_is_after(&closest->start, &part->start)) continue; // too early.
		closest = part;
	}
	return closest;
}


AMPart*
am_partmanager__get_first (AMTrack* track)
{
	//the parts are not ordered so we have to iterate over each part for the given track.

	g_return_val_if_fail(track, NULL);

	AMPart* first = NULL;
	FilterIterator* i = am_part_iterator_new ((Filter)am_part__has_track, track);
	AMPart* part;
	while((part = (AMPart*)filter_iterator_next(i))){
		if(!first || ayyi_pos_is_after(&first->start, &part->start)) first = part;
	}
	filter_iterator_unref(i);
	return first;
}


/*
 *  Return parts in start-time order
 */
const AMPart*
am_partmanager__get_next (AMPart* current, AMTrack* track)
{
	AMPart* closest = NULL;
	GList* l = song->parts->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		if(part->track != track) continue;
		closest = (AMPart*)am_part__is_between(part, current, closest);
	}
	return closest;
}


const AMPart*
am_partmanager__find_by_position (AMTrack* track, AyyiSongPos* pos)
{
	AMPart* part = NULL;

	int64_t p0 = ayyi_pos2mu(pos);

	FilterIterator* i = am_part_iterator_new ((Filter)am_part__has_track, track);
	AMPart* p;
	while((p = (AMPart*)filter_iterator_next(i))){

		int64_t p1 = ayyi_pos2mu(&p->start);
		int64_t p2 = p1 + ayyi_pos2mu(&p->length);

		if((p0 >= p1) && (p0 < p2)){
			dbg (2, "hit: %s", p->name);
			part = p;
			break;
		}
	}
	filter_iterator_unref(i);

	return part;
}


/*
 *  The list must be freed after use
 */
GList*
am_partmanager__get_by_track (AMTrack* track)
{
	GList* parts = NULL;

	GList* l = song->parts->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		if(part->track == track) parts = g_list_append(parts, part);
	}
	return parts;
}


/*
 *  Return newly allocated list of parts sorted in reverse track order.
 */
#if 0
GList*
am_partmanager__get_by_track_r()
{
	GList* l = song->parts->list;
	int nt = am_track_list_count(song->tracks);

	// create an array to hold the Parts for each track
	GList* tracks[nt]; int i; for(i=0;i<nt;i++) tracks[i] = NULL;

	for(;l;l=l->next){
		AMPart* part = l->data;
		AMTrackNum t = part->track->track_num;
		tracks[t] = g_list_append(tracks[t], part);
	}
	GList* parts = NULL;
	int t; for(t=0;t<nt;t++){
		parts = g_list_concat(tracks[t], parts);
	}
	return parts;
}
#endif


GList*
am_partmanager__get_regions_by_pool_item (AMPoolItem* pool_item)
{
	//do a lookup based on guid.

	GList* podlist = NULL;

	AyyiRegion* region = NULL;
	while((region = ayyi_song__audio_region_next(region))){
		if(region->flags & deleted){ dbg(2, "found deleted region. '%s' flags=%x", region->name, region->flags); continue; }

		if(region->source0 == pool_item->source_id[0]){
			dbg(3, "  found: %i: %s", region->shm_idx, region->name);
			podlist = g_list_append(podlist, region);
		}
	}

	if(!podlist && _debug_) dbg(2, "no shared regions found for pool_item '%s'.", pool_item->leafname); //not all ardour sources have regions. This is not neccesarily an error.

	return podlist;
}


void
am_partmanager__make_name_unique (char* source_name_unique, const char* source_name)
{
	//note: parts shouldnt be named the same as an existing region. you may want to use ayyi_region_name_make_unique() instead.
	//      -in fact why not always use ayyi_region_make_name_unique instead??

	AMPart* part = am_song__part_lookup_by_name(source_name);
	if(!part){
		strcpy(source_name_unique, source_name);
		return;
	}

	dbg (2, "name found. making unique...");

	char new_name[64], old_name[64];
	strcpy(old_name, source_name);
	do {
		string_increment_suffix(new_name, old_name, 64);
		dbg (2, "new_name=%s", new_name);
		if(strlen(new_name) >= AYYI_NAME_MAX){ ayyi_log_print(LOG_FAIL, __func__); return; }
		strcpy(old_name, new_name);
	} while (am_song__part_lookup_by_name(new_name));

	strcpy(source_name_unique, new_name);
}


void
am_partmanager__get_pos (GList* selection, AyyiSongPos* start, AMPos* end)
{
	GList* l = selection;
	dbg(2, "n_parts=%i", g_list_length(selection));
	if(l){
		AyyiSongPos selection_start = ((AMPart*)l->data)->start;

		AyyiSongPos part_end;
		am_part__end_pos((AMPart*)l->data, &part_end);

		AyyiSongPos selection_end = ((AMPart*)l->data)->start;

		for(;l;l=l->next){
			AMPart* part = l->data;

			am_part__end_pos(part, &part_end);

			ayyi_pos_min(&selection_start, &part->start);
			ayyi_pos_max(&selection_end, &part_end);
		}
		if(start) *start = selection_start;
		if(end) songpos_ayyi2gui(end, &selection_end);
	}
}


/*
 *  All args can be NULL except partlist.
 */
void
am_partmanager__find_boundary (GList* partlist, AyyiSongPos* start, AyyiSongPos* end, AMTrackNum* track_top, AMTrackNum* track_bot)
{
	AMTrackNum track_min = 256;
	AMTrackNum track_max = 0;
	AyyiSongPos first = {100000000,};
	AyyiSongPos _end = {0,};
	AyyiSongPos part_end;

	GList* l = partlist;
	for(;l;l=l->next){
		AMPart* part = l->data;
		am_part__end_pos(part, &part_end);

		ayyi_pos_min(&first, &part->start);
		ayyi_pos_max(&_end, &part_end);

		int t = am_track_list_position (song->tracks, part->track);
		track_min = MIN(track_min, t);
		track_max = MAX(track_max, t);
	}

	if(start) *start = first;
	if(end) *end = _end;
	if(track_top) *track_top = track_min;
	if(track_bot) *track_bot = track_max;

#ifdef DEBUG
	char bbst[32]; ayyi_pos2bbst(&first, bbst); dbg(2, "offset=%s", bbst);
#endif
}
