/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __model_pool_h__
#define __model_pool_h__

#include <model/am_collection.h>
#include <model/pool_item.h>

#define AM_POOL_ITEM_IS_OK(A) (A && A->state == AM_POOL_ITEM_OK)
#define AM_REGION_FULL 0x0fffffff // a magic region_index number indicating a region representing the whole audio file <-- warning! danger!
#define am_pool__iter_init(I) model_collection_iter_init((AMCollection*)song->pool, I);
#define am_pool__emit(...) ({ if(song->loaded) g_signal_emit_by_name (song->pool, __VA_ARGS__); })
#define am_pool ((AMCollection*)song->pool)

AMPoolItem* am_pool__add_ayyi_file            (AyyiFilesource*);
bool        am_pool__remove                   (AMPoolItem*);
void        am_pool__clear                    ();
bool        am_pool__item_exists              (char* fname);

AMPoolItem* am_pool__get_item_from_idx        (AyyiIdx);
AMPoolItem* am_pool__get_item_from_leafname   (const char*);
AMPoolItem* am_pool__get_item_by_orig_name    (const char*);
GList*      am_pool__get_new_items            ();
AMPoolItem* am_pool__get_item_by_id           (AyyiId);

void        am_pool__print                    ();

#ifdef __am_song_c__
void        am_pool__init                     ();
#endif
#ifdef __am_private__
void        am_pool__free                     ();
void        am_pool__sync                     ();
void        am_pool__add                      (AMPoolItem*);
AMPoolItem* am_pool__add_from_file            (const char* fname);
AMPoolItem* am_pool__find_pending_by_leafname (const char*);
#endif

#endif
