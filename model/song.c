/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __am_song_c__
#define __am_private__

#include <model/common.h>
#include <glib-object.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_dbus_proxy.h>

#include "model/model_types.h"
#include "model/time.h"
#include "model/pool.h"
#include "model/curve.h"
#include "model/channel.h"
#include "model/part_manager.h"
#include "model/midi_part.h"
#include "model/track_list.h"
#include "model/plugin.h"
#include "model/mixer.h"
#include "model/automation.h"
#include "model/am_message.h"
#include "model/transport.h"
#include "model/am_client.h"
#include "model/song.h"

#ifdef USE_DBUS
#include "model/dbus.h"
#endif

extern void     am_track_list_reset                (AMTrackList*);

static void     am_song__on_object_new             (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_object_deleted         (AyyiIdent, GError**, AyyiPropType, gpointer);

static void     am_song__on_song_new               (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_audio_part_new         (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_midi_part_new          (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_audio_track_new        (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_midi_track_new         (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_file_new               (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_part_del               (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_track_del              (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_song__on_song_del               (AyyiIdent, GError**, AyyiPropType, gpointer);

static void     am_client_on_channel               (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_client_on_transport             (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_client_on_locators              (AyyiIdent, GError**, AyyiPropType, gpointer);
static void     am_client_on_progress              (AyyiIdent, GError**, double, gpointer);
static void     am_client_on_property              (AyyiIdent, GError**, AyyiPropType, gpointer);

static AMTrack*_am_song__add_track                 (AMTrackType, AyyiIdx);
#ifdef DEBUG
static bool     am_song__sync_playlists            ();
#endif
static void     am_song__on_playlist_name_change   (AyyiIdent);
static void     am_song__on_tempo_change           ();

static bool     am_server_running                  ();

AMSong* song;

struct _AMSongPrivate {
	gint  _property1;
	GRand* rand;
};

G_DEFINE_TYPE_WITH_PRIVATE (AMSong, am_song, G_TYPE_OBJECT)

enum  {
	AM_SONG_0_PROPERTY,
	AM_SONG_PROPERTY1
};

enum  {
	AM_SONG_CURVE_ADD_SIGNAL = 0,
	AM_SONG_CURVE_CHANGE_SIGNAL,
	AM_SONG_NUM_SIGNALS
};

static guint am_song_signals[AM_SONG_NUM_SIGNALS] = {0};

static void g_cclosure_user_marshal_VOID__INT_INT_INT          (GClosure*, GValue*, guint, const GValue*, gpointer, gpointer);
static void g_cclosure_user_marshal_VOID__POINTER_UINT_POINTER (GClosure*, GValue*, guint, const GValue*, gpointer, gpointer);
static void g_cclosure_user_marshal_VOID__POINTER_POINTER      (GClosure*, GValue*, guint, const GValue*, gpointer, gpointer);
static void g_cclosure_user_marshal_VOID__POINTER_INT          (GClosure*, GValue*, guint, const GValue*, gpointer, gpointer);
static void am_song_finalize                                   (GObject*);
static void am_song_get_property                               (GObject*, guint, GValue*, GParamSpec*);


GQuark
am_error_quark ()
{
	static GQuark quark = 0;
	if (!quark) quark = g_quark_from_static_string ("ayyi_comms_error");
	return quark;
}

typedef enum {
	AM_FILE_INDEX = 1,
	AM_NO_REGION,
	AM_TIMEOUT,
	AM_MASTER
} AMError;

#define AM_ERROR am_error_quark ()


/**
 *  am_song_construct:
 *  Returns: (transfer full)
 */
AMSong*
am_song_construct (GType object_type)
{
	AMSong* self = song = (AMSong*) g_object_new (object_type, NULL);

	self->beats2bar   = 4.0;
	self->sample_rate = 44100;

	void _set_locator (AMObject* loc, int property, AMVal value, AyyiHandler2 _, gpointer user_data)
	{
		int loc_num = GPOINTER_TO_INT(loc->user_data);
		am_transport_set_locator_pos(loc_num, &value.sp);
	}

	AMObject* loc = self->loc;
	for(int i=0;i<AYYI_MAX_LOC;i++){
		am_object_init(&loc[i], 1);
		am_object_val(&loc[i]).sp = (AyyiSongPos){-1, 0,};
		loc[i].vals[0].type = G_TYPE_AYYI_SONG_POS;
		loc[i].set = _set_locator;
		loc[i].user_data = GINT_TO_POINTER(i);
	}
	am_object_val(&loc[AM_LOC_LEFT ]).sp = (AyyiSongPos){ 3 * 4, 0, 0};
	am_object_val(&loc[AM_LOC_RIGHT]).sp = (AyyiSongPos){ 4 * 4, 0, 0};
	am_object_val(&loc[AM_LOC_END  ]).sp = (AyyiSongPos){50 * 4, 0, 0};  // affects displayed songlength before the real song has loaded.

	self->channels  = channels__init(0);
	self->parts     = am_list_new();
	self->map_parts = am_list_new();
	self->tracks    = am_track_list_new();
	am_pool__init();

	self->tempo = observable_new();
	self->tempo->min.f = 0.0;
	self->tempo->max.f = 400.0;

	self->spp = observable_new();

	am_q_init();

	self->priv->rand = g_rand_new();

	am_song__connect("periodic-update", (GCallback)am__mixer_periodic_update, NULL);

	void __on_palette_change (GObject* _song, gpointer user_data)
	{
		AMIter i;
		AMPart* part = NULL;
		am_collection_iter_init (am_parts, &i);
		while((part = (AMPart*)am_collection_iter_next (am_parts, &i))){
			am_part__on_palette_change (part);
		}
	}
	am_song__connect("palette-change", G_CALLBACK(__on_palette_change), NULL);

	g_signal_emit_by_name (self, "palette-change");

	return self;
}


/**
 *  am_song_new:
 *  Returns: (transfer full)
 */
AMSong*
am_song_new ()
{
	return am_song_construct (AM_TYPE_SONG);
}


gint
am_song_get_property1 (AMSong* self)
{
	g_return_val_if_fail (self, 0);

	return self->priv->_property1;
}


static void
g_cclosure_user_marshal_VOID__INT_INT_INT (GClosure* closure, GValue* return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	typedef void (*GMarshalFunc_VOID__INT_INT_INT) (gpointer data1, gint arg_1, gint arg_2, gint arg_3, gpointer data2);
	register GMarshalFunc_VOID__INT_INT_INT callback;
	register GCClosure * cc;
	register gpointer data1, data2;
	cc = (GCClosure *) closure;
	g_return_if_fail (n_param_values == 4);
	if (G_CCLOSURE_SWAP_DATA (closure)) {
		data1 = closure->data;
		data2 = param_values->data[0].v_pointer;
	} else {
		data1 = param_values->data[0].v_pointer;
		data2 = closure->data;
	}
	callback = (GMarshalFunc_VOID__INT_INT_INT) (marshal_data ? marshal_data : cc->callback);
	callback (data1, g_value_get_int (param_values + 1), g_value_get_int (param_values + 2), g_value_get_int (param_values + 3), data2);
}


static void
g_cclosure_user_marshal_VOID__POINTER_UINT_POINTER (GClosure* closure, GValue* return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	typedef void (*GMarshalFunc_VOID__POINTER_UINT_POINTER) (gpointer data1, gpointer arg_1, guint arg_2, gpointer arg_3, gpointer data2);
	register GMarshalFunc_VOID__POINTER_UINT_POINTER callback;
	register gpointer data1, data2;
	register GCClosure* cc = (GCClosure*) closure;
	g_return_if_fail (n_param_values == 4);

	if (G_CCLOSURE_SWAP_DATA (closure)) {
		data1 = closure->data;
		data2 = param_values->data[0].v_pointer;
	} else {
		data1 = param_values->data[0].v_pointer;
		data2 = closure->data;
	}
	callback = (GMarshalFunc_VOID__POINTER_UINT_POINTER) (marshal_data ? marshal_data : cc->callback);
	callback (data1, g_value_get_pointer (param_values + 1), g_value_get_uint (param_values + 2), g_value_get_pointer (param_values + 3), data2);
}


static void
g_cclosure_user_marshal_VOID__POINTER_POINTER (GClosure* closure, GValue* return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	typedef void (*GMarshalFunc_VOID__POINTER_POINTER) (gpointer data1, gpointer arg_1, gpointer arg_2, gpointer data2);
	register GMarshalFunc_VOID__POINTER_POINTER callback;
	register gpointer data1, data2;
	register GCClosure* cc = (GCClosure*) closure;
	g_return_if_fail (n_param_values == 3);

	if (G_CCLOSURE_SWAP_DATA (closure)) {
		data1 = closure->data;
		data2 = param_values->data[0].v_pointer;
	} else {
		data1 = param_values->data[0].v_pointer;
		data2 = closure->data;
	}
	callback = (GMarshalFunc_VOID__POINTER_POINTER) (marshal_data ? marshal_data : cc->callback);
	callback (data1, g_value_get_pointer (param_values + 1), g_value_get_pointer (param_values + 2), data2);
}


static void
g_cclosure_user_marshal_VOID__POINTER_INT (GClosure* closure, GValue* return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	typedef void (*GMarshalFunc_VOID__POINTER_INT) (gpointer data1, gpointer arg_1, gint arg_2, gpointer data2);

	register gpointer data1;
	register gpointer data2;
	register GCClosure* cc = (GCClosure *) closure;
	g_return_if_fail (n_param_values == 3);

	if (G_CCLOSURE_SWAP_DATA (closure)) {
		data1 = closure->data;
		data2 = param_values->data[0].v_pointer;
	} else {
		data1 = param_values->data[0].v_pointer;
		data2 = closure->data;
	}

	register GMarshalFunc_VOID__POINTER_INT callback = (GMarshalFunc_VOID__POINTER_INT) (marshal_data ? marshal_data : cc->callback);
	callback (data1, g_value_get_pointer (param_values + 1), g_value_get_int (param_values + 2), data2);
}

static void
am_song_class_init (AMSongClass* klass)
{
	am_song_parent_class = g_type_class_peek_parent (klass);

	G_OBJECT_CLASS (klass)->get_property = am_song_get_property;
	G_OBJECT_CLASS (klass)->finalize = am_song_finalize;
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_SONG_PROPERTY1, g_param_spec_int ("property1", "property1", "property1", G_MININT, G_MAXINT, 0, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE));

	g_signal_new ("periodic_update", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("song_load", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("song_unload", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("song_length_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("tempo_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("locators_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("progress", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__DOUBLE, G_TYPE_NONE, 1, G_TYPE_DOUBLE);
	g_signal_new ("transport_sync", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("transport_stop", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("transport_play", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("transport_rew", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("transport_ff", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("transport_cycle", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("transport_rec", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("pool_item_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
	g_signal_new ("tracks_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_user_marshal_VOID__INT_INT_INT, G_TYPE_NONE, 3, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT);
	g_signal_new ("tracks_change_by_list", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_user_marshal_VOID__POINTER_UINT_POINTER, G_TYPE_NONE, 3, G_TYPE_POINTER, G_TYPE_UINT, G_TYPE_POINTER);
	g_signal_new ("region_delete", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__INT, G_TYPE_NONE, 1, G_TYPE_INT);
	g_signal_new ("note_selection_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_user_marshal_VOID__POINTER_POINTER, G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
	g_signal_new ("plugin_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
	g_signal_new ("palette_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("songmap_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	am_song_signals[AM_SONG_CURVE_CHANGE_SIGNAL] = g_signal_new ("curve_change", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
	am_song_signals[AM_SONG_CURVE_ADD_SIGNAL] = g_signal_new ("curve-add", AM_TYPE_SONG, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_user_marshal_VOID__POINTER_INT, G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_INT);
}


static void
am_song_init (AMSong* self)
{
	self->priv = am_song_get_instance_private(self);
	self->priv->_property1 = 1;
}


static void
am_song_finalize (GObject* obj)
{
	AMSong* self = AM_SONG (obj);

	g_clear_pointer (&self->input_list, g_list_free);
	g_clear_pointer (&self->bus_list, g_list_free);
	g_clear_pointer (&self->plugins, g_list_free);
	g_clear_pointer (&self->work_queue, g_list_free);
	g_clear_pointer (&self->priv->rand, g_rand_free);

	G_OBJECT_CLASS (am_song_parent_class)->finalize (obj);
}


static void
am_song_get_property (GObject* object, guint property_id, GValue* value, GParamSpec* pspec)
{
	AMSong* self = AM_SONG (object);
	switch (property_id) {
		case AM_SONG_PROPERTY1:
			g_value_set_int (value, am_song_get_property1 (self));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}


typedef struct {
	AMPoolItem* pool_item;
	AyyiHandler callback;
	gpointer    user_data;
} PoolHandlerData;


static PoolHandlerData*
pool_handler_data_new (AMPoolItem* pool_item, AyyiHandler callback, gpointer data)
{
	return AYYI_NEW(PoolHandlerData,
		.pool_item = pool_item,
		.callback = callback,
		.user_data = data
	);
}


void
am_song__add_handlers ()
{
	// strictly speaking we should not add some of these handlers until the service is loaded.

	// add the default handlers to the handler chain for each object type
	AyyiIdent id = {0, -1};
	ayyi_client_watch((id.type = AYYI_OBJECT_SONG,       id), AYYI_OP_NEW, am_song__on_song_new, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_AUDIO_PART, id), AYYI_OP_NEW, am_song__on_audio_part_new, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_PART,       id), AYYI_OP_NEW, am_song__on_audio_part_new, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_MIDI_PART,  id), AYYI_OP_NEW, am_song__on_midi_part_new, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_AUDIO_TRACK,id), AYYI_OP_NEW, am_song__on_audio_track_new, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_MIDI_TRACK, id), AYYI_OP_NEW, am_song__on_midi_track_new, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_FILE,       id), AYYI_OP_NEW, am_song__on_file_new, NULL);

	ayyi_client_watch((id.type = -1,                     id), AYYI_OP_NEW, am_song__on_object_new, NULL);

	ayyi_client_watch((id.type = AYYI_OBJECT_AUDIO_PART, id), AYYI_DEL, am_song__on_part_del, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_MIDI_PART,  id), AYYI_DEL, am_song__on_part_del, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_TRACK,      id), AYYI_DEL, am_song__on_track_del, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_AUDIO_TRACK,id), AYYI_DEL, am_song__on_track_del, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_MIDI_TRACK, id), AYYI_DEL, am_song__on_track_del, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_SONG,       id), AYYI_DEL, am_song__on_song_del, NULL);

	ayyi_client_watch((id.type = -1,                     id), AYYI_DEL, am_song__on_object_deleted, NULL);

	ayyi_client_watch((id.type = AYYI_OBJECT_TRANSPORT,  id), AYYI_SET, am_client_on_transport, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_LOCATORS,   id), AYYI_SET, am_client_on_locators, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_PROGRESS,   id), AYYI_SET, (AyyiPropHandler)am_client_on_progress, NULL);
	ayyi_client_watch((id.type = AYYI_OBJECT_CHAN,       id), AYYI_SET, am_client_on_channel, NULL);

	ayyi_client_watch((id.type = -1,                     id), AYYI_SET, am_client_on_property, NULL);
}


bool
am__register_signals (AyyiService* service)
{
	// the purpose of this is to abstract away the particulars of the messaging system

#ifdef USE_DBUS
	am_dbus__register_signals();
#endif
	return true;
}


void
am_song__free ()
{
	// for debugging only. not normally used. AMSong normally only instantiated once.

	g_object_unref0(song->parts);
	g_object_unref0(song->map_parts);

	am_pool__free();
	channels__unload();

	for(int i=0;i<AYYI_MAX_LOC;i++) am_object_deinit(&song->loc[i]);
}


/*
 *  Load the currently open server song into the client.
 */
void
am_song__load ()
{
	if(song->loaded){
		am_song__unload();
	}

	if(((AyyiSongService*)ayyi.service)->song){
		g_return_if_fail(!song->audiodir);
		song->audiodir = ayyi_song__get_audiodir();
		g_return_if_fail(!song->peaksdir);
		song->peaksdir = g_build_filename(((AyyiSongService*)ayyi.service)->song->path, ((AyyiSongService*)ayyi.service)->song->peaks_dir, NULL);
	}

	am_track_list_reset(song->tracks);

	am_pool__sync();

	am_song__sync_tracks();

	am__mixer_sync();
	channels__load();

	g_return_if_fail(!am_collection_length(am_parts));

	am_song__on_tempo_change();
	song->sample_rate = ((AyyiSongService*)ayyi.service)->song->sample_rate;

	am_palette_load(song->palette); // must be done before doing any drawing.

	song->end_auto = true;

	am_automation_sync();
	am_song__sync_parts();

#ifdef DEBUG
	am_song__sync_playlists();
#endif

	observable_set(song->tempo, (AMVal){.f = ((AyyiSongService*)ayyi.service)->song->bpm});

	am_transport_sync();
	song->spp->max.b = ayyi_beats2samples(AYYI_MAX_BEATS);

	song->loaded = true;
	AYYI_DEBUG if(ayyi.log.to_stdout) printf("%ssong-load%s\n", yellow, white);
	am_song__emit("song-load");
}


/*
 *  Remove all local objects
 */
void
am_song__unload ()
{
	printf("%sunload%70s\n", yellow_r, white);

	_am_song__remove_all_parts(); // we must do this _before_ song->loaded is unset so that signals are emitted for users to clear references.

	song->loaded = false;

	am_pool__clear();
	am_track_list_reset(song->tracks);

	g_clear_pointer(&song->audiodir, g_free);
	g_clear_pointer(&song->peaksdir, g_free);

	g_signal_emit_by_name (song, "song-unload");
}


static void
_am_song__restart (AyyiHandler callback, gpointer user_data)
{
	typedef struct
	{
		AyyiHandler callback;
		gpointer    user_data;
		GCallback   on_load;
#ifdef DEBUG
		bool        on_load_done; // check for double loading. probably remove later
#endif
		int         wait_count;
		guint       timeout;
	} C;

#ifdef DEBUG
	static C* cc;
#endif

	void _am_song__restart_on_load(GObject* object, gpointer _c)
	{
		printf(">>>");
		PF0;
		C* c = _c;
#ifdef DEBUG
		g_return_if_fail((c == cc));
		g_return_if_fail(!c->on_load_done);
		c->on_load_done = true;
#endif

		gpointer self = c->on_load;
		g_signal_handlers_disconnect_by_func(song, self, c);

		g_return_if_fail(song->loaded);

		gboolean _restart_post_load_pause_done (gpointer _c)
		{
			PF;
			C* c = _c;
			g_return_val_if_fail(song->loaded, G_SOURCE_REMOVE);
			call(c->callback, AYYI_NULL_IDENT, NULL, c->user_data);
			g_free(c);
			return G_SOURCE_REMOVE;
		}

		void _am_song__restart__on_shm (const GError* error, gpointer _c)
		{
			C* c = _c;
			am_source_remove0(c->timeout);
			dbg(0, "session='%s'", ((AyyiSongService*)ayyi.service)->song->path);
			am_song__unload();
			am_song__load(); // resync to shm
			g_timeout_add(4000, _restart_post_load_pause_done, c);
		}

		ayyi_client__reattach_shm(ayyi.service, _am_song__restart__on_shm, c);
	}

	gboolean am_song__wait_for_shutdown (gpointer _c)
	{
		C* c = _c;
		g_return_val_if_fail(c, G_SOURCE_REMOVE);
#ifdef DEBUG
		g_return_val_if_fail(c == cc, G_SOURCE_REMOVE);
#endif

		//check the service is not still running...
		if(am_server_running()){
			dbg(0, "not yet ready to restart (%i)", c->wait_count);
			c->wait_count++;
			return c->wait_count < 20 ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
		}
		dbg(0, "restarting service...");
		ayyi.service->ping(NULL);

		gboolean timeout (gpointer _c)
		{
			C* c = _c;
			g_signal_handlers_disconnect_by_func(song, c->on_load, c);
			GError* error = g_error_new(AM_ERROR, AM_TIMEOUT, "restart timed out");
			call(c->callback, AYYI_NULL_IDENT, &error, c->user_data);
			g_free(c);
			return G_SOURCE_REMOVE;
		}
		c->timeout = g_timeout_add(12000, timeout, c);

		return G_SOURCE_REMOVE;
	}

	C* c = AYYI_NEW(C,
		.callback = callback,
		.user_data = user_data,
		.on_load = (GCallback)_am_song__restart_on_load
	);
#ifdef DEBUG
	cc = c;
#endif

	am_song__connect("song-load", G_CALLBACK(_am_song__restart_on_load), c);

	dbg(0, "waiting for server shutdown...");
	g_timeout_add(500, am_song__wait_for_shutdown, c);
}


/**
 *  am_song__load_new:
 *  @arg1: (scope async)
 */
void
am_song__load_new (const char* filename, AyyiHandler callback, gpointer user_data)
{
	PF;
#ifdef STOP_ON_UNLOAD
	typedef struct
	{
		const char* filename;
		AyyiHandler callback;
		gpointer    user_data;
	} C;

	C* c = AYYI_NEW(C,
		.filename = g_strdup(filename),
		.callback = callback,
		.user_data = user_data
	);
	static C* cc; cc = c;

	void am_song__on_load_started(AyyiAction* a)
	{
		C* c = a->app_data;
		a->app_data = NULL; //prevent it being freed
		g_return_if_fail(c);
		g_return_if_fail(c == cc);

		void on_restarted(AyyiIdent obj, GError** error, gpointer _c)
		{
			PF;
			C* c = _c;
			g_return_if_fail(c);
			g_return_if_fail(c == cc);

			//check paths
			{
				g_return_if_fail(((AyyiSongService*)ayyi.service)->song);

				//force both paths to have a trailing slash
				char* p1 = g_build_path("/", ((AyyiSongService*)ayyi.service)->song->path, "/", NULL);
				char* p2 = g_build_path("/", c->filename, "/", NULL);
				if(strcmp(p1, p2)) perr("path mismatch: '%s'. expected: %s", p1, p2);
				g_return_if_fail(!strcmp(p1, p2));
				g_free(p1);
				g_free(p2);
			}

			call(c->callback, AYYI_NULL_IDENT, NULL, c->user_data);
			g_free((char*)c->filename);
			g_free(c);
		}

		_am_song__restart(on_restarted, c);
	}

	AyyiAction* action = ayyi_action_new("song new '%s'", filename);
	action->callback = am_song__on_load_started;
	action->app_data = c;

#else //STOP_ON_UNLOAD
	void
	am_song__load_done(AyyiAction* action)
	{
		g_return_if_fail(action);
		PF;
		HandlerData* d = action->app_data;
		a->app_data = NULL; //prevent it being freed
		if(d) call(d->callback, (AyyiIdent){0, 0}, NULL, d->user_data);
	}

	AyyiAction* action = ayyi_action_new("song new '%s'", filename);
	action->callback = am_song__load_done;
	action->app_data = ayyi_handler_data_new(callback, user_data);
#endif //STOP_ON_UNLOAD

	ayyi_set_string(action, AYYI_OBJECT_SONG, AYYI_NEW_SONG, filename);
}


/**
 *  am_song__reload
 *  @arg0: (scope async)
 */
void
am_song__reload (AyyiHandler callback, gpointer user_data)
{
	PF;

	void am_song__reload_done (AyyiAction* action)
	{
		g_return_if_fail(action);
		HandlerData* d = action->app_data;
		action->app_data = NULL; // prevent it being free'd

		void am_song__reload__on_restarted (AyyiIdent obj, GError** error, gpointer _d)
		{
			PF;
			HandlerData* d = _d;
			call(d->callback, AYYI_NULL_IDENT, error, d->user_data);

			g_free(d);
		}

		_am_song__restart(am_song__reload__on_restarted, d);
	}

	AyyiAction* action = ayyi_action_new("song load");
	action->callback = am_song__reload_done;
	action->app_data = ayyi_handler_data_new(callback, user_data);
 
	ayyi_set_obj(action, AYYI_OBJECT_SONG, 0, AYYI_LOAD_SONG, 0);
}


/**
 *  am_song__save
 *  @arg0: (scope async)
 */
void
am_song__save (AyyiHandler callback, gpointer user_data)
{
	void am_song__save_done(AyyiAction* action)
	{
		g_return_if_fail(action);
		PF;

		ayyi_log_print(LOG_OK, "song saved");

		HandlerData* d = action->app_data;
		if(d) call(d->callback, (AyyiIdent){0, 0}, NULL, d->user_data);
	}

	AyyiAction* action = ayyi_action_new("song save");
	action->callback = am_song__save_done;
	action->app_data = ayyi_handler_data_new(callback, user_data);
 
	ayyi_set_obj(action, AYYI_OBJECT_SONG, 0, AYYI_SAVE, 0);
}


void
am_song__undo (AyyiService* service, int n)
{
#ifdef USE_DBUS
	dbus_undo(service, n);
#endif
}


/*
 *  Start/stop spp/metering timer
 */
void
am_song__enable_meters (bool enable)
{
	gboolean periodic_update (gpointer data)
	{
		AMTrack* tr;
		int t; for(t=0;(tr=song->tracks->track[t]);t++){

			float level = am_track__get_meterlevel(tr);
			if (tr->meterval) observable_float_set(tr->meterval, level);

			if (AM_TRK_IS_AUDIO(tr)){
				const AMChannel* ch = am_track__lookup_channel(tr);
				if (ch) observable_float_set(ch->meter_level, level);
			}
		}

#if 0
		AMIter iter;
		channel_iter_init(&iter);
		AMChannel* channel;
		while((channel = channel_next(&iter))){
			AyyiChannel* ac = ayyi_mixer_container_get_quiet(&((AyyiSongService*)ayyi.service)->amixer->tracks, channel->core_index);
		}
#endif

		uint64_t samples = ((AyyiSongService*)ayyi.service)->song->transport_frame;
		if(samples != song->spp->value.b)
			observable_set(song->spp, (AMVal){.b = samples});

		am_song__emit("periodic-update", NULL);

		return G_SOURCE_CONTINUE;
	}

	if(enable){
		if(!song->periodic) song->periodic = g_timeout_add(40/*ms*/, (gpointer)periodic_update, NULL);
	}else{
		g_source_remove(song->periodic);
		song->periodic = 0;
	}
}


/**
 *  am_song_add_track:
 *  @name: ownership is taken, it will be freed after, so must be dynamically allocated by glib.
 *  @arg3: (scope async)
 *  @arg4: (skip): GJS seems to ignore the hint to skip this argument
 */
void
am_song_add_track (AMTrackType type, char* name, int n_channels, AyyiHandler3 callback, gpointer user_data)
{
	typedef struct
	{
		AyyiHandler3 callback;
		gpointer     user_data;
	} C;

	void am_song_add_track_done (AyyiAction* action)
	{
		C* c = action->app_data;
		g_return_if_fail(c);

		gpointer user_data = action->app_data;
		AyyiIdent id = {action->obj.type, action->ret.idx};
		action->app_data = c->user_data;
		call(c->callback, id, action->ret.error, c->user_data);
		action->app_data = user_data;
	}

	AMTrackNum tnum = am_track_list_find_unused(song->tracks);
	if(tnum < 0) { perr ("cannot find empty track."); return; }

	if(!n_channels) n_channels = 1;

	AyyiAction* a = ayyi_action_new("track add (%s)", name);
	a->obj.type = (type == TRK_TYPE_MIDI) ? AYYI_OBJECT_MIDI_TRACK : AYYI_OBJECT_TRACK;
	a->callback = am_song_add_track_done;

	a->app_data = AYYI_NEW(C,
		.callback = callback,
		.user_data = user_data
	);
#ifdef USE_DBUS
	ayyi_dbus_object_new(a, (char*)name, false, n_channels, 0, NULL, 0, 0);
#endif
}


extern void am_track_list_rebuild_pointers (AMTrackList*);

static AMTrack*
_am_song__add_track (AMTrackType type, AyyiIdx object_index)
{
	// Add a local proxy track.

#ifdef DEBUG
	char* name = NULL;
	switch(type){
		case TRK_TYPE_AUDIO:
			;AyyiTrack* tt = ayyi_song__audio_track_at(object_index);
			name = tt->name;
			break;
		case TRK_TYPE_MIDI:
			;AyyiMidiTrack* t = ayyi_song__midi_track_at(object_index);
			name = t->name;
			break;
		default:
			return NULL;
	}
	dbg(1, "%s%s%s", bold, name, white);
#endif

	AMTrack* tr = am_track__new(type, object_index);
	dbg(2, "%s", tr->name);

	am_track_list_rebuild_pointers(song->tracks); //temp! needed as the list previously was not null terminated.

	g_signal_emit_by_name (song->tracks, "add", tr);

	return tr;
}


/**
 *  am_song_remove_track:
 *  @arg1: (scope async)
 */
void
am_song_remove_track (AMTrack* trk, AyyiHandler callback, gpointer user_data)
{
	void am_song__remove_track_done (AyyiAction* a)
	{
		HandlerData* d = a->app_data;
		if(d){
			call(d->callback, (AyyiIdent){a->obj.type, a->ret.idx}, &a->ret.error, d->user_data);
		}
	}

	AyyiTrack* at   = (AyyiTrack*)am_track__get_shared(trk);

	AyyiAction* a   = ayyi_action_new("audio track delete %i", at->shm_idx);
	a->app_data     = ayyi_handler_data_new(callback, user_data);
	a->callback     = am_song__remove_track_done;
	a->op           = AYYI_DEL;
	a->obj.type     = trk->type == TRK_TYPE_AUDIO ? AYYI_OBJECT_AUDIO_TRACK : (trk->type == TRK_TYPE_MIDI ? AYYI_OBJECT_MIDI_TRACK : 0);
	a->obj_idx.idx1 = at->shm_idx;

	if(AYYI_TRACK_IS_MASTER(at)){
		a->ret.error = g_error_new(AM_ERROR, AM_MASTER, "cannot delete master track");
		ayyi_action_complete(a);
		return;
	}

	g_return_if_fail(a->obj.type);

	void am_song__remove_track_remove_parts_done (AyyiIdent id, GError** error, gpointer action)
	{
		if(error && *error){
			pwarn("%s", (*error)->message);
			return ayyi_action_complete(action);
		}

#ifdef DEBUG
		AMTrack* track = am_track_list_find_by_shm_idx(song->tracks, ((AyyiAction*)action)->obj_idx.idx1, ((AyyiAction*)action)->obj.type);
		if(track){
			const AMPart* part = am_partmanager__get_next(NULL, track);
			if(part) pwarn("parts not deleted!");
		}
#endif

		am_action_execute((AyyiAction*)action);
	}

	am_song__remove_parts_by_track(trk, am_song__remove_track_remove_parts_done, a);
}


/**
 *  am_song__clear_tracks:
 *  @arg0: (scope async)
 */
void
am_song__clear_tracks (AyyiHandler callback, gpointer user_data)
{
	typedef struct {
		int         n_to_delete;
		int         n_deleted;
		AyyiHandler callback;
		gpointer    user_data;
	} C;

	void done (C* c)
	{
		if(c->callback) c->callback(AYYI_NULL_IDENT, NULL, c->user_data);
		g_free(c);
	}

	void am_song__clear_on_delete (AyyiIdent id, GError** error, gpointer _c)
	{
		C* c = _c;

		if(++c->n_deleted >= c->n_to_delete){
			done(c);
		}
	}

	C* c = AYYI_NEW(C,
		.callback = callback,
		.user_data = user_data
	);

	AMTrack* trk = NULL;
	AMIter i;
	am_collection_iter_init (am_tracks, &i);
	am_collection_iter_next(am_tracks, &i); // leave the first track
	while((trk = am_collection_iter_next(am_tracks, &i))){
		c->n_to_delete++;
		am_song_remove_track(trk, am_song__clear_on_delete, c);
	}
	if(!c->n_to_delete) done(c);
}


/*
 *  Ensure sure all local track data is in sync with the server.
 */
void
am_song__sync_tracks ()
{
	GList* am_song__get_midi_track_list ()
	{
		GList* track_list = NULL;

		int i = 0;
		midi_track_shared* shared = 0;
		while((shared = ayyi_song__midi_track_next(shared))){
			dbg (2, "[%u] id=%Lu name=%s", i, shared->id, shared->name);
			if(!strlen(shared->name)){ pwarn ("midi track has no name. ignoring..."); continue; }
			track_list = g_list_append(track_list, GINT_TO_POINTER(i));
			i++;
		}

		dbg (2, "num midi tracks found: %i", g_list_length(track_list));
		return track_list;
	}

	int track_count = 0;
	{
		AyyiTrack* track = NULL;
		while((track = ayyi_song__audio_track_next(track))){
			if(!strlen(track->name)){ dbg (0, "route has no name. ignoring... idx=%i", track->shm_idx); continue; }

			track_count++;
			if(!song->tracks->track[track_count] || song->tracks->track[track_count]->type == TRK_TYPE_NULL){
				am_track__new(TRK_TYPE_AUDIO, track->shm_idx);
			}
		}
	}

	GList* midi_tracks = am_song__get_midi_track_list();
	GList* l = midi_tracks;
	while(l){
		if(++track_count > AM_MAX_TRK) break;

		if((!song->tracks->track[track_count]) || (song->tracks->track[track_count]->type == TRK_TYPE_NULL)){
			am_track__new(TRK_TYPE_MIDI, GPOINTER_TO_INT(l->data));
		}
		l = l->next;
	}
	g_list_free(midi_tracks);

	am_track_list_rebuild_pointers(song->tracks);

	if(!am_tracks->selection){
		am_track_list_select_first();
	}

	AYYI_DEBUG ayyi_log_print(0, "%i tracks found", track_count);
}


void
am_song__sync_parts ()
{
	// ensure all local data structs are in sync with the server.

	//note: currently we dont delete any parts here. Its possible to have partlist items not in shm after this.

	int count = 0;

	GList* parts = ayyi_song__get_audio_part_list(); //copy the list before making modifications

	GList* p = parts;
	for(;p;p=p->next){
		AyyiAudioRegion* r = p->data;
		if(ayyi_object_is_deleted((AyyiItem*)r)) continue;

		AMPart* part = NULL;
		if(!am_collection_find_by_idx((AMCollection*)song->parts, r->shm_idx)){
			part = am_partmanager__add_new_from_index((AyyiIdent){AYYI_OBJECT_AUDIO_PART, r->shm_idx});
		}

		if(part) count++;
	}
	g_list_free(parts);

	// midi parts
	if((parts = ayyi_song__get_midi_part_list())){
		GList* p = parts;
		for(;p;p=p->next){
			AyyiMidiRegion* part = p->data;
			if(part->playlist < 0){ dbg(1, "ignoring unused part..."); continue; }
			/*MidiPart* part =*/ am_partmanager__add_new_from_index((AyyiIdent){AYYI_OBJECT_MIDI_PART, part->shm_idx});
		}
		g_list_free(parts);
	}

	dbg(1, "part count: %u.", count);
}


AMPart*
am_song__get_part_by_region_index (AyyiIdx region_index, AyyiMediaType type)
{
	g_return_val_if_fail(type == AYYI_AUDIO || type == AYYI_MIDI, NULL);

	return am_song__get_part_by_ident((AyyiIdent){
		.type = (type == AYYI_AUDIO) ? AYYI_OBJECT_AUDIO_PART : AYYI_OBJECT_MIDI_PART,
		.idx = region_index
	});
}


/*
 *  This is used as a test for new parts, so failing is ok.
 */
AMPart*
am_song__get_part_by_ident (AyyiIdent obj)
{
	g_return_val_if_fail(obj.type != AYYI_OBJECT_PART || obj.type != AYYI_OBJECT_MIDI_PART || obj.type != AYYI_OBJECT_AUDIO_PART , NULL);

	AMPart* part = NULL;
	GList* l = song->parts->list;
	if(l){
		for(;l;l=l->next){
			part = l->data;
			if(!part->ayyi) continue; // part may have been deleted.
			dbg (3, "  test: gpart->pod_index=%u", part->ident.idx);

			if((part->ident.idx == obj.idx) && (part->ident.type == obj.type)){
				if(obj.type == AYYI_OBJECT_AUDIO_PART && !part->pool_item) perr("!! pool_item");
				if(obj.type == AYYI_OBJECT_AUDIO_PART && !part->pool_item) pwarn("audio part has no pool_item! idx=%i", part->ayyi->shm_idx);
				return part;
			}
		}
		dbg (3, "part not found. region_idx=%u", obj.idx);
	}
	return NULL;
}


AMTrack*
am_song__get_track_for_part (AyyiRegionBase* region, AyyiMediaType type)
{
	g_return_val_if_fail(region, NULL);
	AyyiPlaylist* playlist = ayyi_song__playlist_at(region->playlist);
	if(!playlist){ pwarn ("unable to determine track. part has no playlist! (%i %s)\n", region->shm_idx, (region->name && (region->name[0] != '\0')) ? region->name : "<no name>"); return 0; }

	return am_track_list_find_by_playlist(playlist, type);
}


#if 0
void
am_song__part_selection_remove(const AMPart* gpart, void* sender_win)
{
	PF;
	am_part_selection = g_list_remove(am_part_selection, gpart);
	am_parts__emit ("selection-change", NULL);
}
#endif


/*
 *  @param offset - the amount to move. This is always positive.
 *  @param forwards   - true for forwards movement, false for backwards.
 *  @param track_offset - TODO is ignored. part->track is used instead.
 */
void
am_song__part_selection_move (AyyiSongPos* offset, bool forwards, int track_offset)
{
	g_return_if_fail(offset);

	GList* l = am_parts_selection;
	if(!l) return;

	for(;l;l=l->next){
		AMPart* part = l->data;
		AyyiSongPos new_pos = part->start;
		if(forwards){
			ayyi_pos_add(&new_pos, offset);
		} else {
			ayyi_pos_sub(&new_pos, offset);
		}
#ifdef DEBUG
		char bbst[32]; ayyi_pos2bbst(&new_pos, bbst); dbg(0, "new_pos=%s t=%i '%s'", bbst, part->track->ident.idx, part->track->name);
#endif
		am_part_move(part, &new_pos, part->track, NULL, NULL);
	}
}


void
am_song__get_end (GPos* end)
{
	g_return_if_fail(end);

	songpos_ayyi2gui(end, &((AyyiSongService*)ayyi.service)->song->end);

	AYYI_DEBUG if(_debug_ > 1){ char bbst[64]; pos2bbst(end, bbst); dbg(0, "end=%i %s", end->beat, bbst); }
}


/*
 *  Set the song length in beats
 */
void
am_song__set_end (int end)
{
	dbg(1, "%i", end);
	am_object_val(&song->loc[AM_LOC_END]).sp = (AyyiSongPos){end, 0,};
#ifdef USE_DBUS
	guint64 time64;
	memcpy(&time64, &am_object_val(&song->loc[AM_LOC_END]).sp, 8);

	dbus_set_prop_int64 (NULL, AYYI_OBJECT_SONG, AYYI_END, 0, time64);
#endif
	g_signal_emit_by_name (song, "song-length-change");
}


/**
 *  am_song__get_pool_item_from_region: (skip)
 *
 *  Find the pool_item for the given Part.
 *
 *  Are there any conditions under which this should fail?
 */
AMPoolItem*
am_song__get_pool_item_from_region (AyyiIdx region_idx)
{
	// TODO this fn uses two methods - should it just use the 2nd?

	AMPart* part = am_song__get_part_by_region_index(region_idx, AYYI_AUDIO);
	if(part && part->pool_item){
		dbg(2, "pool item found for region_idx=%u: '%s'", region_idx, part->pool_item->leafname);
		return part->pool_item;
	}

	// the region may be non-active. Lookup the source_id in the filesource list:
	AyyiRegion* shared = ayyi_song__audio_region_at(region_idx);
	if(shared){
		AMPoolItem* pool_item = am_pool__get_item_by_id(shared->source0);
		if(pool_item) return pool_item;
	}

	pwarn("not found. (region_idx=%u)", region_idx);
	return NULL;
}


/**
 *  am_song__get_pool_item_from_region_without_id: (skip)
 *
 *  Find the pool_item for the given *region*.
 *
 *  Used for new regions where am_song__get_pool_item_from_region() fails because the pool_item id is not set.
 */
AMPoolItem*
am_song__get_pool_item_from_region_without_id (AyyiIdx region_idx)
{
	// the region may be non-active. Lookup the source_id in the filesource list
	AyyiRegion* region = ayyi_song__audio_region_at(region_idx);
	if(region){
		uint64_t filesource_id = region->source0;
		if(filesource_id){
			dbg (2, "filesource_id=%Li", filesource_id);
			AyyiFilesource* filesource = ayyi_filesource_get_from_id(filesource_id);
			if(filesource){
				GList* l= song->pool->list;
				for(;l;l=l->next){
					AMPoolItem* pool_item = l->data;
					char* leafname = g_path_get_basename(filesource->name);
					dbg(3, "  %s == %s ?", leafname, pool_item->leafname);
					gboolean found = !strcmp(leafname, pool_item->leafname);
					g_free(leafname);
					if(found) return pool_item;
				}
			}
		}
		else pwarn ("failed to get filesource_id. region_idx=%i", region_idx);
	}

	dbg (2, "pool_item not found. (region_idx=%u)", region_idx);
	return NULL;
}


#ifdef DEBUG
static bool
am_song__sync_playlists ()
{
	int count = 0;
	AyyiPlaylist* shared = NULL;
	while((shared = ayyi_song__playlist_next(shared))){
		count++;
	}

	dbg(2, "num playlists found: %i", count);
	return true;
}
#endif


static void
am_song__on_playlist_name_change (AyyiIdent obj)
{
	// mostly done server-side. Appaently nothing to do here.

	PF;
	g_return_if_fail(obj.type == AYYI_OBJECT_LIST);
}


AMPart*
am_song__part_lookup_by_name (const char* name)
{
	GList* l = song->parts->list;
	for(;l;l=l->next){
		AMPart* item = l->data;
		g_return_val_if_fail(item, NULL);
		if(!strcmp(item->name, name)) return item;
	}
	return NULL;
}


double
am_song__get_tempo ()
{
	return ((AyyiSongService*)ayyi.service)->song->bpm;
}


/**
 *  am_song__set_tempo
 *  @arg1: (scope async)
 */
void
am_song__set_tempo (double tempo, AyyiHandler callback, gpointer user_data)
{
	PF;

	AyyiAction* action = ayyi_action_new("set tempo %.1f", tempo);
	action->callback = ayyi_handler_simple;
	HandlerData* d = ayyi_handler_data_new(callback, user_data);
	action->app_data = d;

	if(tempo == ((AyyiSongService*)ayyi.service)->song->bpm){
		ayyi_action_complete(action);
		return;
	}

	ayyi_set_float(action, 0, 0, AYYI_TEMPO, tempo);
}


/**
 *  am_song__add_part:
 *  @arg0:               either AYYI_AUDIO or AYYI_MIDI
 *  @src_region:         [audio only] specifies an existing region to define the Part contents.
 *                         Use REGION_FULL to create a part containing the whole of the audio file.
 *  @arg2: (nullable):   [audio only] the audio file to use.
 *  @len:                length in mu. If not set, the server will try and use a default length.
 *  @colour:             use 0 for default colour
 *  @arg9: (scope async)
 *
 *  Request the engine to add a new part.
 *
 *  The interface to this function is subject to change. Should there be a separate Copy function?
 *  Or add an extra arg to indicate from_region or from_file. Definitely remove the need for REGION_FULL.
 */
void
am_song__add_part (AyyiMediaType media_type, AyyiIdx src_region_idx, AMPoolItem* pool_item, AyyiIdx track_idx, AyyiSongPos* stime, uint64_t len, unsigned int colour, const char* name, uint32_t inset, AyyiHandler user_callback_, gpointer user_data)
{
	bool from_region;
	AyyiRegion* region = NULL;
	uint64_t id = 0;
	uint32_t src_idx = 0;
	GError* error = NULL;

	g_return_if_fail((media_type == AYYI_AUDIO || media_type == AYYI_MIDI));

	if(media_type == AYYI_MIDI){ // currently only empty new parts are supported - no copying...
		from_region = false;

	}else if(src_region_idx == AM_REGION_FULL || src_region_idx == -1){
		// the new part is based on the whole audio file.
		// note: sometimes the default region is not yet created. We need to take it from the pool_item instead.
		from_region = false;
		g_return_if_fail(pool_item);
		g_return_if_fail(pool_item->state == AM_POOL_ITEM_OK);
		uint32_t file_idx = pool_item->pod_index[0];
		dbg (1, "from file. pool_item='%s' file_idx=%i", pool_item->leafname, file_idx);
		if(file_idx < 0){
			error = g_error_new(AM_ERROR, AM_FILE_INDEX, "File index");
			goto out;
		}
		src_idx = file_idx;
	}else{
		from_region = true;
		region = ayyi_song__audio_region_at(src_region_idx);
		dbg (1, "from_region: src_region_idx=%u", src_region_idx);

		if(!region){
			error = g_error_new(AM_ERROR, AM_NO_REGION, "No region");
			goto out;
		}
		src_idx = src_region_idx;
	}

#ifdef DEBUG
	{
		char bbst[32];
		ayyi_pos2bbst(stime, bbst);
		dbg(2, "pos=%s %i", bbst, ayyi_pos2samples(stime));
	}
#endif

	void am_song__add_part_done (AyyiAction* a)
	{
		HandlerData* d = a->app_data;
		if (d) {
			AyyiIdent id = {a->obj.type, a->ret.idx};
			call(d->callback, id, &a->ret.error, d->user_data);

			if (a->ret.error) {
				// Failed to create part. delete the local proxy part.
				AMPart* part = (AMPart*)am_collection_find_by_id(am_parts, a->obj.id);
				if (part && part->status == PART_LOCAL) {
					_am_song__remove_part(part);
				}
			}
		}
	}

	typedef struct
	{
		uint64_t id;
	} C;

	gboolean add_part__post_check (gpointer _c)
	{
		C* c = _c;

		AMPart* part = (AMPart*)am_collection_find_by_id (am_parts, c->id);
		if (part && part->status != PART_OK) {
			pwarn("id=%"PRIi64" status=%s", c->id, am_part__print_status(part->status));
		}
		g_free(c);
		return G_SOURCE_REMOVE;
	}

	AMPart* part = am_partmanager__add_new_local(media_type);
	part->track = (AMTrack*)am_track_list_find_by_shm_idx(song->tracks, track_idx, media_type == AYYI_AUDIO ? TRK_TYPE_AUDIO : TRK_TYPE_MIDI);
	part->pool_item = pool_item;
	part->start = *stime;
	ayyi_mu2pos(len, &part->length);

	AyyiPlaylist* playlist = ayyi_song__get_playlist_by_track(ayyi_song__audio_track_at(track_idx)); //TODO midi
	if(!playlist) pwarn("playlist not found");

	AyyiRegionBase* r = part->ayyi = (AyyiRegionBase*)AYYI_NEW(AyyiRegion, //TODO currently not freed in case of error, TODO midi
		.shm_idx = -1,
		.id = id = (uint64_t)g_rand_int(song->priv->rand) * (uint64_t)g_rand_int(song->priv->rand),
		.position = ayyi_pos2samples(stime),
		.length = ayyi_mu2samples(len),
		.playlist = playlist ? playlist->shm_idx : -1
	);

	g_signal_emit_by_name (song->parts, "add", part);

	g_timeout_add(4000, add_part__post_check, AYYI_NEW(C,
		//temp
		.id = r->id
	));

  out:;

#ifdef USE_DBUS
	AyyiAction* action = ayyi_action_new("part add (%.50s)", name);
	action->obj.id = id;
	action->obj.type = media_type == AYYI_AUDIO ? AYYI_OBJECT_AUDIO_PART : AYYI_OBJECT_MIDI_PART;
	action->callback = am_song__add_part_done;
	action->app_data = ayyi_handler_data_new(user_callback_, user_data);
	if(error){
		action->ret.error = error;
		ayyi_action_complete(action);
	}else{
		ayyi_dbus_part_new(action, name, id, !from_region, src_idx, track_idx, stime, len, inset);
	}
#endif
}


#ifdef HAVE_INTROSPECTION
/**
 *  am_song__add_part_wrapped: (rename-to am_song__add_part)
 *
 *  Note that the callback arg is GError* and not GError** which GIR cannot handle.
 *
 *  @arg1: (scope async)
 */
void
am_song__add_part_wrapped (AMPartInput* in, AyyiHandler3 callback)
{
	void am_song__add_part_wrapped_done (AyyiIdent id, GError** error, void* _callback)
	{
		AyyiHandler3 callback = _callback;
		callback(id, error ? *error : NULL, NULL);
	}

	AyyiSongPos pos = am_part_input_get_start(in);
	#define INSET 0
	#define COLOUR 0
	am_song__add_part(AYYI_AUDIO, AM_REGION_FULL, am_part_input_get_pool_item(in), am_part_input_get_track(in), &pos, in->_length, COLOUR, am_part_input_get_name(in), INSET, am_song__add_part_wrapped_done, callback);
}
#endif


/**
 *  am_song__remove_part
 *  @arg1: (scope async)
 *
 * Return value: (transfer none)
 */
AyyiAction*
am_song__remove_part (AMPart* part, AyyiHandler callback, gpointer user_data)
{
	g_return_val_if_fail(part, NULL);

	typedef struct _data
	{
		AMPart*         part;
		AyyiPropHandler handler;
		AyyiHandler     callback;
		gpointer        user_data;
	} C;

	void done (AyyiIdent obj, GError** error, gpointer _data)
	{
		if(am_song__get_part_by_ident(obj)) pwarn("part not deleted!");

		C* c = _data;
		if(c){
			call(c->callback, obj, NULL, c->user_data);
			am_part_unref(c->part);

			// because we hide the AyyiAction, we also lose the automatic data freeing.
			g_free(c);
		}
	}

	void am_song__remove_part_handler (AyyiIdent id, GError** error, AyyiPropType _, gpointer user_data)
	{
		// This turns out to be not useful. We cannot call the callback until after all the other handlers are called.

		dbg(2, "idx=%i", id.idx);
		C* c = user_data;
		if(c){
			if(ayyi_client_remove_watch(id.type, AYYI_DEL, id.idx, c->handler)){
				//g_free(data);
			}
		}
		else pwarn("no user callback");
	}

	part->ref_count++;

	C* data = AYYI_NEW(C, 
		.part      = part,
		.callback  = callback,
		.handler   = am_song__remove_part_handler,
		.user_data = user_data
	);

	ayyi_client_watch(part->ident, AYYI_DEL, am_song__remove_part_handler, data);

	return ayyi_song_container_delete_item(PART_IS_AUDIO(part) ? &((AyyiSongService*)ayyi.service)->song->audio_regions : &((AyyiSongService*)ayyi.service)->song->midi_regions, (AyyiItem*)part->ayyi, done, data);
}


void
_am_song__remove_part (AMPart* part)
{
	PF2;
	g_return_if_fail(part);

	if(am_list_is_pending(song->parts, part->ident.idx, am_part_ayyi_to_media_type(part->ident.type)))
		pwarn("PENDING - TESTME");
	else if(!g_list_find(song->parts->list, part))
		pwarn("cannot remove part - not in list.");

	am_collection_remove(am_parts, part);

#ifdef DEBUG
	{ //checks
		if(part->ayyi->id){
			if(am_collection_find_by_id(am_parts, part->ayyi->id)){
				perr("part removed but is still in partlist! id=%"PRIi64, part->ayyi->id);
			}
		}else{
			perr("part id not set. deleted=%i", part->ayyi->flags & deleted);
		}
	}
#endif

	am_parts__emit("delete", part);
	am_part_unref(part);
}


/**
 *  am_song__remove_all_parts
 *  @arg0: (scope async)
 */
void
am_song__remove_all_parts (AyyiHandler callback, gpointer user_data)
{
	typedef struct
	{
		int         n_remaining;
		AyyiHandler callback;
		gpointer    user_data;
	} C;

	C* c = AYYI_NEW(C,
		.n_remaining = g_list_length(song->parts->list), // we could just recount afterwards, but new parts could be created during the remove.
		.callback = callback,
		.user_data = user_data
	);

	void done (AyyiIdent id, GError** error, gpointer user_data)
	{
		C* c = user_data;
		c->n_remaining--;
		if(c->n_remaining < 1){
			call(c->callback, id, error, c->user_data);
		}
	}

	GList* l = song->parts->list;
	for(;l;l=l->next){ // its possible the list may be modified while we loop through it. Hopefully the latter part of the list wont be modified.
		am_song__remove_part(l->data, done, c);
	}
}


/*
 *  Delete all local proxy part objects
 */
void
_am_song__remove_all_parts ()
{
	if(song->parts){
		while(song->parts->list){
			_am_song__remove_part(song->parts->list->data);
		}

#ifdef DEBUG
		if(song->parts->list) pwarn ("partlist is not empty!! size=%i", g_list_length(song->parts->list));
#endif
	}
}


/**
 *  am_song__remove_parts_by_track
 *  @arg1: (scope async)
 */
void
am_song__remove_parts_by_track (AMTrack* trk, AyyiHandler user_callback, gpointer user_data)
{
	typedef struct {
		GList*      actions;
		int         n_complete;
		AyyiHandler user_callback;
		gpointer    user_data;
	} C;

	C* c = AYYI_NEW(C,
		.user_callback = user_callback,
		.user_data = user_data
	);

	void am_song__remove_parts_by_track__item_done (AyyiIdent id, GError** error, gpointer data)
	{
		//TODO check for errors

		C* c = data;
		c->n_complete++;
		if(c->n_complete >= g_list_length(c->actions)){
			dbg(0, "all done.");
			call(c->user_callback, id, NULL, c->user_data);
			g_list_free(c->actions);
			g_free(c);
		}
	}

	GList* l = song->parts->list;
	for(;l;l=l->next){
		const AMPart* part = l->data;
		if(part->track == trk) c->actions = g_list_append(c->actions, ayyi_song_container_delete_item(&((AyyiSongService*)ayyi.service)->song->audio_regions, (AyyiItem*)part->ayyi, am_song__remove_parts_by_track__item_done, c));
	}

	if(!c->actions){
		am_song__remove_parts_by_track__item_done((AyyiIdent){}, NULL, c);
	}
}


/**
 *  am_song__remove_parts_by_file
 *  @arg1: (scope async)
 */
void
am_song__remove_parts_by_file (AMPoolItem* item, AyyiHandler callback, gpointer data)
{
	typedef struct {
		AMPoolItem*     pool_item;
		guint           timeout;
		AyyiHandler     callback;
		gpointer        user_data;
		AyyiPropHandler responder;
	} C;

	void am_song__remove_parts_by_file__on_region_delete (AyyiIdent id, GError** error, AyyiPropType _, gpointer user_data)
	{
		g_return_if_fail(user_data);
		C* d = user_data;

		if(d->pool_item){
			GList* regions = pool_item_get_regions(d->pool_item);
			if(regions){
				dbg(0, "n_remaining=%i", g_list_length(regions));
				g_list_free(regions);
			}else{
				dbg(0, "finished.");
				g_source_remove(d->timeout);
				d->timeout = 0;

				if(d->callback){
					AyyiIdent id;
					d->callback(id, NULL, d->user_data);
				}
				ayyi_client_remove_watch(AYYI_OBJECT_AUDIO_PART, AYYI_DEL, -1, d->responder);
				g_free(d);
			}
		}
	}

	C* d = AYYI_NEW(C,
		.pool_item = item,
		.callback  = callback,
		.user_data = data,
		.responder = am_song__remove_parts_by_file__on_region_delete
	);

	AyyiIdent id = {AYYI_OBJECT_AUDIO_PART, -1};
	ayyi_client_watch(id, AYYI_DEL, am_song__remove_parts_by_file__on_region_delete, d);

	GList* regions = am_partmanager__get_regions_by_pool_item(item);
	dbg(0, "n_parts=%i", g_list_length(regions));
	GList* l = regions;
	for(;l;l=l->next){
		ayyi_song__delete_audio_region((AyyiAudioRegion*)l->data);
	}
	g_list_free(regions);

	gboolean am_song__remove_parts_by_file__timeout(gpointer data)
	{
		C* d = data;
		if(d->callback){
			AyyiIdent id;
			GError* error = g_error_new(G_FILE_ERROR, 3533, "delete timed out"); //TODO domain
			d->callback(id, &error, d->user_data);
			g_error_free(error);
		}
		g_free(d);
		ayyi_client_remove_watch(AYYI_OBJECT_AUDIO_PART, AYYI_DEL, -1, d->responder);

		return G_SOURCE_REMOVE;
	}

	//TODO we shouldn't be doing this here - Actions already have a timeout.
	d->timeout = g_timeout_add(10000, am_song__remove_parts_by_file__timeout, d);
}


/**
 *  am_song__add_pool_item
 *  @arg1: (scope async)
 */
void
am_song__add_pool_item (AMPoolItem* pool_item, AyyiHandler callback, gpointer data)
{
	PF;
	void am_song__add_file_done (AyyiAction* a)
	{
		PoolHandlerData* d = a->app_data;
		if(d){
			if(!a->ret.error){
				g_return_if_fail(d->pool_item);
				dbg(1, "idx=%i id=%Lu %s", a->ret.idx, ayyi_song__filesource_at(a->ret.idx)->id, d->pool_item->leafname);

				// TODO we seem to still be using an old signal
				//am_pool__emit ("item-changed", d->pool_item, AM_CHANGE_IDX, NULL); //notify that the idx has been added.
				g_signal_emit_by_name (song, "pool-item-change", d->pool_item); //notify that the idx has been added.
			}

			call(d->callback, (AyyiIdent){AYYI_OBJECT_FILE, a->ret.idx}, &a->ret.error, d->user_data);
		}
	}

	AyyiAction* a = ayyi_action_new("file add (%s)", pool_item->filename);
	a->obj.type   = AYYI_OBJECT_FILE;
	a->callback   = am_song__add_file_done;
	a->app_data   = pool_handler_data_new(pool_item, callback, data);
#ifdef USE_DBUS
	//TODO will timeout for long files. perhaps call should return imediately?
	ayyi_dbus_object_new(a, pool_item->filename, 0, 0, 0, NULL, 0, 0);
#endif
}


/**
 * am_song__add_file
 * @arg1: (scope async)
 *
 * The returned item will have an initial state of POOL_ITEM_PENDING
 *
 * XReturns: (transfer none)
 * Return value: (transfer none)
 */
AMPoolItem*
am_song__add_file (const char* fname, AyyiHandler callback, gpointer user_data)
{
	if(ayyi_song__have_file(fname)){  // maybe move this check into am_pool__add_from_file ???
		pwarn("duplicate file. not adding.");
		AyyiIdent id;
		GError* error = g_error_new(G_FILE_ERROR, 0, "wont add duplicate file.");
		call(callback, id, &error, user_data);
		if(error) g_error_free(error);
		return NULL;
	}

	AMPoolItem* pool_item = NULL;
	if(!(pool_item = am_pool__add_from_file(fname))){
		pwarn("failed to make pool_item.");
		AyyiIdent id;
		GError* error = g_error_new(G_FILE_ERROR, 0, "failed to make pool_item");
		call(callback, id, &error, user_data);
		if(error) g_error_free(error);
		return NULL;
	}
	dbg(1, "pool_item=%p", pool_item);

	am_song__add_pool_item(pool_item, callback, user_data);

	return pool_item;
}


/**
 *  am_song__remove_file
 *  @arg1: (scope async)
 */
void
am_song__remove_file (AMPoolItem* pool_item, AyyiHandler callback, gpointer data)
{
	PF;

	PoolHandlerData* d = AYYI_NEW(PoolHandlerData,
		.pool_item = pool_item,
		.callback = callback,
		.user_data = data
	);

	void am_song__on_remove_file (AyyiAction* a)
	{
		g_return_if_fail(a);
		PoolHandlerData* d = a->app_data;

		if(!a->ret.error){
			if(d){
				AMPoolItem* item = d->pool_item;
				g_return_if_fail(item);
				if(am_pool__remove(item)){ //for 2nd part of a stereo file, the pool item may have already been deleted.
					am_pool__emit ("delete", item);
					g_object_unref(item);

					ayyi_log_print(LOG_OK, "File removed.");
				}else
					pwarn("not found in pool");
			}
		}

		if(d){
			AyyiIdent id;
			call(d->callback, id, &a->ret.error, d->user_data);
		}
		PF_DONE;
	}

	void am_song__remove_file__remove_parts_done (AyyiIdent id, GError** error, gpointer user_data)
	{
		PoolHandlerData* d = user_data;
		AMPoolItem* pool_item = d->pool_item;

		GList* regions = am_partmanager__get_regions_by_pool_item(pool_item);
		dbg(0, "n_regions=%i", g_list_length(regions));
		if (regions) {
			pwarn("not all regions deleted yet...");
			g_list_free(regions);
		} else {
			if (error && *error) {
				call(d->callback, id, error, d->user_data);
				return;
			}

			if (pool_item->channels > 1) pwarn("TESTME non mono pool_item");

			for (int c=0;c<2;c++) {
				if (c >= pool_item->channels) break;
				if (pool_item->pod_index[c] < 0) { pwarn("fileidx not set!! c=%i", c); continue; }

				AyyiAction* a   = ayyi_action_new("file remove (%s) %i", pool_item->leafname, pool_item->pod_index[c]);
				a->callback     = am_song__on_remove_file;
				a->app_data     = !c ? d : NULL; //cannot use data more than once as it is free'd.
				d->pool_item    = pool_item;
				a->op           = AYYI_DEL;
				a->obj.type     = AYYI_OBJECT_FILE;
				a->obj_idx.idx1 = pool_item->pod_index[c];
				am_action_execute(a);
			}
		}
	}

	GList* regions = am_partmanager__get_regions_by_pool_item(pool_item); //dont bother, just call am_song__remove_parts_by_file()
	if(regions){
		dbg(1, "file is in use - removing regions first...");
		g_list_free(regions);

		am_song__remove_parts_by_file(pool_item, am_song__remove_file__remove_parts_done, d);
	}else{
		AyyiIdent id;
		am_song__remove_file__remove_parts_done(id, NULL, d);
	}
}


/**
 *  am_song__midi_note_add: (skip)
 *  @arg1: ownership of the AyyiMidiNote is taken
 *  @arg2: (scope async)
 */
void
am_song__midi_note_add (AMPart* part, AyyiMidiNote* note, AyyiHandler callback, gpointer user_data)
{
	PF;

	g_return_if_fail(part);

	typedef struct
	{
		AMPart*       part;
		AyyiMidiNote* note;
		AyyiHandler   callback;
		gpointer      user_data;
	} C;

	void
	am_song_midi_note_add_cb (AyyiAction* action)
	{
		PF;
		C* d = action->app_data;
		AyyiMidiNote* note = d->note;
		g_free(note);

		AyyiIdent id = d->part->ident; // should be the ident of the new Note?
		call(d->callback, id, &action->ret.error, d->user_data);
	}

	AyyiAction* a  = ayyi_action_new("midi note add");
	a->op          = AYYI_OP_NEW;
	a->obj.type    = AYYI_OBJECT_MIDI_NOTE;
	a->callback    = am_song_midi_note_add_cb;
	a->app_data    = AYYI_NEW(C,
		.part = part,
		.note = note,
		.callback = callback,
		.user_data = user_data,
	);
	dbg(0, "note=%i, length=%i", note->note, note->length);

	AyyiSongPos stime; ayyi_samples2pos(note->start, &stime);
	uint32_t parent_idx = part->ayyi->shm_idx;
	uint32_t src_idx = note->note;
	uint32_t inset = note->velocity;

	ayyi_dbus_object_new(a, NULL, FALSE, src_idx, parent_idx, &stime, ayyi_samples2mu(note->length), inset);
}


void
am_song__print_tracks ()
{
	UNDERLINE;
	PF;
	printf("                   type\n");
	AMTrackNum t = 0; AMTrack* tr; for(;(tr = song->tracks->track[t]);t++){
		printf("  %2i: %10p %2i %i %s\n", t, tr, tr->ident.idx, tr->type, tr->name);
		if(!tr->type) break;
	}

	ayyi_song__print_tracks();
}


void
am_song__channels_print ()
{
	printf("%s():\n", __func__);

	printf("               type nchans core nplg     strip                  has_pan\n");
	GPtrArray* array = song->channels->array;
	int i;
	for(i=0;i<array->len;i++){
		AMChannel* channel = g_ptr_array_index(array, i);
		g_return_if_fail(channel);
		AyyiChannel* ac = ayyi_mixer_container_get_quiet(&((AyyiSongService*)ayyi.service)->amixer->tracks, channel->ident.idx);
		if (ac) {
			AyyiTrack* track = ayyi_song__audio_track_at(channel->ident.idx);
			char name[64];
			g_strlcpy(name, track ? track->name : "[no track]", AYYI_NAME_MAX);

			printf(" %2i %p %5i %6i %4i %4i %16s %6i\n", i, channel, channel->type, channel->nchans, channel->ident.idx, ayyi_channel__count_plugins(ac), name, ac ? ac->has_pan: 0);
		} else {
			printf(" %2i %p NO AYYI_CHANNEL!\n", i, channel);
		}
	}
}


static void
am_song__on_object_new (AyyiIdent obj, GError** error, AyyiPropType _, gpointer user_data)
{
	//the core has announced that a new object has been made.

	switch(obj.type){
		case AYYI_OBJECT_EMPTY:
			pwarn ("object type not set.");
			break;

		case AYYI_OBJECT_FILE:
		case AYYI_OBJECT_TRACK:
		case AYYI_OBJECT_MIDI_TRACK:
		case AYYI_OBJECT_PART:
		case AYYI_OBJECT_AUDIO_PART:
		case AYYI_OBJECT_MIDI_PART:
		case AYYI_OBJECT_SONG:
			//have their own handlers
			break;
		case AYYI_OBJECT_MIDI_NOTE:
			dbg (0, "OBJECT_MIDI_NOTE (ignoring...)");
			//The message currently cannot tell us which Part the note belongs to, so is useless.
			//Its anyway better to send a parts item-changed message...

			//MidiNote* note = engine_midi_note_idx_get_note(gpart, object_index);
			break;
		case AYYI_OBJECT_LIST:
			dbg (0, "OBJECT_LIST");
			break;
		case AYYI_OBJECT_UNSUPPORTED:
			dbg (0, "OBJECT_UNSUPPORTED");
			break;
		default:
			pwarn ("unknown object type (%s).", ayyi_print_object_type(obj.type));
			break;
	}
}


static void
am_song__on_song_new (AyyiIdent id, GError** error, AyyiPropType _, gpointer user_data)
{
	dbg (2, "...");
	am_song__load();
}


static void
am_song__on_audio_track_new (AyyiIdent obj, GError** error, AyyiPropType _, gpointer user_data)
{
	dbg (1, "OBJECT_TRACK: idx=%i", obj.idx);
	if(!am_track_list_is_known(song->tracks, TRK_TYPE_AUDIO, obj.idx)){
		AMTrack* tr = _am_song__add_track(TRK_TYPE_AUDIO, obj.idx);

		dbg(1, "TODO is track-count updating necc?");
		//song->last_track++;
		//engine_track_sync_count();

		#ifndef ENGINE_HAS_CHANNELS
		channels__append_new(tr);
		#endif
	}
}


static void
am_song__on_midi_track_new (AyyiIdent obj, GError** error, AyyiPropType _, gpointer user_data)
{
	dbg (1, "idx=%i", obj.idx);
	if(!am_track_list_is_known(song->tracks, TRK_TYPE_MIDI, obj.idx)){
		_am_song__add_track(TRK_TYPE_MIDI, obj.idx);
	}
}


static void
am_song__on_file_new (AyyiIdent obj, GError** error, AyyiPropType _, gpointer user_data)
{
	AYYI_DEBUG am_pool__print();
	dbg (1, "idx=%i", obj.idx);

	AMPoolItem* pool_item = am_pool__get_item_from_idx(obj.idx);
	if (pool_item) {
		dbg(1, "already in pool");
		AYYI_DEBUG pool_item_print(pool_item);
	} else {
		AyyiFilesource* file = ayyi_song__filesource_at(obj.idx);
		g_return_if_fail(file);

		if ((pool_item = am_pool__find_pending_by_leafname(file->original_name[0] ? file->original_name : file->name))) {
			AYYI_DEBUG pool_item_print(pool_item);
			am_pool_item_set_from_ayyi(pool_item, file); //change the filename from the original to the imported name.
			return;
		}
		dbg(1, "pending pool_item not found. will add...");

		AyyiFilesource* f = ayyi_song__filesource_at(obj.idx);
		if ((pool_item = am_pool_item_new_from_ayyi(f))) {

			AyyiFilesource* f2 = ayyi_file_get_linked(f);
			if (f2) {
				dbg(1, "found other channel. n=%i", pool_item->channels);
				if(pool_item->pod_index[1] < 0)
					pool_item_add_channel(pool_item, f2);
				else dbg(1, "2nd channel already set");
			}
			else dbg(1, "f2=%p mono?", f2);

			am_pool__add(pool_item);
		}
	}
}


static void
am_song__on_audio_part_new (AyyiIdent id, GError** errror, AyyiPropType _, gpointer user_data)
{
	//what do we do with this region?
	// 1) if we cant find an associated pool_item, there is nothing we can do.
	// 2) if it does have a pool_item, but there are no existing regions, we assume this region is the "default region", ie not a Part.
	// 3) otherwise, we create a new gui part for this region.

	//warning! despite the naming, these are *regions*. They may or may not correspond to Parts.

	//the fact that the id for the pool_item is not yet set is a major pita...
	//we use the id for comparisons, so any code here is likely to be fragile.

	if(!ayyi_region__index_ok(id.idx)){ perr ("part bad index! (%i)", id.idx); return; }

	AyyiRegion* shared = ayyi_song__audio_region_at(id.idx);
	g_return_if_fail(shared);
	if(shared->playlist < 0){ dbg (0, "playlist index not set for new part. ignoring... name=%s", shared->name); return; }
	AyyiPlaylist* playlist = ayyi_song__playlist_at(shared->playlist);
	if(!playlist){ dbg (0, "region has no track (playlist). ignoring... name=%s", shared->name); return; }

	//the first region for a file (the 'default region'), is not intended to have a Part made for it.
	AMPoolItem* pool_item = am_song__get_pool_item_from_region_without_id(id.idx);
	if(pool_item){
		dbg (2, "OBJECT_PART. found pool_item='%s'", pool_item->leafname);
		//is it safe to assume that if a pool_item doesnt have id, we can ignore the request?

if(!pool_item_count_regions(pool_item)) pwarn("testing: adding part even though pool_item has no Default Region (what is status of these now?)");
//		if(pool_item_count_regions(pool_item)){               // <-----this needs id.
			dbg (1, "OBJECT_PART.%i pool_item='%s' has regions. will add AMPart...", id.idx, pool_item->leafname);

			AMPart* part = (AMPart*)am_collection_find_by_id (am_parts, shared->id);
			if (part) {
				dbg(1, "using existing part... id=%Lu", shared->id);
				if (part->status == PART_LOCAL) {
					part->status = PART_OK;
					part->ident = id;
					g_free(part->ayyi);
					part->ayyi = (AyyiRegionBase*)shared;
					am_part__sync(part, AYYI_NO_PROP);

					AYYI_DEBUG am_part__print(part);
				}
				else pwarn("unexpected AMPart status");
				g_signal_emit_by_name (am_parts, "item-changed", part, CHANGE_ALL, NULL);
				return;
			}

			if((part = am_partmanager__add_new_from_index(id))){
				if(part->pool_item && part->pool_item->state == AM_POOL_ITEM_OK){
					ayyi_log_print(0, "new part added.");
					//g_signal_emit_by_name (song->pool, "change");
					g_signal_emit_by_name (song, "pool-item-change", part->pool_item);
					g_signal_emit_by_name (am_parts, "item-changed", part, CHANGE_ALL, NULL);
				}
				else dbg(0, "OBJECT_PART !! pool_item->state=%s", pool_item_print_state(part->pool_item));
			}
	} else {
		dbg (0, "OBJECT_PART %i. cannot find pool_item for this region. Creating PENDING part...", id.idx);
		AMPart* part = am_partmanager__add_new_pending(id.idx, AYYI_AUDIO);
		if(!part) pwarn("PENDING part not created.");
	}
}


static void
am_song__on_midi_part_new (AyyiIdent id, GError** error, AyyiPropType _, gpointer user_data)
{
	dbg (0, "region_idx=%i", id.idx);
	AyyiMidiRegion* region = ayyi_song__midi_region_at(id.idx);
	g_return_if_fail(region);

	{
		//do some sanity checks
		if(region->playlist < 0) { pwarn("invalid! playlist not set"); return; }
	}

	AMPart* part = (AMPart*)am_collection_find_by_id (am_parts, region->id);
	if (part) {
		dbg(0, "using existing part... id=%Lu", region->id);
		if (part->status == PART_LOCAL) {
			part->status = PART_OK;
			part->ident = id;
			g_free(part->ayyi);
			part->ayyi = (AyyiRegionBase*)region;
			am_midi_part__sync((MidiPart*)part);

			am_part__print(part);
		}
		g_signal_emit_by_name (am_parts, "item-changed", part, CHANGE_ALL, NULL);
		return;
	}
	else pwarn("not found! id=%"PRIu64, region->id);
	part = am_partmanager__add_new_from_index(id);
	if (part) {
		g_signal_emit_by_name (song->parts, "add", part);
	}
	else pwarn("failed to add new midi part. idx=%i", id.idx);
}


static void
am_song__on_part_del (AyyiIdent region, GError** error, AyyiPropType _, gpointer user_data)
{
#if 0
	//we cant use this test as it is likely to fail due to ->last being out of range.
	if(!ayyi_region__index_ok(object_idx)){ P_ERR ("PART: bad object index! (%i)\n", object_idx); return; }
#endif

	AMPart* part = am_song__get_part_by_ident(region);
	if(part){
		if(_debug_ > -1){
			AyyiItem* item = ayyi_song__get_item_by_ident(region);
			if(item && (!(item->flags & deleted))){
				pwarn("region has not been deleted");
			}
		}
		_am_song__remove_part(part);
		ayyi_log_print(0, "Part deleted (%i)", region.idx);
	}else{
		//not all regions have parts, so this is not an error. The pool may be showing the region, so it needs to be updated.
		if(region.type == AYYI_OBJECT_AUDIO_PART){
			dbg(2, "%i: no Part for this region. updating pool...", region.idx);
			g_signal_emit_by_name (song, "region-delete", region.idx);
			ayyi_log_print(0, "Region deleted");
		}else{
			dbg(0, "no part found for MIDI_PART %i", region.idx);
		}
	}
}


static void
am_song__on_track_del (AyyiIdent track, GError** error, AyyiPropType _, gpointer user_data)
{
	dbg (1, "%s.%i", ayyi_print_object_type(track.type), track.idx);
	AMTrackType T = (track.type == AYYI_OBJECT_TRACK || track.type == AYYI_OBJECT_AUDIO_TRACK) ? TRK_TYPE_AUDIO : TRK_TYPE_MIDI;
	AMTrack* trk =  am_track_list_find_by_ident (song->tracks, track);
	if(trk){
		if(T == TRK_TYPE_AUDIO) channels__remove_channel(trk);

		trk->type = TRK_TYPE_NULL;

		am_track_list_rebuild_pointers(song->tracks);

		g_signal_emit_by_name (song->tracks, "delete", trk);
	}
	else pwarn("cannot find track. T=%i", T);
}


static void
am_song__on_song_del (AyyiIdent id, GError** error, AyyiPropType _, gpointer user_data)
{
	//TODO handler order is important. clients may need to run before am_song so that data still exists.

	dbg (0, "OBJECT_SONG");
	if(song->loaded)
		am_song__unload();
	else
		pwarn("song not loaded");
}


void
am_song__on_object_deleted(AyyiIdent obj, GError** error, AyyiPropType _, gpointer user_data)
{
	//the core has announced that an object has been deleted.
	PF2;

	switch(obj.type){
		case AYYI_OBJECT_EMPTY:
			pwarn ("object type not set.");
			break;
		case AYYI_OBJECT_AUDIO_PART:
		case AYYI_OBJECT_MIDI_PART:
		case AYYI_OBJECT_TRACK:
		case AYYI_OBJECT_AUDIO_TRACK:
		case AYYI_OBJECT_MIDI_TRACK:
		case AYYI_OBJECT_SONG:
			break;
		case AYYI_OBJECT_LIST:
			dbg (1, "playlist was removed. nothing to do.");
			dbg (1, "n_playlists=%i n_tracks=%i", ayyi_song__get_playlist_count(), ayyi_song__get_track_count());
			break;
		case AYYI_OBJECT_UNSUPPORTED:
			dbg (0, "OBJECT_UNSUPPORTED");
			break;
		default:
			UNEXPECTED_OBJECT_TYPE(obj.type);
			break;
	}
}


static void
am_song__on_audio_part_changed (AyyiIdent obj, AyyiPropType prop)
{
	PF2;
	if(!ayyi_region__index_ok(obj.idx)){ perr ("PART: bad object index! (%i)", obj.idx); return; }

	AMPart* part = am_song__get_part_by_ident(obj);
	if(!part){
		pwarn("audio part not found. idx=%i", obj.idx);
		return;
	}else{
		if(part->status == PART_PENDING){
			dbg(0, "using PENDING part.");
			part->status = PART_OK;
			am_list_unpend(song->parts, part);
		}
	}
	am_part__sync(part, prop);
	g_signal_emit_by_name (am_parts, "item-changed", part, CHANGE_ALL, NULL);
}


static void
am_client_on_channel (AyyiIdent obj, GError** error, AyyiPropType prop, gpointer user_data)
{
	SIG_IN1;

	switch(prop){
		case AYYI_LEVEL:
			{
				AMChannel* channel = channels__get_by_index(obj.idx);
				if(channel){
					observable_float_set(channel->level, am_channel__fader_level(channel));
				}
			}
			break;
		default:
			break;
	}
}


static void
am_client_on_transport(AyyiIdent obj, GError** error, AyyiPropType _, gpointer user_data)
{
	SIG_IN1;
	dbg (1, "speed=%.2f", ((AyyiSongService*)ayyi.service)->song->transport_speed);

	am_transport_sync();

	if(ayyi_transport_is_playing())
		am_song__emit("transport-play"); // TODO only emit this if we weren't playing before.
	else if(ayyi_transport_is_stopped())
		am_song__emit("transport-stop");
}


static void
am_client_on_locators(AyyiIdent obj, GError** error, AyyiPropType _, gpointer user_data)
{
	am_song__emit("locators_change");
}


static void
am_client_on_progress(AyyiIdent obj, GError** error, double val, gpointer user_data)
{
	if(val >= 0.0 && val <= 1.0) am_song__emit ("progress", val);
}


#ifdef DEBUG
#define DBG_PROP if(_debug_ > 1) printf("%s.%s: %s.%i %s\n", func, __func__, ayyi_print_object_type(obj.type), obj.idx, ayyi_print_prop_type(prop))
#else
#define DBG_PROP
#endif

static void
am_client_on_property (AyyiIdent obj, GError** error, AyyiPropType prop, gpointer user_data)
{
	PF2;
#ifdef DEBUG
	static const char* func = __func__;
#endif

	if(!song->loaded) return;

	void handle_track(AMTrack* track, AyyiPropType prop){
		switch(prop){
			case AYYI_OUTPUT:
				DBG_PROP;
				g_signal_emit_by_name (song, "tracks-change", obj.idx, obj.idx, AM_CHANGE_OUTPUT);
				break;
			case AYYI_NAME:
				DBG_PROP;
				_am_track__sync_name(track);
				am_song__emit ("tracks-change", obj.idx, obj.idx, AM_CHANGE_NAME);
				break;
			case AYYI_MUTE:
				DBG_PROP;
				g_signal_emit_by_name (song, "tracks-change", obj.idx, obj.idx, AM_CHANGE_MUTE);
				break;
			case AYYI_SOLO:
				DBG_PROP;
				g_signal_emit_by_name (song, "tracks-change", obj.idx, obj.idx, AM_CHANGE_SOLO);
				break;
			case AYYI_ARM:
				DBG_PROP;
				am_song__emit ("tracks-change", obj.idx, obj.idx, AM_CHANGE_ARMED);
				break;
			default:
				UNEXPECTED_PROPERTY(obj.type);
				break;
		}
	}

	switch(obj.type){
		case AYYI_OBJECT_EMPTY:
			pwarn ("object type not set.");
			break;
		case AYYI_OBJECT_SONG:
			switch(prop){
				case AYYI_TEMPO:
					dbg (1, "TEMPO");
					am_song__on_tempo_change();
					break;
				default:
					dbg (1, "OBJECT_SONG ignoring...");
			}
			break;
		case AYYI_OBJECT_AUDIO_TRACK: {
			;AMTrack* track = NULL;
			if(!(track = am_track_list_find_by_ident(song->tracks, obj))){
				if(!(track = am_track_list_find_by_shm_idx(song->tracks, obj.idx, TRK_TYPE_BUS))){
					pwarn("object not found. %s: %i", ayyi_print_object_type(obj.type), obj.idx);
					return;
				}
			}
			handle_track(track, prop);
			} break;
		case AYYI_OBJECT_MIDI_TRACK: {
			;AMTrack* track = NULL;
			if(!(track = am_track_list_find_by_shm_idx(song->tracks, obj.idx, TRK_TYPE_MIDI))){
				pwarn("object not found. %s: %i", ayyi_print_object_type(obj.type), obj.idx);
				return;
			}
			handle_track(track, prop);
			} break;
		case AYYI_OBJECT_TRACK:
			pwarn("cannot determine track type. changes should specify media type. assuming AUDIO...");
			;AMTrack* track = NULL;
			if(!(track = am_track_list_find_by_shm_idx(song->tracks, obj.idx, TRK_TYPE_AUDIO/* !!! FIXME type */))){
				pwarn("object not found. %s: %i", ayyi_print_object_type(obj.type), obj.idx);
				return;
			}
			handle_track(track, prop);
			break;
		case AYYI_OBJECT_CHAN:
			dbg (1, "OBJECT_CHAN ignoring...");
			break;
		case AYYI_OBJECT_AUDIO_PART:
			am_song__on_audio_part_changed(obj, prop);
			break;
		case AYYI_OBJECT_MIDI_PART:
			if(!ayyi_song__midi_region_index_ok(obj.idx)){ perr ("MIDI PART: bad object index! (%i)", obj.idx); return; }
			dbg (1, "OBJECT_MIDI_PART");
			AMPart* part = am_song__get_part_by_region_index(obj.idx, AYYI_MIDI);
			if(!part){ pwarn("midi part not found. idx=%i", obj.idx); break; }
			am_part__sync(part, AYYI_NO_PROP);
			g_signal_emit_by_name (am_parts, "item-changed", part, CHANGE_ALL, NULL);
			break;
		case AYYI_OBJECT_FILE:
			dbg (1, "AYYI_OBJECT_FILE...");
			AMPoolItem* pool_item = am_pool__get_item_from_idx(obj.idx);
			if(pool_item) g_signal_emit_by_name (song, "pool-item-change", pool_item);
			break;
		case AYYI_OBJECT_LIST:
			dbg (1, "AYYI_OBJECT_LIST... (playlist)");
			switch(prop){
				case AYYI_NO_PROP:
					// change wasnt specified. update everything
					am_song__on_playlist_name_change(obj);
					break;
				case AYYI_NAME:
					am_song__on_playlist_name_change(obj);
					break;
				default:
					UNEXPECTED_PROPERTY(obj.type);
					break;
			}
			break;
		case AYYI_OBJECT_UNSUPPORTED:
			dbg (0, "OBJECT_UNSUPPORTED");
			break;
		default:
			UNEXPECTED_OBJECT_TYPE(obj.type);
			break;
	}
}


static void
am_song__on_tempo_change ()
{
	observable_set(song->tempo, (AMVal){.f = ((AyyiSongService*)ayyi.service)->song->bpm});

	//update part positions (samples to musical-time mapping has changed)
	AMIter i;
	AMPart* part;
	am_collection_iter_init(am_parts, &i);
	while((part = am_collection_iter_next(am_parts, &i))){
		AyyiAudioRegion* region;
		if((region = (AyyiAudioRegion*)(am_part_get_shared(part)))){
			ayyi_samples2pos(region->position, &part->start);
		}
	}

	am_song__emit("tempo-change");
}


void
am_song__clear_songmap ()
{
	if(!song->map_parts->list) return;

	GList* l = song->map_parts->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		g_free(part);
	}
	g_list_free(song->map_parts->list);
	song->map_parts->list = NULL;

	am_song__emit("songmap-change");
}


static bool
am_server_running ()
{
	gint argcp = 0;
	gchar** argvp = NULL;
	GError* error = NULL;
	if(g_shell_parse_argv("/usr/bin/pidof ardourd.bin", &argcp, &argvp, &error)){ // pidof returns 0 if pid found, otherwise non-zero.
		GError* error = NULL;
		int exit_status = 0;
		gchar* standard_out = NULL;
		gchar* standard_error = NULL;
		if(!g_spawn_sync(NULL, argvp, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, &standard_out, &standard_error, &exit_status, &error)){
			ayyi_log_print(LOG_FAIL, "spawning for pid lookup failed");
			GERR_WARN;
		}else{
			if(exit_status){
				if(exit_status != 256) pwarn("unexpected exit code: %i", exit_status);
				dbg(2, "pid lookup: exit status: %i - server process already terminated?", exit_status);
			}else{
				dbg(2, "got server pid - yes, is running");
				return true;
			}
			if(standard_out && standard_out[0] != '\0') dbg(0, "stdout: %s", standard_out);
			if(standard_error && standard_error[0] != '\0') dbg(0, "stderr: %s", standard_error);
			g_clear_pointer(&standard_out, g_free);
			g_clear_pointer(&standard_error, g_free);
		}
		g_strfreev(argvp);
	}
	return false;
}
