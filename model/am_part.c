/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __am_private__
#include <model/common.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include <ayyi/ayyi_shm.h>

#include <model/model_types.h>
#include <model/am_message.h>
#include <model/track_list.h>
#include <model/song.h>
#include <model/pool.h>
#include <model/midi_part.h>
#include <model/am_part.h>
#include <model/time.h>

extern int         idle_ref;
extern int         debug;
extern Observer    observer;

extern gboolean    is_song_shm_local           (void*);

static bool       _am_part_sync_name           (AMPart*, AyyiRegionBase*);
static bool        am_part_check_bounds        (AMPart*);
static AMTrack*   _am_part_get_ayyi_track      (AMPart*);
static bool       _am_part_sync_pos            (AMPart*);
static AMPoolItem* am_part__get_pool_item      (AMPart*);
static uint32_t    am_part__get_auto_fg_colour (AMPart*);
static void        am_part__max_length         (AMPart*, AyyiSongPos* max);

#include "midi_part.c"


gpointer
am_part_copy (gpointer boxed)
{
	AMPart* part = boxed;
	part->ref_count++;
	return boxed;
}


GType
am_part_get_type ()
{
	static GType type = 0;

	if (G_UNLIKELY (!type))
		type = g_boxed_type_register_static ("AMPart", am_part_copy, (GBoxedFreeFunc)am_part_unref);

	return type;
}


/**
 * am_part_new: (skip)
 */
AMPart*
am_part_new ()
{
	AMPart* part = AYYI_NEW(AMPart,
		.ident.type = AYYI_OBJECT_AUDIO_PART,
	);
	part->ident.idx = -1;
	part->fg_colour = am_part__get_auto_fg_colour(part);
	part->length = (AyyiSongPos){8, 0, 0};
	part->ref_count = 1;

	return part;
}


void
am_part_unref (AMPart* part)
{
	void am_part__free (AMPart* part)
	{
		PF2;

		if(PART_IS_MIDI(part)){
			g_clear_pointer(&((AMPartMidi*)part)->note_selection, g_list_free);
		}
		g_free(part);
	}

	dbg(1, "%i --> %i", part->ref_count, part->ref_count - 1);

	part->ref_count--;
	if(part->ref_count < 1 && part->ayyi->id && am_collection_find_by_id(am_parts, part->ayyi->id)) pwarn("freeing a part that is in the partlist!");
	if(part->ref_count < 1) am_part__free(part);
}


typedef struct
{
	AMPart*      part;
	AMTrack*     track;
	AyyiHandler  user_callback;
	gpointer     user_data;

} PartCallbackData;

#define callback_part ((PartCallbackData*)action->app_data)->part


static AyyiAction*
part_action_new (AMPart* part, AyyiHandler callback, gpointer user_data, char* format, ...)
{
	g_return_val_if_fail(part->ref_count > 0, NULL);

	char name[AYYI_NAME_MAX];
	va_list argp;
	va_start(argp, format);
	vsnprintf(name, AYYI_NAME_MAX - 1, format, argp);
	va_end(argp);

	AyyiAction* a = ayyi_action_new(name);
	a->obj.type = PART_IS_AUDIO(part) ? AYYI_OBJECT_AUDIO_PART : AYYI_OBJECT_MIDI_PART;

	*(PartCallbackData*)(a->app_data = g_new0(PartCallbackData, 1)) = (PartCallbackData){
		.part = part,
		.user_callback = callback,
		.user_data = user_data
	};

	dbg(1, "ref_count: %i --> %i", part->ref_count, part->ref_count + 1);

	part->ref_count++;

	return a;
}


/**
 *  am_part_move:
 *  @arg3: (scope async)
 */
void
am_part_move (AMPart* part, AyyiSongPos* pos, AMTrack* new_track, AyyiHandler user_callback, gpointer user_data)
{
	//note: the track is set in the callback as a separate operation. If new_track is NULL, the track is not changed.

	g_return_if_fail(part);
	g_return_if_fail(ayyi_pos_is_valid(pos));
	if (!((AyyiSongService*)ayyi.service)->song) return;

	void am_part_move_done (AyyiAction* action)
	{
		// As well as tidying up following the transaction, this fn also sets the new Part track number.

		PartCallbackData* d = action->app_data;
		AMPart* part = callback_part;
		g_return_if_fail(part);

		dbg(2, "part=%s", part->name);
		_am_part_sync_pos(part);
		am_part_check_bounds(part);

		char buff[AYYI_BBST_MAX];
		ayyi_pos2bbst(&part->start, buff);
		ayyi_log_print(0, "Part moved to %s.", buff);

		if(d->track){ // track has changed
			am_part_set_track(part, d->track, d->user_callback, d->user_data);

		}else{
			g_signal_emit_by_name (am_parts, "item-changed", part, AM_CHANGE_POS_X, NULL);
			call(d->user_callback, (AyyiIdent){action->obj.type, action->ret.idx}, &action->ret.error, d->user_data);
		}

		am_part_unref(part);
	}

	if(new_track){
		dbg(1, "new track: %s", new_track->name);
		part->track = new_track; // ensure that model data reflects the updated track before the operation is complete.
		//TODO if request fails, restore the old track.
	}

	AyyiAction* a       = part_action_new(part, user_callback, user_data, "part move %u", part->ayyi->shm_idx);
	a->callback         = am_part_move_done;
	PartCallbackData* d = a->app_data;
	d->track            = new_track;

#ifdef DEBUG
	char buff[AYYI_BBST_MAX]; ayyi_pos2bbst(pos, buff);
	dbg (1, "part_idx=%i pos=%s", part->ayyi->shm_idx, buff);
#endif

	/*

	currently we are not using the ardour id as an identifier, as it is 64 bit....
	//TODO the ayyi id is now 64 bit so we CAN use it...?

	Unlike other functions, the sample number is being used.
	What time format should we use? we should maybe standardise on the higher resolution song_pos?
	Or are we going to allow either/both?

	*/

	a->obj_idx.idx1 = part->ayyi->shm_idx;
	a->prop         = AYYI_STIME;
	a->i_val        = ayyi_pos2samples(pos);

	ayyi_action_execute(a);
}


/**
 *  am_part_set_track
 *  @arg2: (scope async)
 */
void
am_part_set_track (AMPart* part, AMTrack* new_track, AyyiHandler user_callback, gpointer user_data)
{
	g_return_if_fail(part);
	g_return_if_fail(new_track);

	void
	am_part__set_track_done (AyyiAction* action)
	{
		g_return_if_fail(action);
		PF;

		PartCallbackData* d = action->app_data;
		AyyiIdent id = {action->obj.type, action->ret.idx};

		AMTrack* new_track = ((PartCallbackData*)action->app_data)->track;
		AMPart* part = callback_part;
		g_return_if_fail(part);
		part->track = new_track;

		if(am_part__sync(part, AYYI_NO_PROP)) g_signal_emit_by_name (am_parts, "item-changed", part, AM_CHANGE_POS_X, NULL);

		if(d) call(d->user_callback, id, &action->ret.error, d->user_data);

		am_part_unref(part);
	}

	AyyiAction* a = part_action_new(part, user_callback, user_data, "part set track (part %u --> %s)", part->ayyi->shm_idx, new_track->name);
	a->ret.idx    = part->ayyi->shm_idx; //TODO should not be necc here. Need to clarify what ret.idx should contain when no _new_ object returned.
	a->callback   = am_part__set_track_done;
	((PartCallbackData*)a->app_data)->track = new_track;

	dbg(1, "part_idx=%i", part->ayyi->shm_idx);

	ayyi_set_obj(a, PART_IS_AUDIO(part) ? AYYI_OBJECT_AUDIO_PART : AYYI_OBJECT_MIDI_PART, part->ayyi->shm_idx, AYYI_TRACK, new_track->ident.idx);
}


/**
 *  am_part_trim_left:
 *  @arg1: (scope async)
 */
void
am_part_trim_left (AMPart* part, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(part);

	void part_trim_left_done (AyyiAction* action)
	{
		PF;
		AMPart* part = callback_part;
		g_return_if_fail(part);

		if(am_part__sync(part, AYYI_NO_PROP)) g_signal_emit_by_name (am_parts, "item-changed", part, AM_CHANGE_POS_X & AM_CHANGE_INSET, NULL);

		char bbst[AYYI_BBST_MAX];
		ayyi_pos2bbst(&part->start, bbst);
		ayyi_log_print(0, "Part trimmed to %s offset %i", bbst, part->region_start);

		am_part_unref(part);
	}

	uint64_t mu = ayyi_pos2mu(&part->start);
	uint32_t region_idx = part->ayyi->shm_idx;

	AyyiAction* a = part_action_new(part, callback, user_data, "region trim left. id %u", region_idx);
	a->callback   = part_trim_left_done;

	char buff[AYYI_BBST_MAX]; ayyi_mu2bbst(mu, buff);
	dbg (1, "new_left=%Lumu (%Li samples) %s", mu, ayyi_mu2samples(mu), buff);
	ayyi_set_obj(a, AYYI_OBJECT_PART, region_idx, AYYI_INSET, mu);
}


/**
 *  am_part_split:
 *  @arg2: (scope async)
 *
 *  After the request is initiated, the original part is truncated, and a new part is created.
 *   - the main callback is a reply to our request.
 *   - the second is an instance handler for the original part. It is called before the main callback. It doesnt appear to be very useful.
 */
void
am_part_split (AMPart* part, AyyiSongPos* _pos, AyyiHandler user_callback, gpointer user_data)
{
	void am_part__split_done (AyyiAction* action)
	{
		PF;
		PartCallbackData* d = action->app_data;

		if(d) call(d->user_callback, (AyyiIdent){action->obj.type, action->ret.idx}, &action->ret.error, d->user_data);

		if(action->ret.error){
			//let caller display the error
		}else{
			ayyi_log_print(LOG_OK, "part split");
		}

		am_parts__emit ("selection-change", NULL);

		AMPart* part = callback_part;
		g_return_if_fail(part);
		am_part_unref(part);
	}

	g_return_if_fail(part);

	int64_t mu = ayyi_pos2mu(_pos);

	char buff[32]; ayyi_pos2bbst(_pos, buff); dbg (0, "split_pos=%s mu=%Lu=0x%016Lx %isamples", buff, mu, mu, ayyi_pos2samples(_pos));

#ifdef DEBUG
	char start[32];
	char len[32];
	ayyi_pos2bbst(&part->start, start);
	ayyi_pos2bbst(&part->length, len);
	dbg(1, "part.start=%s part.length=%s", start, len);
#endif

	AyyiAction* a = part_action_new(part, user_callback, user_data, "region split. idx=%u", part->ayyi->shm_idx);
	a->callback = am_part__split_done;

	ayyi_set_obj(a, AYYI_OBJECT_PART, part->ayyi->shm_idx, AYYI_SPLIT, mu);
}


/**
 *  am_part_rename:
 *  @arg2: (scope async)
 */
void
am_part_rename (AMPart* part, const char* new_name, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(part);
	g_return_if_fail(new_name);
	ASSERT_SONG_SHM;
	PF;

	void am_part__rename_done (AyyiAction* action)
	{
		PF;
		PartCallbackData* d = action->app_data;
		AMPart* part = callback_part;
		g_return_if_fail(part);
		if(!am_part__is_deleted(part)){
			_am_part_sync_name(part, am_part_get_shared(part));
			g_signal_emit_by_name (song->parts, "item-changed", part, AM_CHANGE_NAME, NULL);
			call(d->user_callback, (AyyiIdent){action->obj.type, action->ret.idx}, &action->ret.error, d->user_data);
		}
		dbg (1, "new name=%s.", part->name);
		ayyi_log_print(0, "part rename");
		am_part_unref(part);
	}

	AyyiAction* a = part_action_new(part, callback, user_data, "part rename. '%s'", new_name);
	a->callback = am_part__rename_done;

	ayyi_set_string(a, part->ident.type, part->ayyi->shm_idx, new_name);
}


void
am_part_mute_toggle_async (AMPart* part)
{
	pwarn("not implemented in engine");

	if(!ayyi.got_shm) return;

	void
	part_mute_toggle_cb(AyyiAction* action)
	{
		dbg (0, "FIXME pobj_id=%i", callback_part->ayyi->shm_idx);

		if(action->ret.error){
			ayyi_log_print(0, "error. mute failed.");
			return;
		}

		ayyi_log_print(LOG_OK, "part muted ok.");

		am_part_unref(callback_part);
	}

	uint32_t state = am_part__is_muted(part);

	AyyiAction* a = part_action_new(part, NULL, NULL, "part mute (%s)", part->name);
	a->callback = part_mute_toggle_cb;

	ayyi_set_obj(a, part->ident.type, part->ayyi->shm_idx, AYYI_MUTE, state);
}


void
am_part_set_length (AMPart* part)
{
	g_return_if_fail(part);
	PF;

	void
	am_part__set_length_done(AyyiAction* action)
	{
		PF;

		AMPart* part = callback_part;
		g_return_if_fail(part);

		#if 0 //should be done in independent handler.
		g_signal_emit_by_name (am_parts, "item-changed", part, CHANGE_LEN, NULL);
		#endif

		am_part_unref(part);
	}

	AyyiSongPos max; am_part__max_length(part, &max);
	ayyi_pos_min(&part->length, &max);

	uint64_t mu = ayyi_pos2mu(&part->length);

	char bbst[32]; ayyi_pos2bbst(&part->length, bbst);

	if(mu > PART_MAX_MU){ perr ("part too long: %s (%"PRIu64").", bbst, mu); return; }

	AyyiAction* a   = part_action_new(part, NULL, NULL, "part set length (%s %Limu)", bbst, mu);
	a->callback     = am_part__set_length_done;
	a->obj.type     = part->ident.type;
	a->obj_idx.idx1 = part->ayyi->shm_idx;

	dbg (1, "idx=%x len=%s", part->ayyi->shm_idx, bbst);

	ayyi_set_length(a, mu);
}


void
am_part_set_level_async (AMPart* part, float level)
{
	void
	part_set_level_cb(AyyiAction* action)
	{
		g_return_if_fail(action);
	}

	AyyiAction* a  = ayyi_action_new("part_set_level (%.2f)", level);
	a->callback    = part_set_level_cb;

	ayyi_set_float(a, AYYI_OBJECT_PART, part->ayyi->shm_idx, AYYI_PB_LEVEL, level);
}


void
am_part_set_fadein_async (AMPart* part, int fade_time)
{
	void
	part_set_fadein_cb(AyyiAction* action)
	{
		g_signal_emit_by_name (am_parts, "item-changed", callback_part, AM_CHANGE_FADES, NULL);
		am_part_unref(callback_part);
	}

	AyyiAction* a = part_action_new(part, NULL, NULL, "part_set_fadein (%i)", fade_time);
	a->callback = part_set_fadein_cb;

	part_set_fadein_cb(a); // not implemented, return imediately
}


void
am_part_set_fadeout_async (AMPart* part, int fade_time)
{
	void
	part_set_fadeout_cb(AyyiAction* action)
	{
		g_signal_emit_by_name (am_parts, "item-changed", callback_part, AM_CHANGE_FADES, NULL);
		am_part_unref(callback_part);
	}

	AyyiAction* a = part_action_new(part, NULL, NULL, "part_set_fadeout (%i)", fade_time);
	a->callback = part_set_fadeout_cb;

	part_set_fadeout_cb(a); // not implemented, return imediately
}


/*
 *  Return the max allowable length for the given part.
 */
static void
am_part__max_length (AMPart* part, AyyiSongPos* max)
{
	g_return_if_fail(max);
	
	if(PART_IS_AUDIO(part)){
		uint32_t source_length = ((Waveform*)part->pool_item)->n_frames;
		ayyi_samples2pos(source_length - part->region_start, max);
	}else{
		*max = (AyyiSongPos){AM_MAX_PART_LENGTH_BEATS, 0, 0};
	}
}


void
am_part__end_pos (AMPart* part, AyyiSongPos* pos)
{
	g_return_if_fail(ayyi_pos_is_valid(&part->start));
	g_return_if_fail(ayyi_pos_is_valid(&part->length));

	*pos = part->start;

	ayyi_pos_add(pos, &part->length);
}


const char*
am_part_get_name (AMPart* part)
{
	g_return_val_if_fail(part, NULL);

	return part->name;
}


void
am_part__nudge_left (AMPart* part, bool snap, QMap* q_map)
{
	GPos start;
	songpos_ayyi2gui(&start, (AyyiSongPos*)&part->start);
	if(snap){
		q_to_prev(&start, q_map);
	}else{
		GPos nudge = {0, 1, 0};
		pos_subtract(&start, &nudge);
	}
	AyyiSongPos p; songpos_gui2ayyi(&p, &start);
	am_part_move((AMPart*)part, &p, part->track, NULL, NULL);
}


void
am_part__nudge_right (AMPart* part, bool snap, QMap* q_map)
{
	GPos start;
	songpos_ayyi2gui(&start, (AyyiSongPos*)&part->start);
	if(snap){
		q_to_next(&start, q_map);
#ifdef DEBUG
		char bbst1[64]; ayyi_pos2bbst(&part->start, bbst1);
		char bbst2[64]; pos2bbst(&start, bbst2);
		dbg(1, "snap=%i %s-->%s", snap, bbst1, bbst2);
#endif
	}else{
		pos_add(&start, &(GPos){0, 1, 0});
	}
	AyyiSongPos p; songpos_gui2ayyi(&p, &start);
	am_part_move((AMPart*)part, &p, part->track, NULL, NULL);
}


char*
am_part__get_length_bbst (AMPart* part, char* bbst)
{
	g_return_val_if_fail(part, NULL);

	AyyiRegionBase* shared;
	if((shared = am_part_get_shared(part))){
		ayyi_samples2bbst_0(shared->length, bbst);
	}
	return bbst;
}


/**
 *  am_part__set_colour
 *  @arg2: (scope async)
 */
void
am_part__set_colour (AMPart* part, unsigned cidx, AyyiHandler handler, gpointer user_data)
{
	void part_set_colour_done(AyyiAction* action)
	{
		PF;

		AMPart* part = callback_part;
		g_return_if_fail(part);

		part->bg_colour = GPOINTER_TO_INT(((PartCallbackData*)action->app_data)->track);
		if(!part->fg_is_set){
			//the value set here is a cache to save the gui having to calculate it each time.
			part->fg_colour = am_part__get_auto_fg_colour(part);
		}

		g_signal_emit_by_name (am_parts, "item-changed", part, AM_CHANGE_COLOUR, NULL);

		am_part_unref(part);
	}

	g_return_if_fail(part);

	if(cidx >= AM_MAX_COLOUR){
		pwarn ("colouridx out of range: %i", cidx);
		cidx = 0;
	}

	AyyiAction* a  = part_action_new(part, handler, user_data, "part_colour_set (%i)", cidx);
	a->callback    = part_set_colour_done;
	((PartCallbackData*)a->app_data)->track = GINT_TO_POINTER(cidx);

	// Ardour doesnt support region colour.
	part_set_colour_done(a); //FIXME needs to be piped through the messaging system else action will not be freed.
}


/**
 *  am_part__get_fg_colour: (skip)
 */
void
am_part__get_fg_colour (AMPart* part, char* rgb, Palette palette)
{
	//if the colour is not explicitly set, we use an automatically determined contrasting colour.

	//TODO change to take into account part->fg_is_set

	g_return_if_fail(part);

	if(part->fg_colour){
		sprintf(rgb, "#%06x", part->fg_colour >> 8);
	} else {
		uint32_t fg = am_palette_get_contrasting(palette, part->bg_colour) >> 8;
		sprintf(rgb, "#%06x", fg);
		dbg(3, "fg colour not set. contrasting=#%06x", fg);
	}
	dbg(3, "fg=%s", rgb);
}


static uint32_t
am_part__get_auto_fg_colour (AMPart* part)
{
	//if the colour is not explicitly set, we use an automatically determined contrasting colour.

	g_return_val_if_fail(part, 0);

	uint32_t fg = /*part->fg_colour
		? part->fg_colour
		: */am_palette_get_contrasting(song->palette, part->bg_colour);

	return fg;
}


void
am_part__set_fg_colour (AMPart* part, uint32_t colour)
{
	part->fg_colour = colour;
	part->fg_is_set = true;

	g_signal_emit_by_name (am_parts, "item-changed", part, AM_CHANGE_COLOUR, NULL);
}


bool
am_part__get_start_pos (AMPart* part, AMPos* pos)
{
	g_return_val_if_fail(pos, false);

	AyyiRegionBase* region = am_part_get_shared(part);
	g_return_val_if_fail(region, false);

	samples2pos(region->position/* + region->start*/, pos);

	return true;
}


float
am_part__get_level (AMPart* part)
{
	g_return_val_if_fail(part, 0.0);

	if(PART_IS_AUDIO(part)){
		AyyiRegion* region = ayyi_song__audio_region_at(part->ayyi->shm_idx);
		g_return_val_if_fail(region, 0.0);
		return region->level;
	}
	return 0.0;
}


uint32_t
am_part__get_fade_in (AMPart* part)
{
	g_return_val_if_fail(part, 0);
	if(!PART_IS_AUDIO(part)) return 0;

	AyyiRegion* region = (AyyiRegion*)part->ayyi;
	g_return_val_if_fail(region, 0);

	return region->fade_in_length;
}


uint32_t
am_part__get_fade_out (AMPart* part)
{
	g_return_val_if_fail(part, 0);
	if(!PART_IS_AUDIO(part)) return 0;

	AyyiRegion* region = (AyyiRegion*)part->ayyi;
	g_return_val_if_fail(region, 0);

	return region->fade_out_length;
}


bool
am_part__is_muted (AMPart* part)
{
	g_return_val_if_fail(part, false);
	PF;
	dbg(0, "FIXME add flags to midi_region");
	return 0;
	/*
	struct _AyyiRegionBase* region = PART_IS_AUDIO ? (struct _AyyiRegionBase*)region_pod(gpart->ayyi->shm_idx) : (struct _AyyiRegionBase*)midi_region_pod(gpart->ayyi->shm_idx);
	ASSERT_POINTER_FALSE(region, "region");
	return region->flags & 0x1;
	*/
}


bool
am_part__is_deleted (AMPart* part)
{
	g_return_val_if_fail(part, false);
	AyyiRegionBase* region = am_part_get_shared(part);
	g_return_val_if_fail(region, false);
	return region->flags & deleted;
}


/*
 *  If the start of Part is inbetween Start and End, it is returned, otherwise End is returned.
 *  -if End is NULL, Part is returned if it is later than Start.
 *  -if Start is NULL, Part is returned if it is early than End.
 *
 *  -used to find the next part after Start.
 */
const AMPart*
am_part__is_between (const AMPart* part, const AMPart* start, const AMPart* end)
{
	g_return_val_if_fail(part, NULL);

	AyyiSongPos start_pos = {0,};
	if(start) start_pos = start->start;

	if(!ayyi_pos_is_after(&part->start, &start_pos)) return end; // Part is too early.

	if(!end) return part;

	if(ayyi_pos_is_after(&end->start, &part->start)){
		return part; // yes, Part is inbetween start and end.
	}
	else{
		return end; // Part is too late.
	}
}


static AMTrack*
_am_part_get_ayyi_track (AMPart* part)
{
	g_return_val_if_fail(part->ayyi, NULL);

	AyyiRegionBase* region = am_part_get_shared(part);
	return am_song__get_track_for_part(region, am_part__get_media_type(part));
}


/*
 *  Completely syncs the local part to the remote object.
 */
bool
am_part__sync (AMPart* gpart, AyyiPropType prop)
{
	bool err = FALSE;

	g_return_val_if_fail(gpart->ident.type && (gpart->ident.type == AYYI_OBJECT_AUDIO_PART || gpart->ident.type == AYYI_OBJECT_MIDI_PART), false);
	AyyiRegionBase* part = am_part_get_shared(gpart);
	if(!part){ perr ("cannot get ayyi part. idx=%i", gpart->ident.idx); return !(err = TRUE); }
	if(part->flags & deleted){ dbg(0, "deleted! idx=%i", gpart->ident.idx); return false; }
	g_return_val_if_fail(PART_IS_AUDIO(gpart) ? ayyi_region__index_ok(part->shm_idx) : ayyi_song__midi_region_index_ok(part->shm_idx), false);
	if(!(part->id)){ perr ("illegal id 0. idx=%i", part->shm_idx); return false; }

	_am_part_sync_name(gpart, part);
	_am_part_sync_pos(gpart);

	if(PART_IS_AUDIO(gpart)){
		// inset
		AyyiRegion* audio_region = (AyyiRegion*)part;
		gpart->region_start = audio_region->start;
		dbg (2, "name='%s' id=%Lu position=%u region_start=%u", gpart->name, part->id, part->position, audio_region->start);
		if(gpart->region_start && gpart->pool_item){
			if(audio_region->start + audio_region->length > ((Waveform*)gpart->pool_item)->n_frames){
				perr ("bad inset! region_start=%u, len=%u n_samples=%"PRIi64, audio_region->start, audio_region->length, ((Waveform*)gpart->pool_item)->n_frames);
				gpart->region_start = 0;
				err = TRUE;
			}
		}

		gpart->pool_item = am_part__get_pool_item(gpart);
		// let the caller mark the part bad if neccesary
		if(!gpart->pool_item && gpart->status != PART_PENDING){ pwarn("part has no pool item. idx=%i", part->shm_idx); /*part->bad = TRUE;*/ return false; }
	}

	am_part__sync_length(gpart);

	if(prop == AYYI_NO_PROP || prop & AYYI_TRACK){
		AyyiPlaylist* playlist = ayyi_song__playlist_at(part->playlist);
		if(playlist){
			gpart->track = am_track_list_find_by_playlist(playlist, am_part__get_media_type(gpart));
			if(!gpart->track){
				perr ("cannot get track for part! part.idx=%i playlist=%s", gpart->ayyi->shm_idx, playlist->name);
				err = true;
			}
		} else {
			perr ("part has no playlist! idx=%i", gpart->ayyi->shm_idx);
			err = TRUE;
		}
	}

	return !err;
}


/*
 *  Initialise data for a part which is new to the gui but already exists in core.
 */
AMPart*
am_part_init_from_index (AMPart* part, AyyiIdx shm_index)
{
	part->ayyi = (AyyiRegionBase*)ayyi_song__audio_region_at(shm_index);
	g_return_val_if_fail(part->ayyi, NULL);
	part->ident.idx = shm_index;
	part->ident.type = AYYI_OBJECT_AUDIO_PART;

	if(!am_part__sync(part, AYYI_NO_PROP)){ perr ("init failed - aborting..."); return NULL; }

	if(part->length.beat > AM_MAX_PART_LENGTH_BEATS){
		pwarn ("part length too long (%i).", part->length.beat);
		part->length = (AyyiSongPos){100, 0, 0};
	}

	am_part_check_bounds(part);

	dbg (2, "'%s' shm_index=%u start=.", part->name, shm_index);
	return part;
}


static bool
_am_part_sync_name (AMPart* part, AyyiRegionBase* region)
{
	g_return_val_if_fail(part, false);

	g_strlcpy(part->name, region->name, AYYI_NAME_MAX);

	return true;
}


/*
 *  Update the AMPart object with the current server value for the part start-time
 */
static bool
_am_part_sync_pos (AMPart* part)
{
	g_return_val_if_fail(part, false);

	AyyiAudioRegion* region = (AyyiAudioRegion*)(am_part_get_shared(part));
	g_return_val_if_fail(region, false);

	AyyiNFrames samples = region->position;
	dbg (2, "part->start=%u (position=%u start(offset)=%u)", samples, region->position, region->start);
	ayyi_samples2pos(samples, &part->start);

	return true;
}


static bool
am_part_check_bounds (AMPart* part)
{
	g_return_val_if_fail(part, false);
	if(!PART_IS_AUDIO(part)) return FALSE;
	g_return_val_if_fail(part->pool_item, false);
	if(!PART_IS_AUDIO(part)) return TRUE;

	AyyiRegion* shared = ayyi_song__audio_region_at(part->ayyi->shm_idx);
	if(shared){
		uint32_t source_length = ((Waveform*)part->pool_item)->n_frames;
		if(source_length && (shared->length > source_length)){ pwarn ("region is too long for the source! %i '%s', region_length=%u source_length=%u", shared->shm_idx, shared->name, shared->length, source_length); return FALSE; }

		return TRUE;
	}

	return FALSE;
}


void
am_part__sync_length (AMPart* part)
{
	g_return_if_fail(part);

	AyyiRegionBase* region = am_part_get_shared(part);
	g_return_if_fail(region);

	// ensure that because of the loss of precision here, we dont set zero length
	GPos min = {0, 0, 1};
	ayyi_samples2pos(MAX(pos2samples(&min), region->length), &part->length);
}


static AMPoolItem*
am_part__get_pool_item (AMPart* part)
{
	g_return_val_if_fail(part, NULL);

	AyyiAudioRegion* region = (AyyiAudioRegion*)part->ayyi;//ayyi_song__audio_region_at(part->ayyi->shm_idx);
	g_return_val_if_fail(region, NULL);

	uint64_t source_id = region->source0;

	return am_pool__get_item_by_id(source_id);
}


/**
 *  am_part__set_note_selection
 *  @arg1: (element-type AyyiMidiNote)
 */
void
am_part__set_note_selection (AMPart* part, GList* selection)
{
	dbg(2, "note=%i", selection ? ((AyyiMidiNote*)selection->data)->note : -1);

	MidiPart* mpart = (MidiPart*)part;
	if(mpart->note_selection) g_list_free(mpart->note_selection);
	mpart->note_selection = selection;
}


bool
am_part__is_audio (void* _part, gpointer user_data)
{
	AMPart* part = _part;
	return PART_IS_AUDIO(part);
}


bool
am_part__is_midi (void* part, gpointer user_data)
{
	return PART_IS_MIDI((AMPart*)part);
}


bool
am_part__is_at_position (AMPart* part, gpointer _pos)
{
	// perhaps should use the ayyi part start instead

	AyyiSongPos* pos = _pos;
	return !ayyi_pos_cmp(pos, &part->start);
}


bool
am_part__is_at_position_samples (AMPart* part, gpointer _pos)
{
	int64_t pos1 = GPOINTER_TO_INT(_pos);
	int64_t pos2 = ayyi_pos2samples(&part->start);

	return ABS(pos1 - pos2) < 2;
}


bool
am_part__has_pool_item (AMPart* part, gpointer _pool_item)
{
	AMPoolItem* pool_item = _pool_item;
	return part->pool_item == pool_item;
}


bool
am_part__has_track (AMPart* part, AMTrack* track)
{
	g_return_val_if_fail(part, false);

	return part->track == track;
}


bool
am_part__in_track_segment (AMPart* part, AMTrackSegment* ts)
{
	g_return_val_if_fail(part, false);

	if(part->track != ts->track) return false;

	uint64_t seg_start = ayyi_pos2mu(ts->start);
	uint64_t seg_end   = ayyi_pos2mu(ts->end);

	uint64_t part_start = ayyi_pos2mu(&part->start);
	uint64_t part_end   = ayyi_pos2mu(&part->length) + part_start;

	return part_end > seg_start && part_start < seg_end;
}


bool
am_part__has_id (AMPart* part, uint64_t* id)
{
	g_return_val_if_fail(part && id, false);

	return part->ayyi->id == *id;
}


void
am_part__on_palette_change (AMPart* part)
{
	if(!part->fg_is_set){
		part->fg_colour = am_part__get_auto_fg_colour(part);
	}
}


void
am_part__print (AMPart* part)
{
	g_return_if_fail(part);

	char bbst1[64], bbst2[64];
	ayyi_pos2bbst(&part->start, bbst1);
	ayyi_pos2bbst(&part->length, bbst2);
	uint64_t id = 0;
	char* source = part->pool_item ? part->pool_item->leafname : "";
	AyyiRegionBase* region = part->ayyi;
	if(region){
		id = region->id;
	}
	printf("%s(): %02i start=%s length=%s id=%"PRIi64" pool_item=%s %s%s%s\n", __func__, part->ident.idx, bbst1, bbst2, id, source, bold, part->name, white);
}


const char*
am_part__print_status (AMPartStatus status)
{
	static char unk[32];

	switch (status){
#define CASE(x) case PART_##x: return "PART_"#x
		CASE (OK);
		CASE (LOCAL);
		CASE (PENDING);
		CASE (BAD);
#undef CASE
		default:
			snprintf (unk, 31, "UNKNOWN PART STATUS (%d)", status);
			return unk;
	}
	return NULL;
}


