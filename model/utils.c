/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "common.h"
#include <math.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <dirent.h>
#include <fcntl.h>
#include <sndfile.h>
#include <glib/gstdio.h>
#include "model/song.h"
#include "model/utils.h"


float
am_db2gain (float val)
{
	return (pow(10.0, val / 20.0));
}


float
am_gain2db (float gain)
{
	return 20.0 * log10 (gain);
}


/**
 * Given a string, replaces all instances of "oldpiece" with "newpiece".
 *
 * Modified to eliminate recursion and to avoid infinite
 * expansion of string when newpiece contains oldpiece.
 */
char*
replace (const char* string, char* oldpiece, char* newpiece)
{
  int str_index, newstr_index, cpy_len;
  char* c;
  static char newstring[256];

  if((c = (char *) strstr(string, oldpiece)) == NULL) return (char*)string;

  int new_len        = strlen(newpiece);
  int old_len        = strlen(oldpiece);
  int end            = strlen(string)   - old_len;
  int oldpiece_index = c - string;

  newstr_index = 0;
  str_index = 0;
  while(str_index <= end && c != NULL){
    //copy characters from the left of matched pattern occurence
    cpy_len = oldpiece_index-str_index;
    strncpy(newstring+newstr_index, string+str_index, cpy_len);
    newstr_index += cpy_len;
    str_index    += cpy_len;

    //copy replacement characters instead of matched pattern
    strcpy(newstring+newstr_index, newpiece);
    newstr_index += new_len;
    str_index    += old_len;

    //check for another pattern match
    if((c = (char *) strstr(string+str_index, oldpiece)) != NULL) oldpiece_index = c - string;
  }

  //copy remaining characters from the right of last matched pattern
  strcpy(newstring+newstr_index, string+str_index);

  return newstring;
}


/*
 *  Perform a string replacement in-place.
 *  @param string must be allocated with 256 chars. Modified string may be longer than the original.
 */
void
replace2 (char* string, char* oldpiece, char* newpiece)
{
  int str_index, newstr_index, oldpiece_index, end, new_len, old_len, cpy_len;
  char* c;
  static char newstring[256];

  if((c = (char *) strstr(string, oldpiece)) == NULL) return;

  new_len        = strlen(newpiece);
  old_len        = strlen(oldpiece);
  end            = strlen(string)   - old_len;
  oldpiece_index = c - string;

  newstr_index = 0;
  str_index = 0;
  while(str_index <= end && c != NULL){
    //copy characters from the left of matched pattern occurence
    cpy_len = oldpiece_index-str_index;
    strncpy(newstring+newstr_index, string+str_index, cpy_len);
    newstr_index += cpy_len;
    str_index    += cpy_len;

    //copy replacement characters instead of matched pattern
    strcpy(newstring+newstr_index, newpiece);
    newstr_index += new_len;
    str_index    += old_len;

    //check for another pattern match
    if((c = (char *) strstr(string+str_index, oldpiece)) != NULL) oldpiece_index = c - string;
  }

  //copy remaining characters from the right of last matched pattern
  strcpy(newstring+newstr_index, string+str_index);
  strcpy(string, newstring);
}


/*
 *  Returns a newly allocated string
 */
gchar*
str_replace (const gchar* string, const gchar* search, const gchar* replacement)
{
	gchar *str, **arr;

	g_return_val_if_fail (string != NULL, NULL);
	g_return_val_if_fail (search != NULL, NULL);

	if (replacement == NULL) replacement = "";

	arr = g_strsplit (string, search, -1);
	if (arr != NULL && arr[0] != NULL)
		str = g_strjoinv (replacement, arr);
	else
		str = g_strdup (string);

	g_strfreev (arr);
	
	return str;
}


void
escape_for_menu (char* string)
{
	//@string must have an allocation greater than its length. Is assumed to have allocation of 256.
	replace2(string, "_", "__");
}


/*
 *  Gives a full path string for the given file.
 *  -if the given path is relative, it will have the audio home dir prepended to it.
 *  -if the path is already full, it will be unchanged.
 *  Result must be free'd with g_free().
 */
char*
am_audio_path_get_full (const char* path)
{
	//is it full already?
	if(g_path_is_absolute(path)) return g_strdup(path);

	return g_build_filename(song->audiodir, path, NULL);
}


char*
am_format_channel_width (unsigned width)
{
	static char str[3][64] = {"", "mono", "stereo"};
	if(width < 3) return str[width];
	else snprintf(str[0], 63, "channels=%u", width);
	return str[0];
}


/*
 *  Return TRUE if the object exists, FALSE if it doesn't.
 *  For symlinks, the file pointed to must exist.
 */
gboolean
file_exists (const char *path)
{
    struct stat info;

    return !stat(path, &info);
}


void
filename_change_extension (char* path, const char* extn)
{
	g_return_if_fail(path);

	char* dot = strrchr(path, '.');
	if(dot){
		strcpy(dot + 1, extn);
	}else{
		dbg(2, "no file extension found. appending...");
		sprintf(path + strlen(path), ".%s", extn);
	}
}


void
filename_get_extension (const char* path, char* extn)
{
	// extn must be less than 8 chars in length

	g_return_if_fail(path);
	g_return_if_fail(extn);

    gchar** split = g_strsplit(path, ".", 0);
    if(split[0]){
        int i = 1;
        while(split[i]) i++;
        strncpy(extn, split[i - 1], 7);

    }else{
        memset(extn, '\0', 1);
    }

    g_strfreev(split);
}


void
filename_remove_extension (const char* filename, char* truncated)
{
	g_return_if_fail(filename);
	g_return_if_fail(truncated);

	strcpy(truncated, filename);

	char* dot = strrchr(truncated, '.');
	if(dot) *dot = '\0';
}


char*
filename_get_stereo (const char* filename)
{
	// change file.wav to file-L.wav
	// (we assume the extension, if any, is ".wav")

	char* ext = g_strrstr(filename, ".");
	if(!ext) return g_strdup_printf("%s-L", filename);

	char* trunc = g_strndup(filename, ext - filename);

	//if we already have a R extension, remove it.
	gchar* r = g_strrstr(trunc, "-R");
	if(r == trunc + strlen(trunc) -2){
		dbg(2, "     is R");
		gchar* t2 = g_strndup(trunc, strlen(trunc) -2);
		g_free(trunc);
		trunc = t2;
	}
	else dbg(2, "    no. %p", r);

	char* stereo = g_strdup_printf("%s-L.wav", trunc);
	dbg(2, "%s", stereo);
	g_free(trunc);
	return stereo;
}


gboolean
am_file_is_newer (const char* file1, const char* file2)
{
	//return TRUE if file1 date is newer than file2 date.

	struct stat info1;
	struct stat info2;
	if(stat(file1, &info1)) return false;
	if(stat(file2, &info2)) return false;
	if(info1.st_mtime < info2.st_mtime) dbg(0, "%i %i %i", info1.st_mtime, info2.st_mtime, sizeof(time_t));
	return (info1.st_mtime > info2.st_mtime);
}


bool
file_delete_recursive (const char* path)
{
	//TODO test with symlinks.

	g_return_val_if_fail(path && strlen(path), false);
	g_return_val_if_fail(strcmp(path, "/"), false);

	static int max = 48;
	static int n; n = 0;
	static bool test = false;

	bool rm_rf(const char* path)
	{
		bool remove_file(const char* path)
		{
			dbg(2, "%i", n);
			if(!test){
				if(n++ < max){
					if(remove(path) < 0){
						pwarn("cannot delete: %s", path);
						return false;
					}
				}
				else{
					pwarn("max files exceeded (%i).", max);
					return false;
				}
			}
			else dbg(2, "delete: %s", path);
			return true;
		}

		if(g_file_test(path, G_FILE_TEST_IS_DIR)){
			bool ok = true;
			GError** error = NULL;
			GDir* d = g_dir_open(path, 0, error);
			if(d){
				const gchar* name;
				while(ok && (name = g_dir_read_name(d))){
					gchar* p = g_build_filename(path, name, NULL);
					dbg(2, "  %s", p);
					if(g_file_test(p, G_FILE_TEST_IS_DIR)){
						ok = rm_rf(p);
					}else{
						ok = remove_file(p);
					}
					g_free(p);
				}
				g_dir_close(d);
				dbg(2, "dir close. ok=%i", ok);
				if(!ok) return false;
			}
			if(!remove_file(path)) return false;
			dbg(0, "directory removed: %s", path);
		}
		return true;
	}
	return rm_rf(path);
}


/*
 *  Check that a foreign file is of a type that libsndfile can deal with.
 */
static bool
am_sndfile_is_readable (const char* filename)
{
	SF_INFO sfinfo = {0,};
	SNDFILE* sffile;

	if(!(sffile = sf_open(filename, SFM_READ, &sfinfo))){
		ayyi_log_print(LOG_FAIL, "not able to open file");
		dbg (0, "not able to open input file %s.", filename);
		puts(sf_strerror(NULL));    // print the error message from libsndfile:
		return FALSE;
	}
	if(sf_close(sffile)) pwarn ("bad file close.");

	return true;
}


/*
 *  Check that a foreign file is of a type we can deal with, ie it is supported by libsndfile.
 *  If the file is "raw" format, we just check that it exists.
 */
bool
am_audiofile_is_readable (const char* filename)
{
	char extn[8];
	filename_get_extension(filename, extn);
	if(!strcmp(extn, "raw")){
		return file_exists(filename);
	}

	return am_sndfile_is_readable(filename);
}


#ifdef DEBUG
const char*
am_print_change (AMChangeType change)
{
	static char str[256] = {0,};

	if(change == CHANGE_ALL){
		g_strlcpy(str, "ALL", 256);
		return str;
	}

	const char* a[] = {
		"POS_X",
		"LEN",
		"INSET",
		"WIDTH",
		"HEIGHT",
		"OUTPUT",
		"FADES",
		"NAME",
		"TRACK",
		"ZOOM_H",
		"ZOOM_V",
		"COLOUR",
		"ARMED",
		"MUTE",
		"SOLO",
		"SELECTION",
		"BACKGROUND_COLOUR",
		"IDX",
		"POS_Y",
		"VIEWPORT_Y",
  		"SHOW_PART_CONTENTS",
		"SHOW_SONG_OVERVIEW",
	};

	char* p = str;
	int i; for(i=0;i<G_N_ELEMENTS(a);i++){
		if(change & 1 << i){
			strcpy(p, a[i]);
			p += strlen(a[i]);
			strcpy(p++, " ");
		}
	}
	p = '\0';
	return str;
}
#endif


#if 0
void
am_do_series_(Fn functions[], int n_functions, AyyiCallback finished)
{
	//like do_series but takes array instead of pointer array.

	typedef struct _c
	{
		AyyiCallback finished;
	} C;
	C* c = g_new0(C, 1);
	c->finished = finished;

	void series_done(gpointer user_data)
	{
		C* c = user_data;
		call(c->finished, NULL);
		g_free(c);
	}

	Fn* fns = g_malloc0(sizeof(Fn) * (n_functions + 1));
	int i; for(i=0;i<n_functions;i++){
		fns[i] = functions[i];
	}
	am_do_series(fns, series_done, c);
}


void
am_do_series (Fn* functions, AyyiCallback finished, gpointer user_data)
{
	//functions is a null terminated array of function pointers.

	void do_next(DoClosure* c)
	{
		Fn fn = c->functions[c->i];

		if(!fn){
			g_free(c);
			c->finished(c->user_data);
			return;
		}

		c->i++;

		fn(c);
	}

	DoClosure* c = g_new0(DoClosure, 1);
	c->functions = functions;
	c->finished = finished;
	c->user_data = user_data;
	c->next = (Fn)do_next;

	do_next(c);
}
#endif


void
am_object_init (AMObject* object, int n_properties)
{
	object->vals = g_malloc0((n_properties + 1) * sizeof(struct _am_val));
}


void
am_object_deinit (AMObject* object)
{
	g_free(object->vals);
}

