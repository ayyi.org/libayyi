/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <glib.h>
#include <glib-object.h>
#include <gobject/gvaluecollector.h>
#include "model/ayyi_model.h"
#include "am_collection.h"

#define _g_list_free0(var) ((var == NULL) ? NULL : (var = (g_list_free (var), NULL)))
#define _g_ptr_array_free0(var) ((var == NULL) ? NULL : (var = (g_ptr_array_free (var, TRUE), NULL)))

typedef struct _ParamSpecFilterIterator ParamSpecFilterIterator;

struct _AMCollectionPrivate {
	GType t_type;
	GBoxedCopyFunc t_dup_func;
	GDestroyNotify t_destroy_func;
};

struct _AMArrayPrivate {
	GType t_type;
	GBoxedCopyFunc t_dup_func;
	GDestroyNotify t_destroy_func;
};

struct _AMListPrivate {
	GType t_type;
	GBoxedCopyFunc t_dup_func;
	GDestroyNotify t_destroy_func;
};

struct _FilterIteratorPrivate {
	GType t_type;
	GBoxedCopyFunc t_dup_func;
	GDestroyNotify t_destroy_func;
};

struct _ParamSpecFilterIterator {
	GParamSpec parent_instance;
};


G_DEFINE_TYPE_WITH_PRIVATE (AMCollection, am_collection, G_TYPE_OBJECT)
enum  {
	AM_COLLECTION_DUMMY_PROPERTY,
	AM_COLLECTION_T_TYPE,
	AM_COLLECTION_T_DUP_FUNC,
	AM_COLLECTION_T_DESTROY_FUNC
};

static guint    am_collection_real_length     (AMCollection*);
static bool     am_collection_real_contains   (AMCollection*, gconstpointer);
static const AMItem* am_collection_real_find_by_id (AMCollection*, guint64);
static gpointer am_collection_real_find_by_idx(AMCollection*, AyyiIdx);
static gpointer am_collection_real_append     (AMCollection*, gconstpointer);
static void     am_collection_real_remove     (AMCollection*, gconstpointer);
static void     am_collection_real_iter_init  (AMCollection*, AMIter*);
static void*    am_collection_real_iter_next  (AMCollection*, AMIter*);

static void     g_cclosure_user_marshal_VOID__POINTER_UINT_POINTER (GClosure*, GValue* return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data);

static void     am_collection_finalize        (GObject*);
static void     am_collection_get_property    (GObject*, guint property_id, GValue*, GParamSpec*);
static void     am_collection_set_property    (GObject*, guint property_id, const GValue*, GParamSpec*);

G_DEFINE_TYPE_WITH_PRIVATE (AMArray, am_array, AM_TYPE_COLLECTION)
enum  {
	AM_ARRAY_DUMMY_PROPERTY,
	AM_ARRAY_T_TYPE,
	AM_ARRAY_T_DUP_FUNC,
	AM_ARRAY_T_DESTROY_FUNC
};
static guint    am_array_real_length             (AMCollection*);
static bool     am_array_real_contains           (AMCollection*, gconstpointer item);
static const AMItem* am_array_real_find_by_id    (AMCollection*, guint64 id);
static gpointer am_array_real_append             (AMCollection*, gconstpointer item);
static void     am_array_real_remove             (AMCollection*, gconstpointer item);
static void     am_array_real_iter_init          (AMCollection*, AMIter*);
static gconstpointer* am_array_real_iter_next    (AMCollection*, AMIter*);
static void     am_array_finalize                (GObject*);
static void     am_array_get_property            (GObject*, guint property_id, GValue*, GParamSpec*);
static void     am_array_set_property            (GObject*, guint property_id, const GValue*, GParamSpec*);

G_DEFINE_TYPE_WITH_PRIVATE (AMList, am_list, AM_TYPE_COLLECTION)
enum  {
	AM_LIST_DUMMY_PROPERTY,
	AM_LIST_T_TYPE,
	AM_LIST_T_DUP_FUNC,
	AM_LIST_T_DESTROY_FUNC
};
static guint    am_list_real_length              (AMCollection*);
static bool     am_list_real_contains            (AMCollection*, gconstpointer item);
static const AMItem* am_list_real_find_by_id     (AMCollection*, guint64 id);
static gpointer am_list_real_find_by_idx         (AMCollection*, AyyiIdx);
static gpointer am_list_real_append              (AMCollection*, gconstpointer item);
static void     am_list_real_remove              (AMCollection*, gconstpointer item);
static void     am_list_real_iter_init           (AMCollection*, AMIter*);
static gconstpointer* am_list_real_iter_next     (AMCollection*, AMIter*);
static gboolean _ident_equal                     (const AyyiIdent*, const AyyiIdent*);
static void     am_list_finalize                 (GObject*j);
static void     am_list_get_property             (GObject*, guint property_id, GValue*, GParamSpec*);
static void     am_list_set_property             (GObject*, guint property_id, const GValue*, GParamSpec*);

static gpointer filter_iterator_parent_class = NULL;
#define _G_TYPE_INSTANCE_GET_PRIVATE(instance, g_type, c_type) \
       ((c_type*) g_type_instance_get_private ((GTypeInstance*) (instance), (g_type)))
#define FILTER_ITERATOR_GET_PRIVATE(o) (_G_TYPE_INSTANCE_GET_PRIVATE ((o), TYPE_FILTER_ITERATOR, FilterIteratorPrivate))
enum  {
	FILTER_ITERATOR_DUMMY_PROPERTY
};
static void filter_iterator_finalize (FilterIterator* obj);


static guint
am_collection_real_length (AMCollection* self)
{
	g_return_val_if_fail (self != NULL, 0U);
	g_critical ("Type `%s' does not implement abstract method `am_collection_length'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
	return 0U;
}


guint
am_collection_length (AMCollection* self)
{
	return AM_COLLECTION_GET_CLASS (self)->length (self);
}


static bool
am_collection_real_contains (AMCollection* self, gconstpointer item)
{
	g_return_val_if_fail (self != NULL, FALSE);
	g_critical ("Type `%s' does not implement abstract method `am_collection_contains'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
	return FALSE;
}


bool
am_collection_contains (AMCollection* self, gconstpointer item)
{
	return AM_COLLECTION_GET_CLASS (self)->contains (self, item);
}


static const AMItem*
am_collection_real_find_by_id (AMCollection* self, guint64 id)
{
	g_return_val_if_fail (self, NULL);
	g_critical ("Type `%s' does not implement abstract method `am_collection_find_by_id'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
	return NULL;
}


static gpointer
am_collection_real_find_by_idx (AMCollection* self, AyyiIdx idx)
{
	g_return_val_if_fail (self, NULL);
	g_critical ("Type `%s' does not implement abstract method `am_collection_find_by_idx'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
	return NULL;
}


const AMItem*
am_collection_find_by_id (AMCollection* self, guint64 id)
{
	return AM_COLLECTION_GET_CLASS (self)->find_by_id (self, id);
}


gpointer
am_collection_find_by_idx (AMCollection* self, AyyiIdx idx)
{
	return AM_COLLECTION_GET_CLASS (self)->find_by_idx (self, idx);
}


static gpointer
am_collection_real_append (AMCollection* self, gconstpointer item)
{
	g_return_val_if_fail (self, NULL);
	g_critical ("Type `%s' does not implement abstract method `am_collection_append'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
	return NULL;
}


gpointer
am_collection_append (AMCollection* self, gconstpointer item)
{
	return AM_COLLECTION_GET_CLASS (self)->append (self, item);
}


static void
am_collection_real_remove (AMCollection* self, gconstpointer item)
{
	g_return_if_fail (self != NULL);
	g_critical ("Type `%s' does not implement abstract method `am_collection_remove'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
	return;
}


void
am_collection_remove (AMCollection* self, gconstpointer item)
{
	AM_COLLECTION_GET_CLASS (self)->remove (self, item);
}


static void
am_collection_real_iter_init (AMCollection* self, AMIter* iter)
{
	g_return_if_fail (self);
	g_critical ("Type `%s' does not implement abstract method `am_collection_iter_init'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
}


void
am_collection_iter_init (AMCollection* self, AMIter* iter)
{
	AM_COLLECTION_GET_CLASS (self)->iter_init (self, iter);
}


static void*
am_collection_real_iter_next (AMCollection* self, AMIter* iter)
{
	g_return_val_if_fail (self != NULL, NULL);
	g_critical ("Type `%s' does not implement abstract method `am_collection_iter_next'", g_type_name (G_TYPE_FROM_INSTANCE (self)));
	return NULL;
}


void*
am_collection_iter_next (AMCollection* self, AMIter* iter)
{
	return AM_COLLECTION_GET_CLASS (self)->iter_next (self, iter);
}


void
am_collection_selection_reset (AMCollection* self)
{
	g_return_if_fail (self);

	while (TRUE) {
		guint l = g_list_length (self->selection);
		if (!l) {
			break;
		}
		self->selection = g_list_delete_link (self->selection, self->selection);
	}
	observable_set(self->selection2, (AMVal){.p = self->selection});
	g_signal_emit_by_name (self, "selection-change", NULL);
}


/**
 * am_collection_selection_replace
 * @arg1: (element-type gpointer): ownership of this list is taken. The caller must not free it, and if necesary must duplicate it before passing in.
 *
 * Clear any existing part selections, and replace them with new ones.
 */
void
am_collection_selection_replace (AMCollection* collection, GList* items, void* sender)
{
	g_return_if_fail (collection);

	if (song->loaded) {
		bool changed = false;
		GList* l1 = collection->selection;
		GList* l2 = items;
		for(; l1 || l2; l1 = l1->next, l2 = l2->next){
			if(!l1 || !l2){
				changed = true;
				break;
			}
			if(l1->data != l2->data)
				changed = true;
			if(l1->next != l2->next)
				changed = true;
		}
		if(!changed) return;

		g_clear_pointer(&collection->selection, g_list_free);
		collection->selection = items;

		observable_set (collection->selection2, (AMVal){.p = collection->selection});
		g_signal_emit_by_name (collection, "selection-change", sender);
	}
}


/**
 * am_collection_selection_add
 * @arg1: (element-type gpointer)
 */
void
am_collection_selection_add (AMCollection* self, GList* items, void* sender)
{
	g_return_if_fail (self);

	for(GList* l=items;l;l=l->next){
		if(g_list_find (self->selection, l->data)){
			g_print ("*** already in selection list.\n");
		} else {
			self->selection = g_list_prepend (self->selection, l->data);
		}
	}

	if (song->loaded) {
		observable_set(self->selection2, (AMVal){.p = self->selection});
		g_signal_emit_by_name (self, "selection-change", sender);
	}
}


void
am_collection_selection_remove (AMCollection* self, gconstpointer item, void* sender)
{
	g_return_if_fail (self != NULL);

	self->selection = g_list_remove (self->selection, item);
	if (song->loaded) {
		observable_set(self->selection2, (AMVal){.p = self->selection});
		g_signal_emit_by_name (self, "selection-change", sender);
	}
}


gpointer
am_collection_selection_first (AMCollection* self)
{
	g_return_val_if_fail (self, NULL);

	GList* _first = g_list_first (self->selection);
	gconstpointer* first = _first ? _first->data : NULL;
	return (!first || !self->priv->t_dup_func)
		? ((gpointer)first)
		: self->priv->t_dup_func((gpointer)first);
}


/**
 *  am_collection_construct
 */
AMCollection*
am_collection_construct (GType object_type, GType t_type)
{
	GBoxedCopyFunc t_dup_func = NULL;
	GDestroyNotify t_destroy_func = NULL;

	AMCollection* self = (AMCollection*) g_object_new (object_type, NULL);
	self->selection2 = observable_new();

	self->priv->t_type = t_type;
	self->priv->t_dup_func = t_dup_func;
	self->priv->t_destroy_func = t_destroy_func;

	return self;
}


static void
g_cclosure_user_marshal_VOID__POINTER_UINT_POINTER (GClosure* closure, GValue* return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	typedef void (*GMarshalFunc_VOID__POINTER_UINT_POINTER) (gpointer data1, gpointer arg_1, guint arg_2, gpointer arg_3, gpointer data2);
	register GMarshalFunc_VOID__POINTER_UINT_POINTER callback;
	register gpointer data1, data2;
	register GCClosure* cc = (GCClosure*)closure;

	g_return_if_fail (n_param_values == 4);

	if (G_CCLOSURE_SWAP_DATA (closure)) {
		data1 = closure->data;
		data2 = param_values->data[0].v_pointer;
	} else {
		data1 = param_values->data[0].v_pointer;
		data2 = closure->data;
	}
	callback = (GMarshalFunc_VOID__POINTER_UINT_POINTER) (marshal_data ? marshal_data : cc->callback);
	callback (data1, g_value_get_pointer (param_values + 1), g_value_get_uint (param_values + 2), g_value_get_pointer (param_values + 3), data2);
}


static void
am_collection_class_init (AMCollectionClass* klass)
{
	am_collection_parent_class = g_type_class_peek_parent (klass);

	AM_COLLECTION_CLASS (klass)->length = am_collection_real_length;
	AM_COLLECTION_CLASS (klass)->contains = am_collection_real_contains;
	AM_COLLECTION_CLASS (klass)->find_by_id = am_collection_real_find_by_id;
	AM_COLLECTION_CLASS (klass)->find_by_idx = am_collection_real_find_by_idx;
	AM_COLLECTION_CLASS (klass)->append = am_collection_real_append;
	AM_COLLECTION_CLASS (klass)->remove = am_collection_real_remove;
	AM_COLLECTION_CLASS (klass)->iter_init = am_collection_real_iter_init;
	AM_COLLECTION_CLASS (klass)->iter_next = am_collection_real_iter_next;

	G_OBJECT_CLASS (klass)->get_property = am_collection_get_property;
	G_OBJECT_CLASS (klass)->set_property = am_collection_set_property;
	G_OBJECT_CLASS (klass)->finalize = am_collection_finalize;

	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_COLLECTION_T_TYPE, g_param_spec_gtype ("t-type", "type", "type", G_TYPE_NONE, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_COLLECTION_T_DUP_FUNC, g_param_spec_pointer ("t-dup-func", "dup func", "dup func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_COLLECTION_T_DESTROY_FUNC, g_param_spec_pointer ("t-destroy-func", "destroy func", "destroy func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

	g_signal_new ("add", AM_TYPE_COLLECTION, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
	g_signal_new ("delete", AM_TYPE_COLLECTION, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
	g_signal_new ("change", AM_TYPE_COLLECTION, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("item_changed", AM_TYPE_COLLECTION, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_user_marshal_VOID__POINTER_UINT_POINTER, G_TYPE_NONE, 3, G_TYPE_POINTER, G_TYPE_UINT, G_TYPE_POINTER);
	g_signal_new ("selection_change", AM_TYPE_COLLECTION, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
}


static void
am_collection_init (AMCollection* self)
{
	self->priv = am_collection_get_instance_private(self);
}


static void
am_collection_finalize (GObject* obj)
{
	// user_data is not freed here, it must be cleared by the user

	AMCollection* self = AM_COLLECTION(obj);

	_g_list_free0 (self->selection);
	_g_list_free0 (self->pending);
	observable_free (self->selection2);

	G_OBJECT_CLASS (am_collection_parent_class)->finalize (obj);
}


static void
am_collection_get_property (GObject* object, guint property_id, GValue* value, GParamSpec* pspec)
{
	switch (property_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}


static void
am_collection_set_property (GObject* object, guint property_id, const GValue* value, GParamSpec* pspec)
{
	AMCollection* self = AM_COLLECTION(object);

	switch (property_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
		case AM_COLLECTION_T_TYPE:
			self->priv->t_type = g_value_get_gtype (value);
			break;
		case AM_COLLECTION_T_DUP_FUNC:
			self->priv->t_dup_func = g_value_get_pointer (value);
			break;
		case AM_COLLECTION_T_DESTROY_FUNC:
			self->priv->t_destroy_func = g_value_get_pointer (value);
			break;
	}
}


/**
 *  am_array_construct: (constructor)
 */
AMArray*
am_array_construct (GType object_type, GType t_type, GBoxedCopyFunc t_dup_func, GDestroyNotify t_destroy_func, gint n)
{
	AMArray* self = (AMArray*) am_collection_construct (object_type, t_type);

	_g_ptr_array_free0 (self->array);

	self->priv->t_type = t_type;
	self->priv->t_dup_func = t_dup_func;
	self->priv->t_destroy_func = t_destroy_func;
	self->array = g_ptr_array_sized_new((guint)n);

	return self;
}


AMArray*
am_array_new (GType t_type, GBoxedCopyFunc t_dup_func, GDestroyNotify t_destroy_func, gint n)
{
	return am_array_construct (AM_TYPE_ARRAY, t_type, t_dup_func, t_destroy_func, n);
}


gconstpointer*
am_array_at (AMArray* self, gint index)
{
	g_return_val_if_fail(self, NULL);
	return g_ptr_array_index (self->array, (guint)index);
}


static guint
am_array_real_length (AMCollection* base)
{
	AMArray* self = (AMArray*)base;
	return self->array->len;
}


static bool
am_array_real_contains (AMCollection* base, gconstpointer item)
{
	return false;
}


gpointer
am_array_find_by_idx (AMCollection* base, AyyiIdx idx)
{
	GPtrArray* array = ((AMArray*)base)->array;
	for(int i=0;i<array->len;i++){
		AMItem* item = g_ptr_array_index(array, i);
		if (item->ident.idx == idx) return item;
	}
	return NULL;
}

static const AMItem*
am_array_real_find_by_id (AMCollection* base, guint64 id)
{
	return NULL;
}


static gpointer
am_array_real_append (AMCollection* base, gconstpointer item)
{
	g_ptr_array_add(((AMArray*)base)->array, (void*)item);
	return (gpointer)item;
}


static void
am_array_real_remove (AMCollection* base, gconstpointer item)
{
}


static void
am_array_real_iter_init (AMCollection* base, AMIter* iter)
{
	AMArray* self = (AMArray*)base;

	(*iter).i = 0;
	(*iter).ptr = NULL;
	(*iter).array = self->array;
}


static gconstpointer*
am_array_real_iter_next (AMCollection* base, AMIter* iter)
{
	if (iter->i >= (*iter).array->len || iter->i < 0) return NULL;

	gconstpointer* item = g_ptr_array_index (iter->array, (guint) iter->i);
	iter->i++;
	return item;
}


static void
am_array_class_init (AMArrayClass * klass)
{
	am_array_parent_class = g_type_class_peek_parent (klass);

	AM_COLLECTION_CLASS (klass)->length = am_array_real_length;
	AM_COLLECTION_CLASS (klass)->contains = am_array_real_contains;
	AM_COLLECTION_CLASS (klass)->find_by_idx = am_array_find_by_idx;
	AM_COLLECTION_CLASS (klass)->find_by_id = am_array_real_find_by_id;
	AM_COLLECTION_CLASS (klass)->append = am_array_real_append;
	AM_COLLECTION_CLASS (klass)->remove = am_array_real_remove;
	AM_COLLECTION_CLASS (klass)->iter_init = am_array_real_iter_init;
	AM_COLLECTION_CLASS (klass)->iter_next = (AMCollectionNextFn)am_array_real_iter_next;

	G_OBJECT_CLASS (klass)->get_property = am_array_get_property;
	G_OBJECT_CLASS (klass)->set_property = am_array_set_property;
	G_OBJECT_CLASS (klass)->finalize = am_array_finalize;

	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_ARRAY_T_TYPE, g_param_spec_gtype ("t-type", "type", "type", G_TYPE_NONE, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_ARRAY_T_DUP_FUNC, g_param_spec_pointer ("t-dup-func", "dup func", "dup func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_ARRAY_T_DESTROY_FUNC, g_param_spec_pointer ("t-destroy-func", "destroy func", "destroy func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
}


static void
am_array_init (AMArray * self)
{
	self->priv = am_array_get_instance_private(self);
}


static void
am_array_finalize (GObject* obj)
{
	AMArray* self = AM_ARRAY (obj);
	_g_ptr_array_free0 (self->array);
	G_OBJECT_CLASS (am_array_parent_class)->finalize (obj);
}


static void
am_array_get_property (GObject* object, guint property_id, GValue* value, GParamSpec* pspec)
{
	switch (property_id) {
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}


static void
am_array_set_property (GObject* object, guint property_id, const GValue* value, GParamSpec* pspec)
{
	AMArray* self = AM_ARRAY (object);

	switch (property_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
		case AM_ARRAY_T_TYPE:
			self->priv->t_type = g_value_get_gtype (value);
			break;
		case AM_ARRAY_T_DUP_FUNC:
			self->priv->t_dup_func = g_value_get_pointer (value);
			break;
		case AM_ARRAY_T_DESTROY_FUNC:
			self->priv->t_destroy_func = g_value_get_pointer (value);
			break;
	}
}


static guint
am_list_real_length (AMCollection* base)
{
	AMList* self = (AMList*) base;
	return g_list_length (self->list);
}


static bool
am_list_real_contains (AMCollection* base, gconstpointer item)
{
	GList* l = g_list_find (((AMList*)base)->list, item);
	return l != NULL;
}


static const AMItem*
am_list_real_find_by_id (AMCollection* base, guint64 id)
{
	AMList* self = (AMList*) base;

	GList* l = self->list;
	for (;l;l=l->next) {
		AMPart* part = (AMPart*)l->data;
		if (part->ayyi->id == id) {
			return (AMItem*)part;
		}
	}

	return NULL;
}


static gpointer
am_list_real_find_by_idx (AMCollection* collection, AyyiIdx idx)
{
	g_return_val_if_fail (collection, NULL);
	AMList* self = (AMList*)collection;

	if(self == song->pool){
		GList* l = self->list;
		for(;l;l=l->next){
			AMPoolItem* pi = l->data;
			if(pi->pod_index[0] == idx) return pi;
		}
		return NULL;
	}

	g_return_val_if_fail(self == song->parts, NULL);

	GList* l = self->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		if((part->status != PART_LOCAL) && part->ayyi && (part->ayyi->shm_idx == idx)) return part;
	}

	l = ((AMCollection*) self)->pending;
	{
		bool _tmp5_ = TRUE;
		while (TRUE) {
			gboolean _tmp6_ = FALSE;
			if (!_tmp5_) {
				l = l->next;
			}
			_tmp5_ = FALSE;
			if (!l) {
				break;
			}
			AMPart* part = l->data;
			if (part->ayyi) {
				_tmp6_ = (*(*part).ayyi).shm_idx == idx;
			} else {
				_tmp6_ = FALSE;
			}
			if (_tmp6_) {
				gconstpointer _tmp8_;
				return (_tmp8_ = part, ((_tmp8_ == NULL) || (self->priv->t_dup_func == NULL)) ? ((gpointer) _tmp8_) : self->priv->t_dup_func ((gpointer) _tmp8_));
			}
		}
	}
	return NULL;
}


static gpointer
am_list_real_append (AMCollection* base, gconstpointer item)
{
	AMList* self = (AMList*) base;
	AMPart* part = (AMPart*) item;
	if (part->status == PART_PENDING) {
		((AMCollection*) self)->pending = g_list_append (((AMCollection*) self)->pending, (gpointer)item);
	} else {
		self->list = g_list_append (self->list, (gpointer)item);
	}
	return (gpointer)item;
}


static void
am_list_real_remove (AMCollection* collection, gconstpointer item)
{
	AMList* self = (AMList*)collection;

	self->list = g_list_remove (self->list, item);
	collection->pending = g_list_remove (collection->pending, item);
	collection->selection = g_list_remove (collection->selection, item);
}


gpointer
am_list_first (AMList* self)
{
	g_return_val_if_fail (self, NULL);

	return self->list ? self->list->data : NULL;
}


static void
am_list_real_iter_init (AMCollection* base, AMIter* iter)
{
	AMList* self = (AMList*) base;
	iter->i = 0;
	(*iter).ptr = self->list;
	(*iter).list = self->list;
}


static gconstpointer*
am_list_real_iter_next (AMCollection* base, AMIter* iter)
{
	AMList* self = (AMList*) base;

	if (!((*iter).ptr)) {
		return NULL;
	}
	GList* l = (GList*) (*iter).ptr;
	(*iter).ptr = l->next;
	if (!l->data) {
		if ((*iter).list == self->list) {
			(*iter).list = ((AMCollection*) self)->pending;
			(*iter).ptr = (*iter).list;
			l = (GList*) (*iter).ptr;
		}
	}
	return l->data;
}


static gboolean
_ident_equal (const AyyiIdent* s1, const AyyiIdent* s2)
{
	if (s1 == s2) {
		return TRUE;
	}
	if (s1 == NULL) {
		return FALSE;
	}
	if (s2 == NULL) {
		return FALSE;
	}
	if (s1->type != s2->type) {
		return FALSE;
	}
	if (s1->idx != s2->idx) {
		return FALSE;
	}
	return TRUE;
}


gpointer
am_list_find_by_ident (AMList* self, AyyiIdent id)
{
	g_return_val_if_fail (self, NULL);

	GList* l = self->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		if (_ident_equal (&(*part).ident, &id) == TRUE) {
			gpointer _tmp1_;
			gconstpointer _tmp2_;
			_tmp1_ = (_tmp2_ = part, ((_tmp2_ == NULL) || (self->priv->t_dup_func == NULL)) ? ((gpointer) _tmp2_) : self->priv->t_dup_func ((gpointer) _tmp2_));
			return _tmp1_;
		}
	}
	return NULL;
}


void
am_list_unpend (AMList* self, gconstpointer item)
{
	g_return_if_fail (self);

	((AMCollection*) self)->pending = g_list_remove (((AMCollection*)self)->pending, item);
	self->list = g_list_append (self->list, (gpointer)item);
}


bool
am_list_is_pending (AMList* self, AyyiIdx idx, AyyiMediaType type)
{
	g_return_val_if_fail (self, FALSE);
	g_return_val_if_fail ((type == AYYI_AUDIO) || (type == AYYI_MIDI), FALSE);

	GList* l = ((AMCollection*) self)->pending;
	bool _tmp0_ = TRUE;
	while (TRUE) {
		bool _tmp1_ = FALSE;
		if (!_tmp0_) {
			l = l->next;
		}
		_tmp0_ = FALSE;
		if (!l) {
			break;
		}
		AMPart* part = l->data;
		if ((*part).ident.idx == idx) {
			AyyiObjType _tmp2_ = 0;
			if (type == AYYI_AUDIO) {
				_tmp2_ = AYYI_OBJECT_AUDIO_PART;
			} else {
				_tmp2_ = AYYI_OBJECT_MIDI_PART;
			}
			_tmp1_ = (*part).ident.type == _tmp2_;
		} else {
			_tmp1_ = FALSE;
		}
		if (_tmp1_) {
			return TRUE;
		}
	}
	return FALSE;
}


AMList*
am_list_construct (GType object_type, GType t_type, GBoxedCopyFunc t_dup_func, GDestroyNotify t_destroy_func)
{
	AMList* self = (AMList*) am_collection_construct (object_type, t_type);
	self->priv->t_type = t_type;
	self->priv->t_dup_func = t_dup_func;
	self->priv->t_destroy_func = t_destroy_func;
	return self;
}


AMList*
am_list_new ()
{
	GBoxedCopyFunc t_dup_func = NULL;
	GDestroyNotify t_destroy_func = NULL;
	return am_list_construct (AM_TYPE_LIST, 0, t_dup_func, t_destroy_func);
}


static void
am_list_class_init (AMListClass* klass)
{
	am_list_parent_class = g_type_class_peek_parent (klass);

	AM_COLLECTION_CLASS (klass)->length = am_list_real_length;
	AM_COLLECTION_CLASS (klass)->contains = am_list_real_contains;
	AM_COLLECTION_CLASS (klass)->find_by_id = am_list_real_find_by_id;
	AM_COLLECTION_CLASS (klass)->find_by_idx = am_list_real_find_by_idx;
	AM_COLLECTION_CLASS (klass)->append = am_list_real_append;
	AM_COLLECTION_CLASS (klass)->remove = am_list_real_remove;
	AM_COLLECTION_CLASS (klass)->iter_init = am_list_real_iter_init;
	AM_COLLECTION_CLASS (klass)->iter_next = (AMCollectionNextFn)am_list_real_iter_next;

	G_OBJECT_CLASS (klass)->get_property = am_list_get_property;
	G_OBJECT_CLASS (klass)->set_property = am_list_set_property;
	G_OBJECT_CLASS (klass)->finalize = am_list_finalize;

	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_LIST_T_TYPE, g_param_spec_gtype ("t-type", "type", "type", G_TYPE_NONE, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_LIST_T_DUP_FUNC, g_param_spec_pointer ("t-dup-func", "dup func", "dup func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_LIST_T_DESTROY_FUNC, g_param_spec_pointer ("t-destroy-func", "destroy func", "destroy func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
}


static void
am_list_init (AMList* self)
{
	self->priv = am_list_get_instance_private(self);
}


static void
am_list_finalize (GObject* obj)
{
	AMList* self = AM_LIST(obj);
	_g_list_free0 (self->list);
	G_OBJECT_CLASS (am_list_parent_class)->finalize (obj);
}


static void
am_list_get_property (GObject* object, guint property_id, GValue* value, GParamSpec* pspec)
{
	switch (property_id) {
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}


static void
am_list_set_property (GObject* object, guint property_id, const GValue* value, GParamSpec* pspec)
{
	AMList* self = AM_LIST (object);

	switch (property_id) {
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
		case AM_LIST_T_TYPE:
		self->priv->t_type = g_value_get_gtype (value);
		break;
		case AM_LIST_T_DUP_FUNC:
		self->priv->t_dup_func = g_value_get_pointer (value);
		break;
		case AM_LIST_T_DESTROY_FUNC:
		self->priv->t_destroy_func = g_value_get_pointer (value);
		break;
	}
}


FilterIterator*
filter_iterator_construct (GType object_type, GType t_type, GBoxedCopyFunc t_dup_func, GDestroyNotify t_destroy_func, AMCollection* _list, Filter _accept, void* _accept_target)
{
	FilterIterator* self = (FilterIterator*) g_type_create_instance (object_type);
	self->priv->t_type = t_type;
	self->priv->t_dup_func = t_dup_func;
	self->priv->t_destroy_func = t_destroy_func;
	self->list = _list;
	(self->accept_target_destroy_notify == NULL) ? NULL : (self->accept_target_destroy_notify (self->accept_target), NULL);
	self->accept = _accept;
	self->accept_target = _accept_target;
	self->accept_target_destroy_notify = NULL;

	am_collection_iter_init (self->list, &self->iter);

	return self;
}


FilterIterator*
filter_iterator_new (GType t_type, GBoxedCopyFunc t_dup_func, GDestroyNotify t_destroy_func, AMCollection* _list, Filter _accept, void* _accept_target)
{
	return filter_iterator_construct (TYPE_FILTER_ITERATOR, t_type, t_dup_func, t_destroy_func, _list, _accept, _accept_target);
}


gconstpointer*
filter_iterator_next (FilterIterator* self)
{
	g_return_val_if_fail (self, NULL);

	gconstpointer* next;
	while ((next = am_collection_iter_next (self->list, &self->iter))) {
		if(self->accept (next, self->accept_target)) return next;
	}
	return next;
}


static void
value_filter_iterator_init (GValue* value)
{
	value->data[0].v_pointer = NULL;
}


static void
value_filter_iterator_free_value (GValue* value)
{
	if (value->data[0].v_pointer) {
		filter_iterator_unref (value->data[0].v_pointer);
	}
}


static void
value_filter_iterator_copy_value (const GValue* src_value, GValue* dest_value)
{
	if (src_value->data[0].v_pointer) {
		dest_value->data[0].v_pointer = filter_iterator_ref (src_value->data[0].v_pointer);
	} else {
		dest_value->data[0].v_pointer = NULL;
	}
}


static gpointer
value_filter_iterator_peek_pointer (const GValue* value)
{
	return value->data[0].v_pointer;
}


static gchar*
value_filter_iterator_collect_value (GValue* value, guint n_collect_values, GTypeCValue* collect_values, guint collect_flags)
{
	if (collect_values[0].v_pointer) {
		FilterIterator* object;
		object = collect_values[0].v_pointer;
		if (object->parent_instance.g_class == NULL) {
			return g_strconcat ("invalid unclassed object pointer for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
		} else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object), G_VALUE_TYPE (value))) {
			return g_strconcat ("invalid object type `", g_type_name (G_TYPE_FROM_INSTANCE (object)), "' for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
		}
		value->data[0].v_pointer = filter_iterator_ref (object);
	} else {
		value->data[0].v_pointer = NULL;
	}
	return NULL;
}


static gchar*
value_filter_iterator_lcopy_value (const GValue* value, guint n_collect_values, GTypeCValue* collect_values, guint collect_flags)
{
	FilterIterator** object_p;
	object_p = collect_values[0].v_pointer;
	if (!object_p) {
		return g_strdup_printf ("value location for `%s' passed as NULL", G_VALUE_TYPE_NAME (value));
	}
	if (!value->data[0].v_pointer) {
		*object_p = NULL;
	} else if (collect_flags & G_VALUE_NOCOPY_CONTENTS) {
		*object_p = value->data[0].v_pointer;
	} else {
		*object_p = filter_iterator_ref (value->data[0].v_pointer);
	}
	return NULL;
}


GParamSpec*
param_spec_filter_iterator (const gchar* name, const gchar* nick, const gchar* blurb, GType object_type, GParamFlags flags)
{
	ParamSpecFilterIterator* spec;
	g_return_val_if_fail (g_type_is_a (object_type, TYPE_FILTER_ITERATOR), NULL);
	spec = g_param_spec_internal (G_TYPE_PARAM_OBJECT, name, nick, blurb, flags);
	G_PARAM_SPEC (spec)->value_type = object_type;
	return G_PARAM_SPEC (spec);
}


gpointer
value_get_filter_iterator (const GValue* value)
{
	g_return_val_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, TYPE_FILTER_ITERATOR), NULL);
	return value->data[0].v_pointer;
}


void
value_set_filter_iterator (GValue* value, gpointer v_object)
{
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, TYPE_FILTER_ITERATOR));
	FilterIterator* old = value->data[0].v_pointer;
	if (v_object) {
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, TYPE_FILTER_ITERATOR));
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
		value->data[0].v_pointer = v_object;
		filter_iterator_ref (value->data[0].v_pointer);
	} else {
		value->data[0].v_pointer = NULL;
	}
	if (old) {
		filter_iterator_unref (old);
	}
}


void
value_take_filter_iterator (GValue* value, gpointer v_object)
{
	FilterIterator* old;
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, TYPE_FILTER_ITERATOR));
	old = value->data[0].v_pointer;
	if (v_object) {
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, TYPE_FILTER_ITERATOR));
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
		value->data[0].v_pointer = v_object;
	} else {
		value->data[0].v_pointer = NULL;
	}
	if (old) {
		filter_iterator_unref (old);
	}
}


static void
filter_iterator_class_init (FilterIteratorClass* klass)
{
	filter_iterator_parent_class = g_type_class_peek_parent (klass);
	FILTER_ITERATOR_CLASS (klass)->finalize = filter_iterator_finalize;
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
	g_type_class_add_private (klass, sizeof (FilterIteratorPrivate));
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
}


static void
filter_iterator_instance_init (FilterIterator* self)
{
	self->priv = FILTER_ITERATOR_GET_PRIVATE (self);
	self->ref_count = 1;
}


static void
filter_iterator_finalize (FilterIterator* obj)
{
	FilterIterator* self = FILTER_ITERATOR(obj);
	(self->accept_target_destroy_notify == NULL) ? NULL : (self->accept_target_destroy_notify (self->accept_target), NULL);
	self->accept = NULL;
	self->accept_target = NULL;
	self->accept_target_destroy_notify = NULL;
}


GType
filter_iterator_get_type (void)
{
	static volatile gsize filter_iterator_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&filter_iterator_type_id__volatile)) {
		static const GTypeValueTable g_define_type_value_table = { value_filter_iterator_init, value_filter_iterator_free_value, value_filter_iterator_copy_value, value_filter_iterator_peek_pointer, "p", value_filter_iterator_collect_value, "p", value_filter_iterator_lcopy_value };
		static const GTypeInfo g_define_type_info = { sizeof (FilterIteratorClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) filter_iterator_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (FilterIterator), 0, (GInstanceInitFunc) filter_iterator_instance_init, &g_define_type_value_table };
		static const GTypeFundamentalInfo g_define_type_fundamental_info = { (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE) };
		GType filter_iterator_type_id;
		filter_iterator_type_id = g_type_register_fundamental (g_type_fundamental_next (), "FilterIterator", &g_define_type_info, &g_define_type_fundamental_info, 0);
		g_once_init_leave (&filter_iterator_type_id__volatile, filter_iterator_type_id);
	}
	return filter_iterator_type_id__volatile;
}


gpointer
filter_iterator_ref (gpointer instance)
{
	FilterIterator* self = instance;
	g_atomic_int_inc (&self->ref_count);
	return instance;
}


void
filter_iterator_unref (gpointer instance)
{
	FilterIterator* self = instance;
	if (g_atomic_int_dec_and_test (&self->ref_count)) {
		FILTER_ITERATOR_GET_CLASS (self)->finalize (self);
		g_type_free_instance ((GTypeInstance *) self);
	}
}

