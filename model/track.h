/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_track_h__
#define __am_track_h__

#include "model_types.h"
#include "observable.h"

#define AM_ASSERT_TRACK_NUM(A) if(A < 0 || A > AM_MAX_TRK){ perr("track number out of range (%i)", A); return 0; }

#define AM_TRK_IS_AUDIO(A) (A->type == TRK_TYPE_AUDIO)
#define AM_TRK_IS_MIDI(A) (A->type == TRK_TYPE_MIDI)
#define AM_TRACK_TYPE_IS_VALID(A) (A > 0 && A <= TRK_TYPE_MIDI)

//#define TRK_GET_AYYI(T) (AM_TRK_IS_AUDIO(T) ? ayyi_song__audio_track_at(T->ident.idx) : (AyyiTrack*)ayyi_song__midi_track_at(T->ident.idx))
#define am_track__get_shared(T) (AM_TRK_IS_AUDIO((T)) \
	? (AyyiItem*)ayyi_song__audio_track_at((T)->ident.idx) \
	: (AM_TRK_IS_MIDI(T) ? (AyyiItem*)ayyi_song__midi_track_at((T)->ident.idx) : (AyyiItem*)ayyi_song__audio_track_at((T)->ident.idx)))

// more a less a dupe of above, but demonstrates that the AM track type is probably redundant.
#define am_track__get_shared_(T) ((T->ident.type == AYYI_OBJECT_AUDIO_TRACK) \
	? (AyyiItem*)ayyi_song__audio_track_at((T)->ident.idx) \
	: (T->ident.type == AYYI_OBJECT_MIDI_TRACK \
		? (AyyiItem*)ayyi_song__midi_track_at((T)->ident.idx) \
		: (AyyiItem*)ayyi_song__audio_track_at((T)->ident.idx)))

#define am_track__get_media_type(T) (T->ident.type == AYYI_OBJECT_AUDIO_TRACK ? AYYI_AUDIO : AYYI_MIDI)

typedef struct _AMTrackCurves AMTrackCurves;

/*
 *  A track object that acts as a proxy for the Ayyi track object.
 *  -it combines two Ayyi lists: audio tracks and midi tracks.
 */
struct _AMTrack
{
	AyyiIdent           ident;
	AMTrackType         type;
	char*               name;
	bool                visible;         // in theory this should be overridable in ArrTrk.
	GString*            user_notes;
	Observable*         meterval;        // note AMChannel also has meter_level

	struct _AMTrackCurves {
		AMCurve*            vol;         // dynamic array holding the complete bezier path data. is _always_ initialised.
		AMCurve*            pan;
		int                 size : 2;
	} bezier;

	GList*              controls;        // list of AMCurve*

    // derived values are calculated on demand. do not access directly
    struct _ {
        AyyiSongPos     start;
        AyyiSongPos     end;
    }                   cache;
};


GType           am_track_get_type               ();
AMTrack*        am_track__new                   (AMTrackType, AyyiIdx);

char*           am_track__get_channel_label     (const AMTrack*, char* label);

void            am_track__arm                   (AMTrack*, bool arm_state, AyyiHandler, gpointer);
void            am_track__mute                  (AMTrack*, bool mute_state, AyyiHandler, gpointer);
void            am_track__solo                  (AMTrack*, bool solo_state, AyyiHandler, gpointer);

const AMChannel*am_track__lookup_channel        (AMTrack*);
int             am_track__lookup_channel_num    (AMTrack*);

guchar          am_track__is_armed              (AMTrack*);
bool            am_track__is_solod              (AMTrack*);
bool            am_track__is_muted              (const AMTrack*);
bool            am_track__is_master             (AMTrack*);
bool            am_track__is_not_master         (AMTrack*);

bool            am_track__get_connections_string(const AMTrack*, char* label);
bool            am_track__get_name              (const AMTrack*, char* name);
void            am_track__set_name              (const AMTrack*, const char* name, AyyiHandler, gpointer);
int             am_track__get_colour            (AMTrack*);
void            am_track__set_colour            (const AMTrack*, int colour, AyyiHandler, gpointer);
void            am_track__set_input             (AMTrack*, AyyiConnection*);
void            am_track__set_output            (AMTrack*, AyyiConnection*, AyyiHandler, gpointer);
const char*     am_track__get_input_name        (AMTrack*);
const char*     am_track__get_output_name       (AMTrack*);
float           am_track__get_meterlevel        (AMTrack*);
void            am_track__get_meterlevel_master (float level[]);

int             am_track__get_input_num         (AMTrack*);
int             am_track__get_output_num        (AMTrack*);
AyyiConnection* am_track__get_output            (AMTrack*);

void            am_track__get_start_end         (AMTrack*, AyyiSongPos*, AyyiSongPos*);

AMCurve*        am_track__lookup_curve          (AMTrack*, int);
void            am_track__clear_automation      (AMTrack*, AMCurve*, AyyiHandler, gpointer);

void            am_track__enable_metering       (AMTrack*);

bool            am_track__is_empty              (AMTrack*, gpointer);
bool            am_track__is_mono               (AMTrack*, gpointer);
bool            am_track__is_midi               (AMTrack*, gpointer);

bool            am_track__is_valid              (const AMTrack*);

#ifdef DEBUG
const char*     am_track__print_type            (AMTrack*);
#endif

#ifdef __am_private__
bool            am_track__exists                (AMTrack*);
void           _am_track__sync_name             (AMTrack*);
#endif

#endif
