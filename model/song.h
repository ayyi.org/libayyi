/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __am_song_h__
#define __am_song_h__

#include "config.h"
#include <float.h>
#include <glib-object.h>
#include "ayyi/ayyi_client.h"
#include "ayyi/ayyi_song.h"
#include "model/observable.h"
#include "model/am_palette.h"
#include "model/part_input.h"

G_BEGIN_DECLS

#define AM_TYPE_SONG            (am_song_get_type ())
#define AM_SONG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AM_TYPE_SONG, AMSong))
#define AM_SONG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AM_TYPE_SONG, AMSongClass))
#define AM_IS_SONG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AM_TYPE_SONG))
#define AM_IS_SONG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AM_TYPE_SONG))
#define AM_SONG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AM_TYPE_SONG, AMSongClass))

typedef struct _AMSong        AMSong;
typedef struct _AMSongClass   AMSongClass;
typedef struct _AMSongPrivate AMSongPrivate;

struct _AMSong {
	GObject          parent_instance;
	AMSongPrivate*   priv;
	bool             loaded : 1;
	AMObject         loc[10];
	bool             end_auto;
	gint             sample_rate;
	gfloat           beats2bar;
	gchar*           audiodir;
	gchar*           peaksdir;
	Observable*      tempo;
	QMap             q_settings[5];
	AMSnapType       snap_mode;

	AMTrackList*     tracks;
	AMList*          pool;
	AMArray*         channels;
	AMList*          parts;
	AMList*          map_parts;
	GList*           input_list;
	gchar*           input_str;
	GList*           bus_list;
	gchar*           bus_strs;
	GList*           plugins;

	uint32_t         palette[AM_MAX_COLOUR];

	Observable*      spp;

	guint            periodic;
	GList*           work_queue;
};

struct _AMSongClass {
	GObjectClass parent_class;
};

extern AMSong* song;

bool         am__register_signals                 (AyyiService*);

GType        am_song_get_type                     () G_GNUC_CONST;
AMSong*      am_song_new                          ();
AMSong*      am_song_construct                    (GType);

#define      am_song__new                         am_song_new
void         am_song__free                        ();
void         am_song__load                        ();                     // syncs the local song to that on the server.
void         am_song__unload                      ();

void         am_song__load_new                    (const char* filename, AyyiHandler, gpointer); // requests the server to close the current song and load the one given.
void         am_song__reload                      (AyyiHandler, gpointer);
void         am_song__save                        (AyyiHandler, gpointer);

void         am_song__undo                        (AyyiService*, int n);
void         am_song__enable_meters               (bool);

#ifdef __am_private__
void         am_song__add_handlers                ();
#endif

/* Tracks */
void         am_song_add_track                    (AMTrackType, char* name, int n_channels, AyyiHandler3, gpointer);
void         am_song_remove_track                 (AMTrack*, AyyiHandler, gpointer);
void         am_song__clear_tracks                (AyyiHandler, gpointer);
AMTrack*     am_song__get_track_for_part          (AyyiRegionBase*, AyyiMediaType);

/* Parts */
AMPart*      am_song__get_part_by_region_index    (AyyiIdx, AyyiMediaType);
AMPart*      am_song__get_part_by_ident           (AyyiIdent);
AMPart*      am_song__part_lookup_by_name         (const char* name);
void         am_song__part_selection_move         (AyyiSongPos*, bool forwards, int track_offset);
void         am_song__add_part                    (AyyiMediaType, AyyiIdx src_region, AMPoolItem*, AyyiIdx track, AyyiSongPos*, uint64_t len, unsigned int colour, const char* name, uint32_t inset, AyyiHandler, gpointer);
#ifdef HAVE_INTROSPECTION
void         am_song__add_part_wrapped            (AMPartInput*, AyyiHandler3);
#endif
AyyiAction*  am_song__remove_part                 (AMPart*, AyyiHandler, gpointer);
void         am_song__remove_all_parts            (AyyiHandler, gpointer);
void         am_song__remove_parts_by_track       (AMTrack*, AyyiHandler, gpointer);
void         am_song__remove_parts_by_file        (AMPoolItem*, AyyiHandler, gpointer);
#ifdef __am_private__
void        _am_song__remove_part                 (AMPart*);
void        _am_song__remove_all_parts            ();
#endif

/* Files */
void         am_song__add_pool_item               (AMPoolItem*, AyyiHandler, gpointer);
AMPoolItem*  am_song__add_file                    (const char* filename, AyyiHandler, gpointer);
void         am_song__remove_file                 (AMPoolItem*, AyyiHandler, gpointer);
AMPoolItem*  am_song__get_pool_item_from_region   (AyyiIdx);
AMPoolItem*  am_song__get_pool_item_from_region_without_id
                                                  (AyyiIdx);


double       am_song__get_tempo                   ();
void         am_song__set_tempo                   (double tempo, AyyiHandler, gpointer);

void         am_song__get_end                     (AMPos*);
void         am_song__set_end                     (int);


void         am_song__midi_note_add               (AMPart*, AyyiMidiNote*, AyyiHandler, gpointer);

#ifdef __am_private__
void         am_song__sync_tracks                 ();
void         am_song__sync_parts                  ();
#endif

void         am_song__clear_songmap               ();

void         am_song__print_tracks                ();
void         am_song__channels_print              ();

#define am_song__emit(...) ({ if(song->loaded) g_signal_emit_by_name (song, __VA_ARGS__); })
#define am_song__connect(...) { g_signal_connect (song, __VA_ARGS__); }
#define am_song__disconnect(PANEL, FN) {int n; if((n = g_signal_handlers_disconnect_matched (song, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, PANEL)) != 1) pwarn("handler disconnection: %s n=%i", #FN, n); }

#define STOP_ON_UNLOAD // restart the server for each song load

G_END_DECLS

#endif
