/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2009-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __note_selection_h__
#define __note_selection_h__


GList*       note_selection_list__new       (GList**, AMPart*);
void         note_selection_list__free      (GList**);
void         note_selection_list__reset     (GList*, AMPart*);
void         note_selection_list__print     (GList**);

TransitionalNote* transitional_note__new    (AyyiMidiNote*);

#endif
