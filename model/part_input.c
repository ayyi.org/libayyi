/*
 * +----------------------------------------------------------------------+
 * | This file is part of the Ayyi project. http://www.ayyi.org           |
 * | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 * +----------------------------------------------------------------------+
 * | This program is free software; you can redistribute it and/or modify |
 * | it under the terms of the GNU General Public License version 3       |
 * | as published by the Free Software Foundation.                        |
 * +----------------------------------------------------------------------+
 *
 */

#include <glib-object.h>
#include <stdlib.h>
#include "model/ayyi_model.h"
#include "model/part_input.h"

enum { AM_PART_INPUT_0_PROPERTY,
	AM_PART_INPUT_NAME_PROPERTY,
	AM_PART_INPUT_POOL_ITEM_PROPERTY,
	AM_PART_INPUT_TRACK_PROPERTY,
	AM_PART_INPUT_START_PROPERTY,
	AM_PART_INPUT_LENGTH_PROPERTY,
	AM_PART_INPUT_NUM_PROPERTIES
};
static GParamSpec* am_part_input_properties[AM_PART_INPUT_NUM_PROPERTIES];

#define _g_free0(var) (var = (g_free (var), NULL))

struct _AMPartInputPrivate
{
	AyyiMediaType _media_type;
	AyyiIdx       _src_region_idx;
	gchar*        _name;
	AMPoolItem*   _pool_item;
	AyyiIdx       _track_idx;
	AyyiSongPos   _start_time;
};

static gint AMPartInput_private_offset;
static gpointer am_part_input_parent_class = NULL;

GType        am_part_input_get_type       (void) G_GNUC_CONST;
AMPartInput* am_part_input_new            (void);
AMPartInput* am_part_input_construct      (GType object_type);
const gchar* am_part_input_get_name       (AMPartInput* self);
void         am_part_input_set_name       (AMPartInput* self, const gchar* value);
static void  am_part_input_finalize       (GObject* obj);
static GType am_part_input_get_type_once  (void);
static void  am_part_input_get_property   (GObject* object, guint property_id, GValue* value, GParamSpec* pspec);
static void  am_part_input_set_property   (GObject* object, guint property_id, const GValue* value, GParamSpec* pspec);

G_DEFINE_AUTOPTR_CLEANUP_FUNC             (AMPartInput, g_object_unref)

static inline gpointer
am_part_input_get_instance_private (AMPartInput* self)
{
	return G_STRUCT_MEMBER_P (self, AMPartInput_private_offset);
}

AMPartInput*
am_part_input_construct (GType object_type)
{
	AMPartInput* self = (AMPartInput*)g_object_new (object_type, NULL);
	self->priv->_media_type = AYYI_AUDIO;
	self->priv->_src_region_idx = (AyyiIdx)0;
	return self;
}

AMPartInput*
am_part_input_new (void)
{
	return am_part_input_construct (AM_TYPE_PART_INPUT);
}

const gchar*
am_part_input_get_name (AMPartInput* self)
{
	g_return_val_if_fail (self, NULL);

	const gchar* result = self->priv->_name;
	return result;
}

void
am_part_input_set_name (AMPartInput* self, const gchar* value)
{
	g_return_if_fail (self);

	_g_free0 (self->priv->_name);
	self->priv->_name = g_strdup (value);
	g_object_notify_by_pspec ((GObject*)self, am_part_input_properties[AM_PART_INPUT_NAME_PROPERTY]);
}

/**
 * am_part_input_get_pool_item
 *
 * Return value: (transfer none)
 */
AMPoolItem*
am_part_input_get_pool_item (AMPartInput* self)
{
	g_return_val_if_fail (self, NULL);

	return self->priv->_pool_item;
}

void
am_part_input_set_pool_item (AMPartInput* self, AMPoolItem* value)
{
	g_return_if_fail (self != NULL);
	self->priv->_pool_item = value;
	g_object_notify_by_pspec ((GObject *) self, am_part_input_properties[AM_PART_INPUT_POOL_ITEM_PROPERTY]);
}

AyyiIdx
am_part_input_get_track (AMPartInput* self)
{
	g_return_val_if_fail (self, -1);

	return self->priv->_track_idx;
}

void
am_part_input_set_track (AMPartInput* self, AyyiIdx value)
{
	g_return_if_fail (self);

	self->priv->_track_idx = value;
	g_object_notify_by_pspec ((GObject *) self, am_part_input_properties[AM_PART_INPUT_TRACK_PROPERTY]);
}

AyyiSongPos
am_part_input_get_start (AMPartInput* self)
{
	return self->priv->_start_time;
}

void
am_part_input_set_start (AMPartInput* self, AyyiSongPos value)
{
	g_return_if_fail (self);

	self->priv->_start_time = value;
	g_object_notify_by_pspec ((GObject*)self, am_part_input_properties[AM_PART_INPUT_START_PROPERTY]);
}

static void
am_part_input_class_init (AMPartInputClass* klass, gpointer klass_data)
{
	am_part_input_parent_class = g_type_class_peek_parent (klass);
	g_type_class_adjust_private_offset (klass, &AMPartInput_private_offset);
	G_OBJECT_CLASS (klass)->get_property = am_part_input_get_property;
	G_OBJECT_CLASS (klass)->set_property = am_part_input_set_property;
	G_OBJECT_CLASS (klass)->finalize = am_part_input_finalize;
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_PART_INPUT_NAME_PROPERTY, am_part_input_properties[AM_PART_INPUT_NAME_PROPERTY] = g_param_spec_string ("name", "name", "name", NULL, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_PART_INPUT_POOL_ITEM_PROPERTY, am_part_input_properties[AM_PART_INPUT_POOL_ITEM_PROPERTY] = g_param_spec_object ("pool-item", "pool-item", "pool-item", AM_TYPE_POOL_ITEM, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_PART_INPUT_TRACK_PROPERTY, am_part_input_properties[AM_PART_INPUT_TRACK_PROPERTY] = g_param_spec_int ("track", "track", "track", -1, 256*256, -1, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_PART_INPUT_START_PROPERTY, am_part_input_properties[AM_PART_INPUT_START_PROPERTY] = g_param_spec_boxed ("start", "start", "start", ayyi_song_pos_get_type(), G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_PART_INPUT_LENGTH_PROPERTY, am_part_input_properties[AM_PART_INPUT_LENGTH_PROPERTY] = g_param_spec_int64 ("length", "length", "length", G_MININT64, G_MAXINT64, 0, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
}

static void
am_part_input_instance_init (AMPartInput* self, gpointer klass)
{
	self->priv = am_part_input_get_instance_private (self);
}

static void
am_part_input_finalize (GObject* obj)
{
	AMPartInput* self = G_TYPE_CHECK_INSTANCE_CAST (obj, AM_TYPE_PART_INPUT, AMPartInput);
	_g_free0 (self->priv->_name);
	G_OBJECT_CLASS (am_part_input_parent_class)->finalize (obj);
}

static GType
am_part_input_get_type_once (void)
{
	static const GTypeInfo g_define_type_info = {
		sizeof (AMPartInputClass),
		(GBaseInitFunc)NULL,
		(GBaseFinalizeFunc)NULL,
		(GClassInitFunc)am_part_input_class_init,
		(GClassFinalizeFunc)NULL,
		NULL,
		sizeof (AMPartInput),
		0,
		(GInstanceInitFunc)am_part_input_instance_init,
		NULL
	};

	GType am_part_input_type_id = g_type_register_static (G_TYPE_OBJECT, "AMPartInput", &g_define_type_info, 0);
	AMPartInput_private_offset = g_type_add_instance_private (am_part_input_type_id, sizeof (AMPartInputPrivate));
	return am_part_input_type_id;
}

GType
am_part_input_get_type (void)
{
	static volatile gsize am_part_input_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&am_part_input_type_id__volatile)) {
		GType am_part_input_type_id;
		am_part_input_type_id = am_part_input_get_type_once ();
		g_once_init_leave (&am_part_input_type_id__volatile, am_part_input_type_id);
	}
	return am_part_input_type_id__volatile;
}

static void
am_part_input_get_property (GObject* object, guint property_id, GValue* value, GParamSpec* pspec)
{
	AMPartInput* self = G_TYPE_CHECK_INSTANCE_CAST (object, AM_TYPE_PART_INPUT, AMPartInput);

	switch (property_id) {
		case AM_PART_INPUT_NAME_PROPERTY:
			g_value_set_string (value, am_part_input_get_name (self));
			break;
		case AM_PART_INPUT_POOL_ITEM_PROPERTY:
			{
				AMPoolItem* item = am_part_input_get_pool_item (self);
				g_value_set_object (value, item);
			}
			break;
		case AM_PART_INPUT_TRACK_PROPERTY:
			g_value_set_int (value, am_part_input_get_track (self));
			break;
		case AM_PART_INPUT_START_PROPERTY:
			;AyyiSongPos start = am_part_input_get_start (self);
			g_value_set_boxed (value, &start);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
am_part_input_set_property (GObject* object, guint property_id, const GValue* value, GParamSpec* pspec)
{
	AMPartInput* self = G_TYPE_CHECK_INSTANCE_CAST (object, AM_TYPE_PART_INPUT, AMPartInput);

	switch (property_id) {
		case AM_PART_INPUT_NAME_PROPERTY:
			am_part_input_set_name (self, g_value_get_string (value));
			break;
		case AM_PART_INPUT_POOL_ITEM_PROPERTY:
			am_part_input_set_pool_item (self, g_value_get_object (value));
			break;
		case AM_PART_INPUT_TRACK_PROPERTY:
			am_part_input_set_track (self, g_value_get_int (value));
			break;
		case AM_PART_INPUT_START_PROPERTY:
			;AyyiSongPos* start = g_value_get_boxed (value);
			am_part_input_set_start (self, *start);
			break;
		case AM_PART_INPUT_LENGTH_PROPERTY:
			//am_part_input_set_length (self, g_value_get_int64 (value));
			self->_length = g_value_get_int64 (value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}
