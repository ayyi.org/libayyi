/*
libdynarr - A library for dynamic array handling
Copyright (C) 2003 Vanraes Maarten

This file is part of libdynarr package.

The file is provided as-is, no warranty is supplied. Use it at your
own risk.

libdynarr is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libdynarr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABLILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libdynarr; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "config.h"
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

#include "model/common.h"
#include "model/curve.h"
/*
#define LOCK_DEBUG
#define GROW_DEBUG
*/

/*
	dynarr_def_compare
notes:
	this default function can be used with dynarr_indexof function
*/
int
dynarr_def_compare (void*a,void*b)
{
	if(a<b)
		return -1;
	if(a>b)
		return 1;
	return 0;
}

/*
	dynarr_def_tostring
notes:
	this default function can be used with dynarr_tostring function
*/
char*
dynarr_def_tostring (void*a,void*b)
{
	char**str=a;
	if(!str)
		return 0;
	return *str;
}

/*
 *  If plugin is NULL, then the curve is for VOL (autotype=0), or PAN (autotype=1)
 *
 *  If plugin is set, auto_type is the control index.
 */
void*
curve_new (AMTrack* track, AyyiAutoType control_idx, AMPlugin* plugin)
{
	dynarr_p curve = AYYI_NEW(dynarr_t,
		.track     = track,
		.auto_type = control_idx,
		.plugin    = plugin,
	);

	curve_init(curve, sizeof(BPath), 0);

	return curve;
}

/*
	curve_init
locking:
	dynarr will be locked upon successful creation
*/
int
curve_init (dynarr_p dynarr, size_t item_size, size_t size)
{
	if(size){
		dynarr->path = g_malloc(size);
		if(!dynarr->path) return 0;
	}
	else dynarr->path = 0;
#ifdef LOCK_DEBUG
	printf("dynarr %X: init\n",dynarr);
#endif
	pthread_mutex_init(&dynarr->mutex,0);/* check if fails */
#ifdef LOCK_DEBUG
	printf("\tdynarr %X: lock\n",dynarr);
#endif
	pthread_mutex_lock(&dynarr->mutex);  /* check if fails */
	dynarr->size      = size;
	dynarr->item_size = item_size;
	dynarr->grow_size = DEFAULT_GROW_SIZE;
	dynarr->len       = 0;
	return 1;
}

/*
	dynarr_free
locking:
	dynarr must be locked before calling this function
*/
void
dynarr_free (dynarr_p dynarr)
{
	g_clear_pointer(&dynarr->path, g_free);
	g_clear_pointer(&dynarr->name, g_free);
	dynarr->size=0;
	dynarr->len=0;
#ifdef LOCK_DEBUG
	printf("\tdynarr %X: unlock\n",dynarr);
#endif
	pthread_mutex_unlock(&dynarr->mutex);
#ifdef LOCK_DEBUG
	printf("dynarr %X: free\n",dynarr);
#endif
	pthread_mutex_destroy(&dynarr->mutex);

	g_free(dynarr);
}

/*
	dynarr_lock
locking:
	dynarr must be unlocked before calling this function
	dynarr will be locked upon successful return
*/
int
dynarr_lock (dynarr_p dynarr)
{
#ifdef LOCK_DEBUG
	printf("\tdynarr %X: lock\n",dynarr);
#endif
	return pthread_mutex_lock(&dynarr->mutex);
}

/*
	dynarr_unlock
locking:
	dynarr must be locked before calling this function
*/
void
dynarr_unlock (dynarr_p dynarr)
{
#ifdef LOCK_DEBUG
	printf("\tdynarr %X: unlock\n",dynarr);
#endif
	pthread_mutex_unlock(&dynarr->mutex);
}

/*
	dynarr_get_length
locking:
	dynarr must be locked before calling this function
*/
size_t
curve_get_length (dynarr_p dynarr)
{
	return dynarr->len;
}

/*
	dynarr_setlength
locking:
	dynarr must be locked before calling this function
*/
int
curve_setlength (dynarr_p dynarr,size_t length)
{
	size_t s=dynarr->size;
	while(length*dynarr->item_size>s)
		s+=dynarr->grow_size;
	while((s-(length*dynarr->item_size))>dynarr->grow_size)
		s-=dynarr->grow_size;
	dynarr->len=length;

	if(s != dynarr->size) return curve_setsize(dynarr,s);
	return 1;
}

/*
	dynarr_getsize
locking:
	dynarr must be locked before calling this function
*/
size_t
dynarr_getsize (dynarr_p dynarr)
{
	return dynarr->size;
}

/*
	dynarr_setsize
locking:
	dynarr must be locked before calling this function
*/
int
curve_setsize (dynarr_p dynarr, size_t size)
{
	void *v = realloc(dynarr->path,size);
	if(size)
		if(!v)
			return 0;
		else
#ifdef GROW_DEBUG
			printf("\tdynarr %X: grow\n",dynarr)
#endif
			;
	else
		if(v){
			free(v);
			v=0;
		}
	dynarr->path = v;
	dynarr->size = size;
	return 1;
}

/*
	dynarr_getgrow_size
locking:
	dynarr must be locked before calling this function
*/
size_t
dynarr_getgrow_size (dynarr_p dynarr)
{
	return dynarr->grow_size;
}

/*
	dynarr_setgrow_size
locking:
	dynarr must be locked before calling this function
*/
int
dynarr_setgrow_size (dynarr_p dynarr,size_t grow_size)
{
	dynarr->grow_size=grow_size;

	return 1;
}

/*
	dynarr_
locking:
	dynarr must be locked before calling this function
*/
size_t
dynarr_getitem_size (dynarr_p dynarr)
{
	return dynarr->item_size;
}

/*
	dynarr_setitem_size
locking:
	dynarr must be locked before calling this function
*/
int
dynarr_setitem_size (dynarr_p dynarr,size_t item_size)
{
	if(dynarr->len)
		return 0;
	dynarr->item_size=item_size;
	return 1;
}

/*
	dynarr_item
locking:
	dynarr must be locked before calling this function
*/
void*
dynarr_item (dynarr_p dynarr,size_t index)
{
	return (dynarr->path+index*dynarr->item_size);
}

/*
	dynarr_getitem
locking:
	dynarr must be locked before calling this function
*/
void
dynarr_getitem (dynarr_p dynarr,size_t index,void* data)
{
	memcpy(data,dynarr->path+index*dynarr->item_size,dynarr->item_size);
}

/*
	dynarr_setitem
locking:
	dynarr must be locked before calling this function
*/
void
dynarr_setitem (dynarr_p dynarr,size_t index,void* data)
{
	memcpy(dynarr->path+index*dynarr->item_size,data,dynarr->item_size);
}

/*
	dynarr_ins
locking:
	dynarr must be locked before calling this function

returns the address of the inserted item.
*/
void*
curve_ins (dynarr_p dynarr, size_t index)
{
	void* r;
	if((dynarr->len + 1) * dynarr->item_size > dynarr->size){
		dynarr->size += dynarr->grow_size;
		r = realloc(dynarr->path,dynarr->size);
		if(!r){
			dynarr->size -= dynarr->grow_size;
			return 0;
		}
#ifdef GROW_DEBUG
		printf("\tdynarr %X: grow\n",dynarr);
#endif
		dynarr->path = r;
	}
//size_t size = dynarr->item_size;
	char* src = (char*)dynarr->path + dynarr->item_size * index;
	char* dst = (char*)dynarr->path + dynarr->item_size * (index+1);
dbg(0, "moving %i bytes by %u... size=%u",
		dynarr->item_size * (dynarr->len - index),
		dst - src,
		dynarr->item_size);
	//memmove(dynarr->path + dynarr->item_size*(index+1), dynarr->path+dynarr->item_size*index, dynarr->item_size*(dynarr->len++-index));
	memmove(dst, src, dynarr->item_size*(dynarr->len++-index));
	return (char*)dynarr->path + (dynarr->item_size * index);
}

/*
	dynarr_del
locking:
	dynarr must be locked before calling this function
*/
void
curve_del (dynarr_p dynarr, size_t index)
{
	void* r;
	if(index < dynarr->len){
		if((dynarr->size - (dynarr->len * dynarr->item_size)) > dynarr->grow_size){
			dynarr->size -= dynarr->grow_size;
			r = realloc(dynarr->path,dynarr->size);
			if(!r) dynarr->size += dynarr->grow_size;
			else dynarr->path = r;
#ifdef GROW_DEBUG
			printf("\tdynarr %X: grow\n", dynarr);
#endif
		}
		memmove((char*)dynarr->path+dynarr->item_size*index, (char*)dynarr->path+dynarr->item_size*(index+1), dynarr->item_size*(--dynarr->len-index));
	}
}

/*
	dynarr_extract
locking:
	dynarr must be locked before calling this function
*/
void*
dynarr_extract (dynarr_p dynarr)
{
	void*r;
	if(!dynarr->len)
		return 0;
	if((dynarr->size-(dynarr->len*dynarr->item_size))>dynarr->grow_size)
	{
		dynarr->size-=dynarr->grow_size;
		r=realloc(dynarr->path,dynarr->size);
		if(!r)
			dynarr->size+=dynarr->grow_size;
		else
			dynarr->path=r;
#ifdef GROW_DEBUG
			printf("\tdynarr %X: grow\n",dynarr);
#endif
	}
	return dynarr->path+(dynarr->item_size*--dynarr->len);
}

/*
	dynarr_append
locking:
	dynarr must be locked before calling this function
*/
void*
curve_append (dynarr_p dynarr)
{
	void*r;
	if((dynarr->len+1)*dynarr->item_size > dynarr->size){
		dynarr->size += dynarr->grow_size;
		r = realloc(dynarr->path,dynarr->size);
		if(!r){
			dynarr->size -= dynarr->grow_size;
			return 0;
		}
		dynarr->path = r;
#ifdef GROW_DEBUG
			printf("\tdynarr %X: grow\n", dynarr);
#endif
	}
	return (char*)dynarr->path + (dynarr->item_size * dynarr->len++);
}

/*
	dynarr_indexof
locking:
	dynarr must be locked before calling this function
*/
size_t
dynarr_indexof (dynarr_p dynarr,int findfunc(void*,void*),void* param)
{
	size_t r=dynarr->len;
	while((--r!=-1)&&(!findfunc(dynarr->path+r*dynarr->item_size,param)));
	return r;
}

/*
	dynarr_seek
locking:
	dynarr must be lock before calling this function
*/
size_t
dynarr_seek (dynarr_p dynarr,void **item,int seekfunc(void*,void*),void *param)
{
	size_t i=dynarr->len;
	while((--i!=-1)&&(((!(*item=dynarr_item(dynarr,i)))||(!seekfunc(*item,param)))));
	return i;
}

/*
	dynarr_adddynarr
locking:
	dynarr must be locked before calling this function
note
	definately need to implement this more efficient later
*/
int
dynarr_adddynarr (dynarr_p dynarr,dynarr_p add_dynarr)
{
	/* setlength first, then copy everything with memcpy
	 * */
	void *item;
	size_t l=-1;
	while(++l!=add_dynarr->len)
	{
		item = curve_append(dynarr);
		memcpy(item,add_dynarr->path+l*add_dynarr->item_size,dynarr->item_size);
	}
	return 1;
}

/*
	dynarr_clone
locking:
	dynarr must be locked before calling this function
	the cloned dynarr will be locked upon success too
*/
int
curve_clone (dynarr_p dynarr,dynarr_p dynarrclone)
{
	void*newdata=malloc(dynarr->size);
	if(!newdata)
		return 0;
	memcpy(dynarrclone, dynarr, sizeof(*dynarrclone));
	dynarrclone->path = newdata;
	memcpy(dynarrclone->path, dynarr->path, dynarrclone->len * dynarrclone->item_size);
#ifdef LOCK_DEBUG
	printf("dynarr %X: init\n",dynarrclone);
#endif
	pthread_mutex_init(&dynarrclone->mutex,0);
#ifdef LOCK_DEBUG
	printf("\tdynarr %X: lock\n",dynarrclone);
#endif
	pthread_mutex_lock(&dynarrclone->mutex);
	return 1;
}

/*
	dynarr_createFromCallback
locking:
	dynarr will be locked, if it fails, you need to free all subdata, and the dynarr yourself
*/
int
dynarr_createFromCallback (dynarr_p dynarr,int callback(void*,void*,size_t,size_t),void* param,size_t length,size_t item_size)
{
	curve_init(dynarr,item_size,item_size*length);
	dynarr->len=length;
	while((--length!=-1)&&(callback(dynarr->path+length*dynarr->item_size,param,length,item_size)));
	return (length==-1);
}

/*
	dynarr_createFromString
locking:
	dynarr will be locked upon successful creation
note:
	If you have two delimiters following eachother, the empty string
	will only be in the dynarr if the allow_empty_strings is true.
*/
int
dynarr_createFromString (dynarr_p dynarr,const char* str,char delim,int allow_empty_strings)
{
	char *newstr;
	char **item;
	size_t len;
	curve_init(dynarr,sizeof(str),0);
	if(str)
		while(*str)
		{
			newstr=strchr(str,delim);
			if(!newstr)
				len=strlen(str);
			else
				len=newstr-str;
			if(len||allow_empty_strings)
			{
				newstr=malloc(len+1);
				if(!newstr)
				{
					dynarr_free(dynarr);
					return 0;
				}
				strncpy(newstr,str,len);
				newstr[len]=0;
				item = curve_append(dynarr);
				if(!item)
				{
					dynarr_free(dynarr);
					return 0;
				}
				*item=newstr;
				str+=len;
			}
			if(*str)
				str++;
		}
	return 1;
}

/*
	dynarr_createFromArray
locking:
	dynarr will be locked upon successful creation
*/
int
dynarr_createFromArray (dynarr_p dynarr,void* argv,size_t item_size,size_t argc)
{
	curve_init(dynarr,item_size,item_size*argc);
	dynarr->len=argc;
	memcpy(dynarr->path,argv,item_size*argc);
	return 0;
}

static size_t
arraylength (void*array,size_t item_size)
{
	size_t len=0;
	char zero[item_size];
	memset(zero,0,item_size);
	while(memcmp(array,zero,item_size))
	{
		array+=item_size;
		len++;
	}
	return len;
}

/*
	dynarr_createFromNullArray
locking:
	dynarr will be locked upon successful creation
note: redo this more efficiently
*/
int
dynarr_createFromNullArray (dynarr_p dynarr,void* array,size_t item_size)
{
	return dynarr_createFromArray(dynarr,array,item_size,arraylength(array,item_size));
}

/*
	dynarr_toString
locking:
	dynarr must be locked before calling this function
*/
char*
dynarr_toString (dynarr_p dynarr,char* strfunc(void*,void*),char delim,void* param)
{
	size_t l=dynarr->len,lens[l],total=0;
	char *strs[l],*str=0;
	while(--l!=-1)
	{
		strs[l]=strfunc(dynarr->path+l*dynarr->item_size,param);
		lens[l]=strlen(strs[l]);
		total+=lens[l];
		total++;
	}
	str=malloc(total);
	total=0;
	while(++l!=dynarr->len)
	{
		strcpy(str+total,strs[l]);
		total+=lens[l];
		if(l!=dynarr->len-1)
			str[total++]=delim;
	}
	str[total]=0;
	return str;
}


AMPlugin*
curve_get_plugin (dynarr_t* curve)
{
	if(!curve->plugin) printf("%s(): *** curve->plugin NULL\n", __func__);
	return curve->plugin;
}

