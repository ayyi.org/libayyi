/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://ayyi.org               |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __am_private__
#include <model/common.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_dbus.h>
#include "ayyi/ayyi_dbus_proxy.h"

#include "model/plugin.h"
#include "model/track_list.h"
#include "model/song.h"
#include "model/am_client.h"


/**
 *  am_init: (skip)
 *  @arg0: (nullable)
 *
 *  If services is null, the default ayyi-ardour service will be used.
 *
 *  Returns: (skip)
 */
const AyyiClient*
am_init (AyyiService** services)
{
	static bool done = false;
	g_return_val_if_fail(!done, NULL);

	const AyyiClient* a = ayyi_client_init();

	ayyi_client_add_services(services);

	am_plugin__init();
	am_song__new();

	am_song__add_handlers();

	done = true;

	return a;
}


void
am_uninit ()
{
	ayyi_client_disconnect();

	g_clear_pointer(&song->parts, g_object_unref);
	g_clear_pointer(&song->tracks, g_object_unref);
	g_clear_pointer(&song->pool, g_object_unref);
}


/**
 *  am_connect_services
 *  @arg0: (nullable) (scope async)
 *  @arg1: (nullable) (scope async)
 *
 *  Convenience function that will establish both messaging, and shm.
 *  The shm callback is only called once _all_ segments are attached.
 *  -it is the same as calling am_connect_messaging and am_get_shm.
 */
void
am_connect_services (AyyiService** services, AyyiHandler2 on_connect, gpointer _user_data)
{
	g_return_if_fail(services && services[0]);

	typedef struct
	{
		AyyiHandler2  on_all_shm;
		gpointer      user_data;
		int           timeout;
		AyyiService** services;
		int           s;
		AyyiHandler2  on_messaging;
		GError*       error;  // currently only the first error is forwarded.
	} C;

	void clear_timeout (C* c)
	{
		am_source_remove0(c->timeout);
	}

	void cleanup (C* c)
	{
		clear_timeout(c);
		g_free(c);
	}

	gboolean am_connect_timeout (gpointer _c)
	{
		C* c = _c;
		call(c->on_all_shm, c->error, c->user_data);

		//TODO free c - check is always safe
		return G_SOURCE_REMOVE;
	}

	void am__next_service (C* c)
	{
		dbg(2, "s=%i", c->s + 1);
		if(c->services[++c->s])
			am_connect_messaging(c->services[c->s], c->on_messaging, c, NULL);
		else{
			dbg(2, "no more services");
			cleanup(c);
		}
	}

	void am_connect_services__on_shm (AyyiShmCallbackData* d)
	{
		C* c = d->user_data;

		if(!d->shm_seg){
			// its too late to set the GError here. Error is indicated by NULL segment.
			c->on_all_shm(c->error, c->user_data);
			cleanup(c);
			return;
		}

		bool
		service_shm_setup_is_complete (AyyiService* service)
		{
			dbg(1, "shm.song=%p ((AyyiSongService*)ayyi.service)->amixer=%p", ((AyyiSongService*)ayyi.service)->song, ((AyyiSongService*)ayyi.service)->amixer);

			if(service == known_services[AYYI_SERVICE_MIXER]){
				GList* l = service->segs;
				for(;l;l=l->next){
					AyyiCShmSeg* seg = l->data;
					if(!seg->attached || seg->invalid){ dbg(2, "not complete."); return false; }
				}
				return g_list_length(service->segs) >= G_N_ELEMENTS(((AyyiMixerService*)service)->segments);
			}
			return ((((AyyiSongService*)service)->song) && (((AyyiSongService*)service)->amixer));
		}

		if(service_shm_setup_is_complete(d->service)){
			clear_timeout(c);
			//am__register_signals(ayyi.service);
			for(int i=0;c->services[i];i++){
				if(c->services[i] == known_services[AYYI_SERVICE_AYYID1]){ //FIXME create a service_load fn?
					am_song__load();
					break;
				}
			}
			c->on_all_shm(c->error, c->user_data);
		}

		am__next_service(c);
	}

	void am_on_messaging (const GError* error, gpointer _c)
	{
		C* c = _c;

		if(error){
			pwarn("error");
			c->error = g_error_copy(error);
			call(c->on_all_shm, c->error, c->user_data);
		}else{
			AyyiService* service = c->services[c->s];
			am_get_shm(service, am_connect_services__on_shm, c);
		}
	}

	C* c = AYYI_NEW(C,
		.on_all_shm = on_connect,
		.user_data = _user_data,
		.services = services,
		.s = -1,
		.on_messaging = am_on_messaging
	);
	c->timeout = g_timeout_add(4000, am_connect_timeout, c);

	am__next_service(c);
}


/**
 *  am_connect_all:
 *  @arg0: (nullable) (scope async)
 *
 *  Convenience function that will establish both messaging, and shm for the Ardourd song and mixer services.
 *  The shm callback is only called once _all_ segments are attached.
 *  -is the same as calling am_connect_messaging and am_get_shm.
 *
 *  If you wish to specify which services to connect to, use am__connect_services instead
 *
 *  To check for errors, check the GError argument in the callback.
 */
void
am_connect_all (AyyiHandler2 on_connect, gpointer user_data)
{
	AyyiService* services[] = {
		(AyyiService*)&ardourd_song,
		(AyyiService*)&ardourd_mixer,
		NULL,
	};

	typedef struct
	{
		AyyiHandler2 user_callback;
		gpointer     user_data;
		int          timeout;
	} C;

	void am__connect_connected (const GError* error, gpointer _c)
	{
		C* c = _c;
		call(c->user_callback, error, c->user_data);
		g_free(c);
	}

	am_connect_services((AyyiService**)&services, am__connect_connected, AYYI_NEW(C,
		.user_callback = on_connect,
		.user_data = user_data,
	));
}


/**
 *  am_connect_messaging:
 *  @arg1: (nullable) (scope async)
 *
 *  Currently this should not to be used for connecting to plugins because of the signal registration.
 */
bool
am_connect_messaging (AyyiService* service, AyyiHandler2 callback, gpointer user_data, GError** Xerror)
{
	static bool signal_reg_done = false;

	HandlerData* c = AYYI_NEW(HandlerData,
		.callback = (AyyiHandler)callback,
		.user_data = user_data
	);

	void am__connect_messaging__on_connected (const GError* error, gpointer user_data)
	{
		PF;
		HandlerData* c = user_data;
		call((AyyiHandler2)c->callback, error, c->user_data);
		g_free(c);
	}

	if(ayyi_client_connect(service, am__connect_messaging__on_connected, c)){
		if(!signal_reg_done) ayyi_client_set_signal_reg(NEW | DELETED | CHANGED | TRANSPORT | LOCATORS | PROPERTY | PROGRESS);
		signal_reg_done = true;
		return true;
	}
	return false;
}


void
am_get_shm (AyyiService* service, void (*on_shm)(AyyiShmCallbackData*), gpointer user_data)
{
	ayyi_client__dbus_get_shm(service, on_shm, user_data);
}


bool
am_get_shm_ (AyyiService** services, void (*on_shm)(AyyiShmCallbackData*), gpointer user_data)
{
	g_return_val_if_fail(services, false);

	int i; for(i=0;services[i];i++){
		if(!ayyi_client__dbus_get_shm(services[i], on_shm, user_data)) return false;
	}
	return true;
}


/**
 *  am_init_2: (rename-to am_init)
 *  @services: (nullable)
 *
 *  If services is null, the default ayyi-ardour service will be used.
 */
void
am_init_2 (AyyiService** services)
{
	am_init(services);
}
