/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_time_h__
#define __am_time_h__

#include "glib.h"
#include "ayyi/ayyi_time.h"
#include "model/model_types.h"

#define SUBS_PER_BEAT 4           // a gui subbeat is a 16th

#ifdef __am_time_c__
const uint64_t mu_per_sub     = (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT) / 4;
const uint32_t ticks_per_beat = 4 * AYYI_TICKS_PER_SUBBEAT;
#endif

int            pos_beat             (AMPos*);

void           pos2bbst             (AMPos*, char* str);
void           pos2bbst0            (AMPos*, char* str);
void           pos2bbs              (AMPos*, char* str);
uint64_t       pos2mu               (AMPos*);
double         pos2beats            (AMPos*);
bool           songpos_ayyi2gui     (AMPos*, AyyiSongPos*);
AyyiSongPos*   songpos_gui2ayyi     (AyyiSongPos*, AMPos*);
bool           am_pos_is_valid      (AMPos*);
bool           am_pos_is_empty      (AMPos*);

bool           pos_is_before        (const AMPos* pos1, const GPos* pos2);
bool           pos_is_after         (const AMPos* pos1, const GPos* pos2);

void           mu2pos               (uint64_t len, GPos* pos);
void           samples2pos          (uint64_t samples, GPos*);
unsigned       pos2samples          (GPos*);

void           pos_add              (GPos*, const GPos*);
void           pos_subtract         (GPos*, const GPos*);
void           pos_divide           (GPos*, int);
int            pos_diff             (const GPos* pos1, const GPos* pos2);
gboolean       pos_cmp              (const GPos*, const GPos*);
void           pos_min              (GPos*, const GPos*);
void           pos_max              (GPos*, const GPos*);

long           samples2beats        (uint64_t samples);
double         samples2secs         (uint64_t samples);
uint64_t       bars2mu              (float bars);

gboolean       bbst2pos             (const char* bbst, GPos*);
gboolean       bbss2pos             (const char* bbst, AyyiSongPos*);

bool           bbst_increment       (char*);
bool           bbst_decrement       (char*);

void           am_q_init            ();
gboolean       q_to_nearest         (GPos*, QMap*);
void           q_to_prev            (GPos*, QMap*);
void           q_to_next            (GPos*, QMap*);
void           q_to_prev_or_current (GPos*, QMap*);
void           q_to_next_or_current (GPos*, QMap*);

void           am_snap_to_next      (GPos*);
GPos*          am_snap              (GPos*);
AyyiSongPos*   am_snap_             (AyyiSongPos*);

#endif
