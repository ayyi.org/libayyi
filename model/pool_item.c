/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "model/common.h"
#include <libgen.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sndfile.h>

#include "waveform/utils.h"
#include "waveform/private.h" // for waveform_get_n_audio_blocks
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_log.h>
#include "model/pool.h"
#include "model/song.h"
#include "model/time.h"
#include "model/track.h"
#include "model/part_manager.h"
#include "model/pool_item.h"

extern int    peak_mem_size;

static void   pool_item_sync              (AMPoolItem*);
static bool   pool_item_set_file_info     (AMPoolItem*);
static bool   pool_item_set_filename      (AMPoolItem*, const char* fname);
static void   pool_item_set_paths         (AMPoolItem*, char* path);
static gchar* pool_item_find_peakfile     (AMPoolItem*, int ch_num);
static bool   pool_item_generate_rms_file (AMPoolItem*, int ch_num);
static bool   pool_item_rms_file_is_valid (AMPoolItem*, const char* rms_file);
static void   pool_item_get_rms_filename  (char* peakname, const char* filename, int ch_num);
#if 0
static void   pool_item_monitor_waveform  (AMPoolItem*);
#endif

// for rms calculatation
enum sample_type {
    SAMPLE_TYPE_INT_8,
    SAMPLE_TYPE_INT_16,
    SAMPLE_TYPE_INT_32,
    SAMPLE_TYPE_FLOAT_32
};
typedef int AFframecount;
typedef int8_t rms_unit_t;

static void sample_calc_rms (rms_unit_t*, enum sample_type type, const void* buf, AFframecount count, float scale);

static gpointer am_pool_item_parent_class = NULL;

static void am_pool_item_finalize (GObject* obj);


AMPoolItem*
am_pool_item_construct (GType object_type)
{
	AMPoolItem* item     = (AMPoolItem*)waveform_construct(object_type);
	item->state          = AM_POOL_ITEM_NEW;
	item->peakfile_valid = false;
	item->pod_index[0]   = -1;
	item->pod_index[1]   = -1;

	return item;
}


/**
 *  am_pool_item_new: (constructor)
 *
 *  Return value: (transfer full)
 */
AMPoolItem*
am_pool_item_new (void)
{
	return am_pool_item_construct (AM_TYPE_POOL_ITEM);
}


static void
am_pool_item_class_init (AMPoolItemClass * klass)
{
	am_pool_item_parent_class = g_type_class_peek_parent (klass);
	G_OBJECT_CLASS (klass)->finalize = am_pool_item_finalize;
}


static void
am_pool_item_instance_init (AMPoolItem * self) {
}


static void
am_pool_item_finalize (GObject* obj)
{
	G_OBJECT_CLASS (am_pool_item_parent_class)->finalize (obj);
}


GType
am_pool_item_get_type (void)
{
	static volatile gsize am_pool_item_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&am_pool_item_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (AMPoolItemClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) am_pool_item_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (AMPoolItem), 0, (GInstanceInitFunc) am_pool_item_instance_init, NULL };
		GType am_pool_item_type_id;
		am_pool_item_type_id = g_type_register_static (TYPE_WAVEFORM, "AMPoolItem", &g_define_type_info, 0);
		g_once_init_leave (&am_pool_item_type_id__volatile, am_pool_item_type_id);
	}
	return am_pool_item_type_id__volatile;
}


/**
 *  am_pool_item_new_from_ayyi: (skip)
 */
AMPoolItem*
am_pool_item_new_from_ayyi (AyyiFilesource* file)
{
	AMPoolItem* pool_item = am_pool_item_new();
	dbg(2, "item=%p", pool_item);
	pool_item->pod_index[0] = file->shm_idx;

	pool_item_sync(pool_item);

	if (pool_item_file_exists(pool_item)) {
		if (!pool_item_set_file_info(pool_item)) {
			g_object_unref(pool_item);
			return NULL;
		}
	}
	else pwarn ("file doesnt exist: filename='%s'", pool_item->filename);

	// check this is not a dupe
	if(am_pool__item_exists(pool_item->filename)){
		dbg (2, "file is a dupe: '%s'", pool_item->filename); 
		g_object_unref(pool_item);
		return NULL;
	}

	char* fullpath = am_pool_item_get_full_path(pool_item, 0);
	waveform_set_file((Waveform*)pool_item, fullpath);
#if 0
	pool_item_monitor_waveform(pool_item);
#endif

	if(!pool_item->state){
		pool_item_load_peak(pool_item, 0);
		pool_item->state = AM_POOL_ITEM_OK;
	}

	g_free(fullpath);
	return pool_item;
}


#if 0
static void
pool_item_monitor_waveform (AMPoolItem* pool_item)
{
	//temporarily used for debugging

	void pool_item_finalize_notify(gpointer _pi, GObject* was)
	{
		dbg(1, "waveform finalised! %s waveform=%p", ((AMPoolItem*)_pi)->leafname, ((AMPoolItem*)_pi)->waveform);
		//((AMPoolItem*)_pi)->waveform = NULL; // not needed
		g_return_if_fail(!((AMPoolItem*)_pi)->waveform);
	}

	g_object_weak_ref((GObject*)pool_item->waveform, pool_item_finalize_notify, pool_item);
}
#endif


void
am_pool_item_set_from_ayyi (AMPoolItem* item, AyyiFilesource* file)
{
	PF;
	pool_item_set_paths(item, file->name);

	pool_item_add_channel(item, file);

	AyyiFilesource* f2 = ayyi_file_get_linked(file);
	if(f2) pool_item_add_channel(item, f2);

	pool_item_sync(item);
	item->state = AM_POOL_ITEM_OK;

	AYYI_DEBUG pool_item_print(item);
}


bool
am_pool_item_set_file (AMPoolItem* pool_item, const char* fname, bool fast)
{
	pool_item_set_paths(pool_item, (char*)fname);

	if(!fast){
		if(g_file_test(fname, G_FILE_TEST_EXISTS)) pool_item_set_file_info(pool_item);
	}

	pool_item_set_filename(pool_item, fname);

	// check this is not a dupe
	if(!fast){
		if(am_pool__item_exists(pool_item->filename)){
			dbg (0, "file is a dupe: %s. Aborting...", pool_item->filename); 
			return FALSE;
		}
	}

	waveform_set_file((Waveform*)pool_item, fname);

	return true;
}


static bool
pool_item_set_filename (AMPoolItem* pool_item, const char* fname)
{
	//changelog: -we now update leafname here.
	//           -we now dont check the file exists. Should be done elsewhere.

	g_return_val_if_fail(strlen(fname), FALSE);
	g_return_val_if_fail(song->audiodir, FALSE);

	// if the file is within the audio root dir, store the relative path:
	if(fname[0]=='/'){
		dbg (2, "path is absolute.");
		if(strstr(fname, song->audiodir) == fname){
			gchar* relative = g_strdup_printf("%s", fname + strlen(song->audiodir) + 1); //+1 for the "/" char.
			dbg (0, "using relative path: %s.", relative);
			g_strlcpy(pool_item->filename, relative, 256);
			g_free(relative);
		}else{
			g_strlcpy(pool_item->filename, fname, 256);
		}

	}else{
		//path is not absolute.
		dbg (2, "path is not absolute.");
		g_strlcpy(pool_item->filename, fname, 256);
	}
	dbg (2, "filename=%s", pool_item->filename);

	pool_item_set_paths(pool_item, pool_item->filename);

	return true;
}


void
pool_item_add_channel (AMPoolItem* pool_item, AyyiFilesource* file)
{
	int c; for(c=0;c<2;c++){
		if(!pool_item->source_id[c]) break;
	}
	g_return_if_fail(c < 2);

	pool_item->source_id[c] = file->id;
	pool_item->pod_index[c] = file->shm_idx;
	pool_item->channels     = c + 1;

	if(!c) pool_item_load_peak(pool_item, c);

#if 0
	pool_item_print(pool_item);
#endif
}


static void
pool_item_set_paths (AMPoolItem* pool_item, char* path)
{
	g_return_if_fail(pool_item);
	dbg (2, "path=%s", path);

	if(path != pool_item->filename){ // dont pass pool_item->filename as an arg!
		g_strlcpy(pool_item->filename, path, 256);
	}

	audio_path_get_leaf(path, pool_item->leafname); 
}


static bool
pool_item_set_file_info (AMPoolItem* pool_item)
{
	//uses libsndfile to get the number of samples, etc, in the given pool item.

	g_return_val_if_fail(pool_item, false);
	g_return_val_if_fail(pool_item->filename, false);

	Waveform* waveform = (Waveform*)pool_item;

	if(pool_item_is_raw(pool_item)){
		pwarn ("FIXME file info not set for raw file.");
		return false;
	}

	char* filename = am_audio_path_get_full(pool_item->filename);

	SF_INFO sfinfo;
	sfinfo.format  = 0;

	SNDFILE* sffile;
	if(!(sffile = sf_open(filename, SFM_READ, &sfinfo))){
		pool_item->state = AM_POOL_ITEM_INACCESSIBLE;
		pwarn ("%s (%s)", sf_strerror(NULL), filename);
		goto out;
	}

	AYYI_DEBUG_2 printf("%iHz %s frames=%i\n", sfinfo.samplerate, am_format_channel_width(sfinfo.channels), (int)sfinfo.frames);
	if(!pool_item->channels) pool_item->channels = sfinfo.channels; // ->channels can be the combined width of more than one file.
	waveform->samplerate = sfinfo.samplerate;
	waveform->n_frames   = sfinfo.frames;
	waveform->n_channels = sfinfo.channels;
#if 0
	pool_item->bytes_per_sample = 0; //FIXME
#endif

	if(pool_item->channels<1 || pool_item->channels>100){ pwarn ("bad channel count: %i", pool_item->channels); goto out; }
	if(sf_close(sffile)) pwarn ("bad file close.");

	g_free(filename);
	return true;

out:
	g_free(filename);
	return false;
}


static void
pool_item_sync (AMPoolItem* item)
{
	g_return_if_fail(item->pod_index[0] > -1);
	AyyiFilesource* filesource = ayyi_song__filesource_at(item->pod_index[0]);
	g_return_if_fail(filesource);

#if 0 // the filename is no longer modified. use orig_name if you need filename without %L, %R.

	char fname[AYYI_FILENAME_MAX];
	g_strlcpy(fname, filesource->name, AYYI_FILENAME_MAX);

	//strip off the channel number from the end
	char* a[] = {"%L", "%R"};
	for(int c=0;c<G_N_ELEMENTS(a); c++){
		gchar* pos = g_strrstr(fname, a[c]);
		if(pos) *pos = '\0';
	}
#endif

	am_pool_item_set_file(item, filesource->name, 1); // 1 = fast

	item->source_id[0] = filesource->id;
	dbg (2, "%u: source_id=%Lu (%s)", filesource->shm_idx, item->source_id[0], item->leafname);
}


/*
 *  Does the file represented by this pool item exist in the file system?
 */
bool
pool_item_file_exists (AMPoolItem* item)
{
	// try core filename - note that item->filename may not be the full path.
	if(g_file_test(item->filename, G_FILE_TEST_EXISTS)) return true;

	// try audio_home directory
	char try_path[256];
#pragma GCC diagnostic ignored "-Wformat-truncation"
	snprintf(try_path, 255, "%s/%s", song->audiodir, item->leafname);
#pragma GCC diagnostic warning "-Wformat-truncation"
	dbg (2, "try_path='%s'", try_path);
	if(g_file_test(try_path, G_FILE_TEST_EXISTS)) return true;

	return false;
}


/*
 *  Return the peakfile name, and set the relevant pool_item properties.
 */
static gchar*
pool_item_find_peakfile (AMPoolItem* pool_item, int ch_num)
{
	char leafname[256];
	if(ch_num){
		AyyiFilesource* filesource = ayyi_song__filesource_at(pool_item->pod_index[0]);
		char other[256];
		ayyi_file_get_other_channel(filesource, other, 256);
		strcpy(leafname, basename(other));
	}
	else strcpy(leafname, pool_item->leafname);

	char peakpath[256] = {0,};
#pragma GCC diagnostic ignored "-Wformat-truncation"
	snprintf(peakpath, 255, "%s/%s", song->peaksdir, leafname);
#pragma GCC diagnostic warning "-Wformat-truncation"

	// for some reason, peakfiles now have a "%A" in them!
	// (perhaps only if they are recorded rather than imported?)
	char* dot; if((dot = g_strrstr(peakpath, "."))) sprintf(dot, "%%A.");

	filename_change_extension(peakpath, "peak");
	if(!g_file_test(peakpath, G_FILE_TEST_EXISTS)){ 

		// try truncating at the first dot instead of the last.
		// -this seems wrong, but is apparently how Ardour works.
#pragma GCC diagnostic ignored "-Wformat-truncation"
		snprintf(peakpath, 256, "%s/%s", song->peaksdir, leafname);
#pragma GCC diagnostic warning "-Wformat-truncation"
		dot = strstr(peakpath, ".");
		if(!dot) return NULL;
		*dot = '\0';
		filename_change_extension(peakpath, "peak");

		if(!g_file_test(peakpath, G_FILE_TEST_EXISTS)){
			char a[256] = {'\0',};
			snprintf(a, 255, "/%s%%A", basename(leafname));
			gchar* hashed = g_compute_checksum_for_string(G_CHECKSUM_SHA1, a, -1);
			snprintf(peakpath, 255, "%s/%s.peak", song->peaksdir, hashed);
			g_free(hashed);

			if(!g_file_test(peakpath, G_FILE_TEST_EXISTS)){ 
				//it is quite valid for there temporarily to be no peakfile.
				AYYI_DEBUG_2 pwarn ("cannot find peak file (%s)", peakpath);
				return NULL;
			}
		}
	}

	pool_item->peakfile_valid  = true;

	return g_strdup(peakpath);
}


/*
 *  Returns a newly allocated list of idxs to songcore audio events (regions) for the given file.
 *
 *  Note: currently we get the regions directly from the engine - the local client does not independently store region info.
 */
GList*
pool_item_get_regions (AMPoolItem* pool_item)
{
	g_return_val_if_fail(pool_item, NULL);

	// find all the shared regions that have a source_id the same as our pool_item.
	return am_partmanager__get_regions_by_pool_item(pool_item);
}


int
pool_item_count_regions (AMPoolItem* pool_item)
{
	g_return_val_if_fail(pool_item, 0);
	if (!pool_item->source_id[0]) { pwarn ("pool item has missing id. Cannot get regions without id. pool_item='%s'", pool_item->leafname); return 0; }

	int count = 0;
	AyyiAudioRegion* region = NULL;
	while ((region = ayyi_song__audio_region_next(region))) {
		if (region->source0 == pool_item->source_id[0]) {
			count++;
		}
	}
	dbg (2, "pool_item '%s' has %i regions.", pool_item->leafname, count);
	return count;
}


/*
 *  Return a reference to the region representing a whole file.
 *
 *  This assumes that the default region is first in the array. Strictly speaking i guess this isnt guaranteed.
 */
bool
pool_item_get_default_region (AMPoolItem* pool_item, uint32_t* region_idx)
{
	g_return_val_if_fail(pool_item, false);

	AyyiAudioRegion* region = NULL;
	while((region = ayyi_song__audio_region_next(region))){
		if(region->source0 == pool_item->source_id[0]){
			*region_idx = region->shm_idx;
			return true;
		}
	}

	dbg (0, "no shared regions found for pool_item '%s'", pool_item->leafname);
	return 0;
}


bool
pool_item_is_raw (AMPoolItem* pool_item)
{
	char extn[8];
	filename_get_extension(pool_item->filename, extn);

	return strcmp(extn, "raw") ? false : true;
}


int
pool_item_get_width (AMPoolItem* pool_item)
{
	if(pool_item->source_id[1]) return 2;
	if(pool_item->source_id[0]) return 1;
	return 0;
}


/*
 *  Result must be free'd with g_free().
 */
char*
am_pool_item_get_full_path (AMPoolItem* pool_item, int ch_num)
{
	char* src = NULL;
	char f[AYYI_FILENAME_MAX];

	switch(ch_num){
		case 0:
			src = pool_item->filename;
			break;
		case 1:
			;AyyiFilesource* file = ayyi_song__filesource_at(pool_item->pod_index[0]);
			if(file){
				ayyi_file_get_other_channel(file, f, AYYI_FILENAME_MAX);
				src = f;
			}
			break;
		default:
			pwarn("bad channel number: %i", ch_num);
			return NULL;
	}

	char* fullpath = NULL;
	if(src[0] == '/'){
		// absolute path.
		fullpath = g_strdup(src);
	}else{
		// relative paths are relative to the song default audio directory.
		fullpath = g_strdup_printf("%s/%s", song->audiodir, src);
	}

	return fullpath;
}


void
pool_item_get_display_name (AMPoolItem* pool_item, char* name)
{
	char* d = replace(pool_item->leafname, "%L", "");
	g_strlcpy(name, d, AYYI_FILENAME_MAX);
	d = replace(name, "-L.", ".");
	g_strlcpy(name, d, AYYI_FILENAME_MAX);
}


/*
 *  Look through the Ayyi file list for the filesource_id of our pool_item.
 */
bool
pool_item_in_core (AMPoolItem* item)
{
	g_return_val_if_fail(item, false);

	AyyiFilesource* test = NULL;
	while((test = ayyi_song__filesource_next(test))){
		if(test->id == item->source_id[0]) return true;
	}

	return false;
}


bool
pool_item_is_nonempty (AMPoolItem* item, gpointer user_data)
{
	return ((Waveform*)item)->n_frames;
}


/*
 *  Return pool items that are POOL_ITEM_NEW, or POOL_ITEM_PENDING
 */
bool
pool_item_is_new (AMPoolItem* item, gpointer user_data)
{
	return item->state <= AM_POOL_ITEM_PENDING;
}


/*
 *  Load the peak file for the given poolitem into a buffer.
 */
#define USE_CLIENTSIDE_PEAKDATA
#ifndef USE_CLIENTSIDE_PEAKDATA
void
pool_item_load_peak (AMPoolItem* pool_item, int ch_num)
{
	char* filename = pool_item->filename;
	dbg (2, "ch=%i filename=%s", ch_num, filename);
	char* fullpath = am_pool_item_get_full_path(pool_item, 0);

	if(!g_file_test(fullpath, G_FILE_TEST_EXISTS)){ 
		pwarn ("audio file doesnt exist! (%s)", fullpath);
		pool_item->valid = false;
	} else pool_item->valid = true;

	pool_item->peakfile_valid = false; // the peak file can still be valid even if the pool_item is not.

	gchar* peak_file = pool_item_find_peakfile(pool_item, ch_num);
    if(!peak_file) goto out;

	pool_item->peakfile_valid = true;

	if(pool_item->waveform){
		waveform_load_peak(pool_item->waveform, peak_file, ch_num);

		pool_item->max_db = waveform_find_max_audio_level(pool_item->waveform);
	}

	//dbg (2, "done. %s: %isamples (%li beats / %.3f secs).", filename, pool_item->peak_buflen, samples2beats(pool_item->peak_buflen), samples2secs(pool_item->peak_buflen*WF_PEAK_RATIO));

	g_free(peak_file);

	pool_item_generate_rms_file(pool_item, ch_num);

  out:
	g_free(fullpath);
}
#else
void
pool_item_load_peak (AMPoolItem* pool_item, int ch_num)
{
#ifdef DEBUG
	char* filename = pool_item->filename;
	dbg (2, "ch=%i filename=%s", ch_num, filename);
#endif

	char* fullpath = am_pool_item_get_full_path(pool_item, 0);

	if(!g_file_test(fullpath, G_FILE_TEST_EXISTS)){ 
		pwarn ("audio file doesnt exist! (%s)", fullpath);
		pool_item->state = AM_POOL_ITEM_INACCESSIBLE;
	} else pool_item->state = AM_POOL_ITEM_OK;

	pool_item->peakfile_valid = false; //the peak file can still be valid even if the pool_item is not.

	gchar* peak_file = pool_item_find_peakfile(pool_item, ch_num);
    if(!peak_file) goto out;

	pool_item->peakfile_valid = true;

	//waveform_load_peak((Waveform*)pool_item, peak_file, ch_num);
	void on_peak_load(Waveform* w, GError* error, gpointer ch_num)
	{
		AMPoolItem* pi = (AMPoolItem*)w;

		pool_item_generate_rms_file(pi, GPOINTER_TO_INT(ch_num));
	}
	waveform_load((Waveform*)pool_item, on_peak_load, GINT_TO_POINTER(ch_num));

	//dbg (2, "done. %s: %isamples (%li beats / %.3f secs).", filename, pool_item->peak_buflen, samples2beats(pool_item->peak_buflen), samples2secs(pool_item->peak_buflen*WF_PEAK_RATIO));

	g_free(peak_file);

  out:
	g_free(fullpath);
}
#endif


const char*
pool_item_print_state (const AMPoolItem* item)
{
	static char* states[] = {"POOL_ITEM_NEW", "POOL_ITEM_PENDING", "POOL_ITEM_BAD", "POOL_ITEM_INACCESSIBLE", "POOL_ITEM_OK"};
	g_return_val_if_fail(G_N_ELEMENTS(states) == AM_POOL_ITEM_STATE_MAX, NULL);
	g_return_val_if_fail(item->state < AM_POOL_ITEM_STATE_MAX, NULL);
	return states[item->state];
}


#if 0
const char*
pool_item_print_state(unsigned state)
{
  static char states[5][32] = {"POOL_ITEM_NEW", "POOL_ITEM_PENDING", "POOL_ITEM_BAD", "POOL_ITEM_INACCESSIBLE", "POOL_ITEM_OK" };
  if(state > 4) return NULL;
  return states[state];
}
#endif


static void
am_format_frames_as_time (char* formatted, uint64_t samples)
{
	g_return_if_fail(formatted);
	if(!samples){ formatted[0] = '\0'; return; }

	int secs_i = samples / song->sample_rate;
	int ms_i   = samples % song->sample_rate;

	char secs_str[12] = {0,};
	char mins_str[24] = "";
	int mins_i = secs_i / 60;
	if(mins_i){
		snprintf(mins_str, 23, "%i:", mins_i);
		secs_i = secs_i % 60;
	}
	snprintf(secs_str, 11, "%02i", secs_i);

	snprintf(formatted, 47, "%s%s.%03i", mins_str, secs_str, ms_i);
}


void
pool_item_print (const AMPoolItem* item)
{
	g_return_if_fail(item);

	char time[48];
	am_format_frames_as_time(time, waveform_get_n_frames((Waveform*)item));
	printf("%s(): name=%s%s%s chs=%i %i idx=%i,%i buf=%s,%s state=%s samplecount=%"PRIi64" nblocks=%i %s\n", __func__, bold, item->leafname, white, item->channels, ((Waveform*)item)->n_channels, item->pod_index[0], item->pod_index[1], waveform_peak_is_loaded((Waveform*)item, 0) ? "y" : "n", waveform_peak_is_loaded((Waveform*)item, 1) ? "y" : "n", pool_item_print_state(item), ((Waveform*)item)->n_frames, waveform_get_n_audio_blocks((Waveform*)item), time);
}


#define RMS_DATA_SIZE_BYTES 1
#define RMS_READ_SIZE 256

static bool
pool_item_generate_rms_file (AMPoolItem* pool_item, int ch_num)
{
	bool ok = true;
	char* src_path = am_pool_item_get_full_path(pool_item, ch_num);

	char rms_file[256];
	pool_item_get_rms_filename(rms_file, src_path, ch_num);
	dbg(2, "rms_file=%s", rms_file);

	if(pool_item_rms_file_is_valid(pool_item, rms_file)) goto out;

	SF_INFO       sfinfo;
	SNDFILE       *sffile;
	sfinfo.format = 0;

	int maxval = 0;

	if(!(sffile = sf_open(src_path, SFM_READ, &sfinfo))){
		ayyi_log_print(LOG_FAIL, "not able to open file");
		dbg (0, "not able to open input file %s. %s", rms_file, sf_strerror(NULL));
		ok = false;
		goto out;
	}

	FILE* fp = fopen(rms_file, "wb");
	if(!fp){
		ayyi_log_print(LOG_FAIL, "cannot open rms file for writing (%s).\n", rms_file);
		ok = false;
		goto out;
	}

	rms_unit_t out[RMS_READ_SIZE / WF_PEAK_RATIO];
    int readcount;
	int l = 0;
    int16_t* sf_data = g_malloc(sizeof(*sf_data) * RMS_READ_SIZE);
    float*  sf_dataf = g_malloc(sizeof(*sf_dataf) * RMS_READ_SIZE); //FIXME not freed?
    while ((readcount = sf_read_float(sffile, sf_dataf, RMS_READ_SIZE))){

		if(sf_error(sffile)){ pwarn("read error"); break; }

		int j; for(j=0;j<readcount;j++){
			sf_data[j] = sf_dataf[j] * (1 << 16);
		}

		//strictly speaking, the average should be calculated after display scaling - not here.
		float scale = WF_PEAK_RATIO;
		sample_calc_rms(out, SAMPLE_TYPE_INT_16, sf_data, RMS_READ_SIZE, scale);

		int write_size = MAX(readcount / WF_PEAK_RATIO, 1);
		if (fwrite(&out, RMS_DATA_SIZE_BYTES, write_size, fp) != write_size){
			pwarn("bad write.");
			break;
		}

		int i; for(i=0;i<readcount;i++){
			maxval = MAX(sf_data[i] >> 8, maxval);
		}
		dbg(3, "%i read=%i maxval=%i", l, readcount, maxval);
		l++;
	}
	g_free(sf_data);
	g_free(sf_dataf);

	if(sf_close(sffile)) pwarn ("bad file close.");
	fclose(fp);
  out:
	g_free(src_path);
	return ok;
}


//----------------------------------------------------------------


/* If not defined, sample_calc_rms calculates absolute averages. */

#define ENABLE_RMS

#ifndef ENABLE_RMS
#define RMS_INT(type, shift_c) \
    for(j = 0; j < q; j++) \
        sum += ABS(((type *)buf)[j]); \
    shift = shift_c

#else
#define RMS_INT(type, shift_c) \
    for(j = 0; j < q; j++) \
        fsum += (((type *)buf)[j] >> shift_c) * (((type *)buf)[j] >> shift_c)
#endif

#define HRES_MIN                      0.0625 /* inverse power of 2 */
#define HRES_MIN_SHIFT_BITS                4 /* bits to shift x to obtain ((1/x) * HRES_MIN)! */

static int
sample_get_width (enum sample_type type)
{
	switch(type) {
		case SAMPLE_TYPE_INT_8:
			return 1;
		case SAMPLE_TYPE_INT_16:
			return 2;
		case SAMPLE_TYPE_INT_32:
			return 4;
		case SAMPLE_TYPE_FLOAT_32:
			return sizeof(float);
		default:
			perr("Unknown sample type!\n");
			return 0;
	}
}


/*
 *  Calculates the root-mean-square for a range of samples.
 *  @param rms Will receive the averages.
 *  @param buf The samples.
 *  @param count How many samples there are in buf.
 *  @param type The type of the samples in buf.
 *  @param scale The scaling factor: how many samples produce
 *  a single average value.
 */
static void
sample_calc_rms (rms_unit_t* rms, enum sample_type type, const void* buf, AFframecount count, float scale)
{
#ifndef ENABLE_RMS
    long long sum = 0;
#endif
    double fsum = 0;
    int dst_idx = 0;
    int sample_width = sample_get_width(type);
    AFframecount i, j, q;
    AFframecount segment_size = scale * (int)(1 / HRES_MIN);
#ifndef ENABLE_RMS
    int shift = 0;
#endif

    count <<= HRES_MIN_SHIFT_BITS;

    for(i = 0; i < count; i += segment_size) {

        q = MIN(segment_size, count - i) >> HRES_MIN_SHIFT_BITS;
#ifndef ENABLE_RMS
        sum = 0;
#endif
        fsum = 0;

        switch(type) {
        case SAMPLE_TYPE_INT_8:
            RMS_INT(int8_t, 0);
            break;
        case SAMPLE_TYPE_INT_16:
            RMS_INT(int16_t, 8);
            break;
        case SAMPLE_TYPE_INT_32:
            RMS_INT(int32_t, 24);
            break;
        case SAMPLE_TYPE_FLOAT_32:
            for(j = 0; j < q; j++) 
#ifdef ENABLE_RMS
                fsum += ((float *)buf)[j] * ((float *)buf)[j];
#else
                fsum += ABS(((float *)buf)[j]);
#endif
            break;
        default:
            abort();
        }

#ifdef ENABLE_RMS
        if(type == SAMPLE_TYPE_FLOAT_32)
            rms[dst_idx++] = (int)(floor(sqrt(fsum / q) * 128));
        else
            rms[dst_idx++] = (int)(floor(sqrt(fsum / q)));
#else
        if(type == SAMPLE_TYPE_FLOAT_32)
            rms[dst_idx++] = (fsum / q) * 128;
        else 
            rms[dst_idx++] = (sum / q) >> shift;
#endif

        buf += (sample_width * q);
    }
    
}


static bool
pool_item_rms_file_is_valid (AMPoolItem* pool_item, const char* rms_file)
{
	// check the file exists and is dated newer than its source.
	// Outdated files are deleted.

	g_return_val_if_fail(rms_file, false);

	bool valid =true;

	if(!g_file_test(rms_file, G_FILE_TEST_EXISTS)){ 
		dbg (2, "rms_file doesnt exist. (%s)", rms_file);
		return false;
	}

	char* src_path = am_pool_item_get_full_path(pool_item, 0);
	if(am_file_is_newer(rms_file, src_path)) goto out;

	dbg(1, "rms_file is older - deleting... (%s)", rms_file);
	if(unlink(rms_file)) pwarn("failed to delete rms cache file.");
	valid = false;

  out:
	g_free(src_path);
	return valid;
}


static void
pool_item_get_rms_filename (char* peakname, const char* filename, int ch_num)
{
	dbg(2, "%i: %s", ch_num, filename);
	snprintf(peakname, 255, "%s.rms", filename);
	peakname[255] = '\0';
}


