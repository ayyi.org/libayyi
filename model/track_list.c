/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "model/common.h"
#include "model/song.h"
#include "model/am_part.h"
#include "model/curve.h"
#include "model/track_list.h"

struct _AMTrackListPrivate {
    GType t_type;
    GBoxedCopyFunc t_dup_func;
    GDestroyNotify t_destroy_func;

    AMTrack pool[AM_MAX_TRK];

    // derived values are calculated on demand. do not access directly
    struct {
        gint       length;  // both the number of tracks and the index to the next available empty slot
    }              cache;
};

G_DEFINE_TYPE_WITH_PRIVATE (AMTrackList, am_track_list, AM_TYPE_COLLECTION)

enum  {
	AM_TRACK_LIST_DUMMY_PROPERTY,
	AM_TRACK_LIST_T_TYPE,
	AM_TRACK_LIST_T_DUP_FUNC,
	AM_TRACK_LIST_T_DESTROY_FUNC
};

GType           am_track_list_get_type           (void) G_GNUC_CONST;
AMTrackList*    am_track_list_construct          (GType, GType, GBoxedCopyFunc, GDestroyNotify);
static guint    am_track_list_real_length        (AMCollection*);
static bool     am_track_list_real_contains      (AMCollection*, gconstpointer item);
static const AMItem* am_track_list_real_find_by_id (AMCollection*, guint64 id);
static gpointer am_track_list_real_append        (AMCollection*, gconstpointer item);
static void     am_track_list_real_remove        (AMCollection*, gconstpointer item);
static void     am_track_list_real_iter_init     (AMCollection*, AMIter*);
static AMTrack* am_track_list_real_iter_next     (AMCollection*, AMIter*);
static void     am_track_list_finalize           (GObject* obj);
static void     am_track_list_get_property       (GObject*, guint property_id, GValue*, GParamSpec*);
static void     am_track_list_set_property       (GObject*, guint property_id, const GValue*, GParamSpec*);
static AMTrack* am_track_list_get_pool_item      (AMTrackList*, int);


/**
 * am_track_list_at: (skip)
 */
AMTrack*
am_track_list_at (AMTrackList* self, gint index)
{
	g_return_val_if_fail (self, NULL);

	return ((index < 0) || (index > AM_MAX_TRK - 1)) ? NULL : self->track[index];
}


static guint
am_track_list_real_length (AMCollection* base)
{
	AMTrackList* self = (AMTrackList*) base;
	return (guint)am_track_list_count (self);
}


static bool
am_track_list_real_contains (AMCollection* base, gconstpointer item)
{
	return false;
}


static const AMItem*
am_track_list_real_find_by_id (AMCollection* base, guint64 id)
{
	return NULL;
}


static AMTrack*
am_track_allocate (AMTrackNum t)
{
	am_track_list_unallocate(song->tracks, t);

	return am_track_list_get_pool_item(song->tracks, t);
}


static gpointer
am_track_list_real_append (AMCollection* collection, gconstpointer item)
{
	AMTrackList* tracks = (AMTrackList*)collection;

	// find unused array entry
	int p = 0; for(;p<AM_MAX_TRK;p++){
		AMTrack* track = am_track_list_get_pool_item(song->tracks, p);
		if(!track->type) break;
	}

	AMTrackNum t = am_track_list_find_unused(song->tracks);

	AMTrack* tr = tracks->track[t] = am_track_allocate(p);
	*tr = *((AMTrack*)item);

	tr->visible = true;
	tr->cache.start = (AyyiSongPos){1 << 24,};

	if(tracks->priv->cache.length > -1)
		tracks->priv->cache.length++;

	return tr;
}


static void
am_track_list_real_remove (AMCollection* collection, gconstpointer item)
{
	AMTrackList* tracks = (AMTrackList*)collection;

	if(tracks->priv->cache.length > -1)
		tracks->priv->cache.length--;
}


gint
am_track_list_count (AMTrackList* self)
{
	g_return_val_if_fail (self, -1);

	if(self->priv->cache.length > -1){
		return self->priv->cache.length;
	}

	AMTrackNum t; for(t=0;t<AM_MAX_TRK;t++){
		if(!self->track[t] || !self->track[t]->type)
			return self->priv->cache.length = t;
	}

	pwarn("failed");
	return MAX (t, AM_MAX_TRK - 1);
}


gint
am_track_list_get_visible_count (AMTrackList* self)
{
	g_return_val_if_fail (self, 0);

	gint n = 0;
	for(AMTrackNum t=0;t<AM_MAX_TRK;t++) {
		AMTrack* track = self->track[t];
		if (!track) {
			break;
		}
		if (track->visible) {
			n++;
		}
	}
	return n;
}


AMTrack*
am_track_list_find_by_shm_idx (AMTrackList* self, AyyiIdx shm_idx, AMTrackType type)
{
	g_return_val_if_fail (self, NULL);

	AMTrackNum t;
	for(t=0; t<AM_MAX_TRK && self->track[t] && self->track[t]->type; t++){
		if((self->track[t]->type == type) && (self->track[t]->ident.idx == shm_idx)) return self->track[t];
	}
	return NULL;
}


/**
 * am_track_list_find_by_ident: (skip)
 */
AMTrack*
am_track_list_find_by_ident (AMTrackList* self, AyyiIdent id)
{
	g_return_val_if_fail (self, NULL);

	AMTrackType type = 0;

	bool _tmp1_ = id.type == AYYI_OBJECT_AUDIO_TRACK
		? TRUE
		: id.type == AYYI_OBJECT_TRACK;

	if (_tmp1_) {
		type = TRK_TYPE_AUDIO;
	} else {
		if (id.type == AYYI_OBJECT_MIDI_TRACK) {
			type = TRK_TYPE_MIDI;
		} else {
			type = 0;
		}
	}
	return (AMTrack*)am_track_list_find_by_shm_idx (self, id.idx, type);
}


/**
 * am_track_list_find_audio_by_idx: (skip)
 */
AMTrack*
am_track_list_find_audio_by_idx (AMTrackList* self, AyyiIdx shm_index)
{
	g_return_val_if_fail (self != NULL, NULL);

	gint t = 0;
	gboolean _tmp0_;
	t = 0;
	_tmp0_ = TRUE;
	while (TRUE) {
		gboolean _tmp1_ = FALSE;
		gboolean _tmp2_ = FALSE;
		if (!_tmp0_) {
			t++;
		}
		_tmp0_ = FALSE;
		if (!(t < AM_MAX_TRK)) {
			break;
		}
		if (self->track[t]) {
			_tmp2_ = self->track[t]->type == TRK_TYPE_AUDIO;
		} else {
			_tmp2_ = FALSE;
		}
		if (_tmp2_) {
			_tmp1_ = self->track[t]->ident.idx == shm_index;
		} else {
			_tmp1_ = FALSE;
		}
		if (_tmp1_) {
			return self->track[t];
		}
	}
	return NULL;
}


/**
 * am_track_list_find_by_name: (skip)
 */
AMTrack*
am_track_list_find_by_name (AMTrackList* self, gchar* name)
{
	g_return_val_if_fail (self != NULL, NULL);

	gint t = 0;
	bool _tmp0_ = TRUE;
	while (TRUE) {
		if (!_tmp0_) {
			t++;
		}
		_tmp0_ = FALSE;
		if (!(t < AM_MAX_TRK)) {
			break;
		}

		AMTrack* track = self->track[t];
		if (track && track->name == name){
			return track;
		}
	}
	return NULL;
}


/**
 * am_track_list_find_by_playlist: (skip)
 */
AMTrack*
am_track_list_find_by_playlist (AyyiPlaylist* playlist, AyyiMediaType type)
{
	g_return_val_if_fail(type < AYYI_MEDIA_TYPE_MAX, NULL);

	AMTrack* track = NULL;
	AMIter i;
	am_collection_iter_init (am_tracks, &i);
	while((track = am_collection_iter_next(am_tracks, &i))){
		dbg (3, "testing trk='%s': %i / %i %i / %i", track, track->ident.idx, track->ident.idx, playlist->track, am_track__get_media_type(track), type);
		if(track->ident.idx == playlist->track && am_track__get_media_type(track) == type){
			return track;
		}
	}
	pwarn ("track not found. playlist='%s'", playlist->name);
	return NULL;
}


bool
am_track_list_is_known (AMTrackList* self, AMTrackType type, AyyiIdx shm_num)
{
	g_return_val_if_fail (self != NULL, FALSE);

	gint t = 0;
	bool _tmp0_ = true;
	while (TRUE) {
		bool _tmp1_ = FALSE;
		bool _tmp2_ = FALSE;
		if (!_tmp0_) {
			t++;
		}
		_tmp0_ = FALSE;
		if (!(t < AM_MAX_TRK)) {
			break;
		}
		if (self->track[t]) {
			_tmp2_ = self->track[t]->type == type;
		} else {
			_tmp2_ = FALSE;
		}
		if (_tmp2_) {
			_tmp1_ = self->track[t]->ident.idx == shm_num;
		} else {
			_tmp1_ = FALSE;
		}
		if (_tmp1_) {
			return true;
		}
	}
	return false;
}


/**
 * am_track_list_get_first_visible
 * @arg1: type
 *
 * Returns: (transfer none)
 */
AMTrack*
am_track_list_get_first_visible (AMTrackList* self, AMTrackType type)
{
	g_return_val_if_fail (self != NULL, NULL);

	gint t = 0;
	gint _tmp5_;
	t = 0;
	bool _tmp0_ = TRUE;
	while (TRUE) {
		bool _tmp1_ = FALSE;
		bool _tmp2_ = FALSE;
		bool _tmp3_ = FALSE;
		if (!_tmp0_) {
			t++;
		}
		_tmp0_ = FALSE;
		if (t < AM_MAX_TRK) {
			_tmp2_ = self->track[t] != NULL;
		} else {
			_tmp2_ = FALSE;
		}
		if (_tmp2_) {
			_tmp1_ = self->track[t]->type;
		} else {
			_tmp1_ = FALSE;
		}
		if (!_tmp1_) {
			break;
		}
		if (self->track[t]->visible) {
			gboolean _tmp4_ = FALSE;
			if (!((gboolean) type)) {
				_tmp4_ = TRUE;
			} else {
				_tmp4_ = self->track[t]->type == type;
			}
			_tmp3_ = _tmp4_;
		} else {
			_tmp3_ = FALSE;
		}
		if (_tmp3_) {
			return self->track[t];
		}
	}
	_tmp5_ = am_track_list_count (self);
	if ((gboolean) _tmp5_) {
		g_print ("*** get_first_visible(): not found...! t=%i\n", t);
	}
	return NULL;
}


/**
 * am_track_list_each
 * @fn: (scope async)
 */
void
am_track_list_each (AMTrackList* self, AMTrackListTrackEach fn, void* user_data)
{
	g_return_if_fail (self);

	AMIter iter = {0};
	AMTrack* track = NULL;
	am_collection_iter_init ((AMCollection*) self, &iter);
	while (true) {
		track = am_collection_iter_next ((AMCollection*) self, &iter);
		if (!track) {
			break;
		}
		fn (track, user_data);
	}
}


int
am_track_list_position (AMTrackList* self, AMTrack* track)
{
	for(AMTrackNum t = 0; t < AM_MAX_TRK; t++){
		if(self->track[t] == track) return t;
	}
	return -1;
}


/**
 * am_track_list_find_unused:
 *
 * Returns: an internal track index for the first unused track.
 */
AMTrackNum
am_track_list_find_unused (AMTrackList* tracks)
{
	for(unsigned t=0;t<AM_MAX_TRK;t++){
		dbg (2, "t=%i track=%p type=%i", t, tracks->track[t], tracks->track[t] ? tracks->track[t]->type : 0);
		if(!tracks->track[t] || tracks->track[t]->type == TRK_TYPE_NULL) return t;
	}
	return -1;
}


void
am_track_list_rebuild_pointers (AMTrackList* tracks)
{
	// iterating over track[] is easier if the array isnt sparse, so it needs rebuilding after a delete.

	int i = 0;
	for(int t=0;t<AM_MAX_TRK;t++){
		AMTrack* tt = am_track_list_get_pool_item(tracks, t);
		if(tt && tt->type){
			tracks->track[i] = tt;
			i++;
		}
		else if(tt) dbg(3, "    t=%i ! %s type=%i", t, tt->name, tt->type);
	}
	tracks->track[i] = NULL;

	tracks->priv->cache.length = i;
}


void
am_track_list_reset (AMTrackList* tracks)
{
	for (AMTrackNum t=0;t<AM_MAX_TRK;t++) {
		am_track_list_unallocate(tracks, t);
		tracks->track[t] = NULL;
	}

	am_collection_selection_reset((AMCollection*)tracks);
}


void
am_track_list_select_first ()
{
	if(am_tracks->selection) pwarn ("should be empty");

	AMTrack* trk = am_track_list_get_first_visible(song->tracks, TRK_TYPE_AUDIO);
	if(trk){
		GList* items = g_list_prepend(NULL, trk);
		am_collection_selection_add ((AMCollection*)song->tracks, items, NULL);
		g_list_free(items);
	}
}


bool
am_track_list_verify_track (AMTrackList* self, AMTrack* trk)
{
	g_return_val_if_fail (self, FALSE);

	AMTrackNum t = 0;
	bool _tmp0_ = TRUE;
	while (TRUE) {
		if (!_tmp0_) {
			t++;
		}
		_tmp0_ = FALSE;
		if (!(t < AM_MAX_TRK)) {
			break;
		}
		if (self->track[t] == trk) {
			return true;
		}
	}
	return false;
}


bool
am_track_list_verify_tracks (AMTrackList* self)
{
	g_return_val_if_fail (self, false);

#if 0
	for(AMTrackNum t=0;t<AM_MAX_TRK;t++){
		AMTrack* track = self->track[t];
		if(!track) break;
		int idx = track->ident.idx;
		dbg(0, " * %i: %i %i %s", t, idx, track->type, track->name);
	}
#endif

	return true;
}


static void
am_track_list_real_iter_init (AMCollection* base, AMIter* iter)
{
	*iter = (AMIter){0};
}


/*
 *  Note that the track iterator ignores master tracks
 */
static AMTrack*
am_track_list_real_iter_next (AMCollection* collection, AMIter* iter)
{
	AMTrackList* tracks = (AMTrackList*)collection;

	AMTrack* track = tracks->track[iter->i];
	iter->i++;
	if (track && am_track__is_master(track)) {
		track = tracks->track[iter->i++];
	}

	return track;
}


AMTrackList*
am_track_list_construct (GType object_type, GType t_type, GBoxedCopyFunc t_dup_func, GDestroyNotify t_destroy_func)
{
	AMTrackList* self = (AMTrackList*) am_collection_construct (object_type, t_type);

	self->priv->t_type = t_type;
	self->priv->t_dup_func = t_dup_func;
	self->priv->t_destroy_func = t_destroy_func;

	return self;
}


/**
 * am_track_list_new
 *
 * Return value: (transfer full)
 */
AMTrackList*
am_track_list_new ()
{
	GType t_type = 0;
	GBoxedCopyFunc t_dup_func = NULL;
	GDestroyNotify t_destroy_func = NULL;
	return am_track_list_construct (AM_TYPE_TRACK_LIST, t_type, t_dup_func, t_destroy_func);
}


static void
am_track_list_class_init (AMTrackListClass* klass)
{
	am_track_list_parent_class = g_type_class_peek_parent (klass);

	AM_COLLECTION_CLASS (klass)->length = am_track_list_real_length;
	AM_COLLECTION_CLASS (klass)->contains = am_track_list_real_contains;
	AM_COLLECTION_CLASS (klass)->find_by_id = am_track_list_real_find_by_id;
	AM_COLLECTION_CLASS (klass)->append = am_track_list_real_append;
	AM_COLLECTION_CLASS (klass)->remove = am_track_list_real_remove;
	AM_COLLECTION_CLASS (klass)->iter_init = am_track_list_real_iter_init;
	AM_COLLECTION_CLASS (klass)->iter_next = (AMCollectionNextFn)am_track_list_real_iter_next;

	G_OBJECT_CLASS (klass)->get_property = am_track_list_get_property;
	G_OBJECT_CLASS (klass)->set_property = am_track_list_set_property;
	G_OBJECT_CLASS (klass)->finalize = am_track_list_finalize;

	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_TRACK_LIST_T_TYPE, g_param_spec_gtype ("t-type", "type", "type", G_TYPE_NONE, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_TRACK_LIST_T_DUP_FUNC, g_param_spec_pointer ("t-dup-func", "dup func", "dup func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (G_OBJECT_CLASS (klass), AM_TRACK_LIST_T_DESTROY_FUNC, g_param_spec_pointer ("t-destroy-func", "destroy func", "destroy func", G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
}


static void
am_track_list_init (AMTrackList* self)
{
	self->priv = am_track_list_get_instance_private(self);

	void track_list_on_part_add (GObject* _song, AMPart* part, gpointer _)
	{
		g_return_if_fail(part->track);
		AMTrack* track = part->track;
		track->cache.start = (AyyiSongPos){1 << 24,};
		track->cache.end = (AyyiSongPos){0,};
	}

	void track_list_on_part_remove (GObject* _song, AMPart* part, gpointer _)
	{
		g_return_if_fail(part->track);
		AMTrack* track = part->track;
		track->cache.start = (AyyiSongPos){1 << 24,};
		track->cache.end = (AyyiSongPos){0,};
	}

	void track_list_on_part_change (GObject* parts, AMPart* part, int change, void* src, gpointer _)
	{
		g_return_if_fail(part->track);
		AMTrack* track = part->track;
		track->cache.start = (AyyiSongPos){1 << 24,};
		track->cache.end = (AyyiSongPos){0,};
	}

	g_signal_connect(song->parts, "add", G_CALLBACK(track_list_on_part_add), self);
	g_signal_connect(song->parts, "delete", G_CALLBACK(track_list_on_part_remove), self);
	g_signal_connect(song->parts, "item-changed", G_CALLBACK(track_list_on_part_change), self);
}


static void
am_track_list_finalize (GObject* obj)
{
	am_track_list_reset ((AMTrackList*)obj);

	G_OBJECT_CLASS (am_track_list_parent_class)->finalize (obj);
}


static void
am_track_list_get_property (GObject* object, guint property_id, GValue* value, GParamSpec* pspec)
{
	switch (property_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}


static void
am_track_list_set_property (GObject* object, guint property_id, const GValue* value, GParamSpec* pspec)
{
	AMTrackList* self = AM_TRACK_LIST (object);
	switch (property_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
		case AM_TRACK_LIST_T_TYPE:
			self->priv->t_type = g_value_get_gtype (value);
			break;
		case AM_TRACK_LIST_T_DUP_FUNC:
			self->priv->t_dup_func = g_value_get_pointer (value);
			break;
		case AM_TRACK_LIST_T_DESTROY_FUNC:
			self->priv->t_destroy_func = g_value_get_pointer (value);
			break;
	}
}


static AMTrack*
am_track_list_get_pool_item (AMTrackList* tracks, int t)
{
	return &tracks->priv->pool[t];
}


/*
 *  Used to re-initialise track vars. Doesnt allocate or free memory.
 *
 *  Currently, automation widgets are not reset.
 */
void
am_track_list_unallocate (AMTrackList* tracks, AMTrackNum t)
{
	// tracks->track[t] may be nulled, so fetch from the pool
	AMTrack* tr = am_track_list_get_pool_item(tracks, t);
	g_return_if_fail(tr);

	g_clear_pointer(&tr->meterval, observable_free);
	g_clear_pointer(&tr->name, g_free);

	if(tr->bezier.vol) dynarr_free(tr->bezier.vol);
	if(tr->bezier.pan) dynarr_free(tr->bezier.pan);

	*tr = (AMTrack){
		.type       = TRK_TYPE_NULL,
		.ident.idx  = -1
	};

	tracks->priv->cache.length = -1;
}

