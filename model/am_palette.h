/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_palette_h__
#define __am_palette_h__

#define AM_MAX_COLOUR 64
#define ASSERT_PALETTE_NUM(A) if(A < 0 || A >= AM_MAX_COLOUR) return;

#ifndef __GI_SCANNER__
void       am_palette_load            (AMPalette);
void       am_palette_get_float       (AMPalette, int c_idx, float* r, float* g, float* b, const unsigned char alpha);
uint32_t   am_palette_get_contrasting (AMPalette, int c_idx);
bool       am_palette_is_dark         (AMPalette, int c_idx);
#endif

void       am_rgba_to_float           (uint32_t rgba, float* r, float* g, float* b);
void       colour_get_rgba_str        (char* colorstr, int c_idx, const unsigned char alpha);
void       colour_get_rgb_str         (char* colorstr, int c_idx);

#endif
