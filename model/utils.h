/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_utils_h__
#define __am_utils_h__

#include "ayyi/ayyi_typedefs.h"
#include "model/model_types.h"

#define g_list_clear(L) g_list_free(L); L = NULL;
#define g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#ifndef g_list_free0
#define g_list_free0(var) ((var == NULL) ? NULL : (var = (g_list_free (var), NULL)))
#endif
#define am_source_remove0(S) {if(S) g_source_remove(S); S = 0;}

//see also ayyi_handler_simple
#define DO_RESPONSE(TYPE) \
	HandlerData* d = a->app_data; \
	if(d){ \
		AyyiIdent id = {TYPE, a->ret.idx}; \
		call(d->callback, id, &a->ret.error, d->user_data); \
	}


float      am_db2gain                (float);
float      am_gain2db                (float);

char*      replace                   (const char* string, char* oldpiece, char* newpiece);
void       replace2                  (char* string, char* oldpiece, char* newpiece);
gchar*     str_replace               (const gchar* string, const gchar* search, const gchar* replace);
void       escape_for_menu           (char*);

gchar*     am_audio_path_get_full    (const char* path);

char*      am_format_channel_width   (unsigned width);

gboolean   file_exists               (const char* path);
void       filename_change_extension (char* path, const char* extn);
void       filename_get_extension    (const char* path, char* extn);
void       filename_remove_extension (const char* filename, char* truncated);
char*      filename_get_stereo       (const char*);
gboolean   am_file_is_newer          (const char* file1, const char* file2);
bool       file_delete_recursive     (const char*);

bool       am_audiofile_is_readable  (const char* filename);

#ifdef DEBUG
const char* am_print_change          (AMChangeType);
#endif

#if 0
void        am_do_series             (AyyiCallback*, AyyiCallback, gpointer);
#endif

typedef struct
{
	int           i;
	AyyiCallback* functions;
	AyyiCallback  finished;
	gpointer      user_data;
	AyyiCallback  next;
} DoClosure;

typedef union
{
    int         i;
    float       f;
    double      d;
    char*       c;
    int64_t     b;
    void*       p;
    Ptf         pt;
    AMPos       pos;
    AyyiSongPos sp;

} AMVal;

typedef struct _AMObject AMObject;

struct _AMObject
{
    struct _am_val {
        AMVal    val;
        int      type;
    }*           vals;
    void         (*set) (AMObject*, int property, AMVal, AyyiHandler2, gpointer);
    gpointer     user_data;
};

void am_object_init   (AMObject*, int n_properties);
void am_object_deinit (AMObject*);

#define am_object_val(A) ((A)->vals[0].val)

#endif
