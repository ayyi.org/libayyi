/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_types_h__
#define __am_types_h__

#include <stdbool.h>
#include <glib.h>
#include <ayyi/interface.h>

#define AM_MAX_TRK 48
#define AM_MAX_CHS 16
#define AM_MAX_PART_LENGTH_BEATS 8192
#define AM_MAX_COLOUR 64

struct _AMPos
{
    int                beat;
    unsigned short int sub;      // subbeat (4 per beat) - note: not the same as core sub.
    unsigned short int tick;     // 960 per sub, as displayed.
};

typedef enum {
    TRK_TYPE_NULL = 0,
    TRK_TYPE_AUDIO,
    TRK_TYPE_MIDI,
    TRK_TYPE_BUS,
} AMTrackType;

typedef enum {
    Q_16,
    Q_8,
    Q_BEAT,
    Q_BAR,
    Q_EXPONENTIAL,
    Q_MAX
} QType;

typedef enum {
    SNAP_OFF = 0,
    SNAP_BAR,
    SNAP_BEAT,
    SNAP_16,
    SNAP_Q,
    MAX_SNAP,
} AMSnapType;

typedef enum {
    AM_LOC_START = 0,
    AM_LOC_LEFT,
    AM_LOC_RIGHT = 2,
    AM_LOC_END = 9,
    AM_LOC_MAX = 10
} AMLocatorType;

typedef enum
{
    AM_CHANGE_POS_X = 1,            // an object was moved in time.
    AM_CHANGE_LEN   = 1 << 1,
    AM_CHANGE_INSET = 1 << 2,       // front trim
    AM_CHANGE_WIDTH = 1 << 3,
    AM_CHANGE_HEIGHT = 1 << 4,
    AM_CHANGE_OUTPUT = 1 << 5,
    AM_CHANGE_FADES = 1 << 6,       // part fade in and out.
    AM_CHANGE_NAME = 1 << 7,
    AM_CHANGE_TRACK = 1 << 8,       // when applied to tracks, it refers to the track assignment for a widget.
    AM_CHANGE_ZOOM_H = 1 << 9,
    AM_CHANGE_ZOOM_V = 1 << 10,
    AM_CHANGE_COLOUR = 1 << 11,
    AM_CHANGE_ARMED = 1 << 12,
    AM_CHANGE_MUTE = 1 << 13,
    AM_CHANGE_SOLO = 1 << 14,
    AM_CHANGE_SELECTION = 1 << 15,
    AM_CHANGE_BACKGROUND_COLOUR = 1 << 16,
    //AM_CHANGE_IDX = 1 << 17,
    AM_CHANGE_POS_Y = 1 << 18,      // vertical positions have changed. e.g. used on track delete.
    AM_CHANGE_VIEWPORT_Y = 1 << 19, // vertical scrolling
    AM_CHANGE_SHOW_PART_CONTENTS = 1 << 20,
    AM_CHANGE_SHOW_SONG_OVERVIEW = 1 << 21,
    AM_CHANGE_PEAK_GAIN = 1 << 22,
    CHANGE_ALL = 0xffff,
} AMChangeType;


typedef struct { int x; int y; } Pti;
typedef struct { float x, y; } Ptf;
typedef enum { MOVETO, MOVETO_OPEN, CURVETO, LINETO, BP_END } Pathcode;
typedef struct _AMBPath BPath;
struct _AMBPath { Pathcode code; double x1; double y1; double x2; double y2; double x3; double y3; };
typedef struct { double r, g, b; } DColour;
typedef struct { int32_t start, end; } AMiRange;

typedef struct _AMCollection       AMCollection;
typedef struct _AMTrack            AMTrack;
typedef struct _Channel            AMChannel;
typedef struct _AMPart             AMPart;
typedef struct _AMPartMidi         MidiPart;
typedef struct _AMPartMidi         AMPartMidi;
typedef int                        AMTrackNum;
typedef struct _AMPos              GPos;
typedef struct _AMPos              AMPos;
typedef struct _AMPoolItem         AMPoolItem;
typedef struct _q_map              QMap;
typedef struct _q_map              AMQMap;
typedef struct _curve              Curve;
typedef struct _curve              AMCurve;
typedef struct _observer           Observer;
typedef struct _plug               AMPlugin;
typedef struct _AMIter             AMIter;

typedef struct _AMArray               AMArray;
typedef struct _AMList                AMList;
typedef struct _AMTrackList           AMTrackList;
typedef struct _FilterIterator        FilterIterator;
typedef struct _IteratorFilterClass   IteratorFilterClass;
typedef struct _IteratorFilterPrivate IteratorFilterPrivate;

typedef bool (*Filter) (void* item, void* user_data);

#ifndef __GI_SCANNER__
typedef uint32_t Palette[AM_MAX_COLOUR];
typedef uint32_t AMPalette[AM_MAX_COLOUR];
#endif

#define G_TYPE_AYYI_SONG_POS G_TYPE_RESERVED_USER_FIRST

#ifndef __GI_SCANNER__
struct _AMIter
{
	int i;
	void* ptr;
	GList* list;
	GPtrArray* array;
};
#endif

typedef struct
{
	AyyiMidiNote* orig;
	AyyiMidiNote  transient;
} TransitionalNote;

#ifndef __GI_SCANNER__
typedef struct _note_selection_list_item
{
   MidiPart* part;
   GList*    notes;            // list of TransitionalNote*
} NoteSelectionListItem;
#endif

struct _q_map
{
   char      name[64];
   int       size;             // the index of the last valid entry in the pos[] array.
   GPos      pos[33];          // the last entry in pos (pos[size]) should be equal to length.
};

typedef struct
{
   AMTrack*     track;
   AyyiSongPos* start;
   AyyiSongPos* end;

} AMTrackSegment;

#endif
