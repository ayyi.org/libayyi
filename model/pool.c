/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __am_private__
#include "common.h"
#include "waveform/waveform.h"
#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>

#include <model/song.h>
#include <model/am_collection.h>
#include <model/pool.h>


void
am_pool__init ()
{
	GBoxedCopyFunc t_dup_func = NULL;
	GDestroyNotify t_destroy_func = NULL;
	song->pool = am_list_new (t_dup_func, t_destroy_func);

#if 0 // currenly server-side peak generation is disabled, and is done by the client.
	waveform_set_peak_loader(wf_load_ardour_peak);
#endif
}


void
am_pool__free ()
{
	am_pool__clear();
	g_object_unref0(song->pool);
}


void
am_pool__sync ()
{
	// update the song->pool with AMPoolItem's for each ayyi file_source.

	AyyiFilesource* filesource = NULL;
	while((filesource = ayyi_song__filesource_next(filesource))){
		dbg (2, "%p id=%Lu", filesource, filesource->id);
		if(!am_pool__get_item_by_id(filesource->id)){
			am_pool__add_ayyi_file(filesource);
		}
	}

#if 0 // test disabled as it probably gives false warnings
#ifdef DEBUG
	// check all peakfiles were loaded
	int n_missing = 0;
	GList* l = song->pool->list;
	for(;l;l=l->next){
		AMPoolItem* item = l->data;
		int c; for(c=0;c<item->channels;c++){
			if(!waveform_peak_is_loaded((Waveform*)item, c)) n_missing++;
		}
	}
	if(n_missing) pwarn("peak files missing: %i", n_missing);
#endif
#endif
}


void
am_pool__add (AMPoolItem* item)
{
	g_return_if_fail(item);

	if(g_list_find(song->pool->list, item)){ pwarn("item already in pool!"); return; }

	song->pool->list = g_list_append(song->pool->list, item);
	am_pool__emit ("add", item);
}


/*
 *  am_pool__add_from_file
 *
 *  Add an audio file to the audio pool.
 *
 *  -the filename should be considered "raw", so we might need to process it to make it conformant.
 *  -a new pool_item is created and added to song->pool->list.
 *
 *  -currently, the peakfile should already exist.
 *
 */
AMPoolItem*
am_pool__add_from_file (const char* fname)
{
	g_return_val_if_fail(fname && strlen(fname), NULL);

	dbg (1, "'%s'", fname);

	if(!am_audiofile_is_readable(fname)){ return NULL; }

	AMPoolItem* pool_item = am_pool_item_new();
	if(!am_pool_item_set_file(pool_item, fname, 0)){
		g_object_unref(pool_item);
		return NULL;
	}

	if(!pool_item->state){
		pool_item_load_peak(pool_item, 0);
		pool_item->state = AM_POOL_ITEM_PENDING;
	}
	dbg (2, "state=%i", pool_item->state);

	am_pool__add(pool_item);

	return pool_item;
}


/**
 * am_pool__add_ayyi_file
 *
 * If not already existing, create new pool_item and add it to the pool.
 *
 * Return value: (transfer none)
 */
AMPoolItem*
am_pool__add_ayyi_file (AyyiFilesource* filesource)
{
	g_return_val_if_fail(filesource, NULL);
	if(!strlen(filesource->name)){ perr ("cannot add filesource. Name is empty. (id=%"PRIu64")", filesource->id); return NULL; }

	AMPoolItem* pool_item = NULL;
	bool is_new = false;

	int lr = ayyi_file_is_stereo(filesource);
	if(lr){
		// do we already have a pool item for this file pair?
		char other[AYYI_FILENAME_MAX];
		ayyi_file_get_other_channel(filesource, other, AYYI_FILENAME_MAX);
		gchar* leaf = g_path_get_basename(other);
		if((pool_item = am_pool__get_item_from_leafname(leaf))){
			pool_item_add_channel(pool_item, filesource);
		}
		else dbg(2, "pool_item for other channel not found. %s", filesource->name);
		g_free(leaf);
	}

	if(!pool_item){
		if(!(pool_item = am_pool_item_new_from_ayyi(filesource))) return NULL;

		if(!pool_item->state){
			pool_item_load_peak(pool_item, 0);
			pool_item->state = AM_POOL_ITEM_OK;
		}

		am_pool__add(pool_item);
		is_new = true;
	}

	if(lr) pool_item->channels = 2;

	if(pool_item->state != AM_POOL_ITEM_OK) pwarn ("item not valid (%s).", pool_item->filename);

	if(is_new){
		AYYI_DEBUG_2 printf("%spool-add%s\n", yellow, white);
		am_pool__emit ("add", pool_item);
	}

	AYYI_DEBUG_2 pool_item_print(pool_item);
	return pool_item;
}


bool
am_pool__remove (AMPoolItem* item)
{
	if(!g_list_find(song->pool->list, item)) return false;

	song->pool->list = g_list_remove(song->pool->list, item);

	g_object_unref(item);

	return true;
}


/*
 *  Free peak buffers and lists
 */
void
am_pool__clear ()
{
	while(song->pool->list){
		//g_object_unref(song->pool->list->data);
		am_pool__remove(song->pool->list->data);
	}
}


/**
 * am_pool__get_item_from_idx
 *
 * Used speculatively, so failing is not considered to be an error.
 *
 * Return value: (transfer none)
 */
AMPoolItem*
am_pool__get_item_from_idx (AyyiIdx fidx)
{
	GList* l = song->pool->list;
	for(;l;l=l->next){
		AMPoolItem* item = l->data;
		dbg(3, "  %i %s", item->pod_index[0], item->leafname);
		if(item->pod_index[0] == fidx) return item;
	}

	dbg (1, "file not in pool. idx=%u.", fidx);

	return NULL;
}

/**
 *  am_pool__get_item_from_leafname: (skip)
 *
 *  AMPoolItem lookup using the leafname (the filename without the directory information)
 *   - unlike the full pathname, the leafname is not unique. We return only the first poolitem found.
 *   - fn is used speculatively. Failing to find a pool item is not an error.
 */
AMPoolItem*
am_pool__get_item_from_leafname (const char* lname)
{
	GList* l = song->pool->list;
	for(;l;l=l->next){
		AMPoolItem* item = l->data;
		if(!strcmp(lname, item->leafname)) return item;
	}

	// the above test assumes the file we are looking for is a wav file.
	// if not the file extensions will have changed after import.
	// -search again ignoring the extension.

	char wav_leaf[128];
	if(audio_path_get_wav_leaf(wav_leaf, lname, 128)){
		GList* l = song->pool->list;
		for(;l;l=l->next){
			AMPoolItem* item = l->data;
			dbg(3, "    %s - %s", wav_leaf, item->leafname);
			if(!strcmp(wav_leaf, item->leafname)) return item;
		}

		// try for stereo
		char* stereo = filename_get_stereo(wav_leaf);
		for(l=song->pool->list;l;l=l->next){
			AMPoolItem* item = l->data;
			dbg(3, "    %s - %s", stereo, item->leafname);
			if(!strcmp(stereo, item->leafname)) return item;
		}
		g_free(stereo);
	}

	dbg (2, "pool item not found for file: %s.\n", lname);

	return NULL;
}


/**
 *  am_pool__get_item_by_orig_name: (skip)
 */
AMPoolItem*
am_pool__get_item_by_orig_name (const char* orig_name)
{
	g_return_val_if_fail(orig_name && strlen(orig_name), NULL);

	bool pool_item_is_orig_name (AMPoolItem* item, gpointer user_data)
	{
		char* orig_name = user_data;

		for (int i=0;i<2;i++) {
			AyyiFilesource* f = ayyi_song__filesource_at(item->pod_index[0]);
			if (!f) pwarn("no filesource at %i", item->pod_index[0]);
			if (f && !strlen(f->original_name)) pwarn("f->original_name not set");
			dbg(2, "  '%s' '%s'", orig_name, f ? f->original_name : NULL);
			if (f && !strcmp(f->original_name, orig_name)) return true;
		}

		return false;
	}

	char* original_name = g_path_get_basename(orig_name);
	dbg(0, "%s", original_name);
	FilterIterator* filter_iter = filter_iterator_new (0, NULL, NULL, am_pool, (Filter)pool_item_is_orig_name, (gpointer)original_name);
	AMPoolItem* pool_item = (AMPoolItem*)filter_iterator_next(filter_iter);
	filter_iterator_unref(filter_iter);
	g_free(original_name);

	return pool_item;
}


/*
 *  Look thru the pool list to see if a pool_item for the given file already exists.
 */
bool
am_pool__item_exists (char* fname)
{
	for(GList* l = song->pool->list;l;l=l->next){
		AMPoolItem* item = l->data;
		dbg (2, "  fname=%s item=%s", fname, item->filename);
		if(!strcmp(fname, item->filename)) return true;
	}
	dbg (2, "file not found in song->pool->list (%s).", fname);

	return false;
}


/**
 *  am_pool__get_new_items
 *
 *  Return pool items that are POOL_ITEM_NEW, or POOL_ITEM_PENDING
 *   - the list must be free'd after use.
 *
 * Return value: (element-type AMPoolItem*)
 */
GList*
am_pool__get_new_items ()
{
	GList* ret = NULL;
	GList* l = song->pool->list;
	for(;l;l=l->next){
		AMPoolItem* item = l->data;
		if(item->state <= AM_POOL_ITEM_PENDING) ret = g_list_append(ret, item);
	}
	return ret;
}


/*
 *  File can be a full path, but only the basename will be matched.
 *
 *  Note ayyi_filesource now includes the original filename
 */
AMPoolItem*
am_pool__find_pending_by_leafname (const char* file)
{
	AMPoolItem* pool_item = NULL;

	gchar* target = audio_path_get_base(file);

	FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_pool, (Filter)pool_item_is_new, NULL);
	AMPoolItem* p;
	while(!pool_item && (p = (AMPoolItem*)filter_iterator_next(i))){
		gchar* test = audio_path_get_base(p->leafname);
		dbg(3, "     %s %s", test, target);
		if(!strcmp(test, target)){
			dbg(2, "found!");
			pool_item = p;
		}
		g_free(test);
	}
	filter_iterator_unref(i);

	if(target) g_free(target);

	return pool_item;
}


AMPoolItem*
am_pool__find_pending_by_leafname_old (const char* file)
{
	//leaf can be a full path, but only the basename will be matched.

	char filename[256];
	if(!audio_path_get_wav_leaf(filename, file, 256)) return NULL;

	AMPoolItem* item = NULL;
	GList* pending = am_pool__get_new_items();
	if(pending){
		GList* l = pending;
		for(;l;l=l->next){
			AMPoolItem* p = l->data;
			dbg(3, "  %s - %s", filename, p->leafname);
			if(!strcmp(p->leafname, filename)){
				dbg(3, "found");
				item = p;
				break;
			}
		}

		if(!item){
			//try removing stereo filename modifications
			char* end = g_strrstr(filename, "-L.wav");
			if(end){
				char* b = g_strndup(filename, end - filename);
				char* c = g_strdup_printf("%s.wav", b);
				g_free(b);

				char wav[256];
				GList* l = pending;
				for(;l;l=l->next){
					AMPoolItem* p = l->data;
					audio_path_get_wav_leaf(wav, p->leafname, 256);
					dbg(3, "  %s - %s", c, wav);
					if(!strcmp(c, wav)){
						dbg(3, "found");
						item = p;
						break;
					}
				}

				g_free(c);
			}
			else dbg(2, "-L.wav not found in %s", filename);
		}

		g_list_free(pending);
	}
	return item;
}


/**
 *  am_pool__get_item_by_id: (skip)
 */
AMPoolItem*
am_pool__get_item_by_id (guint64 source_id)
{
	for(GList* l = song->pool->list; l; l = l->next){
		AMPoolItem* pool_item = l->data;
		dbg(3, "   test_source_id=%Lu/%Lu", pool_item->source_id[0], pool_item->source_id[1]);
		if((pool_item->source_id[0] == source_id) || (pool_item->source_id[1] == source_id)){
			return pool_item;
		}
	}

	// this is not an error. It may just mean we have to add the source to the pool.
	dbg (2, "engine source_id '%Lu' is not in pool. pool_size=%i", source_id, g_list_length(song->pool->list));

	return NULL;
}


#ifdef DEBUG
void
am_pool__print ()
{
	//show pool overview for debugging.

	GList* l = song->pool->list;
	printf("-----------------------------------------\n%s():\n", __func__);
	printf("  %20s %4s %12s %s %9s\n", "", "idx", "id", "pool_item", "buf");
	for(;l;l=l->next){
		AMPoolItem* item = (AMPoolItem*)l->data;
		printf("  %20s %4i %12"PRIu64" %10p %s %s\n", item->leafname, item->pod_index[0], item->source_id[0], item, waveform_peak_is_loaded((Waveform*)item, 0) ? "y" : "n", pool_item_print_state(item));
	}
	printf("-----------------------------------------\n");

	ayyi_song__print_pool();
}
#endif

