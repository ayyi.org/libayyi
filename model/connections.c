/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <model/common.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_action.h>
#include <model/connections.h>

extern int debug;


#if 0
GList*
engine_connections_sync()
{
	/*
	gets engine connections from the shared_connections shm struct.
	*/
	GList* connection_list = NULL;
	AyyiConnection* conn = NULL;
	while((conn = ayyi_song__audio_connection_next(conn))){
		connection_list = g_list_append(connection_list, GINT_TO_POINTER(conn->shm_idx));
	}

	dbg (2, "num connections found: %i", g_list_length(connection_list));
	return connection_list;
}
#endif


char*
am_connection__name (AyyiIdx c)
{
	AyyiConnection* connection = ayyi_song__connection_at(c);
	if(!connection){ perr ("failed to get ayyi connection. index=%u", c); return NULL; }
	return connection->name;
}


#if 0
AyyiConnection*
am_connection__output_next (AyyiConnection* shared)
{
	//used to iterate through the shm Connections. Returns the connection following the one given, or NULL.

	//ignore the first shm connection (idx=0) "not connected".
	if(!shared) shared = ayyi_song__audio_connection_next(NULL);

	AyyiConnection* next = shared;
	while((next = ayyi_song__audio_connection_next(next))){
		if(next->io == 1) continue; //not interested in inputs
		dbg (3, "found shared connection. %i %s", next->shm_idx, next->name);
		return next;
	}
	dbg (2, "failed to get shared connection struct. shared=%p tot=%i", shared, ayyi_song_container_count_items(&ayyi.service->song->connections));
	return NULL;
}


AyyiConnection*
am_connection__input_next (AyyiConnection* shared)
{
	//used to iterate through the shm Connections. Returns the connection following the one given, or NULL.

	AyyiConnection* next = shared;
	while((next = ayyi_song__audio_connection_next(next))){
		//dbg(0, "  %i: %s", next->shm_idx, next->name);
		if(next->io == 1) return next;
	}

	dbg (2, "failed to get shared connection struct. shared=%p tot=%i", shared, ayyi_song_container_count_items(&ayyi.service->song->connections));
	return NULL;
}
#endif


