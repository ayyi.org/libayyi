/*
dynarr - A dynamic array library for linux
Copyright (C) 2003 Vanraes Maarten

This file is part of dynarr

dynarr is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

dynarr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABLILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dynarr; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#ifndef __DYNARR_H_
#define __DYNARR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <pthread.h>

typedef struct _curve
{
	BPath*          path;
	size_t          size;
	size_t          len;
	size_t          item_size;
	size_t          grow_size;

	char*           name;
	AMTrack*        track;
	AMPlugin*       plugin;
	int             auto_type;
#ifdef LIBGNOMECANVAS_H
	GnomeCanvasPathDef* g_path;
#else
	void*               g_path;
#endif

	pthread_mutex_t	mutex;
} dynarr_t;
typedef dynarr_t*	dynarr_p;
typedef dynarr_p	dynarr_ref_t;
typedef dynarr_ref_t*	dynarr_ref_p;

#ifndef DEFAULT_GROW_SIZE
#define DEFAULT_GROW_SIZE 512
#endif
#define DYNARR_INIT(a) ((dynarr_t){0,0,0,(a),DEFAULT_GROW_SIZE,NULL,0,0,NULL,PTHREAD_MUTEX_INITIALIZER})

int     dynarr_def_compare(void*,void*);
char*   dynarr_def_tostring(void*,void*);

void*   curve_new(AMTrack*, AyyiAutoType, AMPlugin*);
int     curve_init(dynarr_p,size_t,size_t);
void    dynarr_free(dynarr_p);
int     dynarr_lock(dynarr_p);
void    dynarr_unlock(dynarr_p);
size_t  curve_get_length(dynarr_p);
int     curve_setlength(dynarr_p,size_t);
size_t  dynarr_getsize(dynarr_p);
int     curve_setsize(dynarr_p,size_t);
size_t	dynarr_getgrow_size(dynarr_p);
int 	dynarr_setgrow_size(dynarr_p,size_t);
size_t	dynarr_getitem_size(dynarr_p);
int 	dynarr_setitem_size(dynarr_p,size_t);
void*	dynarr_item(dynarr_p,size_t);
void	dynarr_getitem(dynarr_p,size_t,void*);
void	dynarr_setitem(dynarr_p,size_t,void*);
void*	curve_ins(dynarr_p,size_t);
void	curve_del(dynarr_p,size_t);
void*	dynarr_extract(dynarr_p);
void*	curve_append(dynarr_p);
size_t	dynarr_indexof(dynarr_p,int(void*,void*),void*);
int 	dynarr_adddynarr(dynarr_p,dynarr_p);
int 	curve_clone(dynarr_p,dynarr_p);
int 	dynarr_createFromCallback(dynarr_p,int(void*,void*,size_t,size_t),void*,size_t,size_t);
int 	dynarr_createFromString(dynarr_p,const char*,char,int);
int 	dynarr_createFromArray(dynarr_p,void*,size_t,size_t);
int 	dynarr_createFromNullArray(dynarr_p,void*,size_t);
char*	dynarr_toString(dynarr_p,char*(void*,void*),char,void*);

int	dynarr_ref_create(dynarr_ref_p);

AMPlugin* curve_get_plugin(dynarr_t*);

#ifdef __cplusplus
}
#endif

#endif
