/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __am_message_c__
#include "model/common.h"
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_action.h>
#include "model/am_part.h"
#include "model/time.h"
#include "model/am_message.h"


/*
 *  @return: zero means no error.
 */
int
am_action_execute (AyyiAction* ayyi_action)
{
	AyyiAction* a = ayyi_action;
	dbg(2, "op=%i", ayyi_action->op);

	switch(ayyi_action->op){
		case AYYI_OP_NEW:
			// am_action_execute doesnt support AYYI_NEW for most types as there is no mechanism
			// for passing all the needed args.
			return -1;

		case AYYI_GET:
			switch(ayyi_action->prop){
				case AYYI_HISTORY:
					#ifdef USE_DBUS
					dbus_get_prop_string(ayyi_action, a->obj.type, a->prop);
					return 0;
					#endif
					break;
				default:
					perr ("unsupported property: %i", ayyi_action->prop);
					break;
			}
			break;
		case AYYI_SET:
			switch(ayyi_action->prop){
				case AYYI_LENGTH:
					// 64 bit properties
					ayyi_set_length(ayyi_action, ayyi_action->d_val);
					break;

				case AYYI_AUTO_PT:
					// props with 2 float values
					#ifdef USE_DBUS
					;GArray* array = ayyi_index_array_new(a->obj_idx.idx1, a->obj_idx.idx2, a->obj_idx.idx3);
					dbus_set_prop_f_pair(a, a->obj.type, a->prop, array, a->d_val, a->val2.d);
					g_array_unref(array);
					return 0;
					#endif
					break;

				case AYYI_TRANSPORT_REC:
				case AYYI_ARM:
				case AYYI_SOLO:
					// props with boolean values
					#if USE_DBUS
					dbus_set_prop_bool(a, a->obj.type, a->prop, a->obj_idx.idx1, a->i_val);
					#endif
					return 0;
					break;

				case AYYI_HEIGHT:
					#if USE_DBUS
					dbus_set_prop_int(a, a->obj.type, a->prop, a->obj_idx.idx1, a->i_val);
					#endif
					break;

				default:
					// integer properties:

					#ifdef USE_DBUS
					dbus_set_prop_int(ayyi_action, a->obj.type, a->prop, a->obj_idx.idx1, a->i_val);
					return 0;
					#elif USE_OSC
					dbus_set_prop_int(action, a->obj.type, action->prop, action->obj_idx, action->i_val);
					return 0;
					#endif
					break;
			}
			break;

		case AYYI_DEL:
			#ifdef USE_DBUS
			ayyi_dbus_object_del(ayyi_action, ayyi_action->obj.type, a->obj_idx.idx1);
			return 0;
			#endif
			break;

		default:
			perr ("unsupported op type: %s", ayyi_print_optype(ayyi_action->op));
			break;
	}
	return 1;
}


