/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <model/common.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_log.h>
#include <ayyi/ayyi_dbus_proxy.h>

#include "model/curve.h"
#include "model/plugin.h"
#include "model/channel.h"
#include "model/song.h"

GHashTable* plugin_icons = NULL;
AMPlugins am_plugins = {0,};


void
am_plugin__init ()
{
	if(plugin_icons) pwarn("already initialised!");
	plugin_icons = g_hash_table_new(g_str_hash, g_str_equal);
}


AMPlugin*
am_plugin__get_by_idx (int idx)
{
	GList* l= song->plugins;
	for (;l;l=l->next) {
		AMPlugin* plugin = l->data;
		if(plugin->shm_num == idx) return plugin;
	}
	pwarn("plugin not found. idx=%i", idx);
	return NULL;
}


void
am_plugin__sync ()
{
	void plugin_add (AyyiPlugin* shared)
	{
		AMPlugin* plug = g_new0(AMPlugin, 1);

		plug->type    = PLUGIN_TYPE_VST;
		plug->shm_num = shared->idx;
		g_strlcpy(plug->name, shared->name, AYYI_NAME_MAX);

		song->plugins = g_list_append(song->plugins, plug);
	}

	int count = 0;
	AyyiPlugin* plugin = NULL;
	while ((plugin = ayyi_mixer__plugin_next(plugin))) {
		plugin_add(plugin);
		count++;
	}

	ayyi_log_print(0, "audio plugins found: %i", count);
}


GList*
am_plugin__list ()
{
	GList* list = NULL;
	AyyiPlugin* shared = NULL;
	while ((shared = ayyi_mixer__plugin_next(shared))) {
		list = g_list_append(list, shared->name);
	}
	return list;
}


AMPlugin*
am_plugin__get_by_name (const char* name)
{
	GList* l = song->plugins;
	for (;l;l=l->next) {
		AMPlugin* plugin = l->data;
		if(!strcmp(name, plugin->name)) return l->data;
	}
	if (strcmp(name, "Bypass") && strcmp(name, NO_PLUGIN) && strcmp(name, "Edit") && strcmp(name, "Separator")) dbg(0, "plugin name not found: %s", name);
	return NULL;
}


GdkPixbuf*
am_plugin__get_icon (AMPlugin* plugin)
{
	char hash_name[16];
	char icon_name[256];

	sprintf(hash_name, "%.8s", plugin->name);
	gchar* lower = g_ascii_strdown(hash_name, -1);
	replace2(lower, " ", "_");

	char* t;
	if ((t = g_hash_table_lookup(am_plugins.map, lower))) {
		strncpy(icon_name, t, 255);
	} else {
		icon_name[0] = '\0';
	}
	GdkPixbuf* icon = strlen(icon_name) ? g_hash_table_lookup(plugin_icons, icon_name) : NULL;
	if(!icon) icon = g_hash_table_lookup(plugin_icons, "no_plugin");

	g_free(lower);
	return icon;
}


void
am_plugin__change (AMChannel* ch, AyyiIdx plugin_slot, const char* plugin_name, AyyiHandler callback, gpointer user_data)
{
	typedef struct
	{
		AyyiHandler callback;
		gpointer    user_data;
		AMChannel*  channel;
	} C;

	C* c = g_new0(C, 1);
	c->channel = ch;
	c->callback = callback;
	c->user_data = user_data;

	void
	am_plugin__change_done (AyyiAction* action)
	{
		PF;
		C* c = action->app_data;

		am_song__emit("plugin-change", c->channel);

		AyyiIdent obj;
		call(c->callback, obj, &action->ret.error, c->user_data);

		if(!action->ret.error){
			ayyi_log_print(LOG_OK, "plugin changed.");
		}
	}

#ifdef USE_DBUS
	AyyiAction* a  = ayyi_action_new("plugin change");
	a->callback    = am_plugin__change_done;
	a->app_data    = c;

	//FIXME we need another parameter here for plugin_slot - temporarily multiplexed with index.
	plugin_slot = plugin_slot;
	dbus_set_prop_string(a, AYYI_OBJECT_CHAN, AYYI_PLUGIN_SEL, ch->ident.idx | (plugin_slot << 8), plugin_name);
#endif
}


bool
am_plugin__name_valid (const char* plugin_name)
{
	GList* l = song->plugins;
	for (;l;l=l->next) {
		if(!strcmp(plugin_name, l->data)) return TRUE;
	}
	dbg(0, "plugin name not valid: %s", plugin_name);
	return FALSE;
}


