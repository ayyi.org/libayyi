/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#ifdef __am_private__
bool   am_automation_sync             ();
#endif
void   am_automation_point_add        (AMCurve*, int p, AyyiHandler, gpointer);
void   am_automation_point_remove     (AMCurve*, int p, AyyiHandler, gpointer);
void   am_automation_point_change     (AMCurve*, int p, AyyiHandler, gpointer);
void   am_automation_make_empty       (AMCurve*);

float  am_automation_get_value        (AMCurve*, int p, uint32_t time);

#ifdef DEBUG
void   am_automation_print_path_code  (int i, char* s);
void   am_automation_print_path       (AMCurve*);
#endif
