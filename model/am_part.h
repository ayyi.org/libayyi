/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __am_part_h__
#define __am_part_h__

#include <glib-object.h>

#ifdef __GI_SCANNER__
typedef struct _AMPart AMPart;
#endif

#include "model/model_types.h"

#define PART_MAX_MU 300000000000ULL // 1beat = 11025*3840 = 42336000mu.  @120bpm: 1min=42336000*120, 1hr=304,819,200,000

typedef enum
{
	PART_OK = 0,
	PART_LOCAL,
	PART_PENDING,
	PART_BAD,                         // the part is corrupted and shouldnt be used.
} AMPartStatus;

/**
 * AMPart:
 *
 * The AMPart object acts as a proxy for the remote Ayyi Part.
 * It also acts as a unifying abstraction for different shm implementations,
 * stores values of client-specific data, and holds uncommitted local changes.
 */
struct _AMPart
{
	AyyiIdent          ident;
	AyyiRegionBase*    ayyi;
	char               name[AYYI_NAME_MAX];
	AMTrack*           track;
	AMPartStatus       status;
	AyyiSongPos        start;
	uint32_t           region_start;   // the number of samples from the start of the audio file at the start of the part. This is needed here as different engines represent this differently.
	AyyiSongPos        length;

	AMPoolItem*        pool_item;      // the audio file that the part uses.

	int                bg_colour;      // palette index.
	uint32_t           fg_colour;
	bool               fg_is_set;      // if set, fg contains an explicitly specified colour, else fg_colour contains an automatically generated colour.

	int                ref_count;
};


struct _AMPartMidi
{
	AMPart             part;
	GList*             note_selection; // list of MidiNote*
};


#define PART_IS_AUDIO(A) ((A)->ident.type == AYYI_OBJECT_AUDIO_PART)
#define PART_IS_MIDI(A) ((A)->ident.type == AYYI_OBJECT_MIDI_PART)
#define am_part_get_shared(A) (PART_IS_AUDIO(A) ? (AyyiRegionBase*)ayyi_song__audio_region_at(A->ayyi->shm_idx) : (AyyiRegionBase*)ayyi_song__midi_region_at(A->ayyi->shm_idx))
#define am_part_media_to_ayyi_type(A) ((A == AYYI_AUDIO) ? AYYI_OBJECT_AUDIO_PART : AYYI_OBJECT_MIDI_PART)
#define am_part_ayyi_to_media_type(A) ((A == AYYI_OBJECT_AUDIO_PART) ? AYYI_AUDIO : AYYI_MIDI)
#define am_part__get_media_type(P) (P->ident.type == AYYI_OBJECT_AUDIO_PART ? AYYI_AUDIO : AYYI_MIDI)


GType         am_part_get_type          ();
AMPart*       am_part_new               ();
void          am_part_unref             (AMPart*);

#ifdef __am_private__
AMPart*       am_part_init_from_index   (AMPart*, AyyiIdx);
#endif

void          am_part_move              (AMPart*, AyyiSongPos*, AMTrack* new_track, AyyiHandler, gpointer);
void          am_part_set_track         (AMPart*, AMTrack*, AyyiHandler, gpointer);
void          am_part_trim_left         (AMPart*, AyyiHandler, gpointer);
void          am_part_split             (AMPart*, AyyiSongPos*, AyyiHandler, gpointer);
void          am_part_rename            (AMPart*, const char* new_name, AyyiHandler, gpointer);
void          am_part_mute_toggle_async (AMPart*);

void          am_part_set_length          (AMPart*);
void          am_part_set_level_async     (AMPart*, float level);
void          am_part_set_fadein_async    (AMPart*, int fade_time);
void          am_part_set_fadeout_async   (AMPart*, int fade_time);

void          am_part__end_pos            (AMPart*, AyyiSongPos*);
bool          am_part__get_start_pos      (AMPart*, AMPos*);
const char*   am_part_get_name            (AMPart*);
char*          am_part__get_length_bbst   (AMPart*, char* bbst);
void           am_part__nudge_left        (AMPart*, bool snap, AMQMap*);
void           am_part__nudge_right       (AMPart*, bool snap, AMQMap*);

void           am_part__set_colour        (AMPart*, unsigned cidx, AyyiHandler, gpointer);
#ifndef __GI_SCANNER__
void           am_part__get_fg_colour     (AMPart*, char* rgb, Palette);
#endif
void           am_part__set_fg_colour     (AMPart*, uint32_t);

float          am_part__get_level         (AMPart*);
uint32_t       am_part__get_fade_in       (AMPart*);
uint32_t       am_part__get_fade_out      (AMPart*);

bool           am_part__is_muted          (AMPart*);
bool           am_part__is_deleted        (AMPart*);
const AMPart*  am_part__is_between        (const AMPart*, const AMPart* start, const AMPart* end);

void           am_part__set_note_selection(AMPart*, GList*);

// iterator filters
bool           am_part__is_audio          (void* part, gpointer);
bool           am_part__is_midi           (void* part, gpointer);
bool           am_part__is_at_position    (AMPart*, gpointer);
bool           am_part__is_at_position_samples(AMPart*, gpointer);
bool           am_part__has_pool_item     (AMPart*, gpointer);
bool           am_part__has_track         (AMPart*, AMTrack*);
bool           am_part__in_track_segment  (AMPart*, AMTrackSegment*);
bool           am_part__has_id            (AMPart*, uint64_t*);

void           am_part__print             (AMPart*);
const char*    am_part__print_status      (AMPartStatus);

#ifdef __am_private__
void           am_part__sync_length       (AMPart*);
bool           am_part__sync              (AMPart*, AyyiPropType);
#endif
#ifdef __am_song_c__
void           am_part__on_palette_change (AMPart*);
#endif

#endif
