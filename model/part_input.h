/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __am_part_input_h__
#define __am_part_input_h__

#include <glib-object.h>
#include <glib.h>
#include <stdlib.h>

G_BEGIN_DECLS

#define AM_TYPE_PART_INPUT            (am_part_input_get_type ())
#define AM_PART_INPUT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AM_TYPE_PART_INPUT, AMPartInput))
#define AM_PART_INPUT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AM_TYPE_PART_INPUT, AMPartInputClass))
#define AM_IS_PART_INPUT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AM_TYPE_PART_INPUT))
#define AM_IS_PART_INPUT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AM_TYPE_PART_INPUT))
#define AM_PART_INPUT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AM_TYPE_PART_INPUT, AMPartInputClass))

typedef struct _AMPartInput        AMPartInput;
typedef struct _AMPartInputClass   AMPartInputClass;
typedef struct _AMPartInputPrivate AMPartInputPrivate;

struct _AMPartInput {
	GObject             parent_instance;
	AMPartInputPrivate* priv;
	gint64              _length;
};

struct _AMPartInputClass {
	GObjectClass parent_class;
};

GType        am_part_input_get_type      () G_GNUC_CONST;
AMPartInput* am_part_input_new           ();
AMPartInput* am_part_input_construct     (GType object_type);
gint         am_part_input_fn1           (AMPartInput* self);
const gchar* am_part_input_get_name      (AMPartInput* self);
void         am_part_input_set_name      (AMPartInput* self, const gchar* value);
AyyiIdx      am_part_input_get_track     (AMPartInput* self);
void         am_part_input_set_track     (AMPartInput* self, AyyiIdx value);
AMPoolItem*  am_part_input_get_pool_item (AMPartInput* self);
AyyiSongPos  am_part_input_get_start     (AMPartInput* self);

G_END_DECLS

#endif
