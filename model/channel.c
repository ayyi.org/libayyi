/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <math.h>
#include <model/common.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_mixer.h>
#include <model/am_message.h>
#include <model/plugin.h>
#include <model/song.h>
#include "model/mixer.h"
#include "model/connections.h"
#include "model/track.h"
#include "model/track_list.h"
#include "model/channel.h"

extern GHashTable* plugin_icons;

static void channel_init (AMChannel*, AyyiIdx);
static void channel_free (AMChannel*);

// note: the master channel no longer has to be at the end of the channel array.


AMArray*
channels__init (int n_channels)
{
	GBoxedCopyFunc t_dup_func = NULL;
	GDestroyNotify t_destroy_func = NULL;
	GType t_type = 0;
	song->channels = am_array_new (t_type, t_dup_func, t_destroy_func, n_channels);

	GPtrArray* channels = song->channels->array;
	for (int i=0;i<channels->len;i++) {
		AMChannel* channel = g_new0(AMChannel, 1); //g_ptr_array_index(song->channels, i);
		g_ptr_array_add(channels, channel);
		channel_init(channel, -1);
	}

#if 1
	/*  For the simplified model where there is 1:1 relationship for tracks and channels,
	 *  channel selections are mapped to track selections.
	 */
	void am_channel__on_selection_change (Observable* _, AMVal val, gpointer __)
	{
		GList* tracks = NULL;
		for (GList*l=am_channels->selection;l;l=l->next) {
			AMTrack* track = am_channel__lookup_track(l->data);
			if (track) {
				tracks = g_list_append(tracks, track);
			}
		}
		if (tracks)
			am_collection_selection_replace(am_tracks, tracks, NULL);
	}
	observable_subscribe(am_channels->selection2, am_channel__on_selection_change, NULL);
#endif

	return song->channels;
}


/*
 *  Sync the channel-list to the core.
 *  Used primarily following a new song load.
 */
void
channels__load ()
{
	if (!((AyyiSongService*)ayyi.service)->amixer) return;

	int n_channels = ayyi_mixer__count_channels();
	int old_chan_list_len = 0;

	// allocate the array
	if (!song->channels) {
		dbg (1, "creating new channel list... tot_strips=%i", n_channels);
		channels__init(n_channels);
	} else {
		old_chan_list_len = song->channels->array->len;
		dbg (2, "channel list size: %i --> %i.", old_chan_list_len, n_channels);
		if (old_chan_list_len < n_channels) {
			for (int i=0;i<n_channels-old_chan_list_len;i++) {
				g_ptr_array_add(song->channels->array, g_new0(AMChannel, 1));
			}
		}
		else if (old_chan_list_len > n_channels) {
			dbg (2, "shortening the array...");

			// remove references to this strip before we delete it
			for (int i=ayyi_mixer__count_channels();i<old_chan_list_len;i++) {
				AMChannel* channel = g_ptr_array_index(song->channels->array, i);
				if (channel->type == AM_CHAN_MASTER) continue;

				am_channels__emit("delete", channel);
			}

			g_ptr_array_set_size(song->channels->array, n_channels);
			dbg (2, "real new size=%i", song->channels->array->len);
		}
	}

	AMChannel* channel = NULL;
	for (int i=0;i<n_channels;i++) {
		AyyiIdx idx = am__mixer_channel_next(channel);
		AyyiTrack* r = ayyi_song__audio_track_at(idx);
		AyyiChannel* ch = ayyi_mixer__channel_at(idx);
		dbg (2, "%i: setting pod_idx=%i", i, idx);

		channel = g_ptr_array_index(song->channels->array, i);
		dbg (2, "channel[%i]=%p", i, channel);

		channel->type   = AYYI_TRACK_IS_MASTER(r) ? AM_CHAN_MASTER : AM_CHAN_INPUT;
		channel->nchans = ch->n_in;
		channel_init(channel, idx);
		channel->level->value = (AMVal){.f = ch->level};
	}

	am_channels__emit("change");

	dbg (2, "tot_strips=%i", n_channels);
}


void
channels__unload ()
{
	GPtrArray* channels = song->channels->array;
	for(int i=0;i<channels->len;i++){
		AMChannel* channel = g_ptr_array_index(channels, i);
		am_channels__emit("delete", channel);
	}
}


void
channels__append_new (AMTrack* track)
{
	GPtrArray* array = song->channels->array;

	AMChannel* new_channel = g_new0(AMChannel, 1);

	g_ptr_array_add(array, new_channel);

	AMChannel* ch = (AMChannel*)am_array_at(song->channels, array->len - 1);
	ch->type      = AM_CHAN_INPUT;
	ch->nchans    = 1;
	channel_init(ch, track->ident.idx);

	dbg(2, "ch_num=%i type=%i new_n_channels=%i", array->len - 1, ch->type, array->len);
#if 0
	channels_print();
#endif

	am_channels__emit ("add", new_channel);
}


/*
 *  Remove the given track from the channel list.
 */
void
channels__remove_channel (AMTrack* tr)
{
	PF2;
	GPtrArray* array = song->channels->array;
#ifdef DEBUG
	int old_len = array->len;
#endif

	int s = am_track__lookup_channel_num(tr);
	if(s > -1){
		dbg (2, "chan=%i", s);
		AMChannel* old_channel = g_ptr_array_index(array, s);

		// note: mixer_win_delete_strip() requires that we modify the channel array *after* the mixer strips have been modified.
        am_channels__emit("delete", old_channel);

		channel_free(old_channel);
		if(!g_ptr_array_remove_index(array, s)) perr("!!");
		dbg (2, "len: %i --> %i", old_len, array->len);
		if(s < array->len) dbg(2, "type=%i", ((AMChannel*)g_ptr_array_index(array, s))->type);
	}
}


AMChannel*
channels__get_master ()
{
	GPtrArray* array = song->channels->array;
	int i; for(i=0;i<array->len;i++){
		AMChannel* channel = g_ptr_array_index(array, i);
		if(channel->type == AM_CHAN_MASTER) return channel;
	}
	pwarn("master not found.");
	return NULL;
}


AMChannel*
channels__get_by_index (AyyiIdx idx)
{
	return am_array_find_by_idx((AMCollection*)song->channels, idx);
}


static void
channel_init (AMChannel* channel, AyyiIdx idx)
{
	// note: because channel allocation is done as part of the list, its difficult to have a separate channel_new() function.
	//       Make sure all channel properties are properly initialised here

	channel->ident.idx = idx;

	if(!channel->level){
		channel->level = observable_float_new (0.0, 0.0, 4.0);

		void channel_on_level (Observable* level, AMVal val, gpointer _channel)
		{
			AMChannel* channel = _channel;
			AyyiChannel* ac = ayyi_mixer__channel_at(channel->ident.idx);
			if(ac && ac->level != val.f){
				ayyi_channel__set_float(channel->ident.idx, AYYI_LEVEL, val.f);
			}
		}
		observable_subscribe(channel->level, channel_on_level, channel);
	}

	if(!channel->aux_level[0]){
		for(int a=0;a<AYYI_AUX_PER_CHANNEL;a++)
			channel->aux_level[a] = observable_float_new (0.0, -100.0, 11.0);
	}

	if(!channel->meter_level){
		//float meter_min = -70.0; // range of gtk_meter
		channel->meter_level = observable_float_new (-96.0, -96.0, 0.0); // using bst_meter range
	}
}


static void
channel_free (AMChannel* channel)
{
	observable_free0(channel->level);
	observable_free0(channel->meter_level);
	for(int a=0;a<AYYI_AUX_PER_CHANNEL;a++)
		observable_free0(channel->aux_level[a]);

#if 0
	g_object_unref(channel->plugin_list);
#endif
	g_free(channel);
}


#if 0
AMChannel*
channel_first (AMIter* iter)
{
#warning channel_first removeme
	iter->i = -1;
	g_return_val_if_fail(song->channels, NULL);

	iter->i = 0;
	GPtrArray* array = song->channels->array;
	AMChannel* channel = g_ptr_array_index(array, 0);
	return channel;
}


AMChannel*
c hannel_next (AMIter* iter)
{
	GPtrArray* array = song->channels->array;
	if(iter->i >= array->len || iter < 0){ return NULL; }
	AMChannel* channel = g_ptr_array_index(array, iter->i++);
	return channel;
}
#endif


bool
am_channel__is_muted (const AMChannel* channel)
{
	g_return_val_if_fail(channel, false);

	AyyiTrack* track = ayyi_song__audio_track_at(channel->ident.idx);
	return track ? track->flags & muted : FALSE;
}


bool
am_channel__is_solod (const AMChannel* channel)
{
	g_return_val_if_fail(channel, false);

	AyyiTrack* track = ayyi_song__audio_track_at(channel->ident.idx);
	if(track){
		return track->flags & solod;
	}
	return FALSE;
}


bool
am_channel__is_sdef (AMChannel* channel)
{
	return FALSE;
}


bool
am_channel__is_armed (const AMChannel* channel)
{
	g_return_val_if_fail(channel, false);

	AyyiTrack* track = ayyi_song__audio_track_at(channel->ident.idx);
	return track ? track->flags & armed : FALSE;
}


const char*
am_channel__get_name (AMChannel* ch)
{
	AyyiChannel* ac = ayyi_mixer__channel_at(ch->ident.idx);
	return ac ? ac->name : NULL;
}


AMTrack*
am_channel__lookup_track (AMChannel* ch)
{
	AyyiChannel* c = ayyi_mixer__channel_at(ch->ident.idx);
	if(!c) return NULL;
	AyyiTrack* track = ayyi_channel__get_track(c);
	return am_track_list_find_audio_by_idx(song->tracks, track->shm_idx);
}


double
am_channel__fader_level (AMChannel* channel)
{
	AyyiChannel* ch = ayyi_mixer__channel_at_quiet(channel->ident.idx);
	g_return_val_if_fail(ch, 0.0);

	return ch->level;
}


/*
 *  Translate from -1.0 - +1.0 to -10.0 - +10.0.
 */
static double
am__mixer_pan_to_display (double internal)
{
	if(internal < -1.0 || internal > 1.0) pwarn("ayyi pan value out of range: %.2f\n", internal);
	return internal * 10.0;
}


double
am_channel__pan_value (AMChannel* channel, bool* has_pan)
{
	AyyiChannel* ch = ayyi_mixer__channel_at_quiet(channel->ident.idx);
	g_return_val_if_fail(ch, 0.0);
	static int ddd = 0;

	if(ch->has_pan){
		if(has_pan) *has_pan = ch->has_pan;

		if((ch->shm_idx == 1) && !(ddd++%10)) dbg(4, "ch1: pan=%.2f", ch->pan);
		return am__mixer_pan_to_display(ch->pan);
	}
	return 0.0;
}


int
am_channel__get_input_idx (AMChannel* channel)
{
	AyyiTrack* trk = ayyi_song__audio_track_at(channel->ident.idx); //strictly, ident.idx is a mixer_track index.
	return trk ? trk->input_idx : -1;
}


/*
 *  Return the "short" input connection name.
 */
void
am_channel__get_input_string (AMChannel* channel, char* name)
{
	AyyiTrack* trk_shared = ayyi_song__audio_track_at(channel->ident.idx); // strictly, ident.idx is a mixer_track index.
	g_return_if_fail(trk_shared);
	name[0] = '\0';
	if(trk_shared->input_idx < 1) return; // channel has no input connection.

	AyyiConnection* connection = ayyi_song__connection_at(trk_shared->input_idx);
	g_return_if_fail(connection);
	dbg(2, "input=%i name=%s", trk_shared->input_idx, connection->name);
	if(connection->io == 1) pwarn("output is an input!");

	char (*s)[64] = (char (*)[64])g_new0(char, 4 * 64);
	if(ayyi_connection__parse_string(connection, s)){
		strcpy(name, s[0]);
	}
	else pwarn("failed to parse connection string.");

	g_free(s);
}


/*
 *  Return the "short" output connection name.
 *   - we are returning what the output is connected to, so we are returning an _input_
 */
void
am_channel__get_output_string (AMChannel* channel, char* output)
{
	output[0] = '\0';

	AyyiTrack* ayyi_trk = ayyi_song__audio_track_at(channel->ident.idx); //strictly, ident.idx is a mixer_track index.
	g_return_if_fail(ayyi_trk);

	AyyiConnection* connection = ayyi_track__get_output(ayyi_trk);
	if(!connection || !connection->shm_idx) return; //channel has no output connection.

	//this is the connection we are connecting _to_, so it should be an input.
	if(connection->io == 0) pwarn("input is an output!");

	char (*s)[64] = (char (*)[64])g_new0(char, 4 * 64);
	if(ayyi_connection__parse_string(connection, s)){
		strcpy(output, s[0]);
	}
	else pwarn("failed to parse connection string.");

	g_free(s);
}


/*
 *  Return the level in decibels
 */
double
am_channel__get_aux_level (AMChannel* ch, int aux_num)
{
	AyyiAux* aux = ayyi_mixer__aux_get_(ch->ident.idx, aux_num);
	return aux ? am_gain2db(aux->level) : 0.0;
}


void
am_channel__set_aux_level (AMChannel* ch, int aux_num, float value_db, AyyiHandler callback, gpointer user_data)
{
	uint32_t object_idx = ch->ident.idx | (aux_num << 16);

	AyyiAction* a = ayyi_action_new("set aux level (channel %i, aux %i) %.2fdB", ch->ident.idx, aux_num, value_db);
	a->callback   = ayyi_handler_simple;
	a->app_data   = ayyi_handler_data_new(callback, user_data);

	ayyi_set_float(a, AYYI_OBJECT_AUX, object_idx, AYYI_LEVEL, am_db2gain(value_db));
}


/*
 *	Turn on the connection with the given shared->connection_index.
 *	 - if connection is -1, a new bus will be created.
 *
 *	The returned AyyiIdent is a composite of the channel and the aux.
 *
 */
void
am_channel__set_aux_output (const AMChannel* ch, int aux_num, int connection, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(ch->ident.idx < 0xffff);
	g_return_if_fail(aux_num < AYYI_AUX_PER_CHANNEL);

	void am_channel__set_aux_output_done (AyyiAction* action)
	{
		PF;

		if(!action->ret.error){
			ayyi_log_print(0, action->i_val ? "Aux bus set" : "Aux send disabled");
		}

		uint32_t ch_i = action->obj_idx.idx1 & 0xffff;
		int aux_num = action->obj_idx.idx1 >> 16;

		// check that ayyi_aux output was correctly set.
		AyyiAux* aux = ayyi_mixer__aux_get_(ch_i, aux_num);
		if(aux){
			dbg(1, "aux_num=%i bus_num=%i gain=%.4f", aux_num, aux->bus_num, aux->level);
		}else{
			if(action->i_val) pwarn("couldnt get AyyiAux for channel=%i aux_num=%i tot_auxs=%i", action->obj_idx.idx1, aux_num, ayyi_mixer__aux_count(ch_i));
		}

		HandlerData* d = action->app_data;
		if(d){
			call(d->callback, (AyyiIdent){action->obj.type, action->obj_idx.idx1}, &action->ret.error, d->user_data);
		}
	}

	AyyiAction* a   = ayyi_action_new("set aux output (track %i, aux %i)", ch->ident.idx, aux_num);
	a->obj.type     = AYYI_OBJECT_AUX;
	a->prop         = AYYI_OUTPUT;
	a->obj_idx.idx1 = ch->ident.idx | (aux_num << 16);
	a->i_val        = connection;
	a->callback     = am_channel__set_aux_output_done;
	a->app_data     = ayyi_handler_data_new(callback, user_data);
	am_action_execute(a);
}


void
am_channel__set_enable_pan (AMChannel* ch, gboolean enable, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(ch);

	AyyiAction* a   = ayyi_action_new("enable pan (channel %i) %i", ch->ident.idx, enable);
	a->obj.type     = AYYI_OBJECT_CHAN;
	a->obj_idx.idx1 = ch->ident.idx;
	a->ret.idx      = ch->ident.idx;
	a->prop         = AYYI_PAN_ENABLE;
	a->i_val        = enable;
	a->callback     = ayyi_handler_simple;
	a->app_data     = ayyi_handler_data_new(callback, user_data);

	am_action_execute(a);
}


/*
 *  Return only the first routing connection
 *  - api is provisional and is likely to change!
 */
AyyiConnection*
am_channel__get_connected_to (AMChannel* ch)
{
	AyyiTrack* ayyi_track = ayyi_song__audio_track_at(ch->ident.idx);
	g_return_val_if_fail(ayyi_track, NULL);

	AyyiList* routing = ayyi_list__first(ayyi_track->output_routing);

	if(routing) return ayyi_song__connection_at(routing->id);

	return NULL;
}


bool
am_channel__is_type (AMChannel* ch, gpointer _type)
{
	ChannelType type = (ChannelType)_type;
	return ch->type == type;
}


const char*
am_channel__print_type (AMChannel* ch)
{
	static char s[28];

	switch (ch->type){
#define CASE(x) case AM_CHAN_##x: return ""#x
		CASE (INPUT);
		CASE (BUS);
		CASE (GROUP);
		CASE (MASTER);
#undef CASE
		default:
			snprintf (s, 27, "UNKNOWN OBJECT TYPE (%d)", ch->type & 0xff);
			return s;
	}
	return NULL;
}

