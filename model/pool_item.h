/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __pool_item_h__
#define __pool_item_h__

#include "model/model_types.h"
#include "waveform/waveform.h"

G_BEGIN_DECLS

#define AM_TYPE_POOL_ITEM            (am_pool_item_get_type ())
#define AM_POOL_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AM_TYPE_POOL_ITEM, AMPoolItem))
#define AM_POOL_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AM_TYPE_POOL_ITEM, AMPoolItemClass))
#define AM_IS_POOL_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AM_TYPE_POOL_ITEM))
#define AM_IS_POOL_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AM_TYPE_POOL_ITEM))
#define AM_POOL_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AM_TYPE_POOL_ITEM, AMPoolItemClass))

typedef struct _AMPoolItem      AMPoolItem;
typedef struct _AMPoolItemClass AMPoolItemClass;

struct _AMPoolItem
{
	Waveform           waveform;

	AyyiIdx            pod_index[2];    // -1 if not set.
	guint64            source_id[2];

	char               filename[AYYI_FILENAME_MAX]; // this should be the exact name in the songcore.
	char               leafname[256];   // the filename without directory info.
	int                state;

	int                channels;        // channels can be the combined width of more than one file.

	bool               peakfile_valid;

	void*              gui_ref;         // eg GtkTreeRowReference*
};

struct _AMPoolItemClass {
	WaveformClass parent_class;
};

enum
{
   AM_POOL_ITEM_NEW = 0,
   AM_POOL_ITEM_PENDING,   // waiting for remote confirmation
   AM_POOL_ITEM_BAD,
   AM_POOL_ITEM_INACCESSIBLE,
   AM_POOL_ITEM_OK,
   AM_POOL_ITEM_STATE_MAX
};


GType       am_pool_item_get_type         () G_GNUC_CONST;
AMPoolItem* am_pool_item_new              ();
AMPoolItem* am_pool_item_new_from_ayyi    (AyyiFilesource*);
#ifdef __am_private__
void        am_pool_item_set_from_ayyi    (AMPoolItem*, AyyiFilesource*);
#endif
bool        am_pool_item_set_file         (AMPoolItem*, const char* fname, bool fast);
void        pool_item_add_channel         (AMPoolItem*, AyyiFilesource*);
bool        pool_item_file_exists         (AMPoolItem*);
GList*      pool_item_get_regions         (AMPoolItem*);
int         pool_item_count_regions       (AMPoolItem*);
bool        pool_item_get_default_region  (AMPoolItem*, uint32_t* region_idx);
bool        pool_item_is_raw              (AMPoolItem*);
int         pool_item_get_width           (AMPoolItem*);
char*       am_pool_item_get_full_path    (AMPoolItem*, int ch_num);
void        pool_item_get_display_name    (AMPoolItem*, char* name);
bool        pool_item_in_core             (AMPoolItem*);
void        pool_item_load_peak           (AMPoolItem*, int ch_num);
void        pool_item_print               (const AMPoolItem*);
const char* pool_item_print_state         (const AMPoolItem*);

bool        pool_item_is_nonempty         (AMPoolItem*, gpointer);
bool        pool_item_is_new              (AMPoolItem*, gpointer);

G_END_DECLS

#endif
