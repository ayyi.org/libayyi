/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __model_plugin_h__
#define __model_plugin_h__

#include <gdk-pixbuf/gdk-pixbuf.h>

#define NO_PLUGIN "No plugin"

enum {
  PLUGIN_TYPE_VST,
  PLUGIN_TYPE_DSSI
};

struct _plug
{
	char name[64];
	int  type;
	int  shm_num;
};

typedef struct
{
	GHashTable* map;
} AMPlugins;

extern AMPlugins am_plugins;

void       am_plugin__init             ();
AMPlugin*  am_plugin__get_by_idx       (int);
AMPlugin*  am_plugin__get_by_name      (const char*);

void       am_plugin__sync             ();
GList*     am_plugin__list             ();

GdkPixbuf* am_plugin__get_icon         (AMPlugin*);

void       am_plugin__change           (AMChannel*, AyyiIdx plugin_slot, const char* plugin_name, AyyiHandler, gpointer);

bool       am_plugin__name_valid       (const char* plugin_name);

#endif
