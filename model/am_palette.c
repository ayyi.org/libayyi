/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <model/common.h>
#include <model/song.h>
#include "am_palette.h"


/*
 *  Initialise the colour palette.
 *  The first 5 colours are the main gtk style colours (done separately)
 */
void
am_palette_load (Palette palette)
{
	int i;

	palette[5] = 0xffff00ff;
	palette[6] = 0xff6600ff;
	palette[7] = 0xff0000ff;
 
	for(i=0;i<8;i++) palette[8+i]  = ((i * 0xff / 8) << 24) + ((i * 0xff / 8) << 16) + (0xff << 8)           + 0xff;
	for(i=0;i<8;i++) palette[16+i] = ((i * 0xff / 8) << 24) + (0xff           << 16) + ((i * 0xff / 8) << 8) + 0xff;
	for(i=0;i<8;i++) palette[24+i] = (0xff           << 24) + ((i * 0xff / 8) << 16) + ((i * 0xff / 8) << 8) + 0xff;
	for(i=0;i<8;i++) palette[32+i] = ((0xff)         << 24) + ((i * 0xff / 8) << 16) + (0              << 8) + 0xff;
	for(i=0;i<8;i++) palette[40+i] = 0                      + ((i * 0xff / 8) << 16) + (0xff           << 8) + 0xff;
	for(i=0;i<8;i++) palette[56+i] = ((i * 0xff / 8) << 24) + ((i * 0xff / 8) << 16) + ((i * 0xff / 8) << 8) + 0xff;

	for(i=48;i<56;i++) palette[i] = (((((int64_t)rand()) * 0xffffff) / RAND_MAX) << 8) + 0xff;

	if(palette[0] && palette[1] && palette[2]){ // dont emit unless the gtk style is loaded
		am_song__emit("palette-change");
	}
}


#define ensure_colour_idx(c_idx) \
  if(c_idx < 0 || c_idx > 63){ \
    perr ("colouridx out of range: %i", c_idx); \
    c_idx = 0; \
  }


void
am_palette_get_float (Palette palette, int c_idx, float* r, float* g, float* b, const unsigned char alpha)
{
	ensure_colour_idx(c_idx);

	am_rgba_to_float(palette[c_idx], r, g, b);
}


void
am_rgba_to_float (uint32_t rgba, float* r, float* g, float* b)
{
	double _r = (rgba & 0xff000000) >> 24;
	double _g = (rgba & 0x00ff0000) >> 16;
	double _b = (rgba & 0x0000ff00) >>  8;

	*r = _r / 0xff;
	*g = _g / 0xff;
	*b = _b / 0xff;
	dbg (3, "%08x --> %.2f %.2f %.2f", rgba, *r, *g, *b);
}


/*
 *  Return either black or white, to contrast best with the the given colour.
 *
 *  Using b/w doesnt always work, as the contrast can be too high with at least some themes.
 *
 *  There is more than one approach we can take here.
 *    - one is to use brighter darker values of the original colour.
 *    - another is to find a suitable theme colour.
 */
uint32_t
am_palette_get_contrasting (Palette palette, int colour_index)
{
	int r = (palette[colour_index] & 0xff000000) >> 24;
	int g = (palette[colour_index] & 0x00ff0000) >> 16;
	int b = (palette[colour_index] & 0x0000ff00) >>  8;
	int a = (palette[colour_index] & 0x000000ff);

	bool is_dark = am_palette_is_dark(palette, colour_index);
	uint32_t contrasting = is_dark ? 0xffffff00 + a : 0x00000000 + a;

	if(b){ // not black   //TODO make it work for other colours, and !is_dark
		if(is_dark){
			#define SATURATION 3    // unless we go into saturation, colours will not be bright enough.
			#define BLK_OFFSET1 20  // the colour furthest from the dominant colour needs to be boosted less.
			#define BLK_OFFSET2 40
			guint factor = ((0xff * SATURATION) << 8 ) / b; // its possible that this may need to use the *average* colour

			contrasting =
				  (MIN(((r + BLK_OFFSET1) * factor), 0xff) << 24)
				+ (MIN(((g + BLK_OFFSET2) * factor), 0xff) << 16)
				+ (MIN(((b              ) * factor), 0xff) <<  8)
				+ a;
		}
	}

	return contrasting;
}


bool
am_palette_is_dark (Palette palette, int colour_index)
{
	int r = (palette[colour_index] & 0xff000000) >> 24;
	int g = (palette[colour_index] & 0x00ff0000) >> 16;
	int b = (palette[colour_index] & 0x0000ff00) >>  8;

	return (r + g + b) / 3 < 0x7f;
}


void
colour_get_rgb_str (char* colorstr, int c_idx)
{
	ensure_colour_idx(c_idx);

	int r = (song->palette[c_idx] & 0xff000000) >> 24;
	int g = (song->palette[c_idx] & 0x00ff0000) >> 16;
	int b = (song->palette[c_idx] & 0x0000ff00) >>  8;

	snprintf(colorstr, 64, "#%02x%02x%02x", r, g, b);
}


void
colour_get_rgba_str (char* colorstr, int c_idx, const unsigned char alpha)
{
	ensure_colour_idx(c_idx);

	int r = (song->palette[c_idx] & 0xff000000) >> 24;
	int g = (song->palette[c_idx] & 0x00ff0000) >> 16;
	int b = (song->palette[c_idx] & 0x0000ff00) >>  8;

	snprintf(colorstr, 64, "0x%02x%02x%02x%02x", r, g, b, alpha);
}

