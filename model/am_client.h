/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2007-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_client_h__
#define __am_client_h__

#include <glib.h>

#include "ayyi/ayyi_client.h"

const AyyiClient*
             am_init                (AyyiService**);
void         am_uninit              ();
void         am_connect_all         (AyyiHandler2, gpointer);
void         am_connect_services    (AyyiService**, AyyiHandler2, gpointer);
bool         am_connect_messaging   (AyyiService*, AyyiHandler2, gpointer, GError**);
void         am_get_shm             (AyyiService*, void (*on_shm)(AyyiShmCallbackData*), gpointer);
bool         am_get_shm_            (AyyiService**, void (*on_shm)(AyyiShmCallbackData*), gpointer);

#ifdef __GI_SCANNER__
void         am_init_2              (AyyiService** services);
#endif

#endif
