/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Ayyi;

public abstract class Ayyi.Model.Collection<T> : GLib.Object
{
	/*

	Model collections have the following features:
		-provide unified interface for different data types.
		-emit the following signals: add, delete, change, selection-change.
		-support for Selection lists.
		-support for iterators and filtering.

	Does not currently support:
		-sorting

	Iterators are designed to be lightweight and stack allocated.
	Unlike in libgee, they are not gobjects.
	FilterableIterator supports a filter function to iterate over a subset of the collection.

	TODO
	  -some types dont yet have implementations for append/remove.
	   (should these fns also emit signals?)

	*/

	//public int               length;       //need to implement add/remove before we can use this.
	public    GLib.List<T*>    selection;
	protected GLib.List<void*> pending;      //externally(?!) created items that are not yet complete enough to be useable.
	public    void*            user_data;

	public signal void add              (T* item);
	public signal void delete           (T* item);
	public signal void change           ();
	public signal void item_changed     (T* item, /*ChangeType*/uint change, void* sender);
	public signal void selection_change (void* sender);

	~Collection ()
	{
		//print("~Collection\n");
	}

	public abstract uint length     ();
	public abstract bool contains   (T item);
	public abstract T    find_by_id (uint64 id);
	public abstract void append     (T item);
	public abstract void remove     (T item);
	/*
	public T* first()
	{
		return list.first();
	}
	*/

	public abstract void iter_init (Model.Iter* iter);
	/*
	{
		//caller creates the iterator so it can be created on the stack.

		iter->i = 0;
		iter->ptr = null;
		iter->list = list;
		iter->next = (iter) => {
			GList* ptr = ptr->next;
			return ptr->data;
		}
	}
	*/
	public abstract void* iter_next (Model.Iter* iter);

	public void selection_reset (void* sender)
	{
		//selection->free(); // whats the syntax?
		while((bool)selection.length()){ //TODO slow -- just check first element
			selection.delete_link(selection);
		}
		selection_change(sender);
	}

	public void selection_replace (GLib.List<T*> items, void* sender)
	{
		//clear any existing part selections, and replace them with new ones.

		//@param items: ownership of this list is taken. The caller must not free it, and if necesary must duplicate it before passing in.

		while((bool)selection/* && (bool)selection.data*/){
			selection.delete_link(selection);
		}

		selection_add(items, sender);
	}

	/*
	 *   @items: (transfer none)
	 */
	public void selection_add (GLib.List<T*> items, void* sender)
	{

		GLib.List<T*>* l = items;
		for(;(bool)l;l=l->next){
			if((bool)selection.find(l->data)){
				print("*** already in selection list.\n");
			}else{
				// [ReturnsModifiedPointer ()] --- what?
				selection.prepend(l->data);
			}
		}

		if(Ayyi.song->loaded) selection_change (sender);
	}

	public void selection_remove (T item, void* sender)
	{
		selection.remove(item);
		if(Ayyi.song->loaded) selection_change (sender);
	}

	public T selection_first ()
	{
		unowned GLib.List<T*> first = selection.first();
		return (bool)first ? first.data : null;
	}
}


public class Ayyi.AM.Array<T> : Ayyi.Model.Collection<T>
{
	public GLib.PtrArray array;

	public Array (int n)
	{
		array = new GLib.PtrArray.sized(n);
	}

	public T* at (int index)
	{
		return array.index(index);
	}

	public override uint length ()
	{
		return array.len;
	}

	public override bool contains (T item)
	{
		return false;
	}

	public override T find_by_id (uint64 id)
	{
		return null;
	}

	public override void append (T item)
	{
	}

	public override void remove (T item)
	{
	}

	public override void iter_init (Model.Iter* iter)
	{
		//caller creates the iterator so it can create on stack

		iter->i = 0;
		iter->ptr = null;
		iter->array = array;
	}

	public override T* iter_next (Model.Iter* iter)
	{
		if(iter->i >= iter->array.len || iter->i < 0) return null;

		T* item = iter->array.index(iter->i);
		iter->i++;
		return item;
	}
}


public class Ayyi.AM.List<T> : Ayyi.Model.Collection<T>
{
	public GLib.List<T*> list;

	public override uint length ()
	{
		return list.length();
	}

	public override bool contains (T item)
	{
		return (bool)list.find(item);
	}

	/*
	 *  Note that this cannot be used for PoolItem's because there is no 1:1 correspondence
	 *  (A stereo item has 2 Ayyi.Filesource's)
	 */
	public override T find_by_id (uint64 id)
	{
		//TODO check this doesnt return a _copy_ of the item.

		unowned GLib.List<T*> l = (GLib.List<T*>)list;
		for(;(bool)l;l=l.next){
			AM.Part* part = (AM.Part*)l.data;
			if((bool)part->ayyi && part->ayyi->id == id) return part;
		}
		return null;
	}

	public override void append (T item)
	{
		AM.Part* part = (AM.Part*)item;

		if(part.status == AM.PartStatus.PENDING/* || part.status == AM.PartStatus.LOCAL*/)
			pending.append(item);
		else
			list.append(item);
	}

	public override void remove (T item)
	{
		list.remove(item);
		pending.remove(item);
		selection.remove(item);
	}

	public T first ()
	{
		if(!(bool)list) return null;
		return list.first().data;
	}

	public override void iter_init (Model.Iter* iter)
	{
		//caller creates the iterator so it can create on heap

		iter->i = 0;
		iter->ptr = list;
		iter->list = list;
		/*
		iter->next = (void*)iter_next; //either iterators are objects or they are not. If not, there is no purpose in this as we have to pass in the context anyway.

		iter->next = (iter) => {
			GLib.List<T*>* ptr = ptr->next;
			return ptr->data;
		};
		*/
	}

	public override T* iter_next (Model.Iter* iter) //returning T* gives a gconstpointer - why needs to be cast?
	{
		if(!(bool)iter->ptr) return null;

		unowned GLib.List<T*> l = (GLib.List<T*>)iter->ptr;
		/*GLib.List<T*>**/ iter->ptr = l.next;
		if(!(bool)l.data){
			if(iter->list == list){
				iter->ptr = iter->list = pending;
				l = (GLib.List<T*>)iter->ptr;
			}
		}
		return l.data;
	}

	public T find_by_ident (Ident id)
	{
		unowned GLib.List<T*> l = (GLib.List<T*>)list;
		for(;(bool)l;l=l.next){
			AM.Part* part = (AM.Part*)l.data;
			if(part->ident == id) return part;
		}
		return null;
	}

	/* TODO currently we cannot implement this generically as items do not have a standard way of storing the idx
	 *       !! this only works for Parts.
	 */
	public T find_by_idx (int idx)
	{
		//returns the item for the given core object idx.
		//-failing to find an item is not an error.

		unowned GLib.List<T*> l = (GLib.List<T*>)list;
		for(;(bool)l;l=l.next){
			AM.Part* part = (AM.Part*)l.data;
			if((part.status != AM.PartStatus.LOCAL) && ((bool)part->ayyi) && (part->ayyi->shm_idx == idx)) return part;
		}
		l = (GLib.List<T*>)pending;
		for(;(bool)l;l=l.next){
			AM.Part* part = (AM.Part*)l.data;
			if((bool)part->ayyi && part->ayyi->shm_idx == idx) return part;
		}
		//dbg (2, "gui part data not found. objidx=%i\n", part_idx);
		return null;
	}

	public void unpend (T item)
	{
		pending.remove(item);
		list.append(item);
	}

	public bool is_pending (Ayyi.Idx idx, Ayyi.MediaType type)
		requires(type == MediaType.AUDIO || type == MediaType.MIDI)
	{
		unowned GLib.List<T*> l = (GLib.List<T*>)pending;
		for(;(bool)l;l=l.next){
			AM.Part* part = (AM.Part*)l.data;
			if(part->ident.idx == idx && part->ident.type == (type == MediaType.AUDIO ? ObjectType.AUDIO_PART : ObjectType.MIDI_PART)) return true;
		}
		return false;
	}
}


public class Ayyi.AM.TrackList<T> : Ayyi.Model.Collection<T>
{
	public Track* track[48]; //pointer array. non-sparse, and NULL terminated.
	public int    last = 0;  //auto incremented user-track-number for new tracks. If it is zero, then no tracks have ever been created. Note: this track may have been deleted.

	//int         track_count;   //formerly the number of tracks for which gui widgets exist. Now is the number of core tracks.
	                             //-has been removed due to its lack of semantic clarity.

	/*
	public T* first()
	{
		return list.first();
	}
	*/
	public Track* at (int index)
	{
		if(index < 0 || index > 47) return null;
		return track[index];
	}

	public override uint length ()
	{
		return count();
	}

	public override bool contains (T item)
	{
		return false;
	}

	public override T find_by_id (uint64 id)
	{
		return null;
	}

	public override void append (T item)
	{
	}

	public override void remove (T item)
	{
	}

	public int count ()
	{
		//returns the number of active tracks.

		int t; for(t=0;t<AM.MAX_TRK;t++){
			if(!(bool)song->tracks->track[t] || !(bool)song->tracks->track[t]->type) return t;
		}
		print("*** TrackList.count !!\n");
		return int.max(t, AM.MAX_TRK - 1);
	}

	public int get_visible_count ()
	{
		//audio only!

		int n = 0;
		Track* trk = null;
		TrackNum t = 0; for(; (bool)(trk = song->tracks->track[t]); t++){
			if(trk->visible) n++;
		}
		return n;
	}


	public Track* find_by_shm_idx (Ayyi.Idx shm_idx, TrackType type)
	{
		TrackNum t;
		for(t=0; t<AM.MAX_TRK && (bool)song->tracks->track[t] && (bool)song->tracks->track[t]->type; t++){
			if((song->tracks->track[t]->type == type) && (song->tracks->track[t]->ident.idx == shm_idx)) return song->tracks->track[t];
		}
		return null;
	}

	public Track* find_by_ident (Ident id)
	{
		TrackType type = 
			(id.type == ObjectType.AUDIO_TRACK || id.type == ObjectType.TRACK)
				? TrackType.AUDIO
				: (id.type == ObjectType.MIDI_TRACK) ? TrackType.MIDI : 0;
		return find_by_shm_idx(id.idx, type);
	}

	//TODO remove and use find_by_shm_idx with TrackType.AUDIO instead?
	public Track* find_audio_by_idx (Ayyi.Idx shm_index)
	{
		int t; for(t=0;t<AM.MAX_TRK;t++){
			if((bool)song->tracks->track[t] && song->tracks->track[t]->type == TrackType.AUDIO && song->tracks->track[t]->ident.idx == shm_index) return song->tracks->track[t];
		}
		return null;
	}

	public Track* find_by_name (char* name)
	{
		int t; for(t=0;t<AM.MAX_TRK;t++){
			Track* tr = song->tracks->track[t];
			if((bool)tr && song->tracks->track[t]->name == name) return song->tracks->track[t];
		}
		return null;
	}

	public bool is_known (TrackType type, Ayyi.Idx shm_num)
	{
		//return TRUE if the engine track has a corresponding local object.

		int t; for(t=0;t<AM.MAX_TRK;t++){
			if((bool)song->tracks->track[t] && song->tracks->track[t]->type == type && song->tracks->track[t]->ident.idx == shm_num) return true;
		}
		return false;
	}

	public Track* get_first_visible (TrackType type)
	{
		int t; for (t=0; t<AM.MAX_TRK && (bool)song->tracks->track[t] && (bool)song->tracks->track[t]->type; t++){
			if(song->tracks->track[t]->visible && (!(bool)type || song->tracks->track[t]->type == type)) return song->tracks->track[t];
		}
		if((bool)count()) print("*** get_first_visible(): not found...! t=%i\n", t);
		return null;
	}

	[CCode (has_target = false)]
	public delegate bool TrackEach(Track* track, void* user_data);

	public void each (TrackEach fn, void* user_data)
	{
		Model.Iter iter;
		iter_init(&iter);
		Track* track;
		while((bool)(track = iter_next(&iter))){
			fn(track, user_data);
		}
	}

	public bool verify_track (Track* trk)
	{
		for(TrackNum t=0;t<AM.MAX_TRK;t++){
			Track* tr = song->tracks->track[t];
			if(tr == trk) return true;
		}
		return false;
	}

	public bool verify_tracks ()
	{
		Model.Iter iter;
		iter_init(&iter);
		return true;
	}

	public override void iter_init (Model.Iter* iter)
	{
		iter->i = 0;
		iter->ptr = null;
		iter->list = null;
	}

	public override Track* iter_next (Model.Iter* iter)
	{
		Track* tr = song->tracks->track[iter->i];
		iter->i++;
		if((bool)tr && AM.track__is_master(tr)){
			tr = song->tracks->track[iter->i++];
		}
		return tr;
	}
}


public delegate bool Filter(void* item);

public class Ayyi.FilterIterator<T>
{
	public Model.Collection* list;
	public Model.Iter iter;
	public Filter accept;

	public FilterIterator(Model.Collection* _list, Filter _accept)
	{
		if(_debug_ > 1) print("FilterIterator\n");
		list = _list;
		accept = _accept;
		list->iter_init(&iter);
	}

	public T* next()
	{
		T* _next = list->iter_next(&iter);
		//print("FilterIterator.next %p\n", _next);
		while((bool)_next && !accept(_next)){
			_next = list->iter_next(&iter);
			if(_debug_ > 2) print("FilterIterator.next %p\n", _next);
		}
		return _next;
	}
}

