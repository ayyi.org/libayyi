using GLib;
using Gdk;
using Ayyi;

public class Ayyi.ClientController : GLib.Object
{
	private int _property1 = 1;
	public int property1 {
		get { return _property1; }
		//set { _name = value; }
	}

	public signal void periodic_update        ();

	public signal void song_load              ();
	public signal void song_unload            ();
	public signal void song_length_change     ();
	public signal void tempo_change           ();
	public signal void locators_change        ();
	public signal void progress               (double val);

	public signal void transport_sync         ();
	public signal void transport_stop         ();
	public signal void transport_play         ();
	public signal void transport_rew          ();
	public signal void transport_ff           ();
	public signal void transport_cycle        ();
	public signal void transport_rec          ();

	public signal void pool_item_change       (AM.PoolItem* pool_item);

	public signal void tracks_change          (int t1, int t2, int changetype);
	public signal void tracks_change_by_list  (GLib.List<Track*> tracks, uint changetype, void* sender);

	public signal void region_delete          (int region); //this is only emitted for non-Part regions.

	public signal void note_selection_change  (void* sender, GLib.List<MidiNote*> notes);

	public signal void plugin_change          (Channel* channel);
	public signal void curve_change           (AM.Curve* curve);
	public signal void palette_change         ();
	public signal void songmap_change         ();

	public bool loaded;

	//public AM.Object               loc[Loc.MAX_LOC];   //locator positions. 1=LEFT, 2=RIGHT   ---- Loc.MAX_LOC is correct but valac 0.12 wants a 'literal'.
	public AM.Object                 loc[10];            //locator positions. 1=LEFT, 2=RIGHT
	public int                       start;              //beats. Affects playback and arrange canvas display.
	public bool                      end_auto;           //if true, song->end will follow the song contents.
	public int                       sample_rate;
	public float                     beats2bar;          //float??? useful for canvas ops. useful for strange time sigs?

	public char*                     audiodir;           //audio files are found relative to this directory.
	                                                     //It would normally be relative to the project dir, but also can be absolute.
	public char*                     peaksdir;           //full path to the directory containing audio peak files.

	public Gtk.Object*               tempo_adj;

	public QMap                      q_settings[5];      // should be AM.QType.Q_MAX but that gives 'Expression of constant integer type expected'
	public AM.SnapType               snap_mode;          //arguably should be in Arrange.

	public AM.TrackList*             tracks;
	public AM.List<AM.PoolItem*>* pool;
	public AM.Array<Channel*>*       channels;
	public AM.List<AM.Part*>* parts;
	public AM.List*                  map_parts;          //songmap parts that show above the rulerbar.

	public GLib.List<void*>          input_list;         //list of available input channels (currently only used for mixer strip input selection)
	public char*                     input_str;          //one big buffer for all the input channel labels.
	public GLib.List<void*>          bus_list;
	public char*                     bus_strs;

	public GLib.List<void*>          plugins;

	public AM.Palette                palette;

	public uint                      periodic;           //timer

	public GLib.List<void*>          work_queue;

	public ClientController() {
		palette_change(); //TODO constructor cannot be empty else it will not exist.
	}

	//private GLib.List<void*>         parts_pending;      //externally created parts that are not yet complete enough to be useable.
}

