/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Ayyi;
using AM;

public class AM.PartInput : GLib.Object
{
    private Ayyi.MediaType    _media_type;
    private Ayyi.Idx          _src_region_idx;
	private string            _name;
	private Ayyi.AM.PoolItem* _pool_item;
    private Ayyi.Idx*         _track_idx;
    private Ayyi.SongPos*     _start_time;
	public  int64             _length;

	public PartInput ()
	{
		_media_type = Ayyi.MediaType.AUDIO;
		_src_region_idx = 0;
	}

	public string name {
		get { return _name; }
		set { _name = value; }
	}

	public Ayyi.AM.PoolItem* pool_item {
		get { return _pool_item; }
		set { _pool_item = value; }
	}

	public Ayyi.Idx* track {
		get { return _track_idx; }
		set { _track_idx = value; }
	}

	public Ayyi.SongPos* start {
		get { return _start_time; }
		set { _start_time = value; }
	}

	public int64 length {
		get { return _length; }
		set { _length = value; }
	}
}
