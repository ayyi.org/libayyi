using GLib;
using Gdk;
using Ayyi;
using AM;

public delegate void ResultDelegate (Ident id);

namespace ValaUtils {
	/*
	 *  A variation on Song.add_file that allows clients to use yields.
	 */
	public static async void
	add_file (string file, ResultDelegate cb)
	{
		AM.Song.add_file(file, (id)=>{
			cb(id);
			add_file.callback();
		});
		yield;
	}

	/*
	 *  A variation on Song.add_part that allows clients to use yields.
	 */
	public static async void
	add_part (AM.PoolItem* sample, Ayyi.Idx region_idx, Track* track, SongPos* pos, char* name, ResultDelegate cb)
	{
		uint32 colour = 10;
		AM.Song.add_part(MediaType.AUDIO, region_idx, sample, track->ident.idx, pos, 0, colour, name, 0, (id, error)=>{
			cb(id);
			add_part.callback();
		});
		yield;
	}

	public async void
	move_part (AM.Part* part, SongPos* position, ResultDelegate cb)
	{
		AM.move_part(part, position, part->track, (ob, error) => {
			cb(ob);
			move_part.callback();
		});
		yield;
	}
}

