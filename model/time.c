/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __am_time_c__
#include <model/common.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include "model/song.h"
#include "model/time.h"

extern const uint64_t mu_per_sub;
extern const uint32_t ticks_per_beat;


/*
 *	Returns the bar for the given AMPos.
 */
int
pos_bar (AMPos* pos)
{
	g_return_val_if_fail(pos, -1);

	return pos->beat / ((int)song->beats2bar);
}


/*
 *  Returns the beat number within the bar for the given AMPos.
 */
int
pos_beat (AMPos* pos)
{
	g_return_val_if_fail(pos, 0);

	return (pos->beat % (int)song->beats2bar);
}


/*
 *  Converts a song position to a display string in bars, beats, subbeats, ticks.
 */
void
pos2bbst (AMPos* pos, char* str)
{
	sprintf(str, AYYI_BBST_FORMAT_SHORT,
		pos_bar(pos),
		pos_beat(pos) + 1,
		pos->sub + 1,     // 1 - 4
		pos->tick);       // 0 - AYYI_TICKS_PER_SUBBEAT
}


/*
 *  Converts a song position to a display string in bars, beats, subbeats, ticks.
 *  Zero indexing is used.
 *  This is used for lengths, and not for positions.
 */
void
pos2bbst0 (AMPos* pos, char* str)
{
	sprintf(str, AYYI_BBST_FORMAT_SHORT,
		pos_bar(pos),
		pos_beat(pos),
		pos->sub,     // 0 - 3
		pos->tick);   // 0 - AYYI_TICKS_PER_SUBBEAT
}


/*
 *  Converts a song position to a display string in bars, beats, subbeats.
 *  i.e. this is a shorter form than pos2bbst.
 */
void
pos2bbs (AMPos* pos, char* str)
{
	sprintf(str,
		"%03i:%02i:%03i",
		pos_bar(pos),
		pos_beat(pos) +1,
		pos->sub / 960 + 1
	);
}


uint64_t
pos2mu (AMPos* pos)
{
	return
		  ((uint64_t)pos->beat) * ((uint64_t)AYYI_TICKS_PER_BEAT) * ((uint64_t)AYYI_MU_PER_TICK)
		+ pos->sub  * AYYI_TICKS_PER_SUBBEAT * AYYI_MU_PER_TICK
		+ pos->tick                          * AYYI_MU_PER_TICK;
}


double
pos2beats (AMPos* pos)
{
	return (double)pos->beat + (double)(pos->sub / SUBS_PER_BEAT);
}


bool
songpos_ayyi2gui (AMPos* gpos, AyyiSongPos* ayyi_pos)
{
	//copies one song_pos to another.
	//note: the core mu value is thrown away as its too small.

	g_return_val_if_fail(gpos, false);
	g_return_val_if_fail(ayyi_pos, false);
	if(ayyi_pos->sub > AYYI_SUBS_PER_BEAT){ perr ("core sub out of range: %i.", ayyi_pos->sub); return FALSE; }

#if 0
	gpos->beat = ayyi_pos->beat;
	gpos->sub  = ayyi_pos->sub / AYYI_TICKS_PER_SUBBEAT;
	gpos->tick = ayyi_pos->sub % AYYI_TICKS_PER_SUBBEAT;
#else
	int64_t mu = ayyi_pos2mu(ayyi_pos);
	mu2pos(mu, gpos);
#endif

	AYYI_DEBUG_3 {
		char bbst[32]; pos2bbst(gpos, bbst); dbg (0, "%i:%i:%u -> %s", ayyi_pos->beat, ayyi_pos->sub, ayyi_pos->mu, bbst);
	}

	return true;
}


AyyiSongPos*
songpos_gui2ayyi (AyyiSongPos* corepos, AMPos* gpos)
{
	//copies/translates one song_pos to another.

	g_return_val_if_fail (corepos, NULL);
	g_return_val_if_fail (gpos, NULL);
	if (gpos->sub > 4){ perr ("bad gpos arg: subbeat out of range: %i.", gpos->sub); return NULL; }

	corepos->beat = gpos->beat;
	corepos->sub  = gpos->tick + gpos->sub * 960;
	corepos->mu   = 0; //the gui songpos is not high enough res for this.

	dbg (2, "%03i:%02i:%03i -> %03i:%03i:%03i.", gpos->beat, gpos->sub, gpos->tick, corepos->beat, corepos->sub, corepos->mu);

	return corepos;
}


bool
am_pos_is_valid (AMPos* pos)
{
	g_return_val_if_fail(pos, false);

	if(pos->sub  > 4                     ){ perr ("sub out of range (%i)",   pos->sub);  pos->sub  = 3; return FALSE; }
	if(pos->tick > AYYI_TICKS_PER_SUBBEAT){ perr ("ticks out of range (%i)", pos->tick); pos->tick = AYYI_TICKS_PER_SUBBEAT-1; return FALSE; }
	return true;
}


bool
am_pos_is_empty (AMPos* pos)
{
	return (!pos->beat && !pos->sub && !pos->tick);
}


bool
pos_is_before (const AMPos* pos1, const AMPos* pos2)
{
	//returns true if pos1 is before pos2.

	if(pos1->beat < pos2->beat) return TRUE;
	if(pos1->beat > pos2->beat) return FALSE;
	if(pos1->sub  < pos2->sub ) return TRUE;
	if(pos1->sub  > pos2->sub ) return FALSE;
	if(pos1->tick < pos2->tick) return TRUE;
	return FALSE;
}


bool
pos_is_after (const GPos* pos1, const GPos* pos2)
{
	//returns true if pos1 is after pos2.

	if(pos2->beat < pos1->beat) return TRUE;
	if(pos2->beat > pos1->beat) return FALSE;
	if(pos2->sub  < pos1->sub ) return TRUE;
	if(pos2->sub  > pos1->sub ) return FALSE;
	if(pos2->tick < pos1->tick) return TRUE;
	return FALSE;
}


void
mu2pos (uint64_t mu, GPos* pos)
{
	g_return_if_fail(pos);

	uint32_t beats      = mu / AYYI_MU_PER_BEAT;
	uint32_t remainder  = mu % AYYI_MU_PER_BEAT;

	uint32_t subs = remainder / mu_per_sub;
	remainder     = remainder % mu_per_sub;

	pos->beat = beats;
	pos->sub  = subs;
	pos->tick = remainder / AYYI_MU_PER_TICK;
	remainder = remainder % AYYI_MU_PER_TICK;
	if(remainder > (AYYI_MU_PER_TICK / 2)) pos->tick++;
	if(pos->tick == AYYI_TICKS_PER_SUBBEAT){ pos->sub++; pos->tick = 0; } //oh dear
	if(pos->sub  == SUBS_PER_BEAT    ){ pos->beat++; pos->sub = 0; } //oh dear
}


void
pos_add (AMPos* pos, const AMPos* add)
{
	//add "add" to "pos".

	if(!am_pos_is_valid(pos)) perr("pos");
	if(!am_pos_is_valid((GPos*)add)) perr("add");

	unsigned short tick = pos->tick + add->tick;
	pos->tick = tick % AYYI_TICKS_PER_SUBBEAT;

	unsigned short sub = pos->sub + add->sub + tick / AYYI_TICKS_PER_SUBBEAT;
	pos->sub = sub % SUBS_PER_BEAT;

	pos->beat = pos->beat + add->beat + sub / SUBS_PER_BEAT;
}


void
pos_subtract (GPos* pos, const GPos* subtract)
{
	char bbst0[64]; pos2bbst(pos, bbst0);

	int tick    = AYYI_TICKS_PER_SUBBEAT + pos->tick - subtract->tick; //borrow 1 to make sure tick is positive.
	pos->tick   = tick % AYYI_TICKS_PER_SUBBEAT;

	int subbeat = pos->sub - 1 + tick / AYYI_TICKS_PER_SUBBEAT;
	subbeat     = SUBS_PER_BEAT + subbeat - subtract->sub;
	pos->sub    = subbeat % SUBS_PER_BEAT;

	pos->beat  += -1 + subbeat / SUBS_PER_BEAT - subtract->beat;

	dbg(2, "beatx=%i", (int)(subbeat / SUBS_PER_BEAT));

	char bbst1[64], bbst2[64]; pos2bbst(pos, bbst1); pos2bbst((GPos*)subtract, bbst2); dbg(2, "pos=%s --> %s sub=%s", bbst0, bbst1, bbst2);
}


void
pos_divide (GPos* pos, int i)
{
	uint64_t p = (pos->beat * AYYI_TICKS_PER_BEAT + pos->sub * AYYI_TICKS_PER_SUBBEAT + pos->tick) / i;

	pos->beat = p / AYYI_TICKS_PER_BEAT;
	p = p % AYYI_TICKS_PER_BEAT;
	pos->sub = p / AYYI_TICKS_PER_SUBBEAT;
	pos->tick = p % AYYI_TICKS_PER_SUBBEAT;
}


int
pos_diff (const GPos* pos1, const GPos* pos2)
{
	//returns the distance between pos1 and pos2 in ticks
	return (pos1->beat - pos2->beat) * SUBS_PER_BEAT * AYYI_TICKS_PER_SUBBEAT + (pos1->sub - pos2->sub) * AYYI_TICKS_PER_SUBBEAT + (pos1->tick - pos2->tick);
}


gboolean
pos_cmp (const GPos* a, const GPos* b)
{
	//return TRUE if the two positions are different.

	return (a->beat != b->beat) || (a->sub != b->sub) || (a->tick != b->tick);
}


void
pos_min (GPos* a, const GPos* b)
{
	// used to find the earliest in a list of parts.
	// if b is earlier than a, a is set to b, else a is unchanged.

	if (b->beat > a->beat) return;
	if (b->beat < a->beat){ *a = *b; return; }
	if (b->sub  > a->sub ) return;
	if (b->sub  < a->sub ){ *a = *b; return; }
	if (b->tick > a->tick) return;
	*a = *b;
}


void
pos_max (GPos* a, const GPos* b)
{
  // the value returned in a is the later of a and b.

  if (b->beat > a->beat){ *a = *b; return; }
  if (b->beat < a->beat) return;
  if (b->sub  > a->sub ){ *a = *b; return; }
  if (b->sub  < a->sub ) return;
  if (b->tick > a->tick){ *a = *b; return; }
}


void
samples2pos (uint64_t samples, GPos* pos)
{
	//see unit test: test_pos_to_samples().

	if(song->sample_rate < 5000){ pwarn("sample_rate not set"); return; }

	float bpm = ((AyyiSongService*)ayyi.service)->song->bpm;
	float beats_per_second = bpm / 60.0;

	float secs = (float)samples / song->sample_rate;
	if(secs > 1000000000){ perr ("secs too high. samples=%"PRIu64" rate=%i", samples, song->sample_rate); return; }

	int beats = secs * beats_per_second;
	float remainder = secs - beats / beats_per_second;
	int subs = remainder * beats_per_second * 4;
	float secs_remainder = remainder - subs / (beats_per_second * 4);

	float ticks = secs_remainder * beats_per_second * ticks_per_beat + 0.5;

	pos->beat = beats;
	pos->sub  = subs;
	pos->tick = (int)ticks;

	if(pos->tick == AYYI_TICKS_PER_SUBBEAT){ pos->tick = 0; pos->sub += 1; }
	if(pos->sub == SUBS_PER_BEAT){ pos->sub = 0; pos->beat += 1; }

	//if(_debug_){ char bbst[64]; pos2bbst(pos, bbst); printf("%s(): %Lu -> %s\n", __func__, samples, bbst); }
}


unsigned
pos2samples (GPos* pos)
{
	float bpm = ((AyyiSongService*)ayyi.service)->song->bpm;

	float length_of_1_beat_in_secs = 60.0/bpm;
	unsigned samples = (unsigned)(length_of_1_beat_in_secs * song->sample_rate * (pos->beat + pos->sub/4.0 + pos->tick/(4.0 * AYYI_TICKS_PER_SUBBEAT)));

	//char bbst[64]; pos2bbst(pos, bbst); printf("pos2samples(): %s -> %u\n", bbst, samples);

	return samples;
}


long
samples2beats (uint64_t samples)
{
	//returns the number of beats, corresponding to the given number of audio samples.

	long long mu = ayyi_samples2mu(samples);
	long beats = mu / (AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT);
	//dbg(0, "flen=%.3f 1beat=%.3fsecs beats=%.3f mu=%Li", secs, length_of_1_beat_in_secs, beats, mu);

	return beats;
}


double           
samples2secs (uint64_t samples)
{
	return samples / (float)song->sample_rate;
}


uint64_t
bars2mu (float bars)
{
	return bars * song->beats2bar * AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT;
}


gboolean
bbst2pos (const char* bbst, GPos* pos)
{
	//not all parts of the bbst string need to be present. We assume that the string starts with Beats.

	g_return_val_if_fail(bbst, false);
	g_return_val_if_fail(pos, false);

	int seg_index;
	*pos = (GPos){0,};

	gchar** split = g_strsplit(bbst, ":", 4);
	g_return_val_if_fail(split, false);
	for(seg_index=0;seg_index<4;seg_index++){
		if(split[seg_index]){
			switch(seg_index){
				case 0:
					pos->beat = 4 * MAX(0, atoi(split[seg_index]));
					break;
				case 1:
					pos->beat += MAX(0, atoi(split[seg_index]) -1);
					break;
				case 2:
					pos->sub = CLAMP(atoi(split[seg_index]) -1, 0, 4);
					break;
				case 3:
					pos->tick = CLAMP(atoi(split[seg_index]), 0, AYYI_TICKS_PER_SUBBEAT);
					break;
			}
		} else break;
	}

	g_strfreev(split);

#ifdef DEBUG
	char check[32]; pos2bbst(pos, check); dbg (2, "%s", check);
#endif
	return TRUE;
}


gboolean
bbss2pos (const char* bbst, AyyiSongPos* pos)
{
	*pos = (AyyiSongPos){0,};

	gchar** split = g_strsplit(bbst, ":", 4);
	g_return_val_if_fail(split, false);
	int i; for(i=0;i<4;i++){
		if(split[i]){
			switch(i){
				case 0:
					pos->beat = 4 * CLAMP(atoi(split[i]) -1, 0, AYYI_MAX_BEATS);
					break;
				case 1:
					pos->beat += MAX(0, atoi(split[i]) -1);
					break;
				case 2:
					pos->sub = (AYYI_SUBS_PER_BEAT / 4) * CLAMP((atoi(split[i]) -1), 0, 4);
					break;
				case 3:
					pos->sub += 5 * CLAMP((atoi(split[i]) -1), 0, 192);
					break;
			}
		} else break;
	}

	g_strfreev(split);

#ifdef DEBUG
	char check[32]; ayyi_pos2bbss(pos, check); dbg (2, "%s", check);
#endif
	return true;
}


#define BBST3 7
#define BBST4 10
bool
bbst_increment (char* bbst)
{
	char orig[20]; strcpy(orig, bbst);

	char ticks_str[6] = {0,};
	sprintf(ticks_str, "%03i", ((uint32_t)(atoi(bbst + BBST4) + 1)) % (AYYI_TICKS_PER_SUBBEAT + 1));
	memcpy(bbst + BBST4, ticks_str, 3);
	dbg(1, "%s", bbst + BBST4);

	return strcmp(bbst, orig);
}


/*
 *  Reduce the bbst value in response to a user action
 */
bool
bbst_decrement (char* bbst)
{
	char orig[20]; strcpy(orig, bbst);

	int ticks = atoi(bbst + BBST4) - 1;
	if(ticks < 0){
		char _subs[6];
		strncpy(_subs, bbst + BBST3, 2);
		_subs[2] = '\0';
		dbg(1, "subs=%s<", _subs);
		char subs = atoi(_subs) - 1;
		if(subs){
			subs--;
			ticks = AYYI_TICKS_PER_SUBBEAT - 1;

			sprintf(_subs, "%02i", subs + 1);
			memcpy(bbst + BBST3, _subs, 2);
		}else ticks = 1;
	}
	char ticks_str[4];
	sprintf(ticks_str, "%03i", ticks % 8);
	memcpy(bbst + BBST4, ticks_str, 3);
	dbg(1, "%s", bbst + BBST4);

	return strcmp(bbst, orig);
}


/*
 *  Initialise song quantise settings.
 *
 *  A quantise map can be of any length. The final entry is an "extra", which gives us the length of the loop.
 */
void
am_q_init ()
{
	QMap* q = &song->q_settings[Q_16];
	strncpy(q->name, "1/16", 63);
	q->size = 4;
	q->pos[0] = (GPos){0,0,0};
	q->pos[1] = (GPos){0,1,0};
	q->pos[2] = (GPos){0,2,0};
	q->pos[3] = (GPos){0,3,0};
	q->pos[4] = (GPos){1,0,0};

	q = &song->q_settings[Q_8];
	strncpy(q->name, "1/8", 63);
	q->size = 2;
	q->pos[0] = (GPos){0,0,0};
	q->pos[1] = (GPos){0,3,0};
	q->pos[2] = (GPos){1,0,0};

	q = &song->q_settings[Q_BEAT];
	strncpy(q->name, "beat", 63);
	q->size = 1;
	q->pos[0] = (GPos){0,0,0};
	q->pos[1] = (GPos){1,0,0};

	q = &song->q_settings[Q_BAR];
	strncpy(q->name, "bar", 63);
	q->size = 1;
	q->pos[0] = (GPos){0,0,0};
	q->pos[1] = (GPos){4,0,0};

	q = &song->q_settings[Q_EXPONENTIAL];
	strncpy(q->name, "exponential", 63);
	q->size = 5;
	q->pos[0] = (GPos){0,0,0};
	q->pos[1] = (GPos){0,0,AYYI_TICKS_PER_SUBBEAT/4};
	q->pos[2] = (GPos){0,1,0};
	q->pos[3] = (GPos){1,0,0};
	q->pos[4] = (GPos){4,0,0};
	q->pos[5] = (GPos){8,0,0};
}


/*
 *  Return true if the position has changed.
 */
gboolean
q_to_nearest (AMPos* pos, QMap* q_map)
{
	if(!am_pos_is_valid(pos)) return false;

	AMPos orig = *pos;

	// map the position into the quantise map time range:
	// (for simplicity, assume that each q block starts and ends on a beat.)
	AMPos mapped = *pos;
	mapped.beat = pos->beat % q_map->pos[q_map->size].beat;

	AMPos mapping = {pos->beat - mapped.beat, 0, 0};

	// check the mapping
	if(!pos_is_before(&mapped, &q_map->pos[q_map->size])){ char bbst[64]; pos2bbst(&mapped, bbst); perr ("mapping failed. %s", bbst); return FALSE; }

	int i;
	for(i=0;i<q_map->size;i++){
		if(!pos_is_before(&mapped, &q_map->pos[i+1])) continue;

		int distance_to_prev = pos_diff(&mapped, &q_map->pos[i]);
		int distance_to_next = pos_diff(&mapped, &q_map->pos[i+1]);
		int q = distance_to_prev < ABS(distance_to_next) ? i : i + 1;

		char bbst1[64], bbst2[64]; pos2bbst(&mapped, bbst1); pos2bbst(&q_map->pos[i], bbst2);
		dbg(3, "i=%i q=%i  %i--%i %s %s", i, q, distance_to_prev, distance_to_next, bbst1, bbst2);

		*pos = q_map->pos[q];
		pos_add(pos, &mapping); // restore the mapping offset
		break;
	}

	return pos_diff(pos, &orig) ? TRUE : FALSE;
}


/*
 *  This can be used for both snapping and quantization.
 */
void
q_to_prev (GPos* pos, QMap* q_map)
{
	//tested 20080628 - ok for positive pos values.

	if(!q_map) return;

	// create a copy of pos which is in the same range as the q_map.
	GPos mapped = *pos;
	mapped.beat = pos->beat % q_map->pos[q_map->size].beat;

	GPos mapping = {pos->beat - mapped.beat, 0, 0}; //the difference between the mapped and original positions

	// if we are already at the beginning, go to the previous map.
	if(!pos_cmp(&mapped, &q_map->pos[0])){
		if(mapping.beat){
			mapping.beat -= q_map->pos[q_map->size].beat;

			mapped = q_map->pos[q_map->size];
			GPos sub = {0, 0, 1};
			pos_subtract(&mapped, &sub);
			char bbst4[64]; pos2bbst(&mapped, bbst4); dbg(0, "beginning: looping back... mapped_pos=%s", bbst4);
		}else{
			// we are already near the beginning, so cant go to the previous map.
			mapped.beat = 0; mapped.sub = 0; mapped.tick = 0;
		}
	}

	int i;
	for(i=q_map->size-1;i>=0;i--){
		// iterate through the QMap until we get to an entry that is later than our position.
		if(!pos_is_after(&mapped, &q_map->pos[i])) continue;
		*pos = q_map->pos[i];

		pos_add(pos, &mapping); // restore the mapping offset
		break;
	}

	char bbst1[64]; pos2bbst(&q_map->pos[0], bbst1);
	char bbst2[64]; pos2bbst(pos, bbst2);
	dbg(2, "map=%s pos=%s", bbst1, bbst2);
}


/*
 *  Can be used for both snapping and quantization.
 */
void
q_to_next (GPos* pos, QMap* q_map)
{
	if(!q_map) return;
	g_return_if_fail(pos);

	// create a copy of pos which is in the same range as the q_map.
	GPos mapped = *pos;
	// a QMap must start on a beat, so we can ignore subs and ticks
	mapped.beat = pos->beat % q_map->pos[q_map->size].beat;
	GPos mapping = {pos->beat - mapped.beat, 0, 0}; //the difference between the mapped and original positions

#ifdef DEBUG
	if(_debug_ > 1){
		char bbst1[64]; pos2bbst(pos, bbst1);
		char bbst2[64]; pos2bbst(&mapped, bbst2);
		char bbst3[64]; pos2bbst(&mapping, bbst3);
		dbg(0, "pos=%s mapped=%s mapping=%s", bbst1, bbst2, bbst3); 
	}
#endif

	int i; for(i=0;i<q_map->size;i++){
		if(!pos_is_before(&mapped, &q_map->pos[i+1])) continue;
		*pos = q_map->pos[i+1];
		pos_add(pos, &mapping); // restore the mapping offset
		break;
	}
}


/*
 *  Like q_to_prev() except that if the position is already quantised, it wont move.
 */
void
q_to_prev_or_current (GPos* pos, QMap* q_map)
{
	GPos offset = {0, 0, 1};
	pos_add(pos, &offset);
	q_to_prev(pos, q_map);
}


/*
 *  Like q_to_next() except that if the position is already quantised, it wont move.
 */
void
q_to_next_or_current (GPos* pos, QMap* q_map)
{
	GPos offset = {0, 0, 1};
	pos_subtract(pos, &offset);
	q_to_next(pos, q_map);
}


void
am_snap_to_next (GPos* pos)
{
	if(song->snap_mode == SNAP_BAR){ //TODO need complete mapping from snap_mode to q_map
		q_to_next(pos, &song->q_settings[Q_BAR]);
	}
	else pwarn("snapmode not handled");
}


GPos*
am_snap (GPos* pos)
{
	switch(song->snap_mode){
		case SNAP_BAR:
      		q_to_nearest(pos, &song->q_settings[Q_BAR]);
			break;
		case SNAP_BEAT:
      		q_to_nearest(pos, &song->q_settings[Q_BEAT]);
			break;
		case SNAP_16:
      		q_to_nearest(pos, &song->q_settings[Q_16]);
			break;
		default:
			//no snapping. do nothing.
			break;
	}
	return pos;
}


AyyiSongPos*
am_snap_ (AyyiSongPos* _pos)
{
	GPos pos; songpos_ayyi2gui(&pos, _pos);

	switch(song->snap_mode){
		case SNAP_BAR:
      		q_to_nearest(&pos, &song->q_settings[Q_BAR]);
			break;
		case SNAP_BEAT:
      		q_to_nearest(&pos, &song->q_settings[Q_BEAT]);
			break;
		case SNAP_16:
      		q_to_nearest(&pos, &song->q_settings[Q_16]);
			break;
		default:
			// no snapping. do nothing.
			break;
	}

	return songpos_gui2ayyi(_pos, &pos);
}

