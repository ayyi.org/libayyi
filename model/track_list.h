/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
* +----------------------------------------------------------------------+
* | AMTrackList                                                          |
* |                                                                      |
* | AMTrackList is a pointer array backed up by a pool of AMTrack items. |
* | This provides zero-malloc additions and removals and provides        |
* | item pointers that do not change.                                    |
* | In addition it implements the AMCollection iterator and              |
* | FilterIterator.                                                      |
* +----------------------------------------------------------------------+
*/

#ifndef __am_track_list_h__
#define __am_track_list_h__

#include "model/track.h"
#include "model/am_collection.h"

G_BEGIN_DECLS

#define AM_TYPE_TRACK_LIST            (am_track_list_get_type ())
#define AM_TRACK_LIST(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AM_TYPE_TRACK_LIST, AMTrackList))
#define AM_TRACK_LIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AM_TYPE_TRACK_LIST, AMTrackListClass))
#define AM_IS_TRACK_LIST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AM_TYPE_TRACK_LIST))
#define AM_IS_TRACK_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AM_TYPE_TRACK_LIST))
#define AM_TRACK_LIST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AM_TYPE_TRACK_LIST, AMTrackListClass))

typedef struct _AMTrackList        AMTrackList;
typedef struct _AMTrackListClass   AMTrackListClass;
typedef struct _AMTrackListPrivate AMTrackListPrivate;

struct _AMTrackList {
    AMCollection        parent_instance;
    AMTrackListPrivate* priv;
    AMTrack*            track[48];
};

struct _AMTrackListClass {
	AMCollectionClass parent_class;
};

typedef bool (*AMTrackListTrackEach) (AMTrack*, void* user_data);

AMTrackList* am_track_list_new                ();
AMTrack*     am_track_list_at                 (AMTrackList*, gint index);
gint         am_track_list_count              (AMTrackList*);
gint         am_track_list_get_visible_count  (AMTrackList*);
AMTrack*     am_track_list_find_by_shm_idx    (AMTrackList*, AyyiIdx, AMTrackType);
AMTrack*     am_track_list_find_by_ident      (AMTrackList*, AyyiIdent);
AMTrack*     am_track_list_find_audio_by_idx  (AMTrackList*, AyyiIdx);
AMTrack*     am_track_list_find_by_name       (AMTrackList*, gchar* name);
AMTrack*     am_track_list_find_by_playlist   (AyyiPlaylist*, AyyiMediaType);
bool         am_track_list_is_known           (AMTrackList*, AMTrackType, AyyiIdx);
AMTrack*     am_track_list_get_first_visible  (AMTrackList*, AMTrackType);
void         am_track_list_each               (AMTrackList*, AMTrackListTrackEach fn, void*);
int          am_track_list_position           (AMTrackList*, AMTrack*);
AMTrackNum   am_track_list_find_unused        (AMTrackList*);
void         am_track_list_select_first       ();


bool         am_track_list_verify_track       (AMTrackList*, AMTrack*);
bool         am_track_list_verify_tracks      (AMTrackList*);

#define am_tracks                      ((AMCollection*)song->tracks)
#define am_track_iterator_new(F, DATA) filter_iterator_new (0, NULL, NULL, am_tracks, F, DATA)

#ifndef __am_track_private__
void         am_track_list_unallocate         (AMTrackList*, AMTrackNum);
#endif

G_END_DECLS

#endif
