/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2007-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_channel_h__
#define __am_channel_h__

#include "model/am_collection.h"
#include "model/observable.h"

typedef enum {
   AM_CHAN_INPUT = 1,
   AM_CHAN_BUS,
   AM_CHAN_GROUP,  // vca type control strip
   AM_CHAN_MASTER
} ChannelType;

#define MAX_CHS 32
#define CH_IS_MASTER(C) \
	(C && C->type == AM_CHAN_MASTER)
#define ASSERT_CH_NUM(A) \
	if(A < 0 || A > MAX_CHS){ perr("chan num of out range (%i)", A); return NULL; }

#define channel_iter_init(ITER) am_collection_iter_init((AMCollection*)song->channels, ITER)
#define channel_next(ITER)      am_collection_iter_next((AMCollection*)song->channels, ITER)
#define am_channels             ((AMCollection*)song->channels)
#define am_channels__emit(...)  ({ if(song->loaded) g_signal_emit_by_name (song->channels, __VA_ARGS__); })

/*
 *  -channels exist only as members of the GArray song->channels.
 */
struct _Channel {
   AyyiIdent     ident;
   ChannelType   type;
   int           nchans;                 // only stereo or mono supported.

   Observable*   level;
   Observable*   aux_level[AYYI_AUX_PER_CHANNEL];
   Observable*   meter_level;

   gpointer      user_data;
};


AMChannel*  channels__get_master         ();
AMChannel*  channels__get_by_index       (AyyiIdx);

const char* am_channel__get_name         (AMChannel*);
AMTrack*    am_channel__lookup_track     (AMChannel*);
double      am_channel__fader_level      (AMChannel*);
double      am_channel__pan_value        (AMChannel*, bool* has_pan);
bool        am_channel__is_muted         (const AMChannel*);
bool        am_channel__is_solod         (const AMChannel*);
bool        am_channel__is_sdef          (AMChannel*);
bool        am_channel__is_armed         (const AMChannel*);
int         am_channel__get_input_idx    (AMChannel*);
void        am_channel__get_input_string (AMChannel*, char*);
void        am_channel__get_output_string(AMChannel*, char*);
void        am_channel__set_enable_pan   (AMChannel*, gboolean enable, AyyiHandler, gpointer);

#define     am_channel__set_mute(C, VAL) ayyi_channel__set_bool(C->ident.idx, AYYI_MUTE, VAL);
#define     am_channel__set_solo(C, VAL) ayyi_channel__set_bool(C->ident.idx, AYYI_SOLO, VAL);

double      am_channel__get_aux_level    (AMChannel*, int aux_num);
void        am_channel__set_aux_level    (AMChannel*, int aux_num, float, AyyiHandler, gpointer);
void        am_channel__set_aux_output   (const AMChannel*, int aux_num, int connection, AyyiHandler, gpointer);

AyyiConnection*
            am_channel__get_connected_to (AMChannel*);

bool        am_channel__is_type          (AMChannel*, gpointer);
const char* am_channel__print_type       (AMChannel*);


// private

#ifdef __am_song_c__
AMArray*    channels__init               (int n_channels);
void        channels__load               ();
void        channels__unload             ();
void        channels__append_new         (AMTrack*);
void        channels__remove_channel     (AMTrack*);
#endif

#endif
