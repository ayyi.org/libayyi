/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "model/common.h"
#include <math.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_song.h>
#include <model/am_collection.h>
#include <model/channel.h>
#include <model/am_message.h>
#include <model/mixer.h>
#include <model/song.h>


void
am__mixer_sync ()
{
	if(!((AyyiSongService*)ayyi.service)->amixer) return;

	int n_mixer_tracks = ayyi_mixer_container_count_items(&((AyyiSongService*)ayyi.service)->amixer->tracks);
	int n_song_tracks = ayyi_song_container_count_items(&((AyyiSongService*)ayyi.service)->song->audio_tracks);
	if(n_mixer_tracks != n_song_tracks){ perr("shm error. song and mixer out of sync. song=%i mixer=%i", n_song_tracks, n_mixer_tracks); }

	int i = 0;
	AyyiChannel* track = NULL;
	while((track = ayyi_mixer__channel_next(track))){
		dbg (2, "%i: track=%p", i, track->name);

		int n_aux = 0;
		int a; for(a=0;a<AYYI_AUX_PER_CHANNEL;a++){
			AyyiAux* aux = ayyi_mixer__aux_get(track, a);
			if(aux) n_aux++;
		}
		dbg(3, "%i: n_aux=%i", i, n_aux);

		i++;
	}
	AYYI_DEBUG_2 am__mixer_print();
}


void
am__mixer_periodic_update (GObject* _song, gpointer user_data)
{
	AMIter iter;
	channel_iter_init(&iter);
	AMChannel* channel;
	while((channel = channel_next(&iter))){
		int aux_num; for(aux_num=0;aux_num<AYYI_AUX_PER_CHANNEL;aux_num++){
			AyyiAux* aux = ayyi_mixer__aux_get_(channel->ident.idx, aux_num);
			if(aux){
				double aux_level_db = am_channel__get_aux_level(channel, aux_num);
				Observable* o = channel->aux_level[aux_num];
				if(aux_level_db != o->value.f){
					if(aux_level_db > o->max.f || isnan(aux_level_db)){
						pwarn("out of range: %.2fdB", aux_level_db);
					}else{
						observable_set(o, (AMVal){.f = aux_level_db});
					}
				}
			}
		}
	}
}


/*
 *  Return the index for the channel following the one given.
 */
AyyiIdx
am__mixer_channel_next (const AMChannel* channel)
{
	AyyiChannel* prev = channel ? ayyi_mixer__channel_at_quiet(channel->ident.idx) : NULL;
	AyyiChannel* next = ayyi_mixer__channel_next(prev);
	return (next ? next->shm_idx : 0);
}


void
am__mixer_show_routing ()
{
}


void
am_aux__add (const AMChannel* ch, int aux_num, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(ch);

	am_channel__set_aux_output(ch, aux_num, -1, callback, user_data);
}


/**
 *  am_aux__remove:
 *  @arg2: (scope async)
 *
 *  returns an ident of {AYYI_OBJECT_AUX, aux_num} - this is not a useful ident so is subject to review
 */
void
am_aux__remove (const AMChannel* ch, int aux_num, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(ch);

	AyyiChannel* ac = ayyi_mixer__channel_at_quiet(ch->ident.idx);
	if (!(ac && ac->aux[aux_num])) {
		// TODO set error
		call(callback, (AyyiIdent){AYYI_OBJECT_AUX, (ch->ident.idx << 4) | aux_num}, NULL, user_data);
		return;
	}

	void am_aux__remove_done (AyyiAction* action)
	{
		g_return_if_fail(action);

		HandlerData* d = action->app_data;
		if(d){
			call(d->callback, (AyyiIdent){action->obj.type, action->obj_idx.idx1}, &action->ret.error, d->user_data);
		}
	}

	AyyiAction* a   = ayyi_action_new("delete aux (track %i, aux %i)", ch->ident.idx, aux_num);
	a->op           = AYYI_DEL;
	a->obj.type     = AYYI_OBJECT_AUX;
	a->obj_idx.idx1 = (ch->ident.idx << 4) | aux_num; // warning! index is combination of auxnum and channel
	a->callback     = am_aux__remove_done;
	a->app_data     = ayyi_handler_data_new(callback, user_data);
	am_action_execute(a);
}


void
am__mixer_print ()
{
	if (!((AyyiSongService*)ayyi.service)->amixer) return;
	if (strcmp(((AyyiSongService*)ayyi.service)->amixer->service_type, "Ayyi mixer")){ pwarn("bad shm signature! %s\n", ((AyyiSongService*)ayyi.service)->amixer->service_type); return; }

	int n_tracks = ayyi_mixer__count_channels();
	printf("------------------------------------------------\n");
	dbg(0, "");
	if (n_tracks > 0) printf("                name  lvl\n");
	int count = 0;
	AyyiChannel* trk = NULL;
	while ((trk = ayyi_mixer__channel_next(trk))) {
		printf("%02i %02i %14s %.2f %p\n", count, trk->shm_idx, trk->name, trk->level, trk);
		count++;
	}
	printf("total=%i\n", count);
	printf("------------------------------------------------\n");
}


