/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "model/common.h"
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_dbus_proxy.h>
#include "model/time.h"
#include "model/am_message.h"
#include "model/song.h"
#include "model/transport.h"

static void am_transport_sync_locators ();
static void am_transport_set_locator   (int loc_num);


void
am_transport_sync ()
{
	am_transport_sync_locators();
	am_song__emit("transport-sync");

	if(ayyi_transport_is_stopped())      am_song__emit("transport-stop");
	else if(ayyi_transport_is_playing()) am_song__emit("transport-play");
	else if(ayyi_transport_is_ff())      am_song__emit("transport-ff");
}


void
am_transport_set_rec (bool enable, AyyiHandler2 callback, gpointer user_data)
{
	void am_transport_set_rec_done(AyyiAction* action)
	{
		PF;
		HandlerData* d = action->app_data;
		if(d){
			call((AyyiHandler2)d->callback, NULL, d->user_data);
		}
	}

	AyyiAction* a = ayyi_action_new("rec enable toggle");
	a->obj.type   = AYYI_OBJECT_TRANSPORT;
	a->prop       = AYYI_TRANSPORT_REC;
	a->i_val      = enable;
	a->callback   = am_transport_set_rec_done;
	a->app_data   = ayyi_handler_data_new((AyyiHandler)callback, user_data);
	am_action_execute(a);
}


void
ayyi_transport_rew ()
{
	#if USE_DBUS
	dbus_set_prop_bool(NULL, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_REW, 0, TRUE);
	#endif

	//cant get Ardour to notify us when in FF
	am_song__emit("transport-rew");
}


void
am_transport_jump (AyyiSongPos* pos)
{
	PF;
	#if USE_DBUS
	dbus_set_prop_int(NULL, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_LOCATE, 0, ayyi_pos2samples(pos));
	#endif
}


void
am_transport_get_pos (AMPos* pos)
{
	if(!((AyyiSongService*)ayyi.service)->song) return;
	g_return_if_fail(pos);

	if(((AyyiSongService*)ayyi.service)->song->transport_frame > 44100 * 60 * 60 * 4) pwarn("bad transport_frame");
	uint64_t samples = ((AyyiSongService*)ayyi.service)->song->transport_frame;
	samples2pos(samples, pos);
}


AyyiSongPos*
am_transport_get_pos_ (AyyiSongPos* pos)
{
	if(!((AyyiSongService*)ayyi.service)->song) return pos;
	g_return_val_if_fail(pos, pos);

	if(((AyyiSongService*)ayyi.service)->song->transport_frame > 44100 * 60 * 60 * 4) pwarn("bad transport_frame");
	uint64_t samples = ((AyyiSongService*)ayyi.service)->song->transport_frame;
	ayyi_samples2pos(samples, pos);

	return pos;
}


static void
am_transport_sync_locators ()
{
	for (int i=0;i<AM_LOC_END;i++) {
		song->loc[i].vals[0].val.sp = ((AyyiSongService*)ayyi.service)->song->locators[i];
	}

	AYYI_DEBUG_2 {
		char bbst[32]; ayyi_pos2bbst(&song->loc[AM_LOC_LEFT ].vals[0].val.sp, bbst); dbg (0, "1=%s", bbst);
		char bbs2[32]; ayyi_pos2bbst(&song->loc[AM_LOC_RIGHT].vals[0].val.sp, bbs2); dbg (0, "2=%s", bbs2);
	}

	am_object_val(&song->loc[AM_LOC_START]).sp = (AyyiSongPos){0,};
	am_object_val(&song->loc[AM_LOC_END]).sp = ((AyyiSongService*)ayyi.service)->song->end;
}


static void
am_transport_set_locator (int loc_num)
{
	g_return_if_fail(loc_num > 0 || loc_num < AYYI_MAX_LOC);
	dbg (1, "loc_num=%i", loc_num);

	guint64 time64;
	memcpy (&time64, &song->loc[loc_num].vals[0].val.sp, 8);

	dbus_set_prop_int64 (NULL, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_LOCATOR, loc_num, time64);
}


void
am_transport_set_locator_pos (int loc_num, AyyiSongPos* pos)
{
	g_return_if_fail(!(loc_num < 0 || loc_num >= AYYI_MAX_LOC));

	song->loc[loc_num].vals[0].val.sp = *pos;
	am_transport_set_locator(loc_num);
}


void
am_transport_cycle_toggle ()
{
	void transport_cycle_toggle_cb(AyyiAction* action)
	{
		PF;
		am_song__emit("transport-cycle");
	}

	gboolean mode = !ayyi_transport_is_cycling();

	AyyiAction* action = ayyi_action_new("cycle toggle (%i)", mode);
	action->callback = transport_cycle_toggle_cb;

#if USE_DBUS
	dbus_set_prop_bool(action, AYYI_OBJECT_TRANSPORT, AYYI_TRANSPORT_CYCLE, 0, mode);
#endif
}


const char*
am_locator_name (int i)
{
	static char* labels[] = {"Start", "Left Locator", "Right Locator", "", "", "", "", "", "", "End"};
	return (i < G_N_ELEMENTS(labels)) ? labels[i] : "Locator";
}

