/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __am_mixer_h__
#define __am_mixer_h__

AyyiIdx    am__mixer_channel_next    (const AMChannel*);
void       am__mixer_show_routing    ();

void       am_aux__add               (const AMChannel*, int aux_num, AyyiHandler, gpointer);
void       am_aux__remove            (const AMChannel*, int aux_num, AyyiHandler, gpointer);

gboolean   am__mixer_verify          ();
void       am__mixer_print           ();

#ifdef __am_private__
void       am__mixer_sync            ();
#endif
#ifdef __am_song_c__
void       am__mixer_periodic_update (GObject* _song, gpointer user_data);
#endif

#endif
