/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include "math.h"
#include <glib.h>
#include "ayyi/ayyi_utils.h"
#include "observable.h"

typedef struct {
   ObservableFn fn;
   gpointer     user;
} Subscription;


Observable*
observable_new ()
{
	return g_new0(Observable, 1);
}


void
observable_free (Observable* observable)
{
	g_list_free_full(g_steal_pointer(&observable->subscriptions), g_free);
	g_free(observable);
}


void
observable_set (Observable* observable, AMVal value)
{
	// Because of the possibility of uninitialized padding
	// there is no way to check equality of 2 unions so
	// it is not possible to check here if the value has changed

	observable->value = value;

	GList* l = observable->subscriptions;
	for(;l;l=l->next){
		Subscription* subscription = l->data;
		subscription->fn(observable, value, subscription->user);
	}
}


void
observable_subscribe (Observable* observable, ObservableFn fn, gpointer user)
{
	observable->subscriptions = g_list_append(observable->subscriptions, AYYI_NEW(Subscription,
		.fn = fn,
		.user = user
	));
}


/*
 *  Calls back imediately with the current value
 */
void
observable_subscribe_with_state (Observable* observable, ObservableFn fn, gpointer user)
{
	observable_subscribe(observable, fn, user);
	fn(observable, observable->value, user);
}


/*
 *  Disconnect by either fn or user_data if they are set.
 *  If both are set, both must match
 */
void
observable_unsubscribe (Observable* observable, ObservableFn fn, gpointer user)
{
	for(GList* l=observable->subscriptions;l;){
		Subscription* subscription = l->data;
		GList* link = l;
		l = l->next;
		if((!fn || fn == (subscription->fn)) && (!user || (user == subscription->user))){
			g_free(subscription);
			observable->subscriptions = g_list_delete_link(observable->subscriptions, link);
		}
	}
}


Observable*
observable_float_new (float val, float min, float max)
{
	Observable* observable = observable_new();

	observable->value = (AMVal){.f = val};
	observable->min = (AMVal){.f = min};
	observable->max = (AMVal){.f = max};

	return observable;
}


void
observable_float_set (Observable* observable, float value)
{
	if(isnan(value)) return;

	value = CLAMP(value, observable->min.f, observable->max.f);

	if(ABS(observable->value.f - value) > 0.0001){
		observable_set(observable, (AMVal){.f=value});
	}
}


Observable*
observable_new_point (float x, float y)
{
	Observable* pt = (Observable*)g_new0(PtObservable, 1);
	pt->value.pt = (Ptf){x, y};
	return pt;
}


void
observable_point_set (Observable* observable, float* x, float* y)
{
	PtObservable* pt = (PtObservable*)observable;

	pt->change = 0;

	if(x && *x != observable->value.pt.x){
		observable->value.pt.x = CLAMP(*x, observable->min.pt.x, observable->max.pt.x);
		pt->change |= CHANGE_X;
	}

	if(y && *y != observable->value.pt.y){
		observable->value.pt.y = CLAMP(*y, observable->min.pt.y, observable->max.pt.y);
		pt->change |= CHANGE_Y;
	}

	if(pt->change){
		observable_set(observable, observable->value);
	}
}


Observable*
array_observable_new ()
{
	ArrayObservable* observable = g_new0(ArrayObservable, 1);
	observable->array = g_ptr_array_new();

	return (Observable*)observable;
}


void
array_observable_add (Observable* observable, gpointer item)
{
	ArrayObservable* array = (ArrayObservable*)observable;

	observable->value.p = item;
	array->change = OBSERVABLE_ADD;
	g_ptr_array_add(array->array, item);

	observable_set(observable, observable->value);
}


void
array_observable_remove (Observable* observable, gpointer item)
{
	ArrayObservable* array = (ArrayObservable*)observable;

	observable->value.p = item;
	array->change = OBSERVABLE_REMOVE;
	g_ptr_array_remove(array->array, item);

	observable_set(observable, observable->value);
}

