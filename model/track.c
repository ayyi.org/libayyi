/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/**
 *  AMTrack
 *
 *  "Tracks" are compositional objects containing playlists, ie they are
 *  used to group together elements of the composition.
 *  They are associated with but distinct from Channels which represent a signal path.
 *  Tracks have a channel assigned to them. Depending on the model implemented,
 *  sometimes there is a 1:1 relationship.
 *
 *  All track indexes are intended to be internal only.
 *  Track numbers displayed in the GUI should be implemented using the Name property.
 */

#define __am_track_c__
#include <model/common.h>
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_action.h>
#include <ayyi/ayyi_log.h>
#include <ayyi/ayyi_dbus_proxy.h>

#include <model/am_message.h>
#include <model/song.h>
#include <model/curve.h>
#include <model/channel.h>
#include <model/connections.h>
#include <model/automation.h>
#include <model/part_manager.h>
#include <model/track_list.h> //yes, the track knows about its container

#define emit_track_change(A, B, C) g_signal_emit_by_name (song, "tracks-change", A, A, C);

extern bool is_song_shm_local       (void*);
extern bool is_song_shm_local_quiet (void*);

static bool _am_track__init         (AMTrack*, AyyiIdx);


typedef struct
{
	AyyiHandler callback;
	gpointer    user_data;
	AMTrack*    track;

} CallbackData;

#define callback_track ((CallbackData*)action->app_data)->track


static gpointer
am_track_copy (gpointer boxed)
{
	return boxed;
}


GType
am_track_get_type ()
{
	static GType type = 0;

	if (G_UNLIKELY (!type))
		type = g_boxed_type_register_static ("AMTrack", am_track_copy, g_free);

	return type;
}


/**
 * am_track__new
 *
 * Note that because tracks cannot be separated from the track list, this fn actually
 * adds a new track to the list (or to be more precise, allocates it).
 *
 * R eturns: (transfer full)
 * Return value: (transfer full)
 */
AMTrack*
am_track__new (AMTrackType type, AyyiIdx core_index)
{
	PF;

	AMTrack* tr = am_collection_append(am_tracks, &(AMTrack){
		.type        = type,
		.ident.type  = (type == TRK_TYPE_AUDIO) ? AYYI_OBJECT_AUDIO_TRACK : AYYI_OBJECT_MIDI_TRACK,
	});

	tr->bezier.vol = curve_new(tr, VOL, NULL);
	tr->bezier.pan = curve_new(tr, PAN, NULL);
	tr->bezier.vol->name = g_strdup("vol");
	tr->bezier.pan->name = g_strdup("pan");

	_am_track__init(tr, core_index);

#ifdef DEBUG
	am_track_list_verify_tracks(song->tracks);
#endif

#if 0
	if(type == TRK_TYPE_AUDIO) ayyi_song__print_automation_list(track_pod(core_index));
#endif

	return tr;
}


static AyyiAction*
track_action_new (AMTrack* track, AyyiActionCallback callback, char* format, ...)
{
	char name[64];
	va_list argp;
	va_start(argp, format);
	vsnprintf(name, 63, format, argp);
	va_end(argp);

	AyyiAction* a = ayyi_action_new(name);
	a->obj.type = AYYI_OBJECT_TRACK;
	a->obj_idx.idx1 = track->ident.idx;
	a->callback = callback;
	a->app_data = AYYI_NEW(CallbackData,
		.track = track
	);

	return a;
}


char*
am_track__get_channel_label (const AMTrack* tr, char* label)
{
#ifdef ENGINE_HAS_CHANNELS
	engine_get_channel_label(tr, label);
#else
	am_track__get_connections_string(tr, label);
#endif
	return label;
}


/**
 *  am_track__arm:
 *  @arg2: (scope async)
 */
void
am_track__arm (AMTrack* tr, bool arm_state, AyyiHandler callback, gpointer user_data)
{
	AyyiAction* a = track_action_new(tr, ayyi_handler_simple, "Track Arm (%i) trk=%u", arm_state, tr->ident.idx);

	a->obj.type   = AYYI_OBJECT_CHAN;
	a->prop       = AYYI_ARM;
	a->i_val      = arm_state;

	((CallbackData*)a->app_data)->callback = callback;
	((CallbackData*)a->app_data)->user_data = user_data;

	am_action_execute(a);
}


/**
 *  am_track__mute:
 *  @arg2: (scope async)
 */
void
am_track__mute (AMTrack* tr, bool mute_status, AyyiHandler callback, gpointer user_data)
{
	dbg(1, "obj_id=%u", tr->ident.idx);

	AyyiAction* a = track_action_new(tr, ayyi_handler_simple, "track_mute (%i) trk=%u", mute_status, tr->ident.idx);
	((CallbackData*)a->app_data)->callback = callback;
	((CallbackData*)a->app_data)->user_data = user_data;

	ayyi_set_obj(a, AYYI_OBJECT_TRACK, tr->ident.idx, AYYI_MUTE, mute_status);
}


/**
 *  am_track__solo:
 *  @arg2: (scope async)
 */
void
am_track__solo (AMTrack* trk, bool solo_state, AyyiHandler callback, gpointer user_data)
{
	AyyiAction* a   = track_action_new(trk, ayyi_handler_simple, "Track Solo (%i) trk=%u", !solo_state, trk->ident.idx);

	a->obj.type     = AYYI_OBJECT_CHAN;
	a->prop         = AYYI_SOLO;
	a->i_val        = solo_state;
	((CallbackData*)a->app_data)->callback = callback;
	((CallbackData*)a->app_data)->user_data = user_data;

	am_action_execute(a);
}


/**
 *  am_track__lookup_channel:
 *
 *  Returns: (transfer none)
 */
const AMChannel*
am_track__lookup_channel (AMTrack* tr)
{
	for(int i=0;i<song->channels->array->len;i++){
		AMChannel* si = g_ptr_array_index(song->channels->array, i);
		if(si->ident.idx == tr->ident.idx) return si;
	}
	pwarn ("channel not found! track=%i", tr->ident.idx);
	return NULL;
}


int
am_track__lookup_channel_num (AMTrack* tr)
{
	int i;
	for(i=0;i<song->channels->array->len;i++){
		AMChannel* si = g_ptr_array_index(song->channels->array, i);
		if(si->ident.idx == tr->ident.idx) return i;
	}
	pwarn ("channel not found! track=%i", tr->ident.idx);
	return -1;
}


void
_am_track__sync_name (AMTrack* track)
{
	AyyiItem* tr = am_track__get_shared(track);
	if(tr){
		if(track->name) g_free(track->name);
		track->name = g_strdup(tr->name);
	}
}


static bool
_am_track__init (AMTrack* trk, AyyiIdx shm_num)
{
	trk->ident.idx = shm_num;

	_am_track__sync_name(trk);

	if(AM_TRK_IS_AUDIO(trk)){
		AyyiTrack* r = ayyi_song__audio_track_at(trk->ident.idx);
		if(r){
			if(AYYI_TRACK_IS_MASTER(r)){
				trk->visible = FALSE;
				trk->type = TRK_TYPE_BUS;
			}
			dbg(2, "shm_num=%i type=%i flags=%i %s", shm_num, trk->type, r->flags, trk->name);
		}
	}

	return true;
}


/*
 * Check if the track exists in the engine
 */
bool
am_track__exists (AMTrack* tr)
{
	g_return_val_if_fail(tr, true);

	int shm_idx = tr->ident.idx;
	if(tr->type == TRK_TYPE_MIDI) return (ayyi_song__midi_track_at(shm_idx) != NULL);
	else return (ayyi_song__audio_track_at(shm_idx) != NULL);
}


bool
am_track__is_empty (AMTrack* track, gpointer user_data)
{
	FilterIterator* i = am_part_iterator_new ((Filter)am_part__has_track, track);
	AMPart* part = (AMPart*)filter_iterator_next(i);
	filter_iterator_unref(i);
	return !part;
}


bool
am_track__is_mono (AMTrack* track, gpointer user_data)
{
	const AMChannel* ch = am_track__lookup_channel(track);
	if(ch){
		return ch->nchans == 1;
	}
	return false;
}


bool
am_track__is_midi (AMTrack* track, gpointer user_data)
{
	return AM_TRK_IS_MIDI(track);
}


bool
am_track__is_valid (const AMTrack* track)
{
	if (!AM_TRACK_TYPE_IS_VALID(track->type)) { perr("bad track type: %i", track->type); return FALSE; }

	AyyiContainer* container;
	switch(track->type){
		case TRK_TYPE_AUDIO:
			container = &((AyyiSongService*)ayyi.service)->song->audio_tracks;
			break;
		case TRK_TYPE_MIDI:
			container = &((AyyiSongService*)ayyi.service)->song->midi_tracks;
			break;
		default:
			return FALSE;
	}
	return ayyi_container_index_is_used(container, track->ident.idx);
}


unsigned char
am_track__is_armed (AMTrack* tr)
{
	if (!AM_TRK_IS_AUDIO(tr)) return 0;

	AyyiTrack* shared = ayyi_song__audio_track_at(tr->ident.idx);
	if (!is_song_shm_local_quiet(shared)) {
		if (_debug_) perr ("failed to get shared track struct. %u", tr->ident.idx);
		return false;
	}
	dbg (2, "armed=%i", shared->flags & armed);

	return shared->flags & armed;
}


bool
am_track__is_solod (AMTrack* tr)
{
	if(!AM_TRK_IS_AUDIO(tr)) return 0;

	AyyiTrack* route = ayyi_song__audio_track_at(tr->ident.idx);
	g_return_val_if_fail(route, 0);
	return route->flags & solod;
}


bool
am_track__is_muted (const AMTrack* tr)
{
	g_return_val_if_fail(tr, false);
	if(!AM_TRK_IS_AUDIO(tr)) return false;

	AyyiTrack* route = ayyi_song__audio_track_at(tr->ident.idx);
	g_return_val_if_fail(route, false);
	return route->flags & muted;
}


bool
am_track__is_master (AMTrack* tr)
{
	g_return_val_if_fail(tr, false);

	if(AM_TRK_IS_MIDI(tr)) return false;
	AyyiTrack* route = ayyi_song__audio_track_at(tr->ident.idx);
	return route && AYYI_TRACK_IS_MASTER(route);
}


bool
am_track__is_not_master (AMTrack* tr)
{
	return !am_track__is_master(tr);
}


/*
 *  Note: connection is a property of the track: io/outputs
 *
 *  @label should be allocated with size 64 chars
 *
 *  It is normal that the function does not succeed, eg if the track is deleted.
 */
bool
am_track__get_connections_string (const AMTrack* tr, char* label)
{
	g_return_val_if_fail(label, false);
	if(!AM_TRK_IS_AUDIO(tr)){ label[0] = '\0'; return false; }

	AyyiIdx t = tr->ident.idx;

	AyyiTrack* shared = ayyi_song__audio_track_at(t);
	if (!shared) {
		dbg(2, "no shared track found: %u", t);
		return false;
	}

	dbg (2, "track=%s output=%s", shared->name, label);

	AyyiConnection* c = am_track__get_output((AMTrack*)tr);
	if(!c) c = ayyi_song__connection_at(0);
	dbg(2, "connection index=%i %s", c->shm_idx, c->name);

	char short_name[64];
	g_strlcpy(short_name, c->name, 64);
	char* end = strstr(short_name, "{");
	if(!end) end = &short_name[63];
	*end = '\0';

	replace2(short_name, "Master Out", "mixbus");

	strcpy(label, short_name);

	return true;
}


bool
am_track__get_name (const AMTrack* trk, char* name)
{
	ASSERT_SONG_SHM_RET_FALSE;

	if(!AM_TRK_IS_AUDIO(trk)) dbg(2, "shm_num=%i", trk->ident.idx);
	AyyiTrack* shared = (AyyiTrack*)am_track__get_shared(trk);
	if(!is_song_shm_local(shared)){ perr ("failed to get ayyi track. %u", trk->ident.idx); return false; }
	dbg (2, "core_index=%i name=%s", trk->ident.idx, shared->name);
	g_strlcpy(name, shared->name, AYYI_NAME_MAX);

	return true;
}


/**
 *  am_track__set_name:
 *  @arg2: (scope async)
 */
void
am_track__set_name (const AMTrack* track, const char* name, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(name);
	PF;

	// track names should not contain a dot
	replace2((char*)name, ".", "_");

#if 0
	void am_track__set_name_done(AyyiAction* a)
	{
		DO_RESPONSE(AYYI_OBJECT_TRACK);
	}
#endif

	AyyiAction* action = ayyi_action_new("track rename");
	//action->callback = am_track__set_name_done;
	action->obj.type = AYYI_OBJECT_TRACK;
	action->obj_idx.idx1 = track->ident.idx;
	action->callback = ayyi_handler_simple;
	action->app_data = ayyi_handler_data_new(callback, user_data);
 
	dbus_set_prop_string (action, AYYI_OBJECT_TRACK, AYYI_NAME, track->ident.idx, name);
}


int
am_track__get_colour (AMTrack* track)
{
	g_return_val_if_fail(track, -1);

	AyyiContainer* container = AM_TRK_IS_AUDIO(track) ? &((AyyiSongService*)ayyi.service)->song->audio_tracks : &((AyyiSongService*)ayyi.service)->song->midi_tracks;
	AyyiTrack* item = (AyyiTrack*)ayyi_song_container_get_item(container, track->ident.idx);
	return item ? item->colour : -1;
}


/**
 *  am_track__set_colour:
 *  @arg2: (scope async)
 */
void
am_track__set_colour (const AMTrack* trk, int colour, AyyiHandler h, gpointer user_data)
{
	//if the the song server does not support persistent storage of colour, this needs to be called every time the client starts up.

	void am_track__set_colour_done(AyyiAction* action)
	{
		PF;
		AMTrackNum t = am_track_list_position (song->tracks, callback_track);
		g_signal_emit_by_name (song, "tracks-change", t, t, AM_CHANGE_COLOUR);
	}

	dbus_set_prop_int (track_action_new((AMTrack*)trk, am_track__set_colour_done, "track set colour %i", colour), AYYI_OBJECT_TRACK, AYYI_COLOUR, trk->ident.idx, colour);
}


void
am_track__set_input (AMTrack* tr, AyyiConnection* connection)
{
	PF;

	void am_track__set_input_done (AyyiAction* action)
	{
		PF;
		ayyi_log_print(LOG_OK, "input connection changed.");
	}

	AyyiAction* a    = ayyi_action_new("set input (track %i)", tr->ident.idx);
	a->op            = AYYI_SET;
	a->obj.type      = AYYI_OBJECT_TRACK;
	a->prop          = AYYI_INPUT;
	a->obj_idx.idx1  = tr->ident.idx;
	a->i_val         = connection->shm_idx;
	a->callback      = am_track__set_input_done;

	am_action_execute(a);
}


/**
 *  am_track__set_output
 *  @arg2: (scope async)
 */
void
am_track__set_output (AMTrack* tr, AyyiConnection* connection, AyyiHandler callback, gpointer user_data)
{
	// route the track to the given output connection.
	// -if connection is NULL, the track output will be OFF ?
	// return: the id returned is currently that of the AMTrack, not the AyyiConnection.

	g_return_if_fail(tr);

	void
	am_track__set_output_done (AyyiAction* action)
	{
		PF;

		HandlerData* d = action->app_data;
		if(d) call(d->callback, (AyyiIdent){action->obj.type, action->obj_idx.idx1}, &action->ret.error, d->user_data);

		if(!action->ret.error){
			ayyi_log_print(LOG_OK, "output connection changed.");
		}
	}

	AyyiAction* a    = ayyi_action_new("set output (track %i)", tr->ident.idx);
	a->obj.type      = AYYI_OBJECT_TRACK;
	a->prop          = AYYI_OUTPUT;
	a->obj_idx.idx1  = tr->ident.idx;
	a->i_val         = connection->shm_idx;
	a->callback      = am_track__set_output_done;
	a->app_data      = ayyi_handler_data_new(callback, user_data);

	am_action_execute(a);
}


int
am_track__get_input_num (AMTrack* tr)
{
	AyyiTrack* shared = ayyi_song__audio_track_at(tr->ident.idx);
	if(!is_song_shm_local(shared)){ perr ("failed to get shared track struct. %u", tr->ident.idx); return FALSE; }
	dbg (2, "input_idx=%i name=%s", shared->input_idx, ""/*shared->output_name*/);

	return shared->input_idx;
}


/**
 *  am_track__get_output_num: (skip)
 *
 *  This fn is limited by the fact that it can only return 1 output.
 */
int
am_track__get_output_num (AMTrack* tr)
{
	AyyiTrack* ayyi_trk = ayyi_song__audio_track_at(tr->ident.idx);
	if(!ayyi_trk){ perr ("failed to get shared track struct. %u", tr->ident.idx); return false; }

	AyyiList* routing = ayyi_list__first(ayyi_trk->output_routing);
	if(routing){
#ifdef DEBUG
		AyyiConnection* connection = ayyi_song__connection_at(routing->id);
		//FIXME 2 pointer below should be same.
		dbg(1, "  connection_id=%i '%s' %p %p %p", routing->id, connection ? connection->name : "!!", connection, ayyi_song__translate_address(routing->data), routing->data);
#endif
		return routing->id;
	}

	dbg(0, "no routing.");
	return 0;
}


/**
 *  am_track__get_output: (skip)
 *
 *  This fn is limited by the fact that it can only return 1 output.
 */
AyyiConnection*
am_track__get_output (AMTrack* tr)
{
	AyyiTrack* ayyi_trk = ayyi_song__audio_track_at(tr->ident.idx);
	if(!ayyi_trk){ perr ("failed to get shared track struct. %u", tr->ident.idx); return false; }

	AyyiList* routing = ayyi_list__first(ayyi_trk->output_routing);
	if(routing){
		dbg (2, "output_idx=%i name=%s", routing->id, ""/*ayyi_trk->output_name*/);
		AyyiConnection* o = ayyi_song__connection_at(routing->id);
		return o;
	}
	// track is not connected. Return the null output.
	return ayyi_song__connection_at(0);
}


const char*
am_track__get_input_name (AMTrack* tr)
{
	g_return_val_if_fail(tr, NULL);

	AyyiTrack* ayyi_trk = ayyi_song__audio_track_at(tr->ident.idx);
	if(!ayyi_trk){ perr ("failed to get ayyi track. %u\n", tr->ident.idx); return NULL; }
	dbg (2, "name=%s", ayyi_trk->input_name);

	return ayyi_trk->input_name;
}


/*
 *  Tracks can have multiple outputs. This returns the first.
 */
const char*
am_track__get_output_name (AMTrack* tr)
{
	AyyiTrack* ayyi_trk = ayyi_song__audio_track_at(tr->ident.idx);
	if (!ayyi_trk) { perr ("failed to get shared track struct: %u", tr->ident.idx); return false; }

	return ayyi_track__get_output_name(ayyi_trk);
}


void
am_track__get_start_end (AMTrack* tr, AyyiSongPos* _start, AyyiSongPos* _end)
{
	if ((tr->cache.start.beat == 1 << 24) && (tr->cache.end.beat == 0)) {

		FilterIterator* i = am_part_iterator_new ((Filter)am_part__has_track, tr);
		AMPart* part;
		while ((part = (AMPart*)filter_iterator_next(i))) {
			AyyiSongPos s = part->start;
			AyyiSongPos e; am_part__end_pos(part, &e);

			if (s.beat < tr->cache.start.beat) tr->cache.start.beat = s.beat;
			if (e.beat + 1 > tr->cache.end.beat) tr->cache.end.beat = e.beat + 1;
		}
		filter_iterator_unref(i);
	}
	if (_start) *_start = tr->cache.start;
	if (_end) *_end = tr->cache.end;
}


/*
 *  Return value in dB
 *
 *  Ayyi midi tracks do not currently have the level property
 */
float
am_track__get_meterlevel (AMTrack* track)
{
	AyyiTrack* route;

	return track && AM_TRK_IS_AUDIO(track) && ((route = (AyyiTrack*)am_track__get_shared_(track)))
		? route->visible_peak_power[0]
		: -100.0;
}


void
am_track__get_meterlevel_master (float level[])
{
	AyyiChannel* channel = &((AyyiSongService*)ayyi.service)->amixer->master;

	for(int n=0;n<2;n++) level[n] = channel->visible_peak_power[n];
}


/**
 *  am_track__lookup_curve: (skip)
 */
Curve*
am_track__lookup_curve (AMTrack* track, int idx)
{
	g_return_val_if_fail(track, NULL);

	GList* l = track->controls;
	for(;l;l=l->next){
		Curve* test = l->data;
		if(test->auto_type == idx) return test;
	}
	pwarn("curve not found. idx=%i", idx);
	return NULL;
}


/**
 *  am_track__clear_automation
 *  @arg2: (scope async)
 */
void
am_track__clear_automation (AMTrack* track, Curve* curve, AyyiHandler callback, gpointer user_data)
{
	g_return_if_fail(track);
	g_return_if_fail(curve);
	PF;

	typedef struct
	{
		Curve*      curve;
		gpointer    user_data;
		AyyiHandler callback;
		int         n_outstanding;
		GError*     error;
	} C;

	C* c = AYYI_NEW(C,
		.curve = curve,
		.callback = callback,
		.user_data = user_data,
	);

	g_return_if_fail(c->curve->track == track);

	void am_track_clear_automation_done (AyyiIdent id, GError** error, gpointer user_data)
	{
		PF;
		C* c = user_data;
		if(error && *error) c->error = g_error_copy(*error);
		int len = curve_get_length(c->curve);
		dbg(0, "curve=%p len=%i", c->curve, len);
		if(--c->n_outstanding < 1){
			if(len != 0) pwarn("TODO len!=0 make new gerror");
			dbg(0, "callback=%p", c->callback);
			call(c->callback, id, c->error ? &c->error : error, c->user_data);
			g_free(c);
		}
	}

	int len = curve_get_length(c->curve);
	dbg(1, "curve=%p len=%i", c->curve, len);
	if(!len){ dbg(0, "curve already empty"); am_track_clear_automation_done(AYYI_NULL_IDENT, NULL, c); }

	int i; for(i=len-1;i>=0;i--){
		dbg(1, "i=%i", i);
		am_automation_point_remove(c->curve, i, am_track_clear_automation_done, c);
		c->n_outstanding++;
	}
}


/*
 *  Metering is not enabled by default.
 *  Clients must explicity enable it for each track as well
 *  as calling am_song__enable_meters.
 */
void
am_track__enable_metering (AMTrack* track)
{
	g_return_if_fail(track);

	if(!track->meterval) track->meterval = observable_float_new (0., -90., 12.);
}


#ifdef DEBUG
const char*
am_track__print_type (AMTrack* trk)
{
	return NULL;
}
#endif

