/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __transition_observable_h__
#define __transition_observable_h__

#include "transition/transition.h"
#include "model/observable.h"

typedef struct
{
   Observable   observable;
   Observable*  source;
   WfAnimatable animatable;
   WfAnimation* transition;
   AMVal        value;
}
TransitionObservable;

TransitionObservable* transition_observable      (Observable*);
void                  transition_observable_free (Observable*);

#endif
