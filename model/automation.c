/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <math.h>
#include "common.h"
#include <ayyi/ayyi_list.h>
#include <ayyi/ayyi_shm.h>
#include <ayyi/ayyi_client.h>
#include <ayyi/ayyi_song.h>
#include <ayyi/ayyi_mixer.h>
#include <ayyi/ayyi_action.h>
#include "ayyi/ayyi_dbus_proxy.h"

#include "model/time.h"
#include "model/am_message.h"
#include "model/song.h"
#include "model/curve.h"
#include "model/plugin.h"
#include "model/track_list.h"
#include "model/automation.h"

extern gboolean is_mixer_shm_local (void*);

#ifdef DEBUG
static bool   am_automation_verify_path (AMCurve*);
#endif
static double ayyi_automation_xlate_in  (double in);
/*static */double ayyi_automation_xlate_out (double val);


bool
am_automation_sync ()
{
	if (!((AyyiSongService*)ayyi.service)->amixer) return false;

	PF2;

	for (AMTrackNum t=0;t<am_track_list_count(song->tracks);t++) {
		AMTrack* trk = song->tracks->track[t];
		if (!AM_TRK_IS_AUDIO(trk)) continue;

		AyyiChannel* ch = ayyi_mixer__channel_at_quiet(trk->ident.idx);
		for (AyyiAutoType auto_type = 0; auto_type < AUTO_MAX; auto_type++) {
			AyyiContainer* events = &ch->automation[auto_type];
			dbg(2, "t=%i type=%i...", t, auto_type);
			if (!ch->automation[auto_type].block[0]) {
				dbg (2, "track %i has no type%i automation events. abp=%p &=%p=%p", t, auto_type, ((BPath**)&trk->bezier)[auto_type], &trk->bezier.pan, &((BPath**)&trk->bezier)[auto_type]);
				continue;
			}
			// we cant verify the container as it is non-standard - items have no idx.
			if (false && !ayyi_client_container_verify(events, ayyi_mixer_translate_address)) continue;
			int count = ayyi_mixer_container_count_items(&ch->automation[auto_type]);
			if (count < 2) { pwarn ("curve is too short - must have at least 2 points... size=%i type=%i", count, auto_type); continue; }//not really sure if this is an error. We should perhaps add it to the curve, then not draw it.
			dbg(2, "t=%i type=%i: count=%i", t, auto_type, count);
			if (count > 10) ayyi_container_print(&ch->automation[auto_type]);

			int event_num = 0;

			AMCurve* curve = ((Curve**)&trk->bezier)[auto_type];

			double prev = 0;
			shm_event* event = NULL;
			int i = 0;
			while ((event = (shm_event*)ayyi_mixer__container_next_item(events, (AyyiItem*)event))) {
				if (!is_mixer_shm_local(event)) { pwarn("event %i not in mixer shm", i); break; }
				if (event->when < 0) { pwarn ("bad data! automation event time cannot be negative! seg=%i type=%i.", i, auto_type); return false; }
				if (event->when < prev) pwarn ("automation list not sorted properly seg=%i type=%i.", i, auto_type);
				if (event->value > 10.0) { pwarn ("automation value out of range. %.2f. Ignoring...", event->value); continue; }

				BPath* abp = curve_append(curve);
				g_return_val_if_fail(abp, false);

				*abp = (BPath){
					.code = event_num ? LINETO : MOVETO_OPEN,
					.x3 = event->when,
					.y3 = ayyi_automation_xlate_in(event->value)
				};

				AYYI_DEBUG_3 printf("  %10.2f: %4.2f-->%.2f\n", event->when, event->value, abp->y3);

				prev = event->when;
				event_num++;
				i++;
			}

#if 0
			if (abp) {
				// gnomecanvas doesnt show the end point, so we have to add an extra one.
				BPath* end = curve_append(curve);
				dbg (2, "e=%i x=%.2f", event_num, abp->x3);
				end->code = BP_END;
				end->x3 = abp->x3; end->y3 = abp->y3;
			}
#endif

			am_song__emit("curve-change", curve);
		}

		// automation controls
		AyyiAutoType auto_type = 2;
		if (!ch->automation_list) dbg(2, "t=%i: no plugin automation.", t);

		/*
		AyyiList* l; for(l=ayyi_list__first(ch->automation_list); l; l=ayyi_list__next(l)){
			AyyiContainer* events = ayyi_song_translate_address(l->data);
		*/
		AyyiList* l = ch->automation_list;
		AyyiContainer* events = ayyi_list__first_data(&l);
		for (;l;(events = ayyi_list__next_data(&l))) {
			if (!ayyi_client_container_verify(events, ayyi_mixer_translate_address)) continue;
			dbg(1, "t=%i type=%i control_name=%s n_events=%i", t, auto_type, l->name, ayyi_mixer_container_count_items(events));

			// TODO the automationlist should use ->id to tell us which plugin it belongs to.
			// ayyi_list->type is not being used, so we can use that.
			// -we need to know the plugin, the insert num, and the control num.

			//for now we take the first plugin:
			AMPlugin* plugin = am_plugin__get_by_idx(0);

			AMCurve* curve = curve_new(trk, auto_type, plugin);
			trk->controls = g_list_append(trk->controls, curve);
			auto_type++;

			shm_event* event = NULL;
			BPath* abp = NULL;
			int event_num = 0;
			double prev = 0;
			int i = 0;
			while ((event = ayyi_mixer__container_next(events, (AyyiItem*)event))) {
				if (event->when < prev) pwarn ("automation list not sorted properly seg=%i type=%i.", i, auto_type);
				if (event->value > 10.0) { pwarn ("automation value out of range. %.2f. Ignoring...", event->value); continue; }

				abp = curve_append(curve);
				g_return_val_if_fail(abp, false);

				*abp = (BPath){
					.code = event_num ? LINETO : MOVETO_OPEN,
					.x1 = 0.0,         .y1 = 0.0,
					.x2 = 0.0,         .y2 = 0.0,
					.x3 = event->when, .y3 = ayyi_automation_xlate_in(event->value)
				};

				AYYI_DEBUG_3 printf("  %10.2f: %4.2f-->%.2f\n", event->when, event->value, abp->y3);

				prev = event->when;
				event_num++;
				i++;
			}
		}
	}
	return true;
}


/*
 *  Ask the server to add a point to the given automation curve
 *
 *  @p is the index to the new point which has already been added to the local Curve.
 */
void
am_automation_point_add (AMCurve* curve, int p, AyyiHandler callback, gpointer user_data)
{
	PF;

#ifdef DEBUG
	if (!am_automation_verify_path(curve)) {
		perr("bad path");
		am_automation_print_path(curve);
		return;
	}
#endif

	BPath* bpath = (BPath*)curve->path;
	BPath* bp  = &bpath[p];

	int plugin_slot = 0;

	AyyiAction* a = ayyi_action_new("automation point add '%s'", curve->track->name);
	a->callback   = ayyi_handler_simple;
	a->app_data   = ayyi_handler_data_new(callback, user_data);
#ifdef USE_DBUS
	GArray* array = ayyi_index_array_new(curve->track->ident.idx, curve->auto_type, plugin_slot);
	dbus_set_prop_f_pair(a, AYYI_OBJECT_AUTO, AYYI_ADD_POINT, array, bp->x3, ayyi_automation_xlate_out(bp->y3));
	g_array_unref (array);
#endif
}


void
am_automation_point_remove (AMCurve* curve, int p, AyyiHandler callback, gpointer user_data)
{
	PF;

#ifdef DEBUG
	int len = curve_get_length(curve);
#endif
	curve_del(curve, p);
	dbg(0, "len: %i --> %i", len, curve_get_length(curve));

	AyyiAction* a = ayyi_action_new("automation point remove");
	a->callback   = ayyi_handler_simple;
	a->app_data   = ayyi_handler_data_new(callback, user_data);

	GArray* array = ayyi_index_array_new(curve->track->ident.idx, curve->auto_type, p);
	dbus_set_prop_f_pair(a, AYYI_OBJECT_AUTO, AYYI_DEL_POINT, array, 0.0, 0.0);
	g_array_unref (array);
}


void
am_automation_point_change (AMCurve* curve, int p, AyyiHandler callback, gpointer user_data)
{
	void am_automation_point_change_cb (AyyiAction* action)
	{
	}

	BPath* bp = &curve->path[p];
	g_return_if_fail(bp);
#ifdef DEBUG
	g_return_if_fail(am_automation_verify_path(curve));
#endif

	if (bp->x3 > ayyi_pos2samples(&am_object_val(&song->loc[AM_LOC_END]).sp)) {
		pwarn("cannot modify point after song end");
		return;
	}

	AyyiAction* a   = ayyi_action_new("automation update");
	a->callback     = am_automation_point_change_cb;
	a->op           = AYYI_SET;
	a->obj.type     = AYYI_OBJECT_AUTO;
	a->prop         = AYYI_AUTO_PT;
	a->obj_idx.idx1 = curve->track->ident.idx;
	a->obj_idx.idx2 = curve->auto_type;
	a->obj_idx.idx3 = p;
	a->d_val        = bp->x3;
	a->val2.d       = ayyi_automation_xlate_out(bp->y3);

	dbg (1, "p=%i x=%.2f y=%.2f-->%.2f", p, a->d_val, bp->y3, a->val2.d);

	am_action_execute(a);
}


/*
 *  Create a default empty curve with a start and end point so that it can be edited.
 */
void
am_automation_make_empty (AMCurve* curve)
{
	PF;

	g_return_if_fail(!curve->path);

	curve_setsize(curve, 2);
	curve_setlength(curve, 2);

	BPath* path = (BPath*)curve->path;

	path[0] = (BPath){
		.code = MOVETO_OPEN,
		.x3 = 0.,
		.y3 = 30.,
	};

	path[1] = (BPath){
		.code = BP_END,
		.x3 = 44100.,
		.y3 = 30.,
	};

	//am_automation_print_path(curve);

	am_song__emit ("curve-add", curve, -1);

	am_automation_point_add(curve, 0, NULL, NULL);
	am_automation_point_add(curve, 1, NULL, NULL);
}


/*
 *  Subpath is needed to avoid iterating over the whole curve
 */
float
am_automation_get_value (AMCurve* curve, int subpath, uint32_t time)
{
	if (subpath > -1) {
		BPath* path = (BPath*)curve->path;
		BPath* p1 = &path[subpath];
		BPath* p2 = &path[subpath + 1];
		uint32_t t = time - p1->x3;
		uint32_t t2 = p2->x3 - p1->x3;
		if (!t2) return p1->y3;

		float y2 = p2->y3 - p1->y3;
		return p1->y3 + y2 * (float)t / (float)t2;
	}

	return 0.;
}


/*
 *  Convert from gain values to normalised pixel values
 *
 *  Model y values are not logarithmic and go from 0.0 to 2.0 (1.0 is unity gain).
 *  Internal y values use log scale and go from 100.0 to 0.0.
 */
static double
ayyi_automation_xlate_in (double gain)
{
	// TODO do other automation types use the same scale?

#if 0
	if(in > 1.0){
		y = _map(in, 1.0, 4.0, 75.0, 100.0);
	}else{
		y = 65 * (in - 0.1);
	}
#else
	double y = (gain == 0)
		? 0
		: pow((6.0*log(gain)/log(2.0)+192.0)/198.0, 8.0);

	dbg (2, "y=%.2f-->%.2f", gain, y);
#endif

	return 100. - 100. * y;
}


/*
 *  @val:    in range 100 to 0. Logarithmic.
 *  @return: normally 0 to 2.0 giving max 6dB gain.
 */
/*static */double
ayyi_automation_xlate_out (double val)
{
	if (val < 0.0 || val > 100.0){ perr ("bad value: %.2f", val); return 0.0; }

	if (val == 0.0) return 2.0; //check!! shouldnt be needed, but...

	val = (100.0 - val) / 100.; //normalise

	if (val == 0.0) val = 0.0;
	else            val = pow (2.0,(sqrt(sqrt(sqrt(val)))*198.0-192.0)/6.0);

	return val;
}


#ifdef DEBUG
static bool
am_automation_verify_path (AMCurve* curve)
{
	size_t len = curve_get_length(curve);
	BPath* path = (BPath*)curve->path;

#if 0 // it is no longer a requirement that the last segment be BP_END
	BPath* end = &path[len -1];
	if (end->code != BP_END) pwarn("end should be BP_END ?");
#endif

	for(int i=1;i<len;i++){
		BPath* p = &path[i];
		if (p->x3 <= path[i - 1].x3) return false;
	}

	return true;
}
#endif


void
am_automation_print_path_code (int i, char* s)
{
	switch (i) {
		case MOVETO_OPEN:
			strcpy(s, "MOVETO_OPEN");
			break;
		case LINETO:
			strcpy(s, "LINETO");
			break;
		case BP_END:
			strcpy(s, "END");
			break;
		default:
			sprintf(s, "%i", i);
			break;
	}
}


#ifdef DEBUG
void
am_automation_print_path (AMCurve* curve)
{
	BPath* abp = curve->path;

	if (!abp) dbg (0, "path is empty.");

	for(int i=0;i<curve_get_length(curve);i++){
		BPath* p = &abp[i];
		if (!p) break;

		char code[64]; am_automation_print_path_code(p->code, code);

		printf(" %02i: %11s %.0f (%.1fs) %.2f\n", i, code, p->x3, p->x3 / song->sample_rate, p->y3);
	}
	printf("--------------------\n");
}
#endif
