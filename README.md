Libayyi
=======

Libayyi provides functionality to connect to Ayyi services and interact with shared objects such as the Mixer and the Song.

See [ayyi.org](https://www.ayyi.org) for more information about the Ayyi project.
