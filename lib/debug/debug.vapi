
//this is manually created - do not delete.

[CCode(
	cheader_filename = "stdint.h",
	lower_case_cprefix = "", cprefix = "")]

public int _debug_;

[CCode(cheader_filename="debug/debug.h", cname = "pwarn")]
[PrintfFormat]
public void warn(string format, ...);
